define(['dart_sdk'], function(dart_sdk) {
  'use strict';
  const core = dart_sdk.core;
  const _interceptors = dart_sdk._interceptors;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const list = Object.create(dart.library);
  let JSArrayOfDetailsList = () => (JSArrayOfDetailsList = dart.constFn(_interceptors.JSArray$(list.DetailsList)))();
  const CT = Object.create(null);
  const name$ = dart.privateName(list, "DetailsList.name");
  const count$ = dart.privateName(list, "DetailsList.count");
  const imageUrl$ = dart.privateName(list, "DetailsList.imageUrl");
  list.DetailsList = class DetailsList extends core.Object {
    get name() {
      return this[name$];
    }
    set name(value) {
      super.name = value;
    }
    get count() {
      return this[count$];
    }
    set count(value) {
      super.count = value;
    }
    get imageUrl() {
      return this[imageUrl$];
    }
    set imageUrl(value) {
      super.imageUrl = value;
    }
  };
  (list.DetailsList.new = function(opts) {
    let imageUrl = opts && 'imageUrl' in opts ? opts.imageUrl : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let count = opts && 'count' in opts ? opts.count : null;
    this[imageUrl$] = imageUrl;
    this[name$] = name;
    this[count$] = count;
    ;
  }).prototype = list.DetailsList.prototype;
  dart.addTypeTests(list.DetailsList);
  dart.setLibraryUri(list.DetailsList, "package:laundry/model/list.dart");
  dart.setFieldSignature(list.DetailsList, () => ({
    __proto__: dart.getFields(list.DetailsList.__proto__),
    name: dart.finalFieldType(core.String),
    count: dart.finalFieldType(core.String),
    imageUrl: dart.finalFieldType(core.String)
  }));
  dart.defineLazy(list, {
    /*list.categoryData*/get categoryData() {
      return JSArrayOfDetailsList().of([new list.DetailsList.new({imageUrl: "assets/page1/tops.png", name: "Kandora", count: "6"}), new list.DetailsList.new({imageUrl: "assets/page1/bottoms.png", name: "Ghutra", count: "6"}), new list.DetailsList.new({imageUrl: "assets/page1/dress.png", name: "Underwear", count: "6"}), new list.DetailsList.new({imageUrl: "assets/page1/coat.png", name: "Classic", count: "6"}), new list.DetailsList.new({imageUrl: "assets/page1/suits.png", name: "Abaya", count: "6"}), new list.DetailsList.new({imageUrl: "assets/page1/bottoms.png", name: "Scarf", count: "6"})]);
    },
    set categoryData(_) {}
  });
  dart.trackLibraries("packages/laundry/model/list", {
    "package:laundry/model/list.dart": list
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["list.dart"],"names":[],"mappings":";;;;;;;;;;;;;IACe;;;;;;IACA;;;;;;IACA;;;;;;;;QAEK;QAAe;QAAW;IAA1B;IAAe;IAAW;;EAAO;;;;;;;;;;MAGnC,iBAAY;YAAG,4BAC3B,oCACU,+BACN,kBACC,OAEL,oCACU,kCACN,iBACC,OAEL,oCACU,gCACN,oBACC,OAEL,oCACU,+BACN,kBACC,OAEL,oCACU,gCACN,gBACC,OAEL,oCACU,kCACN,gBACC","file":"list.ddc.js"}');
  // Exports:
  return {
    model__list: list
  };
});

//# sourceMappingURL=list.ddc.js.map
