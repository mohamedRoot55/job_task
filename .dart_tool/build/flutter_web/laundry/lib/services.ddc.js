define(['dart_sdk', 'packages/graphql/client', 'packages/flutter/src/foundation/_bitfield_web', 'packages/graphql_flutter/graphql_flutter', 'packages/flutter/src/widgets/actions', 'packages/flutter/material'], function(dart_sdk, packages__graphql__client, packages__flutter__src__foundation___bitfield_web, packages__graphql_flutter__graphql_flutter, packages__flutter__src__widgets__actions, packages__flutter__material) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const link_http = packages__graphql__client.src__link__http__link_http;
  const graphql_client = packages__graphql__client.src__graphql_client;
  const normalized_in_memory = packages__graphql__client.src__cache__normalized_in_memory;
  const query_options = packages__graphql__client.src__core__query_options;
  const query_result = packages__graphql__client.src__core__query_result;
  const change_notifier = packages__flutter__src__foundation___bitfield_web.src__foundation__change_notifier;
  const caches = packages__graphql_flutter__graphql_flutter.src__caches;
  const graphql_provider = packages__graphql_flutter__graphql_flutter.src__widgets__graphql_provider;
  const query = packages__graphql_flutter__graphql_flutter.src__widgets__query;
  const widget_inspector = packages__flutter__src__widgets__actions.src__widgets__widget_inspector;
  const framework = packages__flutter__src__widgets__actions.src__widgets__framework;
  const text = packages__flutter__src__widgets__actions.src__widgets__text;
  const scroll_view = packages__flutter__src__widgets__actions.src__widgets__scroll_view;
  const scaffold = packages__flutter__material.src__material__scaffold;
  const app_bar = packages__flutter__material.src__material__app_bar;
  const services = Object.create(dart.library);
  const services_screen = Object.create(dart.library);
  let ValueNotifierOfGraphQLClient = () => (ValueNotifierOfGraphQLClient = dart.constFn(change_notifier.ValueNotifier$(graphql_client.GraphQLClient)))();
  let ObjectToString = () => (ObjectToString = dart.constFn(dart.fnType(core.String, [core.Object])))();
  let BuildContextAndintToWidget = () => (BuildContextAndintToWidget = dart.constFn(dart.fnType(framework.Widget, [framework.BuildContext, core.int])))();
  let FetchMoreOptionsTodynamic = () => (FetchMoreOptionsTodynamic = dart.constFn(dart.fnType(dart.dynamic, [query_options.FetchMoreOptions])))();
  let FutureOfQueryResult = () => (FutureOfQueryResult = dart.constFn(async.Future$(query_result.QueryResult)))();
  let VoidToFutureOfQueryResult = () => (VoidToFutureOfQueryResult = dart.constFn(dart.fnType(FutureOfQueryResult(), [])))();
  let QueryResult__ToListView = () => (QueryResult__ToListView = dart.constFn(dart.fnType(scroll_view.ListView, [query_result.QueryResult], {fetchMore: FetchMoreOptionsTodynamic(), refetch: VoidToFutureOfQueryResult()}, {})))();
  const CT = Object.create(null);
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.fn(normalized_in_memory.typenameDataIdFromObject, ObjectToString());
    },
    get C2() {
      return C2 = dart.constList([], widget_inspector._Location);
    },
    get C1() {
      return C1 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C2 || CT.C2,
        [_Location_name]: null,
        [_Location_column]: 14,
        [_Location_line]: 19,
        [_Location_file]: "org-dartlang-app:///packages/laundry/services.dart"
      });
    },
    get C5() {
      return C5 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "child",
        [_Location_column]: 7,
        [_Location_line]: 19,
        [_Location_file]: null
      });
    },
    get C6() {
      return C6 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "client",
        [_Location_column]: 7,
        [_Location_line]: 20,
        [_Location_file]: null
      });
    },
    get C4() {
      return C4 = dart.constList([C5 || CT.C5, C6 || CT.C6], widget_inspector._Location);
    },
    get C3() {
      return C3 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C4 || CT.C4,
        [_Location_name]: null,
        [_Location_column]: 12,
        [_Location_line]: 18,
        [_Location_file]: "org-dartlang-app:///packages/laundry/services.dart"
      });
    },
    get C9() {
      return C9 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "data",
        [_Location_column]: 21,
        [_Location_line]: 35,
        [_Location_file]: null
      });
    },
    get C8() {
      return C8 = dart.constList([C9 || CT.C9], widget_inspector._Location);
    },
    get C7() {
      return C7 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C8 || CT.C8,
        [_Location_name]: null,
        [_Location_column]: 16,
        [_Location_line]: 35,
        [_Location_file]: "org-dartlang-app:///packages/laundry/services.dart"
      });
    },
    get C12() {
      return C12 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "title",
        [_Location_column]: 9,
        [_Location_line]: 35,
        [_Location_file]: null
      });
    },
    get C11() {
      return C11 = dart.constList([C12 || CT.C12], widget_inspector._Location);
    },
    get C10() {
      return C10 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C11 || CT.C11,
        [_Location_name]: null,
        [_Location_column]: 15,
        [_Location_line]: 34,
        [_Location_file]: "org-dartlang-app:///packages/laundry/services.dart"
      });
    },
    get C15() {
      return C15 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "itemBuilder",
        [_Location_column]: 15,
        [_Location_line]: 62,
        [_Location_file]: null
      });
    },
    get C16() {
      return C16 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "itemCount",
        [_Location_column]: 15,
        [_Location_line]: 65,
        [_Location_file]: null
      });
    },
    get C14() {
      return C14 = dart.constList([C15 || CT.C15, C16 || CT.C16], widget_inspector._Location);
    },
    get C13() {
      return C13 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C14 || CT.C14,
        [_Location_name]: null,
        [_Location_column]: 29,
        [_Location_line]: 61,
        [_Location_file]: "org-dartlang-app:///packages/laundry/services.dart"
      });
    },
    get C19() {
      return C19 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "options",
        [_Location_column]: 11,
        [_Location_line]: 38,
        [_Location_file]: null
      });
    },
    get C20() {
      return C20 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "builder",
        [_Location_column]: 11,
        [_Location_line]: 56,
        [_Location_file]: null
      });
    },
    get C18() {
      return C18 = dart.constList([C19 || CT.C19, C20 || CT.C20], widget_inspector._Location);
    },
    get C17() {
      return C17 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C18 || CT.C18,
        [_Location_name]: null,
        [_Location_column]: 13,
        [_Location_line]: 37,
        [_Location_file]: "org-dartlang-app:///packages/laundry/services.dart"
      });
    },
    get C23() {
      return C23 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "appBar",
        [_Location_column]: 7,
        [_Location_line]: 34,
        [_Location_file]: null
      });
    },
    get C24() {
      return C24 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "body",
        [_Location_column]: 7,
        [_Location_line]: 37,
        [_Location_file]: null
      });
    },
    get C22() {
      return C22 = dart.constList([C23 || CT.C23, C24 || CT.C24], widget_inspector._Location);
    },
    get C21() {
      return C21 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C22 || CT.C22,
        [_Location_name]: null,
        [_Location_column]: 12,
        [_Location_line]: 33,
        [_Location_file]: "org-dartlang-app:///packages/laundry/services.dart"
      });
    },
    get C25() {
      return C25 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C2 || CT.C2,
        [_Location_name]: null,
        [_Location_column]: 12,
        [_Location_line]: 13,
        [_Location_file]: "org-dartlang-app:///packages/laundry/services_screen.dart"
      });
    }
  });
  let C0;
  let C2;
  const _Location_parameterLocations = dart.privateName(widget_inspector, "_Location.parameterLocations");
  const _Location_name = dart.privateName(widget_inspector, "_Location.name");
  const _Location_column = dart.privateName(widget_inspector, "_Location.column");
  const _Location_line = dart.privateName(widget_inspector, "_Location.line");
  const _Location_file = dart.privateName(widget_inspector, "_Location.file");
  let C1;
  let C5;
  let C6;
  let C4;
  let C3;
  services.Services = class Services extends framework.StatelessWidget {
    build(context) {
      let httpLink = new link_http.HttpLink.new({uri: "https://atlantic.education/"});
      let client = new (ValueNotifierOfGraphQLClient()).new(new graphql_client.GraphQLClient.new({link: httpLink, cache: new caches.NormalizedInMemoryCache.new({dataIdFromObject: C0 || CT.C0})}));
      return new graphql_provider.GraphQLProvider.new({child: new services_screen.ServicesSereen.new({$creationLocationd_0dea112b090073317d4: C1 || CT.C1}), client: client, $creationLocationd_0dea112b090073317d4: C3 || CT.C3});
    }
  };
  (services.Services.new = function(opts) {
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    services.Services.__proto__.new.call(this, {$creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = services.Services.prototype;
  dart.addTypeTests(services.Services);
  dart.setMethodSignature(services.Services, () => ({
    __proto__: dart.getMethods(services.Services.__proto__),
    build: dart.fnType(framework.Widget, [framework.BuildContext])
  }));
  dart.setLibraryUri(services.Services, "package:laundry/services.dart");
  services.ServicesScreen = class ServicesScreen extends framework.StatefulWidget {
    createState() {
      return new services._ServicesScreenState.new();
    }
  };
  (services.ServicesScreen.new = function(opts) {
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    services.ServicesScreen.__proto__.new.call(this, {$creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = services.ServicesScreen.prototype;
  dart.addTypeTests(services.ServicesScreen);
  dart.setMethodSignature(services.ServicesScreen, () => ({
    __proto__: dart.getMethods(services.ServicesScreen.__proto__),
    createState: dart.fnType(services._ServicesScreenState, [])
  }));
  dart.setLibraryUri(services.ServicesScreen, "package:laundry/services.dart");
  let C9;
  let C8;
  let C7;
  let C12;
  let C11;
  let C10;
  let C15;
  let C16;
  let C14;
  let C13;
  let C19;
  let C20;
  let C18;
  let C17;
  let C23;
  let C24;
  let C22;
  let C21;
  services._ServicesScreenState = class _ServicesScreenState extends framework.State$(services.ServicesScreen) {
    build(context) {
      return new scaffold.Scaffold.new({appBar: new app_bar.AppBar.new({title: new text.Text.new("Categories", {$creationLocationd_0dea112b090073317d4: C7 || CT.C7}), $creationLocationd_0dea112b090073317d4: C10 || CT.C10}), body: new query.Query.new({options: new query_options.QueryOptions.new({document: "        query MyQuery {\n  products(first: 10) {\n    nodes {\n      id\n      productId\n      image {\n        uri\n        title\n        srcSet\n        sourceUrl\n      }\n      name\n      onSale\n    }\n  }\n}\n        }"}), builder: dart.fn((result, opts) => {
            let fetchMore = opts && 'fetchMore' in opts ? opts.fetchMore : null;
            let refetch = opts && 'refetch' in opts ? opts.refetch : null;
            return new scroll_view.ListView.builder({itemBuilder: dart.fn((context, index) => framework.Widget._check(result.data), BuildContextAndintToWidget()), itemCount: 1, $creationLocationd_0dea112b090073317d4: C13 || CT.C13});
          }, QueryResult__ToListView()), $creationLocationd_0dea112b090073317d4: C17 || CT.C17}), $creationLocationd_0dea112b090073317d4: C21 || CT.C21});
    }
  };
  (services._ServicesScreenState.new = function() {
    services._ServicesScreenState.__proto__.new.call(this);
    ;
  }).prototype = services._ServicesScreenState.prototype;
  dart.addTypeTests(services._ServicesScreenState);
  dart.setMethodSignature(services._ServicesScreenState, () => ({
    __proto__: dart.getMethods(services._ServicesScreenState.__proto__),
    build: dart.fnType(framework.Widget, [framework.BuildContext])
  }));
  dart.setLibraryUri(services._ServicesScreenState, "package:laundry/services.dart");
  services_screen.ServicesSereen = class ServicesSereen extends framework.StatefulWidget {
    createState() {
      return new services_screen._ServicesSereenState.new();
    }
  };
  (services_screen.ServicesSereen.new = function(opts) {
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    services_screen.ServicesSereen.__proto__.new.call(this, {$creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = services_screen.ServicesSereen.prototype;
  dart.addTypeTests(services_screen.ServicesSereen);
  dart.setMethodSignature(services_screen.ServicesSereen, () => ({
    __proto__: dart.getMethods(services_screen.ServicesSereen.__proto__),
    createState: dart.fnType(services_screen._ServicesSereenState, [])
  }));
  dart.setLibraryUri(services_screen.ServicesSereen, "package:laundry/services_screen.dart");
  let C25;
  services_screen._ServicesSereenState = class _ServicesSereenState extends framework.State$(services_screen.ServicesSereen) {
    build(context) {
      return new services.Services.new({$creationLocationd_0dea112b090073317d4: C25 || CT.C25});
    }
  };
  (services_screen._ServicesSereenState.new = function() {
    services_screen._ServicesSereenState.__proto__.new.call(this);
    ;
  }).prototype = services_screen._ServicesSereenState.prototype;
  dart.addTypeTests(services_screen._ServicesSereenState);
  dart.setMethodSignature(services_screen._ServicesSereenState, () => ({
    __proto__: dart.getMethods(services_screen._ServicesSereenState.__proto__),
    build: dart.fnType(framework.Widget, [framework.BuildContext])
  }));
  dart.setLibraryUri(services_screen._ServicesSereenState, "package:laundry/services_screen.dart");
  dart.trackLibraries("packages/laundry/services", {
    "package:laundry/services.dart": services,
    "package:laundry/services_screen.dart": services_screen
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["services.dart","services_screen.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;UAO4B;AACT,qBAAW,iCAAc;AACL,mBAAS,yCAC1C,4CACQ,QAAQ,SACP;AAKX,YAAO,kDACE,uGACC,MAAM;IAElB;;;;;;EACF;;;;;;;;;AAIwC;IAAsB;;;;;;EAC9D;;;;;;;;;;;;;;;;;;;;;;;;;;UAI4B;AACxB,YAAO,oCACG,+BACC,kBAAK,qIAER,8BACO,8CAAuB,kPAkBvB,SACK;gBAEZ;gBADF;AAGE,kBAAgB,gDACD,SAAc,SAAa,UACtC,wBAAO,AAAO,MAAD,kDAEJ;;IAIvB;;;;;EACF;;;;;;;;;AC/DwC;IAAsB;;;;;;EAC9D;;;;;;;;;UAI4B;AACxB,YAAO;IACT;;;;;EACF","file":"services.ddc.js"}');
  // Exports:
  return {
    services: services,
    services_screen: services_screen
  };
});

//# sourceMappingURL=services.ddc.js.map
