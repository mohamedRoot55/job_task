define(['dart_sdk', 'packages/flutter/src/widgets/actions', 'packages/graphql/client', 'packages/flutter/src/foundation/_bitfield_web', 'packages/path_provider/path_provider', 'packages/graphql/src/core/raw_operation_data', 'packages/gql/language', 'packages/connectivity/connectivity', 'packages/graphql/src/link/fetch_result'], function(dart_sdk, packages__flutter__src__widgets__actions, packages__graphql__client, packages__flutter__src__foundation___bitfield_web, packages__path_provider__path_provider, packages__graphql__src__core__raw_operation_data, packages__gql__language, packages__connectivity__connectivity, packages__graphql__src__link__fetch_result) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const ui = dart_sdk.ui;
  const io = dart_sdk.io;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const framework = packages__flutter__src__widgets__actions.src__widgets__framework;
  const async$ = packages__flutter__src__widgets__actions.src__widgets__async;
  const widget_inspector = packages__flutter__src__widgets__actions.src__widgets__widget_inspector;
  const binding = packages__flutter__src__widgets__actions.src__widgets__binding;
  const query_options = packages__graphql__client.src__core__query_options;
  const query_result = packages__graphql__client.src__core__query_result;
  const observable_query = packages__graphql__client.src__core__observable_query;
  const graphql_client = packages__graphql__client.src__graphql_client;
  const in_memory_html = packages__graphql__client.src__cache__in_memory_html;
  const normalized_in_memory = packages__graphql__client.src__cache__normalized_in_memory;
  const optimistic = packages__graphql__client.src__cache__optimistic;
  const helpers = packages__graphql__client.src__utilities__helpers;
  const key = packages__flutter__src__foundation___bitfield_web.src__foundation__key;
  const change_notifier = packages__flutter__src__foundation___bitfield_web.src__foundation__change_notifier;
  const path_provider = packages__path_provider__path_provider.path_provider;
  const operation$ = packages__graphql__src__core__raw_operation_data.src__link__operation;
  const messages = packages__graphql__src__core__raw_operation_data.src__websocket__messages;
  const parser = packages__gql__language.src__language__parser;
  const connectivity = packages__connectivity__connectivity.connectivity;
  const fetch_result = packages__graphql__src__link__fetch_result.src__link__fetch_result;
  const query = Object.create(dart.library);
  const graphql_provider = Object.create(dart.library);
  const caches = Object.create(dart.library);
  const cache_provider = Object.create(dart.library);
  const graphql_flutter = Object.create(dart.library);
  const subscription = Object.create(dart.library);
  const mutation = Object.create(dart.library);
  const graphql_consumer = Object.create(dart.library);
  const $isNotEmpty = dartx.isNotEmpty;
  const $_get = dartx._get;
  let StreamBuilderOfQueryResult = () => (StreamBuilderOfQueryResult = dart.constFn(async$.StreamBuilder$(query_result.QueryResult)))();
  let ValueKeyOfString = () => (ValueKeyOfString = dart.constFn(key.ValueKey$(core.String)))();
  let AsyncSnapshotOfQueryResult = () => (AsyncSnapshotOfQueryResult = dart.constFn(async$.AsyncSnapshot$(query_result.QueryResult)))();
  let BuildContextAndAsyncSnapshotOfQueryResultToWidget = () => (BuildContextAndAsyncSnapshotOfQueryResultToWidget = dart.constFn(dart.fnType(framework.Widget, [framework.BuildContext, AsyncSnapshotOfQueryResult()])))();
  let VoidToNull = () => (VoidToNull = dart.constFn(dart.fnType(core.Null, [])))();
  let FutureOfString = () => (FutureOfString = dart.constFn(async.Future$(core.String)))();
  let VoidToFutureOfString = () => (VoidToFutureOfString = dart.constFn(dart.fnType(FutureOfString(), [])))();
  let ConnectivityResultToFuture = () => (ConnectivityResultToFuture = dart.constFn(dart.fnType(async.Future, [connectivity.ConnectivityResult])))();
  const CT = Object.create(null);
  dart.defineLazy(CT, {
    get C2() {
      return C2 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "key",
        [_Location_column]: 7,
        [_Location_line]: 87,
        [_Location_file]: null
      });
    },
    get C3() {
      return C3 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "initialData",
        [_Location_column]: 7,
        [_Location_line]: 88,
        [_Location_file]: null
      });
    },
    get C4() {
      return C4 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "stream",
        [_Location_column]: 7,
        [_Location_line]: 89,
        [_Location_file]: null
      });
    },
    get C5() {
      return C5 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "builder",
        [_Location_column]: 7,
        [_Location_line]: 90,
        [_Location_file]: null
      });
    },
    get C1() {
      return C1 = dart.constList([C2 || CT.C2, C3 || CT.C3, C4 || CT.C4, C5 || CT.C5], widget_inspector._Location);
    },
    get C0() {
      return C0 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C1 || CT.C1,
        [_Location_name]: null,
        [_Location_column]: 12,
        [_Location_line]: 86,
        [_Location_file]: "org-dartlang-app:///packages/graphql_flutter/src/widgets/query.dart"
      });
    },
    get C8() {
      return C8 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "context",
        [_Location_column]: 38,
        [_Location_line]: 17,
        [_Location_file]: null
      });
    },
    get C7() {
      return C7 = dart.constList([C8 || CT.C8], widget_inspector._Location);
    },
    get C6() {
      return C6 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C7 || CT.C7,
        [_Location_name]: null,
        [_Location_column]: 35,
        [_Location_line]: 17,
        [_Location_file]: "org-dartlang-app:///packages/graphql_flutter/src/widgets/graphql_provider.dart"
      });
    },
    get C11() {
      return C11 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "client",
        [_Location_column]: 7,
        [_Location_line]: 46,
        [_Location_file]: null
      });
    },
    get C12() {
      return C12 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "child",
        [_Location_column]: 7,
        [_Location_line]: 47,
        [_Location_file]: null
      });
    },
    get C10() {
      return C10 = dart.constList([C11 || CT.C11, C12 || CT.C12], widget_inspector._Location);
    },
    get C9() {
      return C9 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C10 || CT.C10,
        [_Location_name]: null,
        [_Location_column]: 12,
        [_Location_line]: 45,
        [_Location_file]: "org-dartlang-app:///packages/graphql_flutter/src/widgets/graphql_provider.dart"
      });
    },
    get C13() {
      return C13 = dart.const({
        __proto__: ui.AppLifecycleState.prototype,
        [_name]: "AppLifecycleState.inactive",
        index: 1
      });
    },
    get C14() {
      return C14 = dart.const({
        __proto__: ui.AppLifecycleState.prototype,
        [_name]: "AppLifecycleState.paused",
        index: 2
      });
    },
    get C15() {
      return C15 = dart.const({
        __proto__: ui.AppLifecycleState.prototype,
        [_name]: "AppLifecycleState.resumed",
        index: 0
      });
    },
    get C16() {
      return C16 = dart.constMap(core.String, dart.dynamic, []);
    },
    get C19() {
      return C19 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "key",
        [_Location_column]: 7,
        [_Location_line]: 107,
        [_Location_file]: null
      });
    },
    get C20() {
      return C20 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "initialData",
        [_Location_column]: 7,
        [_Location_line]: 108,
        [_Location_file]: null
      });
    },
    get C21() {
      return C21 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "stream",
        [_Location_column]: 7,
        [_Location_line]: 109,
        [_Location_file]: null
      });
    },
    get C22() {
      return C22 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: null,
        [_Location_name]: "builder",
        [_Location_column]: 7,
        [_Location_line]: 110,
        [_Location_file]: null
      });
    },
    get C18() {
      return C18 = dart.constList([C19 || CT.C19, C20 || CT.C20, C21 || CT.C21, C22 || CT.C22], widget_inspector._Location);
    },
    get C17() {
      return C17 = dart.const({
        __proto__: widget_inspector._Location.prototype,
        [_Location_parameterLocations]: C18 || CT.C18,
        [_Location_name]: null,
        [_Location_column]: 12,
        [_Location_line]: 103,
        [_Location_file]: "org-dartlang-app:///packages/graphql_flutter/src/widgets/mutation.dart"
      });
    }
  });
  const options$ = dart.privateName(query, "Query.options");
  const builder$ = dart.privateName(query, "Query.builder");
  query.Query = class Query extends framework.StatefulWidget {
    get options() {
      return this[options$];
    }
    set options(value) {
      super.options = value;
    }
    get builder() {
      return this[builder$];
    }
    set builder(value) {
      super.builder = value;
    }
    createState() {
      return new query.QueryState.new();
    }
  };
  (query.Query.new = function(opts) {
    let key = opts && 'key' in opts ? opts.key : null;
    let options = opts && 'options' in opts ? opts.options : null;
    let builder = opts && 'builder' in opts ? opts.builder : null;
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    this[options$] = options;
    this[builder$] = builder;
    query.Query.__proto__.new.call(this, {key: key, $creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = query.Query.prototype;
  dart.addTypeTests(query.Query);
  dart.setMethodSignature(query.Query, () => ({
    __proto__: dart.getMethods(query.Query.__proto__),
    createState: dart.fnType(query.QueryState, [])
  }));
  dart.setLibraryUri(query.Query, "package:graphql_flutter/src/widgets/query.dart");
  dart.setFieldSignature(query.Query, () => ({
    __proto__: dart.getFields(query.Query.__proto__),
    options: dart.finalFieldType(query_options.QueryOptions),
    builder: dart.finalFieldType(dart.fnType(framework.Widget, [query_result.QueryResult], {fetchMore: dart.fnType(dart.dynamic, [query_options.FetchMoreOptions]), refetch: dart.fnType(async.Future$(query_result.QueryResult), [])}, {}))
  }));
  const _options = dart.privateName(query, "_options");
  const _initQuery = dart.privateName(query, "_initQuery");
  const _Location_parameterLocations = dart.privateName(widget_inspector, "_Location.parameterLocations");
  const _Location_name = dart.privateName(widget_inspector, "_Location.name");
  const _Location_column = dart.privateName(widget_inspector, "_Location.column");
  const _Location_line = dart.privateName(widget_inspector, "_Location.line");
  const _Location_file = dart.privateName(widget_inspector, "_Location.file");
  let C2;
  let C3;
  let C4;
  let C5;
  let C1;
  let C0;
  const observableQuery = dart.privateName(query, "QueryState.observableQuery");
  query.QueryState = class QueryState extends framework.State$(query.Query) {
    get observableQuery() {
      return this[observableQuery];
    }
    set observableQuery(value) {
      this[observableQuery] = value;
    }
    get [_options]() {
      let options = this.widget.options;
      return new query_options.WatchQueryOptions.new({document: options.document, documentNode: options.documentNode, variables: options.variables, fetchPolicy: options.fetchPolicy, errorPolicy: options.errorPolicy, pollInterval: options.pollInterval, fetchResults: true, context: options.context, optimisticResult: options.optimisticResult});
    }
    [_initQuery]() {
      let t0;
      let client = graphql_provider.GraphQLProvider.of(this.context).value;
      if (!(client != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql_flutter/src/widgets/query.dart", 57, 12, "client != null");
      t0 = this.observableQuery;
      t0 == null ? null : t0.close();
      this.observableQuery = client.watchQuery(this[_options]);
    }
    didChangeDependencies() {
      super.didChangeDependencies();
      this[_initQuery]();
    }
    didUpdateWidget(oldWidget) {
      query.Query._check(oldWidget);
      super.didUpdateWidget(oldWidget);
      if (!dart.test(this.observableQuery.options.areEqualTo(this[_options]))) {
        this[_initQuery]();
      }
    }
    dispose() {
      let t0;
      t0 = this.observableQuery;
      t0 == null ? null : t0.close();
      super.dispose();
    }
    build(context) {
      let t0, t0$, t0$0, t0$1;
      return new (StreamBuilderOfQueryResult()).new({key: new (ValueKeyOfString()).new((t0$ = (t0 = this.observableQuery, t0 == null ? null : t0.options), t0$ == null ? null : t0$.toKey())), initialData: (t0$1 = (t0$0 = this.observableQuery, t0$0 == null ? null : t0$0.latestResult), t0$1 == null ? new query_result.QueryResult.new({loading: true}) : t0$1), stream: this.observableQuery.stream, builder: dart.fn((buildContext, snapshot) => {
          let t0;
          t0 = this.widget;
          return t0 == null ? null : t0.builder(snapshot.data, {refetch: dart.bind(this.observableQuery, 'refetch'), fetchMore: dart.bind(this.observableQuery, 'fetchMore')});
        }, BuildContextAndAsyncSnapshotOfQueryResultToWidget()), $creationLocationd_0dea112b090073317d4: C0 || CT.C0});
    }
  };
  (query.QueryState.new = function() {
    this[observableQuery] = null;
    query.QueryState.__proto__.new.call(this);
    ;
  }).prototype = query.QueryState.prototype;
  dart.addTypeTests(query.QueryState);
  dart.setMethodSignature(query.QueryState, () => ({
    __proto__: dart.getMethods(query.QueryState.__proto__),
    [_initQuery]: dart.fnType(dart.void, []),
    build: dart.fnType(framework.Widget, [framework.BuildContext])
  }));
  dart.setGetterSignature(query.QueryState, () => ({
    __proto__: dart.getGetters(query.QueryState.__proto__),
    [_options]: query_options.WatchQueryOptions
  }));
  dart.setLibraryUri(query.QueryState, "package:graphql_flutter/src/widgets/query.dart");
  dart.setFieldSignature(query.QueryState, () => ({
    __proto__: dart.getFields(query.QueryState.__proto__),
    observableQuery: dart.fieldType(observable_query.ObservableQuery)
  }));
  let C8;
  let C7;
  let C6;
  const client$ = dart.privateName(graphql_provider, "GraphQLProvider.client");
  const child$ = dart.privateName(graphql_provider, "GraphQLProvider.child");
  graphql_provider.GraphQLProvider = class GraphQLProvider extends framework.StatefulWidget {
    get client() {
      return this[client$];
    }
    set client(value) {
      super.client = value;
    }
    get child() {
      return this[child$];
    }
    set child(value) {
      super.child = value;
    }
    static of(context) {
      let t0;
      let inheritedGraphqlProvider = graphql_provider._InheritedGraphQLProvider.of(context, {$creationLocationd_0dea112b090073317d4: C6 || CT.C6});
      t0 = inheritedGraphqlProvider;
      return t0 == null ? null : t0.client;
    }
    createState() {
      return new graphql_provider._GraphQLProviderState.new();
    }
  };
  (graphql_provider.GraphQLProvider.new = function(opts) {
    let key = opts && 'key' in opts ? opts.key : null;
    let client = opts && 'client' in opts ? opts.client : null;
    let child = opts && 'child' in opts ? opts.child : null;
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    this[client$] = client;
    this[child$] = child;
    graphql_provider.GraphQLProvider.__proto__.new.call(this, {key: key, $creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = graphql_provider.GraphQLProvider.prototype;
  dart.addTypeTests(graphql_provider.GraphQLProvider);
  dart.setMethodSignature(graphql_provider.GraphQLProvider, () => ({
    __proto__: dart.getMethods(graphql_provider.GraphQLProvider.__proto__),
    createState: dart.fnType(framework.State$(framework.StatefulWidget), [])
  }));
  dart.setLibraryUri(graphql_provider.GraphQLProvider, "package:graphql_flutter/src/widgets/graphql_provider.dart");
  dart.setFieldSignature(graphql_provider.GraphQLProvider, () => ({
    __proto__: dart.getFields(graphql_provider.GraphQLProvider.__proto__),
    client: dart.finalFieldType(change_notifier.ValueNotifier$(graphql_client.GraphQLClient)),
    child: dart.finalFieldType(framework.Widget)
  }));
  let C11;
  let C12;
  let C10;
  let C9;
  graphql_provider._GraphQLProviderState = class _GraphQLProviderState extends framework.State$(graphql_provider.GraphQLProvider) {
    didValueChange() {
      return this.setState(dart.fn(() => {
      }, VoidToNull()));
    }
    initState() {
      super.initState();
      this.widget.client.addListener(dart.bind(this, 'didValueChange'));
    }
    dispose() {
      let t0;
      t0 = this.widget.client;
      t0 == null ? null : t0.removeListener(dart.bind(this, 'didValueChange'));
      super.dispose();
    }
    build(context) {
      return new graphql_provider._InheritedGraphQLProvider.new({client: this.widget.client, child: this.widget.child, $creationLocationd_0dea112b090073317d4: C9 || CT.C9});
    }
  };
  (graphql_provider._GraphQLProviderState.new = function() {
    graphql_provider._GraphQLProviderState.__proto__.new.call(this);
    ;
  }).prototype = graphql_provider._GraphQLProviderState.prototype;
  dart.addTypeTests(graphql_provider._GraphQLProviderState);
  dart.setMethodSignature(graphql_provider._GraphQLProviderState, () => ({
    __proto__: dart.getMethods(graphql_provider._GraphQLProviderState.__proto__),
    didValueChange: dart.fnType(dart.void, []),
    build: dart.fnType(framework.Widget, [framework.BuildContext])
  }));
  dart.setLibraryUri(graphql_provider._GraphQLProviderState, "package:graphql_flutter/src/widgets/graphql_provider.dart");
  graphql_provider._InheritedGraphQLProvider = class _InheritedGraphQLProvider extends framework.InheritedWidget {
    static of(context, opts) {
      let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
      return graphql_provider._InheritedGraphQLProvider.as(context.inheritFromWidgetOfExactType(dart.wrapType(graphql_provider._InheritedGraphQLProvider)));
    }
    updateShouldNotify(oldWidget) {
      graphql_provider._InheritedGraphQLProvider._check(oldWidget);
      return !dart.equals(this.clientValue, oldWidget.clientValue);
    }
  };
  (graphql_provider._InheritedGraphQLProvider.new = function(opts) {
    let client = opts && 'client' in opts ? opts.client : null;
    let child = opts && 'child' in opts ? opts.child : null;
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    this.client = client;
    this.clientValue = client.value;
    graphql_provider._InheritedGraphQLProvider.__proto__.new.call(this, {child: child, $creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = graphql_provider._InheritedGraphQLProvider.prototype;
  dart.addTypeTests(graphql_provider._InheritedGraphQLProvider);
  dart.setMethodSignature(graphql_provider._InheritedGraphQLProvider, () => ({
    __proto__: dart.getMethods(graphql_provider._InheritedGraphQLProvider.__proto__),
    updateShouldNotify: dart.fnType(core.bool, [core.Object])
  }));
  dart.setLibraryUri(graphql_provider._InheritedGraphQLProvider, "package:graphql_flutter/src/widgets/graphql_provider.dart");
  dart.setFieldSignature(graphql_provider._InheritedGraphQLProvider, () => ({
    __proto__: dart.getFields(graphql_provider._InheritedGraphQLProvider.__proto__),
    client: dart.finalFieldType(change_notifier.ValueNotifier$(graphql_client.GraphQLClient)),
    clientValue: dart.finalFieldType(graphql_client.GraphQLClient)
  }));
  caches.InMemoryCache = class InMemoryCache extends in_memory_html.InMemoryCache {};
  (caches.InMemoryCache.new = function(opts) {
    let t0;
    let storagePrefix = opts && 'storagePrefix' in opts ? opts.storagePrefix : null;
    caches.InMemoryCache.__proto__.new.call(this, {storagePrefix: core.String._check((t0 = storagePrefix, t0 == null ? caches.flutterStoragePrefix : t0))});
    ;
  }).prototype = caches.InMemoryCache.prototype;
  dart.addTypeTests(caches.InMemoryCache);
  dart.setLibraryUri(caches.InMemoryCache, "package:graphql_flutter/src/caches.dart");
  caches.NormalizedInMemoryCache = class NormalizedInMemoryCache extends normalized_in_memory.NormalizedInMemoryCache {};
  (caches.NormalizedInMemoryCache.new = function(opts) {
    let dataIdFromObject = opts && 'dataIdFromObject' in opts ? opts.dataIdFromObject : null;
    let prefix = opts && 'prefix' in opts ? opts.prefix : "@cache/reference";
    caches.NormalizedInMemoryCache.__proto__.new.call(this, {dataIdFromObject: dataIdFromObject, prefix: prefix, storagePrefix: caches.flutterStoragePrefix});
    ;
  }).prototype = caches.NormalizedInMemoryCache.prototype;
  dart.addTypeTests(caches.NormalizedInMemoryCache);
  dart.setLibraryUri(caches.NormalizedInMemoryCache, "package:graphql_flutter/src/caches.dart");
  caches.OptimisticCache = class OptimisticCache extends optimistic.OptimisticCache {};
  (caches.OptimisticCache.new = function(opts) {
    let dataIdFromObject = opts && 'dataIdFromObject' in opts ? opts.dataIdFromObject : null;
    let prefix = opts && 'prefix' in opts ? opts.prefix : "@cache/reference";
    caches.OptimisticCache.__proto__.new.call(this, {dataIdFromObject: dataIdFromObject, prefix: prefix, storagePrefix: caches.flutterStoragePrefix});
    ;
  }).prototype = caches.OptimisticCache.prototype;
  dart.addTypeTests(caches.OptimisticCache);
  dart.setLibraryUri(caches.OptimisticCache, "package:graphql_flutter/src/caches.dart");
  dart.defineLazy(caches, {
    /*caches.flutterStoragePrefix*/get flutterStoragePrefix() {
      return dart.fn(() => async.async(core.String, function*() {
        return (yield path_provider.getApplicationDocumentsDirectory()).path;
      }), VoidToFutureOfString())();
    }
  });
  const child$0 = dart.privateName(cache_provider, "CacheProvider.child");
  cache_provider.CacheProvider = class CacheProvider extends framework.StatefulWidget {
    get child() {
      return this[child$0];
    }
    set child(value) {
      super.child = value;
    }
    createState() {
      return new cache_provider._CacheProviderState.new();
    }
  };
  (cache_provider.CacheProvider.new = function(opts) {
    let key = opts && 'key' in opts ? opts.key : null;
    let child = opts && 'child' in opts ? opts.child : null;
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    this[child$0] = child;
    cache_provider.CacheProvider.__proto__.new.call(this, {key: key, $creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = cache_provider.CacheProvider.prototype;
  dart.addTypeTests(cache_provider.CacheProvider);
  dart.setMethodSignature(cache_provider.CacheProvider, () => ({
    __proto__: dart.getMethods(cache_provider.CacheProvider.__proto__),
    createState: dart.fnType(cache_provider._CacheProviderState, [])
  }));
  dart.setLibraryUri(cache_provider.CacheProvider, "package:graphql_flutter/src/widgets/cache_provider.dart");
  dart.setFieldSignature(cache_provider.CacheProvider, () => ({
    __proto__: dart.getFields(cache_provider.CacheProvider.__proto__),
    child: dart.finalFieldType(framework.Widget)
  }));
  const _name = dart.privateName(ui, "_name");
  let C13;
  let C14;
  let C15;
  const State_WidgetsBindingObserver$36 = class State_WidgetsBindingObserver extends framework.State$(cache_provider.CacheProvider) {};
  (State_WidgetsBindingObserver$36.new = function() {
    State_WidgetsBindingObserver$36.__proto__.new.call(this);
  }).prototype = State_WidgetsBindingObserver$36.prototype;
  dart.applyMixin(State_WidgetsBindingObserver$36, binding.WidgetsBindingObserver);
  cache_provider._CacheProviderState = class _CacheProviderState extends State_WidgetsBindingObserver$36 {
    initState() {
      super.initState();
      binding.WidgetsBinding.instance.addObserver(this);
    }
    didChangeDependencies() {
      let t0;
      this.client = graphql_provider.GraphQLProvider.of(this.context).value;
      if (!(this.client != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql_flutter/src/widgets/cache_provider.dart", 34, 12, "client != null");
      t0 = this.client.cache;
      t0 == null ? null : t0.restore();
      super.didChangeDependencies();
    }
    dispose() {
      super.dispose();
      binding.WidgetsBinding.instance.removeObserver(this);
    }
    didChangeAppLifecycleState(state) {
      let t0, t0$, t0$0;
      if (!(this.client != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql_flutter/src/widgets/cache_provider.dart", 50, 12, "client != null");
      switch (state) {
        case C13 || CT.C13:
        {
          t0 = this.client.cache;
          t0 == null ? null : t0.save();
          break;
        }
        case C14 || CT.C14:
        {
          t0$ = this.client.cache;
          t0$ == null ? null : t0$.save();
          break;
        }
        case C15 || CT.C15:
        {
          t0$0 = this.client.cache;
          t0$0 == null ? null : t0$0.restore();
          break;
        }
        default:
        {
          break;
        }
      }
    }
    build(context) {
      return this.widget.child;
    }
  };
  (cache_provider._CacheProviderState.new = function() {
    this.client = null;
    cache_provider._CacheProviderState.__proto__.new.call(this);
    ;
  }).prototype = cache_provider._CacheProviderState.prototype;
  dart.addTypeTests(cache_provider._CacheProviderState);
  dart.setMethodSignature(cache_provider._CacheProviderState, () => ({
    __proto__: dart.getMethods(cache_provider._CacheProviderState.__proto__),
    build: dart.fnType(framework.Widget, [framework.BuildContext])
  }));
  dart.setLibraryUri(cache_provider._CacheProviderState, "package:graphql_flutter/src/widgets/cache_provider.dart");
  dart.setFieldSignature(cache_provider._CacheProviderState, () => ({
    __proto__: dart.getFields(cache_provider._CacheProviderState.__proto__),
    client: dart.fieldType(graphql_client.GraphQLClient)
  }));
  let C16;
  const _is_Subscription_default = Symbol('_is_Subscription_default');
  const operationName$ = dart.privateName(subscription, "Subscription.operationName");
  const query$ = dart.privateName(subscription, "Subscription.query");
  const variables$ = dart.privateName(subscription, "Subscription.variables");
  const builder$0 = dart.privateName(subscription, "Subscription.builder");
  const onCompleted$ = dart.privateName(subscription, "Subscription.onCompleted");
  const initial$ = dart.privateName(subscription, "Subscription.initial");
  subscription.Subscription$ = dart.generic(T => {
    let _SubscriptionStateOfT = () => (_SubscriptionStateOfT = dart.constFn(subscription._SubscriptionState$(T)))();
    class Subscription extends framework.StatefulWidget {
      get operationName() {
        return this[operationName$];
      }
      set operationName(value) {
        super.operationName = value;
      }
      get query() {
        return this[query$];
      }
      set query(value) {
        super.query = value;
      }
      get variables() {
        return this[variables$];
      }
      set variables(value) {
        super.variables = value;
      }
      get builder() {
        return this[builder$0];
      }
      set builder(value) {
        super.builder = value;
      }
      get onCompleted() {
        return this[onCompleted$];
      }
      set onCompleted(value) {
        super.onCompleted = value;
      }
      get initial() {
        return this[initial$];
      }
      set initial(value) {
        super.initial = value;
      }
      createState() {
        return new (_SubscriptionStateOfT()).new();
      }
    }
    (Subscription.new = function(operationName, query, opts) {
      let variables = opts && 'variables' in opts ? opts.variables : C16 || CT.C16;
      let key = opts && 'key' in opts ? opts.key : null;
      let builder = opts && 'builder' in opts ? opts.builder : null;
      let initial = opts && 'initial' in opts ? opts.initial : null;
      let onCompleted = opts && 'onCompleted' in opts ? opts.onCompleted : null;
      let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
      this[operationName$] = operationName;
      this[query$] = query;
      this[variables$] = variables;
      this[builder$0] = builder;
      this[initial$] = initial;
      this[onCompleted$] = onCompleted;
      Subscription.__proto__.new.call(this, {key: key, $creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
      ;
    }).prototype = Subscription.prototype;
    dart.addTypeTests(Subscription);
    Subscription.prototype[_is_Subscription_default] = true;
    dart.setMethodSignature(Subscription, () => ({
      __proto__: dart.getMethods(Subscription.__proto__),
      createState: dart.fnType(subscription._SubscriptionState$(T), [])
    }));
    dart.setLibraryUri(Subscription, "package:graphql_flutter/src/widgets/subscription.dart");
    dart.setFieldSignature(Subscription, () => ({
      __proto__: dart.getFields(Subscription.__proto__),
      operationName: dart.finalFieldType(core.String),
      query: dart.finalFieldType(core.String),
      variables: dart.finalFieldType(core.Map$(core.String, dart.dynamic)),
      builder: dart.finalFieldType(dart.fnType(framework.Widget, [], {error: dart.dynamic, loading: core.bool, payload: T}, {})),
      onCompleted: dart.finalFieldType(dart.fnType(dart.void, [])),
      initial: dart.finalFieldType(T)
    }));
    return Subscription;
  });
  subscription.Subscription = subscription.Subscription$();
  dart.addTypeTests(subscription.Subscription, _is_Subscription_default);
  const _loading = dart.privateName(subscription, "_loading");
  const _data = dart.privateName(subscription, "_data");
  const _error = dart.privateName(subscription, "_error");
  const _subscription = dart.privateName(subscription, "_subscription");
  const _currentConnectivityResult = dart.privateName(subscription, "_currentConnectivityResult");
  const _networkSubscription = dart.privateName(subscription, "_networkSubscription");
  const _onData = dart.privateName(subscription, "_onData");
  const _onError = dart.privateName(subscription, "_onError");
  const _onDone = dart.privateName(subscription, "_onDone");
  const _initSubscription = dart.privateName(subscription, "_initSubscription");
  const _onNetworkChange = dart.privateName(subscription, "_onNetworkChange");
  const _is__SubscriptionState_default = Symbol('_is__SubscriptionState_default');
  subscription._SubscriptionState$ = dart.generic(T => {
    let SubscriptionOfT = () => (SubscriptionOfT = dart.constFn(subscription.Subscription$(T)))();
    let __ToWidget = () => (__ToWidget = dart.constFn(dart.fnType(framework.Widget, [], {error: dart.dynamic, loading: core.bool, payload: T}, {})))();
    class _SubscriptionState extends framework.State$(subscription.Subscription$(T)) {
      [_initSubscription]() {
        let t0;
        let client = graphql_provider.GraphQLProvider.of(this.context).value;
        if (!(client != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql_flutter/src/widgets/subscription.dart", 52, 12, "client != null");
        let operation = new operation$.Operation.new({documentNode: parser.parseString(this.widget.query), variables: this.widget.variables, operationName: this.widget.operationName});
        let stream = client.subscribe(operation);
        if (this[_subscription] == null) {
          if (this.widget.initial != null) {
            this.setState(dart.fn(() => {
              this[_loading] = true;
              this[_data] = this.widget.initial;
              this[_error] = null;
            }, VoidToNull()));
          }
        }
        t0 = this[_subscription];
        t0 == null ? null : t0.cancel();
        this[_subscription] = stream.listen(dart.bind(this, _onData), {onError: dart.bind(this, _onError), onDone: dart.bind(this, _onDone)});
      }
      initState() {
        this[_networkSubscription] = connectivity.Connectivity.new().onConnectivityChanged.listen(dart.fn(result => async.async(dart.dynamic, (function*() {
          return yield this[_onNetworkChange](result);
        }).bind(this)), ConnectivityResultToFuture()));
        super.initState();
      }
      didChangeDependencies() {
        super.didChangeDependencies();
        this[_initSubscription]();
      }
      didUpdateWidget(oldWidget) {
        SubscriptionOfT()._check(oldWidget);
        super.didUpdateWidget(oldWidget);
        if (this.widget.query != oldWidget.query || this.widget.operationName != oldWidget.operationName || dart.test(helpers.areDifferentVariables(this.widget.variables, oldWidget.variables))) {
          this[_initSubscription]();
        }
      }
      dispose() {
        let t0, t0$;
        t0 = this[_subscription];
        t0 == null ? null : t0.cancel();
        t0$ = this[_networkSubscription];
        t0$ == null ? null : t0$.cancel();
        super.dispose();
      }
      [_onData](message) {
        this.setState(dart.fn(() => {
          this[_loading] = false;
          this[_data] = T.as(message.data);
          this[_error] = message.errors;
        }, VoidToNull()));
      }
      [_onError](error) {
        this.setState(dart.fn(() => {
          this[_loading] = false;
          this[_data] = null;
          this[_error] = messages.SubscriptionError.is(error) ? error.payload : error;
        }, VoidToNull()));
      }
      [_onDone]() {
        if (this.widget.onCompleted != null) {
          this.widget.onCompleted();
        }
      }
      [_onNetworkChange](result) {
        return async.async(dart.dynamic, (function* _onNetworkChange() {
          if (dart.equals(this[_currentConnectivityResult], connectivity.ConnectivityResult.none) && (dart.equals(result, connectivity.ConnectivityResult.mobile) || dart.equals(result, connectivity.ConnectivityResult.wifi))) {
            this[_currentConnectivityResult] = result;
            if (dart.test(io.Platform.isAndroid)) {
              try {
                let nsLookupResult = (yield io.InternetAddress.lookup("google.com"));
                if (dart.test(nsLookupResult[$isNotEmpty]) && dart.test(nsLookupResult[$_get](0).rawAddress[$isNotEmpty])) {
                  this[_initSubscription]();
                }
              } catch (e) {
                let _ = dart.getThrown(e);
                if (io.SocketException.is(_)) {
                  this[_currentConnectivityResult] = connectivity.ConnectivityResult.none;
                } else
                  throw e;
              }
            } else {
              this[_initSubscription]();
            }
          } else {
            this[_currentConnectivityResult] = result;
          }
        }).bind(this));
      }
      build(context) {
        return __ToWidget()._check(this.widget.builder)({loading: this[_loading], error: this[_error], payload: this[_data]});
      }
    }
    (_SubscriptionState.new = function() {
      this[_loading] = true;
      this[_data] = null;
      this[_error] = null;
      this[_subscription] = null;
      this[_currentConnectivityResult] = null;
      this[_networkSubscription] = null;
      _SubscriptionState.__proto__.new.call(this);
      ;
    }).prototype = _SubscriptionState.prototype;
    dart.addTypeTests(_SubscriptionState);
    _SubscriptionState.prototype[_is__SubscriptionState_default] = true;
    dart.setMethodSignature(_SubscriptionState, () => ({
      __proto__: dart.getMethods(_SubscriptionState.__proto__),
      [_initSubscription]: dart.fnType(dart.void, []),
      [_onData]: dart.fnType(dart.void, [fetch_result.FetchResult]),
      [_onError]: dart.fnType(dart.void, [core.Object]),
      [_onDone]: dart.fnType(dart.void, []),
      [_onNetworkChange]: dart.fnType(async.Future, [connectivity.ConnectivityResult]),
      build: dart.fnType(framework.Widget, [framework.BuildContext])
    }));
    dart.setLibraryUri(_SubscriptionState, "package:graphql_flutter/src/widgets/subscription.dart");
    dart.setFieldSignature(_SubscriptionState, () => ({
      __proto__: dart.getFields(_SubscriptionState.__proto__),
      [_loading]: dart.fieldType(core.bool),
      [_data]: dart.fieldType(T),
      [_error]: dart.fieldType(dart.dynamic),
      [_subscription]: dart.fieldType(async.StreamSubscription$(fetch_result.FetchResult)),
      [_currentConnectivityResult]: dart.fieldType(connectivity.ConnectivityResult),
      [_networkSubscription]: dart.fieldType(async.StreamSubscription$(connectivity.ConnectivityResult))
    }));
    return _SubscriptionState;
  });
  subscription._SubscriptionState = subscription._SubscriptionState$();
  dart.addTypeTests(subscription._SubscriptionState, _is__SubscriptionState_default);
  const options$0 = dart.privateName(mutation, "Mutation.options");
  const builder$1 = dart.privateName(mutation, "Mutation.builder");
  mutation.Mutation = class Mutation extends framework.StatefulWidget {
    get options() {
      return this[options$0];
    }
    set options(value) {
      super.options = value;
    }
    get builder() {
      return this[builder$1];
    }
    set builder(value) {
      super.builder = value;
    }
    createState() {
      return new mutation.MutationState.new();
    }
  };
  (mutation.Mutation.new = function(opts) {
    let key = opts && 'key' in opts ? opts.key : null;
    let options = opts && 'options' in opts ? opts.options : null;
    let builder = opts && 'builder' in opts ? opts.builder : null;
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    this[options$0] = options;
    this[builder$1] = builder;
    mutation.Mutation.__proto__.new.call(this, {key: key, $creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = mutation.Mutation.prototype;
  dart.addTypeTests(mutation.Mutation);
  dart.setMethodSignature(mutation.Mutation, () => ({
    __proto__: dart.getMethods(mutation.Mutation.__proto__),
    createState: dart.fnType(mutation.MutationState, [])
  }));
  dart.setLibraryUri(mutation.Mutation, "package:graphql_flutter/src/widgets/mutation.dart");
  dart.setFieldSignature(mutation.Mutation, () => ({
    __proto__: dart.getFields(mutation.Mutation.__proto__),
    options: dart.finalFieldType(query_options.MutationOptions),
    builder: dart.finalFieldType(dart.fnType(framework.Widget, [dart.fnType(dart.void, [core.Map$(core.String, dart.dynamic)], {optimisticResult: core.Object}, {}), query_result.QueryResult]))
  }));
  const _options$ = dart.privateName(mutation, "_options");
  const _initQuery$ = dart.privateName(mutation, "_initQuery");
  let C19;
  let C20;
  let C21;
  let C22;
  let C18;
  let C17;
  const client = dart.privateName(mutation, "MutationState.client");
  const observableQuery$ = dart.privateName(mutation, "MutationState.observableQuery");
  mutation.MutationState = class MutationState extends framework.State$(mutation.Mutation) {
    get client() {
      return this[client];
    }
    set client(value) {
      this[client] = value;
    }
    get observableQuery() {
      return this[observableQuery$];
    }
    set observableQuery(value) {
      this[observableQuery$] = value;
    }
    get [_options$]() {
      return new query_options.WatchQueryOptions.new({document: this.widget.options.document, documentNode: this.widget.options.documentNode, variables: this.widget.options.variables, fetchPolicy: this.widget.options.fetchPolicy, errorPolicy: this.widget.options.errorPolicy, fetchResults: false, context: this.widget.options.context});
    }
    [_initQuery$]() {
      let t1;
      this.client = graphql_provider.GraphQLProvider.of(this.context).value;
      if (!(this.client != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql_flutter/src/widgets/mutation.dart", 52, 12, "client != null");
      t1 = this.observableQuery;
      t1 == null ? null : t1.close();
      this.observableQuery = this.client.watchQuery(this[_options$]);
    }
    didChangeDependencies() {
      super.didChangeDependencies();
      this[_initQuery$]();
    }
    didUpdateWidget(oldWidget) {
      mutation.Mutation._check(oldWidget);
      super.didUpdateWidget(oldWidget);
      if (!dart.test(this.observableQuery.options.areEqualTo(this[_options$]))) {
        this[_initQuery$]();
      }
    }
    runMutation(variables, opts) {
      let t1;
      let optimisticResult = opts && 'optimisticResult' in opts ? opts.optimisticResult : null;
      let mutationCallbacks = new query_options.MutationCallbacks.new({cache: this.client.cache, queryId: this.observableQuery.queryId, options: this.widget.options});
      return (t1 = this.observableQuery, t1.variables = variables, t1.options.optimisticResult = optimisticResult, t1.onData(mutationCallbacks.callbacks), t1).fetchResults();
    }
    dispose() {
      let t1;
      t1 = this.observableQuery;
      t1 == null ? null : t1.close({force: false});
      super.dispose();
    }
    build(context) {
      let t1, t1$, t1$0, t1$1, t1$2;
      return new (StreamBuilderOfQueryResult()).new({key: new (ValueKeyOfString()).new((t1$ = (t1 = this.observableQuery, t1 == null ? null : t1.options), t1$ == null ? null : t1$.toKey())), initialData: (t1$1 = (t1$0 = this.observableQuery, t1$0 == null ? null : t1$0.latestResult), t1$1 == null ? new query_result.QueryResult.new() : t1$1), stream: (t1$2 = this.observableQuery, t1$2 == null ? null : t1$2.stream), builder: dart.fn((buildContext, snapshot) => this.widget.builder(dart.bind(this, 'runMutation'), snapshot.data), BuildContextAndAsyncSnapshotOfQueryResultToWidget()), $creationLocationd_0dea112b090073317d4: C17 || CT.C17});
    }
  };
  (mutation.MutationState.new = function() {
    this[client] = null;
    this[observableQuery$] = null;
    mutation.MutationState.__proto__.new.call(this);
    ;
  }).prototype = mutation.MutationState.prototype;
  dart.addTypeTests(mutation.MutationState);
  dart.setMethodSignature(mutation.MutationState, () => ({
    __proto__: dart.getMethods(mutation.MutationState.__proto__),
    [_initQuery$]: dart.fnType(dart.void, []),
    runMutation: dart.fnType(query_result.MultiSourceResult, [core.Map$(core.String, dart.dynamic)], {optimisticResult: core.Object}, {}),
    build: dart.fnType(framework.Widget, [framework.BuildContext])
  }));
  dart.setGetterSignature(mutation.MutationState, () => ({
    __proto__: dart.getGetters(mutation.MutationState.__proto__),
    [_options$]: query_options.WatchQueryOptions
  }));
  dart.setLibraryUri(mutation.MutationState, "package:graphql_flutter/src/widgets/mutation.dart");
  dart.setFieldSignature(mutation.MutationState, () => ({
    __proto__: dart.getFields(mutation.MutationState.__proto__),
    client: dart.fieldType(graphql_client.GraphQLClient),
    observableQuery: dart.fieldType(observable_query.ObservableQuery)
  }));
  const builder$2 = dart.privateName(graphql_consumer, "GraphQLConsumer.builder");
  graphql_consumer.GraphQLConsumer = class GraphQLConsumer extends framework.StatelessWidget {
    get builder() {
      return this[builder$2];
    }
    set builder(value) {
      super.builder = value;
    }
    build(context) {
      let t1;
      let client = (t1 = graphql_provider.GraphQLProvider.of(context), t1 == null ? null : t1.value);
      if (!(client != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql_flutter/src/widgets/graphql_consumer.dart", 21, 12, "client != null");
      return this.builder(client);
    }
  };
  (graphql_consumer.GraphQLConsumer.new = function(opts) {
    let key = opts && 'key' in opts ? opts.key : null;
    let builder = opts && 'builder' in opts ? opts.builder : null;
    let $36creationLocationd_0dea112b090073317d4 = opts && '$creationLocationd_0dea112b090073317d4' in opts ? opts.$creationLocationd_0dea112b090073317d4 : null;
    this[builder$2] = builder;
    graphql_consumer.GraphQLConsumer.__proto__.new.call(this, {key: key, $creationLocationd_0dea112b090073317d4: $36creationLocationd_0dea112b090073317d4});
    ;
  }).prototype = graphql_consumer.GraphQLConsumer.prototype;
  dart.addTypeTests(graphql_consumer.GraphQLConsumer);
  dart.setMethodSignature(graphql_consumer.GraphQLConsumer, () => ({
    __proto__: dart.getMethods(graphql_consumer.GraphQLConsumer.__proto__),
    build: dart.fnType(framework.Widget, [framework.BuildContext])
  }));
  dart.setLibraryUri(graphql_consumer.GraphQLConsumer, "package:graphql_flutter/src/widgets/graphql_consumer.dart");
  dart.setFieldSignature(graphql_consumer.GraphQLConsumer, () => ({
    __proto__: dart.getFields(graphql_consumer.GraphQLConsumer.__proto__),
    builder: dart.finalFieldType(dart.fnType(framework.Widget, [graphql_client.GraphQLClient]))
  }));
  dart.trackLibraries("packages/graphql_flutter/graphql_flutter", {
    "package:graphql_flutter/src/widgets/query.dart": query,
    "package:graphql_flutter/src/widgets/graphql_provider.dart": graphql_provider,
    "package:graphql_flutter/src/caches.dart": caches,
    "package:graphql_flutter/src/widgets/cache_provider.dart": cache_provider,
    "package:graphql_flutter/graphql_flutter.dart": graphql_flutter,
    "package:graphql_flutter/src/widgets/subscription.dart": subscription,
    "package:graphql_flutter/src/widgets/mutation.dart": mutation,
    "package:graphql_flutter/src/widgets/graphql_consumer.dart": graphql_consumer
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["src/widgets/query.dart","src/widgets/graphql_provider.dart","src/caches.dart","src/widgets/cache_provider.dart","src/widgets/subscription.dart","src/widgets/mutation.dart","src/widgets/graphql_consumer.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IA2BqB;;;;;;IACA;;;;;;;AAGS;IAAY;;;QAT5B;QACK;QACA;;IADA;IACA;AACZ,+CAAW,GAAG;;EAAC;;;;;;;;;;;;;;;;;;;;;;;;;;;IAUJ;;;;;;;AAGK,oBAAU,AAAO;AAEpC,YAAO,oDAEK,AAAQ,OAAD,yBACH,AAAQ,OAAD,0BACV,AAAQ,OAAD,yBACL,AAAQ,OAAD,2BACP,AAAQ,OAAD,4BACN,AAAQ,OAAD,6BACP,eACL,AAAQ,OAAD,4BACE,AAAQ,OAAD;IAE7B;;;AAGsB,mBAAyB,AAAY,oCAAT;AAChD,YAAO,AAAO,MAAD,IAAI;AAEO,WAAxB;0BAAiB;AAC4B,MAA7C,uBAAkB,AAAO,MAAD,YAAY;IACtC;;AAI+B,MAAvB;AACM,MAAZ;IACF;;yBAG2B;AACO,MAA1B,sBAAgB,SAAS;AAE/B,qBAAK,AAAgB,AAAQ,wCAAW;AAC1B,QAAZ;;IAEJ;;;AAI0B,WAAxB;0BAAiB;AACF,MAAT;IACR;UAG0B;;AACxB,YAAO,8CACA,6EAAI,OAAiB,2BAAjB,OAA0B,6BACQ,oDAA9B,OAAiB,4BAAjB,OAAiC,2CAAqB,wBAC3D,AAAgB,sCACf,SACM,cACc;;AAE3B,eAAO;qCAAQ,WACb,AAAS,QAAD,iBACiB,UAAhB,6CACkB,UAAhB;;IAInB;;;IAjEgB;;;EAkElB;;;;;;;;;;;;;;;;;;;;;;IC1FqC;;;;;;IACtB;;;;;;cAEuC;;AAClB,qCACF,8CAAG,OAAO;AAExC,WAAO,wBAAwB;0BAAxB,OAA0B;IACnC;;AAGuC;IAAuB;;;QAhBxD;QACC;QACA;;IADA;IACA;AACF,oEAAW,GAAG;;EAAC;;;;;;;;;;;;;;;;;;AAiBK,2BAAS;;IAAM;;AAIrB,MAAX;AAEmC,MAAzC,AAAO,AAAO,yCAAY;IAC5B;;;AAI+C,WAA7C,AAAO;mBAAA,OAAQ,4BAAe;AAEf,MAAT;IACR;UAG0B;AACxB,YAAO,6DACG,AAAO,2BACR,AAAO;IAElB;;;;;EACF;;;;;;;;;cASoD;;AAC9C,YACI,+CADJ,AAAQ,OAAD,8BAA8B;IACL;;wDAMc;AAChD,YAAmB,cAAZ,kBAAe,AAAU,SAAD;IACjC;;;QAfO;QACE;;IADF;IAEW,mBAAE,AAAO,MAAD;AACpB,gFAAa,KAAK;;EAAC;;;;;;;;;;;;;;;QC3CN;AACd,sFAAmC,KAAd,aAAa,QAAb,OAAiB;;EAAqB;;;;;QAK5B;QAC3B;AACJ,+EACqB,gBAAgB,UAC1B,MAAM,iBACC;;EAChB;;;;;QAK6B;QAC3B;AACJ,uEACqB,gBAAgB,UAC1B,MAAM,iBACC;;EAChB;;;;MA5Bc,2BAAoB;YACtC,AAA4D;AAAhD,cAA2C,EAA1C,MAAM;MAAwC;;;;;ICGjD;;;;;;;AAGwB;IAAqB;;;QAP9C;QACK;;;AACZ,gEAAW,GAAG;;EAAC;;;;;;;;;;;;;;;;;;;;;;AAcD,MAAX;AAEmC,MAA1B,AAAS,4CAAY;IACtC;;;AAK4C,MAA1C,cAAyB,AAAY,oCAAT;AAC5B,YAAO,AAAO,eAAG;AAEM,WAAvB,AAAO;mBAAA,OAAO;AAEe,MAAvB;IACR;;AAIiB,MAAT;AAEsC,MAA7B,AAAS,+CAAe;IACzC;+BAGkD;;AAChD,YAAO,AAAO,eAAG;AAEjB,cAAQ,KAAK;;;AAKW,eAApB,AAAO;uBAAA,OAAO;AACd;;;;AAGoB,gBAApB,AAAO;wBAAA,OAAO;AACd;;;;AAGuB,iBAAvB,AAAO;yBAAA,OAAO;AACd;;;;AAGA;;;IAEN;UAG0B;AAAY,YAAA,AAAO;IAAK;;;IArDpC;;;EAsDhB;;;;;;;;;;;;;;;;;;;;;;MC7Ce;;;;;;MACA;;;;;;MACc;;;;;;MACE;;;;;;MACC;;;;;;MACtB;;;;;;;AAG+B;MAAuB;;iCAjBvD,eACA;UACA;UACK;UACK;UACV;UACA;;MANA;MACA;MACA;MAEU;MACV;MACA;AACF,kDAAW,GAAG;;IAAC;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAuBE,qBAAyB,AAAY,oCAAT;AAChD,cAAO,AAAO,MAAD,IAAI;AACD,wBAAY,4CACZ,mBAAY,AAAO,+BACtB,AAAO,sCACH,AAAO;AAGE,qBAAS,AAAO,MAAD,WAAW,SAAS;AAE7D,YAAI,AAAc,uBAAG;AAEnB,cAAI,AAAO,uBAAW;AAKlB,YAJF,cAAS;AACQ,cAAf,iBAAW;AACW,cAAtB,cAAQ,AAAO;AACF,cAAb,eAAS;;;;AAKQ,aAAvB;4BAAe;AAKd,QAJD,sBAAgB,AAAO,MAAD,kBACpB,oCACS,mCACD;MAEZ;;AAK0E,QADxE,6BAAuB,AAAe,AAAsB,6DACxD,QAAoB;AAAiB,uBAAM,uBAAiB,MAAM;QAAC;AAEtD,QAAX;MACR;;AAI+B,QAAvB;AACa,QAAnB;MACF;;iCAGqC;AACH,QAA1B,sBAAgB,SAAS;AAE/B,YAAI,AAAO,qBAAS,AAAU,SAAD,UACzB,AAAO,6BAAiB,AAAU,SAAD,4BACjC,8BAAsB,AAAO,uBAAW,AAAU,SAAD;AAChC,UAAnB;;MAEJ;;;AAIyB,aAAvB;4BAAe;AACe,cAA9B;6BAAsB;AACP,QAAT;MACR;gBAE+B;AAK3B,QAJF,cAAS;AACS,UAAhB,iBAAW;AACc,UAAzB,cAAqB,KAAb,AAAQ,OAAD;AACQ,UAAvB,eAAS,AAAQ,OAAD;;MAEpB;iBAE2B;AAKvB,QAJF,cAAS;AACS,UAAhB,iBAAW;AACC,UAAZ,cAAQ;AACqD,UAA7D,eAAgB,8BAAN,KAAK,IAAyB,AAAM,KAAD,WAAW,KAAK;;MAEjE;;AAGE,YAAI,AAAO,2BAAe;AACJ,UAApB,AAAO;;MAEX;yBAE2C;AAApB;AAErB,cAA+B,YAA3B,kCAAiD,0CACzC,YAAP,MAAM,EAAuB,2CACnB,YAAP,MAAM,EAAuB;AACA,YAAnC,mCAA6B,MAAM;AAInC,0BAAa;AACX;AACQ,sCAAiB,MAAsB,0BAAO;AACpD,8BAAI,AAAe,cAAD,4BACd,AAAc,AAAI,AAAW,cAAf,QAAC;AACE,kBAAnB;;;oBAGwB;AAA1B;AACoD,kBAApD,mCAAgD;;;;;AAG/B,cAAnB;;;AAGiC,YAAnC,mCAA6B,MAAM;;QAEvC;;YAGgC;AAC9B,cAAc,qBAAP,+BACI,uBACF,uBACE;MAEb;;;MA/HK,iBAAW;MACd;MACM;MACwB;MAEb;MACoB;;;IA0HzC;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IC/IwB;;;;;;IACA;;;;;;;AAGS;IAAe;;;QATlC;QACK;QACA;;IADA;IACA;AACZ,qDAAW,GAAG;;EAAC;;;;;;;;;;;;;;;;;;;;;;;IAUN;;;;;;IACE;;;;;;;AAEkB,gEAElB,AAAO,AAAQ,4CACX,AAAO,AAAQ,6CAClB,AAAO,AAAQ,4CACb,AAAO,AAAQ,8CACf,AAAO,AAAQ,+CACd,gBACL,AAAO,AAAQ;IACzB;;;AAIuC,MAA1C,cAAyB,AAAY,oCAAT;AAC5B,YAAO,AAAO,eAAG;AAEO,WAAxB;0BAAiB;AAC4B,MAA7C,uBAAkB,AAAO,uBAAW;IACtC;;AAI+B,MAAvB;AACM,MAAZ;IACF;;+BAG8B;AACI,MAA1B,sBAAgB,SAAS;AAG/B,qBAAK,AAAgB,AAAQ,wCAAW;AAC1B,QAAZ;;IAEJ;gBAKuB;;UACd;AAED,8BAAoB,gDACjB,AAAO,4BACL,AAAgB,uCAChB,AAAO;AAGlB,YAMK,OANG,sBACA,eAAY,SAAS,EACrB,AAAQ,8BAAmB,gBAAgB,EAC3C,UAAO,AACJ,iBADqB;IAIlC;;;AAIsC,WAApC;0BAAiB,iBAAa;AACf,MAAT;IACR;UAG0B;;AACxB,YAAO,8CAIA,6EAAI,OAAiB,2BAAjB,OAA0B,6BACQ,oDAA9B,OAAiB,4BAAjB,OAAiC,iGACtC,OAAiB,uBAChB,SACM,cACc,aAEpB,AAAO,8BACZ,sBACA,AAAS,QAAD;IAIhB;;;IArFc;IACE;;;EAqFlB;;;;;;;;;;;;;;;;;;;;IC1G+B;;;;;;UAGH;;AAEJ,yBAAyB,oCAAG,OAAO,gBAAV,OAAa;AAC1D,YAAO,AAAO,MAAD,IAAI;AAEjB,YAAO,cAAQ,MAAM;IACvB;;;QAbY;QACK;;;AACZ,oEAAW,GAAG;;EAAC","file":"graphql_flutter.ddc.js"}');
  // Exports:
  return {
    src__widgets__query: query,
    src__widgets__graphql_provider: graphql_provider,
    src__caches: caches,
    src__widgets__cache_provider: cache_provider,
    graphql_flutter: graphql_flutter,
    src__widgets__subscription: subscription,
    src__widgets__mutation: mutation,
    src__widgets__graphql_consumer: graphql_consumer
  };
});

//# sourceMappingURL=graphql_flutter.ddc.js.map
