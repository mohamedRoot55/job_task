define(['dart_sdk'], function(dart_sdk) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const html = dart_sdk.html;
  const _native_typed_data = dart_sdk._native_typed_data;
  const convert = dart_sdk.convert;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const websocket_browser = Object.create(dart.library);
  const websocket_stub = Object.create(dart.library);
  const websocket = Object.create(dart.library);
  const websocket_status = Object.create(dart.library);
  const $onClose = dartx.onClose;
  const $toString = dartx.toString;
  const $onError = dartx.onError;
  const $onMessage = dartx.onMessage;
  const $data = dartx.data;
  const $onLoad = dartx.onLoad;
  const $result = dartx.result;
  const $onOpen = dartx.onOpen;
  const $buffer = dartx.buffer;
  const $sendByteBuffer = dartx.sendByteBuffer;
  let dynamicTovoid = () => (dynamicTovoid = dart.constFn(dart.fnType(dart.void, [dart.dynamic])))();
  let CloseEventToNull = () => (CloseEventToNull = dart.constFn(dart.fnType(core.Null, [html.CloseEvent])))();
  let EventToNull = () => (EventToNull = dart.constFn(dart.fnType(core.Null, [html.Event])))();
  let FutureOfNull = () => (FutureOfNull = dart.constFn(async.Future$(core.Null)))();
  let MessageEventToFutureOfNull = () => (MessageEventToFutureOfNull = dart.constFn(dart.fnType(FutureOfNull(), [html.MessageEvent])))();
  let ListOfint = () => (ListOfint = dart.constFn(core.List$(core.int)))();
  const CT = Object.create(null);
  const _streamConsumer = dart.privateName(websocket_browser, "_streamConsumer");
  const _streamController = dart.privateName(websocket_browser, "_streamController");
  const _socket$ = dart.privateName(websocket_browser, "_socket");
  const _send = dart.privateName(websocket_browser, "_send");
  const closeCode = dart.privateName(websocket_browser, "WebSocket.closeCode");
  const closeReason = dart.privateName(websocket_browser, "WebSocket.closeReason");
  const done = dart.privateName(websocket_browser, "WebSocket.done");
  websocket_browser.WebSocket = class WebSocket extends core.Object {
    get closeCode() {
      return this[closeCode];
    }
    set closeCode(value) {
      this[closeCode] = value;
    }
    get closeReason() {
      return this[closeReason];
    }
    set closeReason(value) {
      this[closeReason] = value;
    }
    get done() {
      return this[done];
    }
    set done(value) {
      super.done = value;
    }
    static connect(url, opts) {
      let protocols = opts && 'protocols' in opts ? opts.protocols : null;
      return async.async(websocket_browser.WebSocket, function* connect() {
        let s = html.WebSocket.new(url, protocols);
        yield s[$onOpen].first;
        return new websocket_browser.WebSocket.__(s);
      });
    }
    [_send](data) {
      if (typeof data == 'string') {
        return this[_socket$].send(data);
      }
      if (ListOfint().is(data)) {
        return this[_socket$][$sendByteBuffer](_native_typed_data.NativeUint8List.fromList(data)[$buffer]);
      }
      dart.throw(new core.UnsupportedError.new("unspported data type " + dart.str(data)));
    }
    add(data) {
      return this[_streamConsumer].add(data);
    }
    addStream(stream) {
      async.Stream._check(stream);
      return this[_streamConsumer].addStream(stream);
    }
    addUtf8Text(bytes) {
      return this[_streamConsumer].add(convert.utf8.decode(bytes));
    }
    close(code = null, reason = null) {
      this[_streamConsumer].close();
      if (code != null) {
        this[_socket$].close(code, reason);
      } else {
        this[_socket$].close();
      }
      return this.done;
    }
    get extensions() {
      return this[_socket$].extensions;
    }
    get protocol() {
      return this[_socket$].protocol;
    }
    get readyState() {
      return this[_socket$].readyState;
    }
    get stream() {
      return this[_streamController].stream;
    }
  };
  (websocket_browser.WebSocket.__ = function(_socket) {
    this[_streamConsumer] = async.StreamController.new();
    this[closeCode] = null;
    this[closeReason] = null;
    this[_streamController] = async.StreamController.broadcast();
    this[_socket$] = _socket;
    this[done] = _socket[$onClose].first;
    this[_streamConsumer].stream.listen(dart.fn(data => this[_send](data), dynamicTovoid()), {onError: dart.fn(error => this[_send](dart.toString(error)), dynamicTovoid())});
    this[_socket$][$onClose].listen(dart.fn(event => {
      this.closeCode = event.code;
      this.closeReason = event.reason;
      this[_streamController].close();
    }, CloseEventToNull()));
    this[_socket$][$onError].listen(dart.fn(error => {
      this[_streamController].addError(error);
    }, EventToNull()));
    this[_socket$][$onMessage].listen(dart.fn(message => async.async(core.Null, (function*() {
      let data = message[$data];
      if (typeof data == 'string') {
        this[_streamController].add(data);
        return;
      }
      if (html.Blob.is(data)) {
        let reader = html.FileReader.new();
        reader.readAsArrayBuffer(data);
        yield reader[$onLoad].first;
        this[_streamController].add(reader[$result]);
        return;
      }
      dart.throw(new core.UnsupportedError.new("unspported data type " + dart.str(data)));
    }).bind(this)), MessageEventToFutureOfNull()));
  }).prototype = websocket_browser.WebSocket.prototype;
  dart.addTypeTests(websocket_browser.WebSocket);
  websocket_browser.WebSocket[dart.implements] = () => [websocket_stub.WebSocket];
  dart.setMethodSignature(websocket_browser.WebSocket, () => ({
    __proto__: dart.getMethods(websocket_browser.WebSocket.__proto__),
    [_send]: dart.fnType(dart.void, [dart.dynamic]),
    add: dart.fnType(dart.void, [dart.dynamic]),
    addStream: dart.fnType(async.Future, [core.Object]),
    addUtf8Text: dart.fnType(dart.void, [core.List$(core.int)]),
    close: dart.fnType(async.Future, [], [core.int, core.String])
  }));
  dart.setGetterSignature(websocket_browser.WebSocket, () => ({
    __proto__: dart.getGetters(websocket_browser.WebSocket.__proto__),
    extensions: core.String,
    protocol: core.String,
    readyState: core.int,
    stream: async.Stream
  }));
  dart.setLibraryUri(websocket_browser.WebSocket, "package:websocket/src/websocket_browser.dart");
  dart.setFieldSignature(websocket_browser.WebSocket, () => ({
    __proto__: dart.getFields(websocket_browser.WebSocket.__proto__),
    [_socket$]: dart.fieldType(html.WebSocket),
    [_streamConsumer]: dart.finalFieldType(async.StreamController),
    closeCode: dart.fieldType(core.int),
    closeReason: dart.fieldType(core.String),
    done: dart.finalFieldType(async.Future),
    [_streamController]: dart.fieldType(async.StreamController)
  }));
  websocket_stub.WebSocket = class WebSocket extends core.Object {
    static connect(url, opts) {
      let protocols = opts && 'protocols' in opts ? opts.protocols : null;
      return async.async(websocket_stub.WebSocket, function* connect() {
        return dart.throw(websocket_stub._unsupportedError);
      });
    }
    add(data) {
      return dart.throw(websocket_stub._unsupportedError);
    }
    addStream(stream) {
      async.Stream._check(stream);
      return dart.throw(websocket_stub._unsupportedError);
    }
    addUtf8Text(bytes) {
      return dart.throw(websocket_stub._unsupportedError);
    }
    close(code = null, reason = null) {
      return dart.throw(websocket_stub._unsupportedError);
    }
    get closeCode() {
      return dart.throw(websocket_stub._unsupportedError);
    }
    get closeReason() {
      return dart.throw(websocket_stub._unsupportedError);
    }
    get extensions() {
      return dart.throw(websocket_stub._unsupportedError);
    }
    get protocol() {
      return dart.throw(websocket_stub._unsupportedError);
    }
    get readyState() {
      return dart.throw(websocket_stub._unsupportedError);
    }
    get done() {
      return dart.throw(websocket_stub._unsupportedError);
    }
    get stream() {
      return dart.throw(websocket_stub._unsupportedError);
    }
  };
  (websocket_stub.WebSocket.new = function() {
    ;
  }).prototype = websocket_stub.WebSocket.prototype;
  dart.addTypeTests(websocket_stub.WebSocket);
  websocket_stub.WebSocket[dart.implements] = () => [async.StreamConsumer];
  dart.setMethodSignature(websocket_stub.WebSocket, () => ({
    __proto__: dart.getMethods(websocket_stub.WebSocket.__proto__),
    add: dart.fnType(dart.void, [dart.dynamic]),
    addStream: dart.fnType(async.Future, [core.Object]),
    addUtf8Text: dart.fnType(dart.void, [core.List$(core.int)]),
    close: dart.fnType(async.Future, [], [core.int, core.String])
  }));
  dart.setGetterSignature(websocket_stub.WebSocket, () => ({
    __proto__: dart.getGetters(websocket_stub.WebSocket.__proto__),
    closeCode: core.int,
    closeReason: core.String,
    extensions: core.String,
    protocol: core.String,
    readyState: core.int,
    done: async.Future,
    stream: async.Stream
  }));
  dart.setLibraryUri(websocket_stub.WebSocket, "package:websocket/src/websocket_stub.dart");
  dart.defineLazy(websocket_stub, {
    /*websocket_stub._unsupportedError*/get _unsupportedError() {
      return new core.UnsupportedError.new("Cannot work with WebSocket without dart:html or dart:io.");
    }
  });
  websocket_status.WebSocketStatus = class WebSocketStatus extends core.Object {};
  (websocket_status.WebSocketStatus.new = function() {
    ;
  }).prototype = websocket_status.WebSocketStatus.prototype;
  dart.addTypeTests(websocket_status.WebSocketStatus);
  dart.setLibraryUri(websocket_status.WebSocketStatus, "package:websocket/src/websocket_status.dart");
  dart.defineLazy(websocket_status.WebSocketStatus, {
    /*websocket_status.WebSocketStatus.normalClosure*/get normalClosure() {
      return 1000;
    },
    /*websocket_status.WebSocketStatus.goingAway*/get goingAway() {
      return 1001;
    },
    /*websocket_status.WebSocketStatus.protocolError*/get protocolError() {
      return 1002;
    },
    /*websocket_status.WebSocketStatus.unsupportedData*/get unsupportedData() {
      return 1003;
    },
    /*websocket_status.WebSocketStatus.reserved1004*/get reserved1004() {
      return 1004;
    },
    /*websocket_status.WebSocketStatus.noStatusReceived*/get noStatusReceived() {
      return 1005;
    },
    /*websocket_status.WebSocketStatus.abnormalClosure*/get abnormalClosure() {
      return 1006;
    },
    /*websocket_status.WebSocketStatus.invalidFramePayloadData*/get invalidFramePayloadData() {
      return 1007;
    },
    /*websocket_status.WebSocketStatus.policyViolation*/get policyViolation() {
      return 1008;
    },
    /*websocket_status.WebSocketStatus.messageTooBig*/get messageTooBig() {
      return 1009;
    },
    /*websocket_status.WebSocketStatus.missingMandatoryExtension*/get missingMandatoryExtension() {
      return 1010;
    },
    /*websocket_status.WebSocketStatus.internalServerError*/get internalServerError() {
      return 1011;
    },
    /*websocket_status.WebSocketStatus.reserved1015*/get reserved1015() {
      return 1015;
    }
  });
  dart.trackLibraries("packages/websocket/src/websocket_browser", {
    "package:websocket/src/websocket_browser.dart": websocket_browser,
    "package:websocket/src/websocket_stub.dart": websocket_stub,
    "package:websocket/websocket.dart": websocket,
    "package:websocket/src/websocket_status.dart": websocket_status
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["websocket_browser.dart","websocket_stub.dart","websocket_status.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAoFM;;;;;;IAGG;;;;;;IAYM;;;;;;mBAvDJ;UACU;AAFa;AAIxB,gBAAS,mBAAU,GAAG,EAAE,SAAS;AACnB,QAApB,MAAM,AAAE,AAAO,CAAR;AACP,cAAiB,oCAAE,CAAC;MACtB;;YAEgC;AAC9B,UAAS,OAAL,IAAI;AACN,cAAO,AAAQ,qBAAK,IAAI;;AAE1B,UAAS,eAAL,IAAI;AACN,cAAO,AAAQ,iCAAyB,AAAe,4CAAN,IAAI;;AAGH,MAApD,WAAM,8BAAiB,AAA4B,mCAAL,IAAI;IACpD;QAG8B;AAAS,YAAA,AAAgB,2BAAI,IAAI;IAAC;;0BAGxC;AAAW,YAAA,AAAgB,iCAAU,MAAM;IAAC;gBAGzC;AAAU,YAAA,AAAgB,2BAAI,AAAK,oBAAO,KAAK;IAAE;UAG1D,aAAa;AACN,MAAvB,AAAgB;AAChB,UAAI,IAAI,IAAI;AACiB,QAA3B,AAAQ,qBAAM,IAAI,EAAE,MAAM;;AAEX,QAAf,AAAQ;;AAEV,YAAO;IACT;;AASyB,YAAA,AAAQ;IAAU;;AAGpB,YAAA,AAAQ;IAAQ;;AAGjB,YAAA,AAAQ;IAAU;;AASY,YAAA,AAAkB;IAAM;;6CA7F3D;IAFM,wBAAkB;IA0ErC;IAGG;IAcyC,0BAC3B;IA1FJ;IAAgB,aAAE,AAAQ,AAAQ,OAAT;AAIvC,IAHD,AAAgB,AAAO,oCACrB,QAAC,QAAS,YAAM,IAAI,+BACX,QAAC,SAAU,YAAY,cAAN,KAAK;AAM/B,IAJF,AAAQ,AAAQ,gCAAO,QAAiB;AAChB,MAAtB,iBAAY,AAAM,KAAD;AACS,MAA1B,mBAAc,AAAM,KAAD;AACM,MAAzB,AAAkB;;AAIlB,IAFF,AAAQ,AAAQ,gCAAO,QAAY;AACA,MAAjC,AAAkB,iCAAS,KAAK;;AAiBhC,IAfF,AAAQ,AAAU,kCAAO,QAAmB;AACpC,iBAAO,AAAQ,OAAD;AACpB,UAAS,OAAL,IAAI;AACqB,QAA3B,AAAkB,4BAAI,IAAI;AAC1B;;AAEF,UAAS,aAAL,IAAI;AACA,qBAAc;AACU,QAA9B,AAAO,MAAD,mBAAmB,IAAI;AACJ,QAAzB,MAAM,AAAO,AAAO,MAAR;AACwB,QAApC,AAAkB,4BAAI,AAAO,MAAD;AAC5B;;AAGkD,MAApD,WAAM,8BAAiB,AAA4B,mCAAL,IAAI;IACnD;EACH;;;;;;;;;;;;;;;;;;;;;;;;;;;;;mBChCS;UACU;AAFa;AAI5B,0BAAM;MAAiB;;QAEG;AAAS,wBAAM;IAAiB;;0BAEtC;AAAW,wBAAM;IAAiB;gBAE/B;AAAU,wBAAM;IAAiB;UAE1C,aAAa;AAAY,wBAAM;IAAiB;;AAE7C,wBAAM;IAAiB;;AAElB,wBAAM;IAAiB;;AAExB,wBAAM;IAAiB;;AAEzB,wBAAM;IAAiB;;AAExB,wBAAM;IAAiB;;AAE1B,wBAAM;IAAiB;;AAEU,wBAAM;IAAiB;;;;EAC7E;;;;;;;;;;;;;;;;;;;;;;MA/BM,gCAAiB;YAAG,+BACtB;;;;;;ECWJ;;;;MAbmB,8CAAa;;;MACb,0CAAS;;;MACT,8CAAa;;;MACb,gDAAe;;;MACf,6CAAY;;;MACZ,iDAAgB;;;MAChB,gDAAe;;;MACf,wDAAuB;;;MACvB,gDAAe;;;MACf,8CAAa;;;MACb,0DAAyB;;;MACzB,oDAAmB;;;MACnB,6CAAY","file":"websocket_browser.ddc.js"}');
  // Exports:
  return {
    src__websocket_browser: websocket_browser,
    src__websocket_stub: websocket_stub,
    websocket: websocket,
    src__websocket_status: websocket_status
  };
});

//# sourceMappingURL=websocket_browser.ddc.js.map
