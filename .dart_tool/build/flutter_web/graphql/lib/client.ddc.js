define(['dart_sdk', 'packages/quiver/core', 'packages/graphql/src/link/fetch_result', 'packages/graphql/src/core/raw_operation_data', 'packages/http/src/base_client', 'packages/gql/language', 'packages/gql/src/ast/ast', 'packages/http/http', 'packages/http_parser/http_parser'], function(dart_sdk, packages__quiver__core, packages__graphql__src__link__fetch_result, packages__graphql__src__core__raw_operation_data, packages__http__src__base_client, packages__gql__language, packages__gql__src__ast__ast, packages__http__http, packages__http_parser__http_parser) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const collection = dart_sdk.collection;
  const _js_helper = dart_sdk._js_helper;
  const _interceptors = dart_sdk._interceptors;
  const html = dart_sdk.html;
  const convert = dart_sdk.convert;
  const io = dart_sdk.io;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const core$ = packages__quiver__core.core;
  const socket_client = packages__graphql__src__link__fetch_result.src__socket_client;
  const fetch_result = packages__graphql__src__link__fetch_result.src__link__fetch_result;
  const link = packages__graphql__src__link__fetch_result.src__link__link;
  const messages = packages__graphql__src__core__raw_operation_data.src__websocket__messages;
  const operation$ = packages__graphql__src__core__raw_operation_data.src__link__operation;
  const raw_operation_data = packages__graphql__src__core__raw_operation_data.src__core__raw_operation_data;
  const link_http_helper_deprecated_stub = packages__graphql__src__core__raw_operation_data.src__link__http__link_http_helper_deprecated_stub;
  const exception = packages__http__src__base_client.src__exception;
  const client = packages__http__src__base_client.src__client;
  const streamed_response = packages__http__src__base_client.src__streamed_response;
  const request = packages__http__src__base_client.src__request;
  const base_request = packages__http__src__base_client.src__base_request;
  const parser = packages__gql__language.src__language__parser;
  const printer = packages__gql__language.src__language__printer;
  const ast = packages__gql__src__ast__ast.src__ast__ast;
  const multipart_file = packages__http__http.src__multipart_file;
  const multipart_request = packages__http__http.src__multipart_request;
  const media_type = packages__http_parser__http_parser.src__media_type;
  const cache = Object.create(dart.library);
  const helpers = Object.create(dart.library);
  const lazy_cache_map = Object.create(dart.library);
  const traverse = Object.create(dart.library);
  const graphql_error = Object.create(dart.library);
  const _base_exceptions = Object.create(dart.library);
  const in_memory_html = Object.create(dart.library);
  const link_web_socket = Object.create(dart.library);
  const query_manager = Object.create(dart.library);
  const scheduler = Object.create(dart.library);
  const observable_query = Object.create(dart.library);
  const query_result = Object.create(dart.library);
  const exceptions = Object.create(dart.library);
  const network_exception_stub = Object.create(dart.library);
  const operation_exception = Object.create(dart.library);
  const io_network_exception = Object.create(dart.library);
  const query_options = Object.create(dart.library);
  const internal = Object.create(dart.library);
  const client$ = Object.create(dart.library);
  const graphql_client = Object.create(dart.library);
  const link_http = Object.create(dart.library);
  const http_config = Object.create(dart.library);
  const fallback_http_config = Object.create(dart.library);
  const link_error = Object.create(dart.library);
  const link_auth = Object.create(dart.library);
  const optimistic = Object.create(dart.library);
  const normalized_in_memory = Object.create(dart.library);
  const in_memory = Object.create(dart.library);
  const $length = dartx.length;
  const $containsKey = dartx.containsKey;
  const $_get = dartx._get;
  const $forEach = dartx.forEach;
  const $_set = dartx._set;
  const $addAll = dartx.addAll;
  const $reduce = dartx.reduce;
  const $map = dartx.map;
  const $toList = dartx.toList;
  const $contains = dartx.contains;
  const $entries = dartx.entries;
  const $isEmpty = dartx.isEmpty;
  const $isNotEmpty = dartx.isNotEmpty;
  const $keys = dartx.keys;
  const $values = dartx.values;
  const $addEntries = dartx.addEntries;
  const $clear = dartx.clear;
  const $remove = dartx.remove;
  const $removeWhere = dartx.removeWhere;
  const $putIfAbsent = dartx.putIfAbsent;
  const $update = dartx.update;
  const $updateAll = dartx.updateAll;
  const $containsValue = dartx.containsValue;
  const $cast = dartx.cast;
  const $_equals = dartx._equals;
  const $join = dartx.join;
  const $retainWhere = dartx.retainWhere;
  const $add = dartx.add;
  const $toString = dartx.toString;
  const $where = dartx.where;
  const $split = dartx.split;
  const $first = dartx.first;
  const $firstWhere = dartx.firstWhere;
  let StringAnddynamicToNull = () => (StringAnddynamicToNull = dart.constFn(dart.fnType(core.Null, [core.String, dart.dynamic])))();
  let LinkedHashMapOfString$dynamic = () => (LinkedHashMapOfString$dynamic = dart.constFn(collection.LinkedHashMap$(core.String, dart.dynamic)))();
  let MapOfString$dynamic = () => (MapOfString$dynamic = dart.constFn(core.Map$(core.String, dart.dynamic)))();
  let IdentityMapOfString$dynamic = () => (IdentityMapOfString$dynamic = dart.constFn(_js_helper.IdentityMap$(core.String, dart.dynamic)))();
  let JSArrayOfMapOfString$dynamic = () => (JSArrayOfMapOfString$dynamic = dart.constFn(_interceptors.JSArray$(MapOfString$dynamic())))();
  let MapOfString$dynamicAndMapOfString$dynamicToMapOfString$dynamic = () => (MapOfString$dynamicAndMapOfString$dynamicToMapOfString$dynamic = dart.constFn(dart.fnType(MapOfString$dynamic(), [MapOfString$dynamic(), MapOfString$dynamic()])))();
  let MapOfString$Object = () => (MapOfString$Object = dart.constFn(core.Map$(core.String, core.Object)))();
  let MapEntryOfString$Object = () => (MapEntryOfString$Object = dart.constFn(core.MapEntry$(core.String, core.Object)))();
  let MapEntryOfString$ObjectToMapEntryOfString$Object = () => (MapEntryOfString$ObjectToMapEntryOfString$Object = dart.constFn(dart.fnType(MapEntryOfString$Object(), [MapEntryOfString$Object()])))();
  let MapEntryOfString$ObjectTovoid = () => (MapEntryOfString$ObjectTovoid = dart.constFn(dart.fnType(dart.void, [MapEntryOfString$Object()])))();
  let IterableOfMapEntryOfString$Object = () => (IterableOfMapEntryOfString$Object = dart.constFn(core.Iterable$(MapEntryOfString$Object())))();
  let VoidToObject = () => (VoidToObject = dart.constFn(dart.fnType(core.Object, [])))();
  let ObjectToObject = () => (ObjectToObject = dart.constFn(dart.fnType(core.Object, [core.Object])))();
  let StringAndObjectToObject = () => (StringAndObjectToObject = dart.constFn(dart.fnType(core.Object, [core.String, core.Object])))();
  let _HashSetOfObject = () => (_HashSetOfObject = dart.constFn(collection._HashSet$(core.Object)))();
  let StringAndObjectToMapEntryOfString$Object = () => (StringAndObjectToMapEntryOfString$Object = dart.constFn(dart.fnType(MapEntryOfString$Object(), [core.String, core.Object])))();
  let ListOfObject = () => (ListOfObject = dart.constFn(core.List$(core.Object)))();
  let MapOfString$int = () => (MapOfString$int = dart.constFn(core.Map$(core.String, core.int)))();
  let ListOfMapOfString$int = () => (ListOfMapOfString$int = dart.constFn(core.List$(MapOfString$int())))();
  let ListOfLocation = () => (ListOfLocation = dart.constFn(core.List$(graphql_error.Location)))();
  let MapOfString$intToLocation = () => (MapOfString$intToLocation = dart.constFn(dart.fnType(graphql_error.Location, [MapOfString$int()])))();
  let LocationToString = () => (LocationToString = dart.constFn(dart.fnType(core.String, [graphql_error.Location])))();
  let HashMapOfString$dynamic = () => (HashMapOfString$dynamic = dart.constFn(collection.HashMap$(core.String, dart.dynamic)))();
  let SubscriptionDataToFetchResult = () => (SubscriptionDataToFetchResult = dart.constFn(dart.fnType(fetch_result.FetchResult, [messages.SubscriptionData])))();
  let IdentityMapOfString$ObservableQuery = () => (IdentityMapOfString$ObservableQuery = dart.constFn(_js_helper.IdentityMap$(core.String, observable_query.ObservableQuery)))();
  let FutureOfQueryResult = () => (FutureOfQueryResult = dart.constFn(async.Future$(query_result.QueryResult)))();
  let QueryResultToFutureOfQueryResult = () => (QueryResultToFutureOfQueryResult = dart.constFn(dart.fnType(FutureOfQueryResult(), [query_result.QueryResult])))();
  let CacheToCache = () => (CacheToCache = dart.constFn(dart.fnType(cache.Cache, [cache.Cache])))();
  let ListOfGraphQLError = () => (ListOfGraphQLError = dart.constFn(core.List$(graphql_error.GraphQLError)))();
  let dynamicToGraphQLError = () => (dynamicToGraphQLError = dart.constFn(dart.fnType(graphql_error.GraphQLError, [dart.dynamic])))();
  let IdentityMapOfString$WatchQueryOptions = () => (IdentityMapOfString$WatchQueryOptions = dart.constFn(_js_helper.IdentityMap$(core.String, query_options.WatchQueryOptions)))();
  let ListOfString = () => (ListOfString = dart.constFn(core.List$(core.String)))();
  let LinkedMapOfDuration$ListOfString = () => (LinkedMapOfDuration$ListOfString = dart.constFn(_js_helper.LinkedMap$(core.Duration, ListOfString())))();
  let LinkedMapOfDuration$Timer = () => (LinkedMapOfDuration$Timer = dart.constFn(_js_helper.LinkedMap$(core.Duration, async.Timer)))();
  let StringTobool = () => (StringTobool = dart.constFn(dart.fnType(core.bool, [core.String])))();
  let JSArrayOfString = () => (JSArrayOfString = dart.constFn(_interceptors.JSArray$(core.String)))();
  let TimerTovoid = () => (TimerTovoid = dart.constFn(dart.fnType(dart.void, [async.Timer])))();
  let StreamSubscriptionOfQueryResult = () => (StreamSubscriptionOfQueryResult = dart.constFn(async.StreamSubscription$(query_result.QueryResult)))();
  let LinkedHashSetOfStreamSubscriptionOfQueryResult = () => (LinkedHashSetOfStreamSubscriptionOfQueryResult = dart.constFn(collection.LinkedHashSet$(StreamSubscriptionOfQueryResult())))();
  let StreamControllerOfQueryResult = () => (StreamControllerOfQueryResult = dart.constFn(async.StreamController$(query_result.QueryResult)))();
  let QueryResultTovoid = () => (QueryResultTovoid = dart.constFn(dart.fnType(dart.void, [query_result.QueryResult])))();
  let FutureOfNull = () => (FutureOfNull = dart.constFn(async.Future$(core.Null)))();
  let QueryResultToFutureOfNull = () => (QueryResultToFutureOfNull = dart.constFn(dart.fnType(FutureOfNull(), [query_result.QueryResult])))();
  let LinkedHashSetOfQueryResultSource = () => (LinkedHashSetOfQueryResultSource = dart.constFn(collection.LinkedHashSet$(query_result.QueryResultSource)))();
  let JSArrayOfGraphQLError = () => (JSArrayOfGraphQLError = dart.constFn(_interceptors.JSArray$(graphql_error.GraphQLError)))();
  let GraphQLErrorToString = () => (GraphQLErrorToString = dart.constFn(dart.fnType(core.String, [graphql_error.GraphQLError])))();
  let JSArrayOfQueryResultTovoid = () => (JSArrayOfQueryResultTovoid = dart.constFn(_interceptors.JSArray$(QueryResultTovoid())))();
  let ObjectTobool = () => (ObjectTobool = dart.constFn(dart.fnType(core.bool, [core.Object])))();
  let MapOfString$String = () => (MapOfString$String = dart.constFn(core.Map$(core.String, core.String)))();
  let IdentityMapOfString$StreamedResponse = () => (IdentityMapOfString$StreamedResponse = dart.constFn(_js_helper.IdentityMap$(core.String, streamed_response.StreamedResponse)))();
  let FutureOfvoid = () => (FutureOfvoid = dart.constFn(async.Future$(dart.void)))();
  let VoidToFutureOfvoid = () => (VoidToFutureOfvoid = dart.constFn(dart.fnType(FutureOfvoid(), [])))();
  let StreamControllerOfFetchResult = () => (StreamControllerOfFetchResult = dart.constFn(async.StreamController$(fetch_result.FetchResult)))();
  let StreamOfFetchResult = () => (StreamOfFetchResult = dart.constFn(async.Stream$(fetch_result.FetchResult)))();
  let OperationToStreamOfFetchResult = () => (OperationToStreamOfFetchResult = dart.constFn(dart.fnType(StreamOfFetchResult(), [operation$.Operation])))();
  let OperationAndFnToStreamOfFetchResult = () => (OperationAndFnToStreamOfFetchResult = dart.constFn(dart.fnType(StreamOfFetchResult(), [operation$.Operation], [OperationToStreamOfFetchResult()])))();
  let IdentityMapOfString$MultipartFile = () => (IdentityMapOfString$MultipartFile = dart.constFn(_js_helper.IdentityMap$(core.String, multipart_file.MultipartFile)))();
  let MapOfString$MultipartFile = () => (MapOfString$MultipartFile = dart.constFn(core.Map$(core.String, multipart_file.MultipartFile)))();
  let dynamicTodynamic = () => (dynamicTodynamic = dart.constFn(dart.fnType(dart.dynamic, [dart.dynamic])))();
  let IdentityMapOfString$ListOfString = () => (IdentityMapOfString$ListOfString = dart.constFn(_js_helper.IdentityMap$(core.String, ListOfString())))();
  let JSArrayOfMultipartFile = () => (JSArrayOfMultipartFile = dart.constFn(_interceptors.JSArray$(multipart_file.MultipartFile)))();
  let IdentityMapOfString$String = () => (IdentityMapOfString$String = dart.constFn(_js_helper.IdentityMap$(core.String, core.String)))();
  let FetchResultToFetchResult = () => (FetchResultToFetchResult = dart.constFn(dart.fnType(fetch_result.FetchResult, [fetch_result.FetchResult])))();
  let dynamicToNull = () => (dynamicToNull = dart.constFn(dart.fnType(core.Null, [dart.dynamic])))();
  let IdentityMapOfString$MapOfString$String = () => (IdentityMapOfString$MapOfString$String = dart.constFn(_js_helper.IdentityMap$(core.String, MapOfString$String())))();
  let JSArrayOfOptimisticPatch = () => (JSArrayOfOptimisticPatch = dart.constFn(_interceptors.JSArray$(optimistic.OptimisticPatch)))();
  let OptimisticPatchTobool = () => (OptimisticPatchTobool = dart.constFn(dart.fnType(core.bool, [optimistic.OptimisticPatch])))();
  let VoidToNull = () => (VoidToNull = dart.constFn(dart.fnType(core.Null, [])))();
  let ObjectToListOfString = () => (ObjectToListOfString = dart.constFn(dart.fnType(ListOfString(), [core.Object])))();
  let ObjectAndObjectAndTraversalTovoid = () => (ObjectAndObjectAndTraversalTovoid = dart.constFn(dart.fnType(dart.void, [core.Object, core.Object, traverse.Traversal])))();
  const CT = Object.create(null);
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.fn(helpers._recursivelyAddAll, MapOfString$dynamicAndMapOfString$dynamicToMapOfString$dynamic());
    },
    get C1() {
      return C1 = dart.const({
        __proto__: lazy_cache_map.CacheState.prototype,
        [_name$]: "CacheState.OPTIMISTIC",
        index: 0
      });
    },
    get C2() {
      return C2 = dart.constList([C1 || CT.C1], lazy_cache_map.CacheState);
    },
    get C4() {
      return C4 = dart.const({
        __proto__: core.Duration.prototype,
        [Duration__duration]: 10000000
      });
    },
    get C5() {
      return C5 = dart.const({
        __proto__: core.Duration.prototype,
        [Duration__duration]: 5000000
      });
    },
    get C6() {
      return C6 = dart.const({
        __proto__: core.Duration.prototype,
        [Duration__duration]: 30000000
      });
    },
    get C3() {
      return C3 = dart.const({
        __proto__: socket_client.SocketClientConfig.prototype,
        [SocketClientConfig_initPayload]: null,
        [SocketClientConfig_queryAndMutationTimeout]: C4 || CT.C4,
        [SocketClientConfig_delayBetweenReconnectionAttempts]: C5 || CT.C5,
        [SocketClientConfig_inactivityTimeout]: C6 || CT.C6,
        [SocketClientConfig_autoReconnect]: true
      });
    },
    get C7() {
      return C7 = dart.const({
        __proto__: query_options.ErrorPolicy.prototype,
        [_name$0]: "ErrorPolicy.all",
        index: 2
      });
    },
    get C8() {
      return C8 = dart.const({
        __proto__: query_options.ErrorPolicy.prototype,
        [_name$0]: "ErrorPolicy.ignore",
        index: 1
      });
    },
    get C9() {
      return C9 = dart.const({
        __proto__: query_options.ErrorPolicy.prototype,
        [_name$0]: "ErrorPolicy.none",
        index: 0
      });
    },
    get C10() {
      return C10 = dart.const({
        __proto__: observable_query.QueryLifecycle.prototype,
        [_name$1]: "QueryLifecycle.UNEXECUTED",
        index: 0
      });
    },
    get C11() {
      return C11 = dart.const({
        __proto__: observable_query.QueryLifecycle.prototype,
        [_name$1]: "QueryLifecycle.PENDING",
        index: 1
      });
    },
    get C12() {
      return C12 = dart.const({
        __proto__: observable_query.QueryLifecycle.prototype,
        [_name$1]: "QueryLifecycle.POLLING",
        index: 2
      });
    },
    get C13() {
      return C13 = dart.const({
        __proto__: observable_query.QueryLifecycle.prototype,
        [_name$1]: "QueryLifecycle.POLLING_STOPPED",
        index: 3
      });
    },
    get C14() {
      return C14 = dart.const({
        __proto__: observable_query.QueryLifecycle.prototype,
        [_name$1]: "QueryLifecycle.SIDE_EFFECTS_PENDING",
        index: 4
      });
    },
    get C15() {
      return C15 = dart.const({
        __proto__: observable_query.QueryLifecycle.prototype,
        [_name$1]: "QueryLifecycle.SIDE_EFFECTS_BLOCKING",
        index: 5
      });
    },
    get C16() {
      return C16 = dart.const({
        __proto__: observable_query.QueryLifecycle.prototype,
        [_name$1]: "QueryLifecycle.COMPLETED",
        index: 6
      });
    },
    get C17() {
      return C17 = dart.const({
        __proto__: observable_query.QueryLifecycle.prototype,
        [_name$1]: "QueryLifecycle.CLOSED",
        index: 7
      });
    },
    get C18() {
      return C18 = dart.constList([C10 || CT.C10, C11 || CT.C11, C12 || CT.C12, C13 || CT.C13, C14 || CT.C14, C15 || CT.C15, C16 || CT.C16, C17 || CT.C17], observable_query.QueryLifecycle);
    },
    get C19() {
      return C19 = dart.constList([], QueryResultTovoid());
    },
    get C20() {
      return C20 = dart.const({
        __proto__: query_result.QueryResultSource.prototype,
        [_name$2]: "QueryResultSource.Loading",
        index: 0
      });
    },
    get C21() {
      return C21 = dart.const({
        __proto__: query_result.QueryResultSource.prototype,
        [_name$2]: "QueryResultSource.Cache",
        index: 1
      });
    },
    get C22() {
      return C22 = dart.const({
        __proto__: query_result.QueryResultSource.prototype,
        [_name$2]: "QueryResultSource.OptimisticResult",
        index: 2
      });
    },
    get C23() {
      return C23 = dart.const({
        __proto__: query_result.QueryResultSource.prototype,
        [_name$2]: "QueryResultSource.Network",
        index: 3
      });
    },
    get C24() {
      return C24 = dart.constList([C20 || CT.C20, C21 || CT.C21, C22 || CT.C22, C23 || CT.C23], query_result.QueryResultSource);
    },
    get C25() {
      return C25 = dart.constList([], graphql_error.GraphQLError);
    },
    get C26() {
      return C26 = dart.const({
        __proto__: query_options.FetchPolicy.prototype,
        [_name$0]: "FetchPolicy.cacheFirst",
        index: 0
      });
    },
    get C27() {
      return C27 = dart.const({
        __proto__: query_options.FetchPolicy.prototype,
        [_name$0]: "FetchPolicy.cacheAndNetwork",
        index: 1
      });
    },
    get C28() {
      return C28 = dart.const({
        __proto__: query_options.FetchPolicy.prototype,
        [_name$0]: "FetchPolicy.cacheOnly",
        index: 2
      });
    },
    get C29() {
      return C29 = dart.const({
        __proto__: query_options.FetchPolicy.prototype,
        [_name$0]: "FetchPolicy.noCache",
        index: 3
      });
    },
    get C30() {
      return C30 = dart.const({
        __proto__: query_options.FetchPolicy.prototype,
        [_name$0]: "FetchPolicy.networkOnly",
        index: 4
      });
    },
    get C31() {
      return C31 = dart.constList([C26 || CT.C26, C27 || CT.C27, C28 || CT.C28, C29 || CT.C29, C30 || CT.C30], query_options.FetchPolicy);
    },
    get C32() {
      return C32 = dart.constList([C9 || CT.C9, C8 || CT.C8, C7 || CT.C7], query_options.ErrorPolicy);
    },
    get C33() {
      return C33 = dart.fn(helpers.notNull, ObjectTobool());
    },
    get C34() {
      return C34 = dart.constMap(core.String, dart.dynamic, []);
    },
    get C35() {
      return C35 = dart.constList([], core.String);
    },
    get C36() {
      return C36 = dart.const({
        __proto__: convert.Utf8Codec.prototype,
        [Utf8Codec__allowMalformed]: false
      });
    }
  });
  cache.Cache = class Cache extends core.Object {
    read(key) {
    }
    write(key, value) {
    }
    save() {
      return async.async(dart.void, function* save() {
      });
    }
    restore() {
    }
    reset() {
    }
  };
  (cache.Cache.new = function() {
    ;
  }).prototype = cache.Cache.prototype;
  dart.addTypeTests(cache.Cache);
  dart.setMethodSignature(cache.Cache, () => ({
    __proto__: dart.getMethods(cache.Cache.__proto__),
    read: dart.fnType(dart.dynamic, [core.String]),
    write: dart.fnType(dart.void, [core.String, dart.dynamic]),
    save: dart.fnType(async.Future$(dart.void), []),
    restore: dart.fnType(dart.void, []),
    reset: dart.fnType(dart.void, [])
  }));
  dart.setLibraryUri(cache.Cache, "package:graphql/src/cache/cache.dart");
  let C0;
  helpers.notNull = function notNull(any) {
    return any != null;
  };
  helpers.areDifferentVariables = function areDifferentVariables(a, b) {
    if (a == null && b == null) {
      return false;
    }
    if (a == null || b == null) {
      return true;
    }
    if (a[$length] != b[$length]) {
      return true;
    }
    let areDifferent = false;
    a[$forEach](dart.fn((key, value) => {
      if (!dart.test(b[$containsKey](key)) || !dart.equals(b[$_get](key), value)) {
        areDifferent = true;
      }
    }, StringAnddynamicToNull()));
    return areDifferent;
  };
  helpers._recursivelyAddAll = function _recursivelyAddAll(target, source) {
    target = LinkedHashMapOfString$dynamic().from(lazy_cache_map.unwrapMap(target));
    source = lazy_cache_map.unwrapMap(source);
    source[$forEach](dart.fn((key, value) => {
      if (dart.test(target[$containsKey](key)) && core.Map.is(target[$_get](key)) && value != null && MapOfString$dynamic().is(value)) {
        target[$_set](key, helpers._recursivelyAddAll(MapOfString$dynamic().as(target[$_get](key)), value));
      } else {
        target[$_set](key, value);
      }
    }, StringAnddynamicToNull()));
    return target;
  };
  helpers.deeplyMergeLeft = function deeplyMergeLeft(maps) {
    let t0;
    return (t0 = JSArrayOfMapOfString$dynamic().of([new (IdentityMapOfString$dynamic()).new()]), t0[$addAll](maps), t0)[$reduce](C0 || CT.C0);
  };
  const _name$ = dart.privateName(lazy_cache_map, "_name");
  let C1;
  let C2;
  lazy_cache_map.CacheState = class CacheState extends core.Object {
    toString() {
      return this[_name$];
    }
  };
  (lazy_cache_map.CacheState.new = function(index, _name) {
    this.index = index;
    this[_name$] = _name;
    ;
  }).prototype = lazy_cache_map.CacheState.prototype;
  dart.addTypeTests(lazy_cache_map.CacheState);
  dart.setLibraryUri(lazy_cache_map.CacheState, "package:graphql/src/cache/lazy_cache_map.dart");
  dart.setFieldSignature(lazy_cache_map.CacheState, () => ({
    __proto__: dart.getFields(lazy_cache_map.CacheState.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(lazy_cache_map.CacheState, ['toString']);
  lazy_cache_map.CacheState.OPTIMISTIC = C1 || CT.C1;
  lazy_cache_map.CacheState.values = C2 || CT.C2;
  const _dereference = dart.privateName(lazy_cache_map, "_dereference");
  const _data = dart.privateName(lazy_cache_map, "_data");
  lazy_cache_map.LazyDereferencingMap = class LazyDereferencingMap extends core.Object {
    get data() {
      return this[_data];
    }
    getValue(value) {
      let t0;
      let result = (t0 = this[_dereference](value), t0 == null ? value : t0);
      if (core.List.is(result)) {
        return result[$map](core.Object, dart.bind(this, 'getValue'))[$toList]();
      }
      if (MapOfString$Object().is(result)) {
        return new lazy_cache_map.LazyDereferencingMap.new(result, {dereference: this[_dereference]});
      }
      return result;
    }
    _get(key) {
      return this.getValue(this.data[$_get](key));
    }
    get(key) {
      return this.getValue(this.data[$_get](key));
    }
    containsKey(key) {
      return this.data[$containsKey](key);
    }
    containsValue(value) {
      return this.values[$contains](value);
    }
    get entries() {
      return this.data[$entries][$map](MapEntryOfString$Object(), dart.fn(entry => new (MapEntryOfString$Object()).__(entry.key, this.getValue(entry.value)), MapEntryOfString$ObjectToMapEntryOfString$Object()));
    }
    forEach(f) {
      function _forEachEntry(entry) {
        f(entry.key, entry.value);
      }
      dart.fn(_forEachEntry, MapEntryOfString$ObjectTovoid());
      this.entries[$forEach](_forEachEntry);
    }
    get isEmpty() {
      return this.data[$isEmpty];
    }
    get isNotEmpty() {
      return this.data[$isNotEmpty];
    }
    get keys() {
      return this.data[$keys];
    }
    get length() {
      return this.data[$length];
    }
    map(K2, V2, f) {
      function _mapEntry(entry) {
        return f(entry.key, entry.value);
      }
      dart.fn(_mapEntry, dart.fnType(core.MapEntry$(K2, V2), [MapEntryOfString$Object()]));
      return core.Map$(K2, V2).fromEntries(this.entries[$map](core.MapEntry$(K2, V2), _mapEntry));
    }
    get values() {
      return this.data[$values][$map](core.Object, dart.bind(this, 'getValue'));
    }
    _set(key, value$) {
      let value = value$;
      core.String._check(key);
      this.data[$_set](key, lazy_cache_map.unwrap(value));
      return value$;
    }
    addAll(other) {
      MapOfString$Object()._check(other);
      this.data[$addAll](MapOfString$Object().as(lazy_cache_map.unwrap(other)));
    }
    addEntries(entries) {
      IterableOfMapEntryOfString$Object()._check(entries);
      this.data[$addEntries](entries);
    }
    clear() {
      this.data[$clear]();
    }
    remove(key) {
      return this.getValue(this.data[$remove](key));
    }
    removeWhere(test) {
      this.data[$removeWhere](test);
    }
    putIfAbsent(key, ifAbsent) {
      core.String._check(key);
      VoidToObject()._check(ifAbsent);
      return this.getValue(this.data[$putIfAbsent](key, ifAbsent));
    }
    update(key, update, opts) {
      core.String._check(key);
      ObjectToObject()._check(update);
      let ifAbsent = opts && 'ifAbsent' in opts ? opts.ifAbsent : null;
      VoidToObject()._check(ifAbsent);
      return this.getValue(this.data[$update](key, update, {ifAbsent: ifAbsent}));
    }
    updateAll(update) {
      StringAndObjectToObject()._check(update);
      this.data[$updateAll](update);
    }
    cast(K, V) {
      dart.throw(new core.UnsupportedError.new("Cannot cast a lazy cache map map"));
    }
  };
  (lazy_cache_map.LazyDereferencingMap.new = function(data, opts) {
    let dereference = opts && 'dereference' in opts ? opts.dereference : null;
    this[_data] = MapOfString$Object().as(lazy_cache_map.unwrap(data));
    this[_dereference] = dereference;
    ;
  }).prototype = lazy_cache_map.LazyDereferencingMap.prototype;
  lazy_cache_map.LazyDereferencingMap.prototype[dart.isMap] = true;
  dart.addTypeTests(lazy_cache_map.LazyDereferencingMap);
  lazy_cache_map.LazyDereferencingMap[dart.implements] = () => [core.Map$(core.String, core.Object)];
  dart.setMethodSignature(lazy_cache_map.LazyDereferencingMap, () => ({
    __proto__: dart.getMethods(lazy_cache_map.LazyDereferencingMap.__proto__),
    getValue: dart.fnType(core.Object, [core.Object]),
    _get: dart.fnType(core.Object, [core.Object]),
    [$_get]: dart.fnType(core.Object, [core.Object]),
    get: dart.fnType(core.Object, [core.Object]),
    containsKey: dart.fnType(core.bool, [core.Object]),
    [$containsKey]: dart.fnType(core.bool, [core.Object]),
    containsValue: dart.fnType(core.bool, [core.Object]),
    [$containsValue]: dart.fnType(core.bool, [core.Object]),
    forEach: dart.fnType(dart.void, [dart.fnType(dart.void, [core.String, core.Object])]),
    [$forEach]: dart.fnType(dart.void, [dart.fnType(dart.void, [core.String, core.Object])]),
    map: dart.gFnType((K2, V2) => [core.Map$(K2, V2), [dart.fnType(core.MapEntry$(K2, V2), [core.String, core.Object])]]),
    [$map]: dart.gFnType((K2, V2) => [core.Map$(K2, V2), [dart.fnType(core.MapEntry$(K2, V2), [core.String, core.Object])]]),
    _set: dart.fnType(dart.void, [core.Object, core.Object]),
    [$_set]: dart.fnType(dart.void, [core.Object, core.Object]),
    addAll: dart.fnType(dart.void, [core.Object]),
    [$addAll]: dart.fnType(dart.void, [core.Object]),
    addEntries: dart.fnType(dart.void, [core.Object]),
    [$addEntries]: dart.fnType(dart.void, [core.Object]),
    clear: dart.fnType(dart.void, []),
    [$clear]: dart.fnType(dart.void, []),
    remove: dart.fnType(core.Object, [core.Object]),
    [$remove]: dart.fnType(core.Object, [core.Object]),
    removeWhere: dart.fnType(dart.void, [dart.fnType(core.bool, [core.String, core.Object])]),
    [$removeWhere]: dart.fnType(dart.void, [dart.fnType(core.bool, [core.String, core.Object])]),
    putIfAbsent: dart.fnType(core.Object, [core.Object, core.Object]),
    [$putIfAbsent]: dart.fnType(core.Object, [core.Object, core.Object]),
    update: dart.fnType(core.Object, [core.Object, core.Object], {ifAbsent: core.Object}, {}),
    [$update]: dart.fnType(core.Object, [core.Object, core.Object], {ifAbsent: core.Object}, {}),
    updateAll: dart.fnType(dart.void, [core.Object]),
    [$updateAll]: dart.fnType(dart.void, [core.Object]),
    cast: dart.gFnType((K, V) => [core.Map$(K, V), []]),
    [$cast]: dart.gFnType((K, V) => [core.Map$(K, V), []])
  }));
  dart.setGetterSignature(lazy_cache_map.LazyDereferencingMap, () => ({
    __proto__: dart.getGetters(lazy_cache_map.LazyDereferencingMap.__proto__),
    data: core.Map$(core.String, core.Object),
    entries: core.Iterable$(core.MapEntry$(core.String, core.Object)),
    [$entries]: core.Iterable$(core.MapEntry$(core.String, core.Object)),
    isEmpty: core.bool,
    [$isEmpty]: core.bool,
    isNotEmpty: core.bool,
    [$isNotEmpty]: core.bool,
    keys: core.Iterable$(core.String),
    [$keys]: core.Iterable$(core.String),
    length: core.int,
    [$length]: core.int,
    values: core.Iterable$(core.Object),
    [$values]: core.Iterable$(core.Object)
  }));
  dart.setLibraryUri(lazy_cache_map.LazyDereferencingMap, "package:graphql/src/cache/lazy_cache_map.dart");
  dart.setFieldSignature(lazy_cache_map.LazyDereferencingMap, () => ({
    __proto__: dart.getFields(lazy_cache_map.LazyDereferencingMap.__proto__),
    [_dereference]: dart.finalFieldType(dart.fnType(core.Object, [core.Object])),
    [_data]: dart.finalFieldType(core.Map$(core.String, core.Object))
  }));
  dart.defineExtensionMethods(lazy_cache_map.LazyDereferencingMap, [
    '_get',
    'containsKey',
    'containsValue',
    'forEach',
    'map',
    '_set',
    'addAll',
    'addEntries',
    'clear',
    'remove',
    'removeWhere',
    'putIfAbsent',
    'update',
    'updateAll',
    'cast'
  ]);
  dart.defineExtensionAccessors(lazy_cache_map.LazyDereferencingMap, [
    'entries',
    'isEmpty',
    'isNotEmpty',
    'keys',
    'length',
    'values'
  ]);
  const cacheState$ = dart.privateName(lazy_cache_map, "LazyCacheMap.cacheState");
  lazy_cache_map.LazyCacheMap = class LazyCacheMap extends lazy_cache_map.LazyDereferencingMap {
    get cacheState() {
      return this[cacheState$];
    }
    set cacheState(value) {
      super.cacheState = value;
    }
    get isOptimistic() {
      return dart.equals(this.cacheState, lazy_cache_map.CacheState.OPTIMISTIC);
    }
    getValue(value) {
      let t0;
      let result = (t0 = this[_dereference](value), t0 == null ? value : t0);
      if (core.List.is(result)) {
        return result[$map](core.Object, dart.bind(this, 'getValue'))[$toList]();
      }
      if (MapOfString$Object().is(result)) {
        return new lazy_cache_map.LazyCacheMap.new(result, {dereference: this[_dereference]});
      }
      return result;
    }
    get hashCode() {
      return core$.hash3(this[_data], this[_dereference], this.cacheState);
    }
    _equals(other) {
      if (other == null) return false;
      return lazy_cache_map.LazyCacheMap.is(other) && dart.equals(other[_data], this[_data]) && dart.equals(other[_dereference], this[_dereference]) && dart.equals(other.cacheState, this.cacheState);
    }
  };
  (lazy_cache_map.LazyCacheMap.new = function(data, opts) {
    let t0;
    let dereference = opts && 'dereference' in opts ? opts.dereference : null;
    let cacheState = opts && 'cacheState' in opts ? opts.cacheState : null;
    this[cacheState$] = (t0 = cacheState, t0 == null ? lazy_cache_map.LazyCacheMap.is(data) ? data.cacheState : null : t0);
    lazy_cache_map.LazyCacheMap.__proto__.new.call(this, data, {dereference: dereference});
    ;
  }).prototype = lazy_cache_map.LazyCacheMap.prototype;
  dart.addTypeTests(lazy_cache_map.LazyCacheMap);
  dart.setMethodSignature(lazy_cache_map.LazyCacheMap, () => ({
    __proto__: dart.getMethods(lazy_cache_map.LazyCacheMap.__proto__),
    _equals: dart.fnType(core.bool, [core.Object]),
    [$_equals]: dart.fnType(core.bool, [core.Object])
  }));
  dart.setGetterSignature(lazy_cache_map.LazyCacheMap, () => ({
    __proto__: dart.getGetters(lazy_cache_map.LazyCacheMap.__proto__),
    isOptimistic: core.bool
  }));
  dart.setLibraryUri(lazy_cache_map.LazyCacheMap, "package:graphql/src/cache/lazy_cache_map.dart");
  dart.setFieldSignature(lazy_cache_map.LazyCacheMap, () => ({
    __proto__: dart.getFields(lazy_cache_map.LazyCacheMap.__proto__),
    cacheState: dart.finalFieldType(lazy_cache_map.CacheState)
  }));
  dart.defineExtensionMethods(lazy_cache_map.LazyCacheMap, ['_equals']);
  dart.defineExtensionAccessors(lazy_cache_map.LazyCacheMap, ['hashCode']);
  lazy_cache_map.unwrap = function unwrap(possibleLazyMap) {
    return lazy_cache_map.LazyDereferencingMap.is(possibleLazyMap) ? possibleLazyMap.data : possibleLazyMap;
  };
  lazy_cache_map.unwrapMap = function unwrapMap(possibleLazyMap) {
    return lazy_cache_map.LazyDereferencingMap.is(possibleLazyMap) ? possibleLazyMap.data : possibleLazyMap;
  };
  const transform$ = dart.privateName(traverse, "Traversal.transform");
  const transformSideEffect$ = dart.privateName(traverse, "Traversal.transformSideEffect");
  const seenObjects$ = dart.privateName(traverse, "Traversal.seenObjects");
  traverse.Traversal = class Traversal extends core.Object {
    get transform() {
      return this[transform$];
    }
    set transform(value) {
      this[transform$] = value;
    }
    get transformSideEffect() {
      return this[transformSideEffect$];
    }
    set transformSideEffect(value) {
      this[transformSideEffect$] = value;
    }
    get seenObjects() {
      return this[seenObjects$];
    }
    set seenObjects(value) {
      this[seenObjects$] = value;
    }
    alreadySeen(node) {
      let wasAdded = this.seenObjects.add(node);
      return !dart.test(wasAdded);
    }
    traverseValues(node) {
      return node[$map](core.String, core.Object, dart.fn((key, value) => new (MapEntryOfString$Object()).__(key, this.traverse(value)), StringAndObjectToMapEntryOfString$Object()));
    }
    traverse(node) {
      let t0;
      let transformed = this.transform(node);
      if (dart.test(this.alreadySeen(node))) {
        t0 = transformed;
        return t0 == null ? node : t0;
      }
      if (transformed != null) {
        if (this.transformSideEffect != null) {
          this.transformSideEffect(transformed, node, this);
        }
        return transformed;
      }
      if (ListOfObject().is(node)) {
        return node[$map](core.Object, dart.fn(node => this.traverse(node), ObjectToObject()))[$toList]();
      }
      if (MapOfString$Object().is(node)) {
        return this.traverseValues(node);
      }
      return node;
    }
  };
  (traverse.Traversal.new = function(transform, opts) {
    let transformSideEffect = opts && 'transformSideEffect' in opts ? opts.transformSideEffect : null;
    let seenObjects = opts && 'seenObjects' in opts ? opts.seenObjects : null;
    this[transform$] = transform;
    this[transformSideEffect$] = transformSideEffect;
    this[seenObjects$] = seenObjects;
    this.seenObjects == null ? this.seenObjects = new (_HashSetOfObject()).new() : null;
  }).prototype = traverse.Traversal.prototype;
  dart.addTypeTests(traverse.Traversal);
  dart.setMethodSignature(traverse.Traversal, () => ({
    __proto__: dart.getMethods(traverse.Traversal.__proto__),
    alreadySeen: dart.fnType(core.bool, [core.Object]),
    traverseValues: dart.fnType(core.Map$(core.String, core.Object), [core.Map$(core.String, core.Object)]),
    traverse: dart.fnType(core.Object, [core.Object])
  }));
  dart.setLibraryUri(traverse.Traversal, "package:graphql/src/utilities/traverse.dart");
  dart.setFieldSignature(traverse.Traversal, () => ({
    __proto__: dart.getFields(traverse.Traversal.__proto__),
    transform: dart.fieldType(dart.fnType(core.Object, [core.Object])),
    transformSideEffect: dart.fieldType(dart.fnType(dart.void, [core.Object, core.Object, traverse.Traversal])),
    seenObjects: dart.fieldType(collection.HashSet$(core.Object))
  }));
  const line = dart.privateName(graphql_error, "Location.line");
  const column = dart.privateName(graphql_error, "Location.column");
  graphql_error.Location = class Location extends core.Object {
    get line() {
      return this[line];
    }
    set line(value) {
      super.line = value;
    }
    get column() {
      return this[column];
    }
    set column(value) {
      super.column = value;
    }
    toString() {
      return "{ line: " + dart.str(this.line) + ", column: " + dart.str(this.column) + " }";
    }
  };
  (graphql_error.Location.fromJSON = function(data) {
    this[line] = data[$_get]("line");
    this[column] = data[$_get]("column");
    ;
  }).prototype = graphql_error.Location.prototype;
  dart.addTypeTests(graphql_error.Location);
  dart.setLibraryUri(graphql_error.Location, "package:graphql/src/exceptions/graphql_error.dart");
  dart.setFieldSignature(graphql_error.Location, () => ({
    __proto__: dart.getFields(graphql_error.Location.__proto__),
    line: dart.finalFieldType(core.int),
    column: dart.finalFieldType(core.int)
  }));
  dart.defineExtensionMethods(graphql_error.Location, ['toString']);
  const raw$ = dart.privateName(graphql_error, "GraphQLError.raw");
  const message$ = dart.privateName(graphql_error, "GraphQLError.message");
  const locations$ = dart.privateName(graphql_error, "GraphQLError.locations");
  const path$ = dart.privateName(graphql_error, "GraphQLError.path");
  const extensions$ = dart.privateName(graphql_error, "GraphQLError.extensions");
  graphql_error.GraphQLError = class GraphQLError extends core.Object {
    get raw() {
      return this[raw$];
    }
    set raw(value) {
      super.raw = value;
    }
    get message() {
      return this[message$];
    }
    set message(value) {
      super.message = value;
    }
    get locations() {
      return this[locations$];
    }
    set locations(value) {
      super.locations = value;
    }
    get path() {
      return this[path$];
    }
    set path(value) {
      super.path = value;
    }
    get extensions() {
      return this[extensions$];
    }
    set extensions(value) {
      super.extensions = value;
    }
    toString() {
      return dart.str(this.message) + ": " + dart.str(core.List.is(this.locations) ? this.locations[$map](core.String, dart.fn(l => "[" + dart.str(dart.toString(l)) + "]", LocationToString()))[$join]("") : "Undefined location");
    }
  };
  (graphql_error.GraphQLError.new = function(opts) {
    let raw = opts && 'raw' in opts ? opts.raw : null;
    let message = opts && 'message' in opts ? opts.message : null;
    let locations = opts && 'locations' in opts ? opts.locations : null;
    let path = opts && 'path' in opts ? opts.path : null;
    let extensions = opts && 'extensions' in opts ? opts.extensions : null;
    this[raw$] = raw;
    this[message$] = message;
    this[locations$] = locations;
    this[path$] = path;
    this[extensions$] = extensions;
    ;
  }).prototype = graphql_error.GraphQLError.prototype;
  (graphql_error.GraphQLError.fromJSON = function(raw) {
    this[raw$] = raw;
    this[message$] = typeof dart.dsend(raw, '_get', ["message"]) == 'string' ? core.String.as(dart.dsend(raw, '_get', ["message"])) : "Invalid server response: message property needs to be of type String";
    this[locations$] = ListOfMapOfString$int().is(dart.dsend(raw, '_get', ["locations"])) ? ListOfLocation().from(ListOfMapOfString$int().as(dart.dsend(raw, '_get', ["locations"]))[$map](graphql_error.Location, dart.fn(location => new graphql_error.Location.fromJSON(location), MapOfString$intToLocation()))) : null;
    this[path$] = core.List.as(dart.dsend(raw, '_get', ["path"]));
    this[extensions$] = MapOfString$dynamic().as(dart.dsend(raw, '_get', ["extensions"]));
    ;
  }).prototype = graphql_error.GraphQLError.prototype;
  dart.addTypeTests(graphql_error.GraphQLError);
  dart.setLibraryUri(graphql_error.GraphQLError, "package:graphql/src/exceptions/graphql_error.dart");
  dart.setFieldSignature(graphql_error.GraphQLError, () => ({
    __proto__: dart.getFields(graphql_error.GraphQLError.__proto__),
    raw: dart.finalFieldType(dart.dynamic),
    message: dart.finalFieldType(core.String),
    locations: dart.finalFieldType(core.List$(graphql_error.Location)),
    path: dart.finalFieldType(core.List),
    extensions: dart.finalFieldType(core.Map$(core.String, dart.dynamic))
  }));
  dart.defineExtensionMethods(graphql_error.GraphQLError, ['toString']);
  _base_exceptions.ClientException = class ClientException extends core.Object {};
  (_base_exceptions.ClientException.new = function() {
    ;
  }).prototype = _base_exceptions.ClientException.prototype;
  dart.addTypeTests(_base_exceptions.ClientException);
  _base_exceptions.ClientException[dart.implements] = () => [core.Exception];
  dart.setLibraryUri(_base_exceptions.ClientException, "package:graphql/src/exceptions/_base_exceptions.dart");
  _base_exceptions.ClientCacheException = class ClientCacheException extends core.Object {};
  (_base_exceptions.ClientCacheException.new = function() {
    ;
  }).prototype = _base_exceptions.ClientCacheException.prototype;
  dart.addTypeTests(_base_exceptions.ClientCacheException);
  _base_exceptions.ClientCacheException[dart.implements] = () => [_base_exceptions.ClientException];
  dart.setLibraryUri(_base_exceptions.ClientCacheException, "package:graphql/src/exceptions/_base_exceptions.dart");
  const overflowError$ = dart.privateName(_base_exceptions, "NormalizationException.overflowError");
  const message$0 = dart.privateName(_base_exceptions, "NormalizationException.message");
  const value$ = dart.privateName(_base_exceptions, "NormalizationException.value");
  _base_exceptions.NormalizationException = class NormalizationException extends core.Object {
    get overflowError() {
      return this[overflowError$];
    }
    set overflowError(value) {
      this[overflowError$] = value;
    }
    get message() {
      return this[message$0];
    }
    set message(value) {
      this[message$0] = value;
    }
    get value() {
      return this[value$];
    }
    set value(value) {
      this[value$] = value;
    }
  };
  (_base_exceptions.NormalizationException.new = function(message, overflowError, value) {
    this[message$0] = message;
    this[overflowError$] = overflowError;
    this[value$] = value;
    ;
  }).prototype = _base_exceptions.NormalizationException.prototype;
  dart.addTypeTests(_base_exceptions.NormalizationException);
  _base_exceptions.NormalizationException[dart.implements] = () => [_base_exceptions.ClientCacheException];
  dart.setLibraryUri(_base_exceptions.NormalizationException, "package:graphql/src/exceptions/_base_exceptions.dart");
  dart.setFieldSignature(_base_exceptions.NormalizationException, () => ({
    __proto__: dart.getFields(_base_exceptions.NormalizationException.__proto__),
    overflowError: dart.fieldType(core.StackOverflowError),
    message: dart.fieldType(core.String),
    value: dart.fieldType(core.Object)
  }));
  const message$1 = dart.privateName(_base_exceptions, "CacheMissException.message");
  const missingKey$ = dart.privateName(_base_exceptions, "CacheMissException.missingKey");
  _base_exceptions.CacheMissException = class CacheMissException extends core.Object {
    get message() {
      return this[message$1];
    }
    set message(value) {
      this[message$1] = value;
    }
    get missingKey() {
      return this[missingKey$];
    }
    set missingKey(value) {
      this[missingKey$] = value;
    }
  };
  (_base_exceptions.CacheMissException.new = function(message, missingKey) {
    this[message$1] = message;
    this[missingKey$] = missingKey;
    ;
  }).prototype = _base_exceptions.CacheMissException.prototype;
  dart.addTypeTests(_base_exceptions.CacheMissException);
  _base_exceptions.CacheMissException[dart.implements] = () => [_base_exceptions.ClientCacheException];
  dart.setLibraryUri(_base_exceptions.CacheMissException, "package:graphql/src/exceptions/_base_exceptions.dart");
  dart.setFieldSignature(_base_exceptions.CacheMissException, () => ({
    __proto__: dart.getFields(_base_exceptions.CacheMissException.__proto__),
    message: dart.fieldType(core.String),
    missingKey: dart.fieldType(core.String)
  }));
  const failure$ = dart.privateName(_base_exceptions, "UnhandledFailureWrapper.failure");
  _base_exceptions.UnhandledFailureWrapper = class UnhandledFailureWrapper extends core.Object {
    get failure() {
      return this[failure$];
    }
    set failure(value) {
      this[failure$] = value;
    }
    get message() {
      return "Unhandled Failure " + dart.str(this.failure);
    }
    toString() {
      return this.message;
    }
  };
  (_base_exceptions.UnhandledFailureWrapper.new = function(failure) {
    this[failure$] = failure;
    ;
  }).prototype = _base_exceptions.UnhandledFailureWrapper.prototype;
  dart.addTypeTests(_base_exceptions.UnhandledFailureWrapper);
  _base_exceptions.UnhandledFailureWrapper[dart.implements] = () => [_base_exceptions.ClientException];
  dart.setGetterSignature(_base_exceptions.UnhandledFailureWrapper, () => ({
    __proto__: dart.getGetters(_base_exceptions.UnhandledFailureWrapper.__proto__),
    message: core.String
  }));
  dart.setLibraryUri(_base_exceptions.UnhandledFailureWrapper, "package:graphql/src/exceptions/_base_exceptions.dart");
  dart.setFieldSignature(_base_exceptions.UnhandledFailureWrapper, () => ({
    __proto__: dart.getFields(_base_exceptions.UnhandledFailureWrapper.__proto__),
    failure: dart.fieldType(core.Object)
  }));
  dart.defineExtensionMethods(_base_exceptions.UnhandledFailureWrapper, ['toString']);
  _base_exceptions.translateFailure = function translateFailure(failure) {
    if (_base_exceptions.ClientException.is(failure)) {
      return failure;
    }
    return new _base_exceptions.UnhandledFailureWrapper.new(failure);
  };
  const _writeToStorage = dart.privateName(in_memory_html, "_writeToStorage");
  const _readFromStorage = dart.privateName(in_memory_html, "_readFromStorage");
  const storagePrefix$ = dart.privateName(in_memory_html, "InMemoryCache.storagePrefix");
  const masterKey = dart.privateName(in_memory_html, "InMemoryCache.masterKey");
  const data = dart.privateName(in_memory_html, "InMemoryCache.data");
  in_memory_html.InMemoryCache = class InMemoryCache extends core.Object {
    get storagePrefix() {
      return this[storagePrefix$];
    }
    set storagePrefix(value) {
      super.storagePrefix = value;
    }
    get masterKey() {
      return this[masterKey];
    }
    set masterKey(value) {
      this[masterKey] = value;
    }
    get data() {
      return this[data];
    }
    set data(value) {
      this[data] = value;
    }
    read(key) {
      if (dart.test(this.data[$containsKey](key))) {
        return this.data[$_get](key);
      }
      return null;
    }
    write(key, value) {
      if (dart.test(this.data[$containsKey](key)) && core.Map.is(this.data[$_get](key)) && value != null && MapOfString$dynamic().is(value)) {
        this.data[$_set](key, helpers.deeplyMergeLeft(JSArrayOfMapOfString$dynamic().of([MapOfString$dynamic().as(this.data[$_get](key)), value])));
      } else {
        this.data[$_set](key, value);
      }
    }
    save() {
      return async.async(dart.void, (function* save() {
        yield this[_writeToStorage]();
      }).bind(this));
    }
    restore() {
      return async.async(dart.void, (function* restore() {
        this.data = (yield this[_readFromStorage]());
      }).bind(this));
    }
    reset() {
      this.data[$clear]();
    }
    [_writeToStorage]() {
      return async.async(dart.dynamic, (function* _writeToStorage() {
        html.window.localStorage[$_set](this.masterKey, convert.jsonEncode(this.data));
      }).bind(this));
    }
    [_readFromStorage]() {
      return async.async(HashMapOfString$dynamic(), (function* _readFromStorage() {
        try {
          let decoded = convert.jsonDecode(html.window.localStorage[$_get](this.masterKey));
          return HashMapOfString$dynamic().from(core.Map._check(decoded));
        } catch (e) {
          let error = dart.getThrown(e);
          core.print(error);
          return new (IdentityMapOfString$dynamic()).new();
        }
      }).bind(this));
    }
  };
  (in_memory_html.InMemoryCache.new = function(opts) {
    let t0;
    let storagePrefix = opts && 'storagePrefix' in opts ? opts.storagePrefix : "";
    this[masterKey] = null;
    this[data] = new (IdentityMapOfString$dynamic()).new();
    this[storagePrefix$] = storagePrefix;
    this.masterKey = (t0 = this.storagePrefix, t0 == null ? "" + "_graphql_cache" : t0);
  }).prototype = in_memory_html.InMemoryCache.prototype;
  dart.addTypeTests(in_memory_html.InMemoryCache);
  in_memory_html.InMemoryCache[dart.implements] = () => [cache.Cache];
  dart.setMethodSignature(in_memory_html.InMemoryCache, () => ({
    __proto__: dart.getMethods(in_memory_html.InMemoryCache.__proto__),
    read: dart.fnType(dart.dynamic, [core.String]),
    write: dart.fnType(dart.void, [core.String, dart.dynamic]),
    save: dart.fnType(async.Future$(dart.void), []),
    restore: dart.fnType(async.Future$(dart.void), []),
    reset: dart.fnType(dart.void, []),
    [_writeToStorage]: dart.fnType(async.Future, []),
    [_readFromStorage]: dart.fnType(async.Future$(collection.HashMap$(core.String, dart.dynamic)), [])
  }));
  dart.setLibraryUri(in_memory_html.InMemoryCache, "package:graphql/src/cache/in_memory_html.dart");
  dart.setFieldSignature(in_memory_html.InMemoryCache, () => ({
    __proto__: dart.getFields(in_memory_html.InMemoryCache.__proto__),
    storagePrefix: dart.finalFieldType(core.String),
    masterKey: dart.fieldType(core.String),
    data: dart.fieldType(collection.HashMap$(core.String, dart.dynamic))
  }));
  const SocketClientConfig_initPayload = dart.privateName(socket_client, "SocketClientConfig.initPayload");
  const Duration__duration = dart.privateName(core, "Duration._duration");
  let C4;
  const SocketClientConfig_queryAndMutationTimeout = dart.privateName(socket_client, "SocketClientConfig.queryAndMutationTimeout");
  let C5;
  const SocketClientConfig_delayBetweenReconnectionAttempts = dart.privateName(socket_client, "SocketClientConfig.delayBetweenReconnectionAttempts");
  let C6;
  const SocketClientConfig_inactivityTimeout = dart.privateName(socket_client, "SocketClientConfig.inactivityTimeout");
  const SocketClientConfig_autoReconnect = dart.privateName(socket_client, "SocketClientConfig.autoReconnect");
  let C3;
  const _socketClient = dart.privateName(link_web_socket, "_socketClient");
  const _doOperation = dart.privateName(link_web_socket, "_doOperation");
  const url$ = dart.privateName(link_web_socket, "WebSocketLink.url");
  const config$ = dart.privateName(link_web_socket, "WebSocketLink.config");
  link_web_socket.WebSocketLink = class WebSocketLink extends link.Link {
    get url() {
      return this[url$];
    }
    set url(value) {
      super.url = value;
    }
    get config() {
      return this[config$];
    }
    set config(value) {
      super.config = value;
    }
    [_doOperation](operation, forward = null) {
      if (this[_socketClient] == null) {
        this.connectOrReconnect();
      }
      return this[_socketClient].subscribe(new messages.SubscriptionRequest.new(operation), true).map(fetch_result.FetchResult, dart.fn(result => new fetch_result.FetchResult.new({data: result.data, errors: core.List.as(result.errors), context: operation.getContext(), extensions: operation.extensions}), SubscriptionDataToFetchResult()));
    }
    connectOrReconnect() {
      let t1;
      t1 = this[_socketClient];
      t1 == null ? null : t1.dispose();
      this[_socketClient] = new socket_client.SocketClient.new(this.url, {config: this.config});
    }
    dispose() {
      return async.async(dart.void, (function* dispose() {
        let t1;
        yield (t1 = this[_socketClient], t1 == null ? null : t1.dispose());
        this[_socketClient] = null;
      }).bind(this));
    }
  };
  (link_web_socket.WebSocketLink.new = function(opts) {
    let url = opts && 'url' in opts ? opts.url : null;
    let config = opts && 'config' in opts ? opts.config : C3 || CT.C3;
    this[_socketClient] = null;
    this[url$] = url;
    this[config$] = config;
    link_web_socket.WebSocketLink.__proto__.new.call(this);
    this.request = dart.bind(this, _doOperation);
  }).prototype = link_web_socket.WebSocketLink.prototype;
  dart.addTypeTests(link_web_socket.WebSocketLink);
  dart.setMethodSignature(link_web_socket.WebSocketLink, () => ({
    __proto__: dart.getMethods(link_web_socket.WebSocketLink.__proto__),
    [_doOperation]: dart.fnType(async.Stream$(fetch_result.FetchResult), [operation$.Operation], [dart.fnType(async.Stream$(fetch_result.FetchResult), [operation$.Operation])]),
    connectOrReconnect: dart.fnType(dart.void, []),
    dispose: dart.fnType(async.Future$(dart.void), [])
  }));
  dart.setLibraryUri(link_web_socket.WebSocketLink, "package:graphql/src/link/web_socket/link_web_socket.dart");
  dart.setFieldSignature(link_web_socket.WebSocketLink, () => ({
    __proto__: dart.getFields(link_web_socket.WebSocketLink.__proto__),
    url: dart.finalFieldType(core.String),
    config: dart.finalFieldType(socket_client.SocketClientConfig),
    [_socketClient]: dart.fieldType(socket_client.SocketClient)
  }));
  const _resolveQueryEagerly = dart.privateName(query_manager, "_resolveQueryEagerly");
  const _resolveQueryOnNetwork = dart.privateName(query_manager, "_resolveQueryOnNetwork");
  const _getOptimisticQueryResult = dart.privateName(query_manager, "_getOptimisticQueryResult");
  const _errorsFromResult = dart.privateName(query_manager, "_errorsFromResult");
  const _name$0 = dart.privateName(query_options, "_name");
  let C7;
  let C8;
  let C9;
  const link$ = dart.privateName(query_manager, "QueryManager.link");
  const cache$ = dart.privateName(query_manager, "QueryManager.cache");
  const scheduler$ = dart.privateName(query_manager, "QueryManager.scheduler");
  const idCounter = dart.privateName(query_manager, "QueryManager.idCounter");
  const queries = dart.privateName(query_manager, "QueryManager.queries");
  query_manager.QueryManager = class QueryManager extends core.Object {
    get link() {
      return this[link$];
    }
    set link(value) {
      super.link = value;
    }
    get cache() {
      return this[cache$];
    }
    set cache(value) {
      super.cache = value;
    }
    get scheduler() {
      return this[scheduler$];
    }
    set scheduler(value) {
      this[scheduler$] = value;
    }
    get idCounter() {
      return this[idCounter];
    }
    set idCounter(value) {
      this[idCounter] = value;
    }
    get queries() {
      return this[queries];
    }
    set queries(value) {
      this[queries] = value;
    }
    watchQuery(options) {
      let observableQuery = new observable_query.ObservableQuery.new({queryManager: this, options: options});
      this.setQuery(observableQuery);
      return observableQuery;
    }
    query(options) {
      return this.fetchQuery("0", options);
    }
    mutate(options) {
      return this.fetchQuery("0", options).then(query_result.QueryResult, dart.fn(result => async.async(query_result.QueryResult, (function*() {
        let mutationCallbacks = new query_options.MutationCallbacks.new({cache: this.cache, options: options, queryId: "0"});
        let callbacks = mutationCallbacks.callbacks;
        for (let callback of callbacks) {
          yield callback(result);
        }
        return result;
      }).bind(this)), QueryResultToFutureOfQueryResult()));
    }
    fetchQuery(queryId, options) {
      return async.async(query_result.QueryResult, (function* fetchQuery() {
        let t1;
        let allResults = this.fetchQueryAsMultiSourceResult(queryId, options);
        t1 = allResults.networkResult;
        return t1 == null ? allResults.eagerResult : t1;
      }).bind(this));
    }
    fetchQueryAsMultiSourceResult(queryId, options) {
      let eagerResult = this[_resolveQueryEagerly](queryId, options);
      return new query_result.MultiSourceResult.new({eagerResult: eagerResult, networkResult: dart.test(query_options.shouldStopAtCache(options.fetchPolicy)) && !dart.test(eagerResult.loading) ? null : this[_resolveQueryOnNetwork](queryId, options)});
    }
    [_resolveQueryOnNetwork](queryId, options) {
      return async.async(query_result.QueryResult, (function* _resolveQueryOnNetwork() {
        let t1;
        let operation = (t1 = operation$.Operation.fromOptions(options), t1.setContext(options.context), t1);
        let fetchResult = null;
        let queryResult = null;
        try {
          fetchResult = (yield link.execute({link: this.link, operation: operation}).first);
          if (fetchResult.data != null && !dart.equals(options.fetchPolicy, query_options.FetchPolicy.noCache)) {
            this.cache.write(operation.toKey(), fetchResult.data);
          }
          queryResult = this.mapFetchResultToQueryResult(fetchResult, options, {source: query_result.QueryResultSource.Network});
        } catch (e) {
          let failure = dart.getThrown(e);
          queryResult == null ? queryResult = new query_result.QueryResult.new({source: query_result.QueryResultSource.Network}) : null;
          queryResult.exception = operation_exception.coalesceErrors({exception: queryResult.exception, clientException: exceptions.translateFailure(failure)});
        }
        this.cleanupOptimisticResults(queryId);
        if (!dart.equals(options.fetchPolicy, query_options.FetchPolicy.noCache) && normalized_in_memory.NormalizedInMemoryCache.is(this.cache)) {
          queryResult.data = this.cache.read(operation.toKey());
        }
        this.addQueryResult(queryId, queryResult);
        return queryResult;
      }).bind(this));
    }
    [_resolveQueryEagerly](queryId, options) {
      let cacheKey = options.toKey();
      let queryResult = new query_result.QueryResult.new({loading: true});
      try {
        if (options.optimisticResult != null) {
          queryResult = this[_getOptimisticQueryResult](queryId, {cacheKey: cacheKey, optimisticResult: options.optimisticResult});
        }
        if (dart.test(query_options.shouldRespondEagerlyFromCache(options.fetchPolicy)) && !dart.test(queryResult.optimistic)) {
          let data = this.cache.read(cacheKey);
          if (data != null) {
            queryResult = new query_result.QueryResult.new({data: data, source: query_result.QueryResultSource.Cache});
          }
          if (dart.equals(options.fetchPolicy, query_options.FetchPolicy.cacheOnly) && dart.test(queryResult.loading)) {
            queryResult = new query_result.QueryResult.new({source: query_result.QueryResultSource.Cache, exception: new operation_exception.OperationException.new({clientException: new _base_exceptions.CacheMissException.new("Could not find that operation in the cache. (FetchPolicy.cacheOnly)", cacheKey)})});
          }
        }
      } catch (e) {
        let failure = dart.getThrown(e);
        queryResult.exception = operation_exception.coalesceErrors({exception: queryResult.exception, clientException: exceptions.translateFailure(failure)});
      }
      this.addQueryResult(queryId, queryResult);
      return queryResult;
    }
    refetchQuery(queryId) {
      let options = this.queries[$_get](queryId).options;
      return this.fetchQuery(queryId, options);
    }
    getQuery(queryId) {
      if (dart.test(this.queries[$containsKey](queryId))) {
        return this.queries[$_get](queryId);
      }
      return null;
    }
    addQueryResult(queryId, queryResult, opts) {
      let writeToCache = opts && 'writeToCache' in opts ? opts.writeToCache : false;
      let observableQuery = this.getQuery(queryId);
      if (dart.test(writeToCache)) {
        this.cache.write(observableQuery.options.toKey(), queryResult.data);
      }
      if (observableQuery != null && !dart.test(observableQuery.controller.isClosed)) {
        observableQuery.addResult(queryResult);
      }
    }
    [_getOptimisticQueryResult](queryId, opts) {
      let cacheKey = opts && 'cacheKey' in opts ? opts.cacheKey : null;
      let optimisticResult = opts && 'optimisticResult' in opts ? opts.optimisticResult : null;
      if (!optimistic.OptimisticCache.is(this.cache)) dart.assertFailed("can't optimisticly update non-optimistic cache", "org-dartlang-app:///packages/graphql/src/core/query_manager.dart", 259, 12, "cache is OptimisticCache");
      optimistic.OptimisticCache.as(this.cache).addOptimisiticPatch(queryId, dart.fn(cache => {
        let t3;
        t3 = cache;
        t3.write(cacheKey, optimisticResult);
        return t3;
      }, CacheToCache()));
      let queryResult = new query_result.QueryResult.new({data: this.cache.read(cacheKey), source: query_result.QueryResultSource.OptimisticResult});
      return queryResult;
    }
    cleanupOptimisticResults(cacheKey) {
      if (optimistic.OptimisticCache.is(this.cache)) {
        optimistic.OptimisticCache.as(this.cache).removeOptimisticPatch(cacheKey);
      }
    }
    rebroadcastQueries() {
      for (let query of this.queries[$values]) {
        if (dart.test(query.isRebroadcastSafe)) {
          let cachedData = this.cache.read(query.options.toKey());
          if (cachedData != null) {
            query.addResult(this.mapFetchResultToQueryResult(new fetch_result.FetchResult.new({data: cachedData}), query.options, {source: query_result.QueryResultSource.Cache}));
          }
        }
      }
    }
    setQuery(observableQuery) {
      this.queries[$_set](observableQuery.queryId, observableQuery);
    }
    closeQuery(observableQuery, opts) {
      let fromQuery = opts && 'fromQuery' in opts ? opts.fromQuery : false;
      if (!dart.test(fromQuery)) {
        observableQuery.close({fromManager: true});
      }
      this.queries[$remove](observableQuery.queryId);
    }
    generateQueryId() {
      let requestId = this.idCounter;
      this.idCounter = dart.notNull(this.idCounter) + 1;
      return requestId;
    }
    mapFetchResultToQueryResult(fetchResult, options, opts) {
      let source = opts && 'source' in opts ? opts.source : null;
      let errors = null;
      let data = null;
      if (fetchResult.errors != null && dart.test(fetchResult.errors[$isNotEmpty])) {
        switch (options.errorPolicy) {
          case C7 || CT.C7:
          {
            errors = this[_errorsFromResult](fetchResult);
            data = fetchResult.data;
            break;
          }
          case C8 || CT.C8:
          {
            data = fetchResult.data;
            break;
          }
          case C9 || CT.C9:
          default:
          {
            errors = this[_errorsFromResult](fetchResult);
            break;
          }
        }
      } else {
        data = fetchResult.data;
      }
      return new query_result.QueryResult.new({data: data, source: source, exception: operation_exception.coalesceErrors({graphqlErrors: errors})});
    }
    [_errorsFromResult](fetchResult) {
      return ListOfGraphQLError().from(fetchResult.errors[$map](graphql_error.GraphQLError, dart.fn(rawError => new graphql_error.GraphQLError.fromJSON(rawError), dynamicToGraphQLError())));
    }
  };
  (query_manager.QueryManager.new = function(opts) {
    let link = opts && 'link' in opts ? opts.link : null;
    let cache = opts && 'cache' in opts ? opts.cache : null;
    this[scheduler$] = null;
    this[idCounter] = 1;
    this[queries] = new (IdentityMapOfString$ObservableQuery()).new();
    this[link$] = link;
    this[cache$] = cache;
    this.scheduler = new scheduler.QueryScheduler.new({queryManager: this});
  }).prototype = query_manager.QueryManager.prototype;
  dart.addTypeTests(query_manager.QueryManager);
  dart.setMethodSignature(query_manager.QueryManager, () => ({
    __proto__: dart.getMethods(query_manager.QueryManager.__proto__),
    watchQuery: dart.fnType(observable_query.ObservableQuery, [query_options.WatchQueryOptions]),
    query: dart.fnType(async.Future$(query_result.QueryResult), [query_options.QueryOptions]),
    mutate: dart.fnType(async.Future$(query_result.QueryResult), [query_options.MutationOptions]),
    fetchQuery: dart.fnType(async.Future$(query_result.QueryResult), [core.String, query_options.BaseOptions]),
    fetchQueryAsMultiSourceResult: dart.fnType(query_result.MultiSourceResult, [core.String, query_options.BaseOptions]),
    [_resolveQueryOnNetwork]: dart.fnType(async.Future$(query_result.QueryResult), [core.String, query_options.BaseOptions]),
    [_resolveQueryEagerly]: dart.fnType(query_result.QueryResult, [core.String, query_options.BaseOptions]),
    refetchQuery: dart.fnType(async.Future$(query_result.QueryResult), [core.String]),
    getQuery: dart.fnType(observable_query.ObservableQuery, [core.String]),
    addQueryResult: dart.fnType(dart.void, [core.String, query_result.QueryResult], {writeToCache: core.bool}, {}),
    [_getOptimisticQueryResult]: dart.fnType(query_result.QueryResult, [core.String], {cacheKey: core.String, optimisticResult: core.Object}, {}),
    cleanupOptimisticResults: dart.fnType(dart.void, [core.String]),
    rebroadcastQueries: dart.fnType(dart.void, []),
    setQuery: dart.fnType(dart.void, [observable_query.ObservableQuery]),
    closeQuery: dart.fnType(dart.void, [observable_query.ObservableQuery], {fromQuery: core.bool}, {}),
    generateQueryId: dart.fnType(core.int, []),
    mapFetchResultToQueryResult: dart.fnType(query_result.QueryResult, [fetch_result.FetchResult, query_options.BaseOptions], {source: query_result.QueryResultSource}, {}),
    [_errorsFromResult]: dart.fnType(core.List$(graphql_error.GraphQLError), [fetch_result.FetchResult])
  }));
  dart.setLibraryUri(query_manager.QueryManager, "package:graphql/src/core/query_manager.dart");
  dart.setFieldSignature(query_manager.QueryManager, () => ({
    __proto__: dart.getFields(query_manager.QueryManager.__proto__),
    link: dart.finalFieldType(link.Link),
    cache: dart.finalFieldType(cache.Cache),
    scheduler: dart.fieldType(scheduler.QueryScheduler),
    idCounter: dart.fieldType(core.int),
    queries: dart.fieldType(core.Map$(core.String, observable_query.ObservableQuery))
  }));
  const _pollingTimers = dart.privateName(scheduler, "_pollingTimers");
  const queryManager$ = dart.privateName(scheduler, "QueryScheduler.queryManager");
  const registeredQueries = dart.privateName(scheduler, "QueryScheduler.registeredQueries");
  const intervalQueries = dart.privateName(scheduler, "QueryScheduler.intervalQueries");
  scheduler.QueryScheduler = class QueryScheduler extends core.Object {
    get queryManager() {
      return this[queryManager$];
    }
    set queryManager(value) {
      this[queryManager$] = value;
    }
    get registeredQueries() {
      return this[registeredQueries];
    }
    set registeredQueries(value) {
      this[registeredQueries] = value;
    }
    get intervalQueries() {
      return this[intervalQueries];
    }
    set intervalQueries(value) {
      this[intervalQueries] = value;
    }
    fetchQueriesOnInterval(timer, interval) {
      this.intervalQueries[$_get](interval)[$retainWhere](dart.fn(queryId => {
        if (this.registeredQueries[$_get](queryId) == null) {
          return false;
        }
        let pollInterval = new core.Duration.new({seconds: this.registeredQueries[$_get](queryId).pollInterval});
        return dart.test(this.registeredQueries[$containsKey](queryId)) && pollInterval._equals(interval);
      }, StringTobool()));
      if (dart.test(this.intervalQueries[$_get](interval)[$isEmpty])) {
        this.intervalQueries[$remove](interval);
        this[_pollingTimers][$remove](interval);
        timer.cancel();
        return;
      }
      this.intervalQueries[$_get](interval)[$forEach](dart.bind(this.queryManager, 'refetchQuery'));
    }
    startPollingQuery(options, queryId) {
      if (!(options.pollInterval != null && dart.notNull(options.pollInterval) > 0)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql/src/scheduler/scheduler.dart", 67, 12, "options.pollInterval != null && options.pollInterval > 0");
      this.registeredQueries[$_set](queryId, options);
      let interval = new core.Duration.new({seconds: options.pollInterval});
      if (dart.test(this.intervalQueries[$containsKey](interval))) {
        this.intervalQueries[$_get](interval)[$add](queryId);
      } else {
        this.intervalQueries[$_set](interval, JSArrayOfString().of([queryId]));
        this[_pollingTimers][$_set](interval, async.Timer.periodic(interval, dart.fn(timer => this.fetchQueriesOnInterval(timer, interval), TimerTovoid())));
      }
    }
    stopPollingQuery(queryId) {
      this.registeredQueries[$remove](queryId);
    }
  };
  (scheduler.QueryScheduler.new = function(opts) {
    let queryManager = opts && 'queryManager' in opts ? opts.queryManager : null;
    this[registeredQueries] = new (IdentityMapOfString$WatchQueryOptions()).new();
    this[intervalQueries] = new (LinkedMapOfDuration$ListOfString()).new();
    this[_pollingTimers] = new (LinkedMapOfDuration$Timer()).new();
    this[queryManager$] = queryManager;
    ;
  }).prototype = scheduler.QueryScheduler.prototype;
  dart.addTypeTests(scheduler.QueryScheduler);
  dart.setMethodSignature(scheduler.QueryScheduler, () => ({
    __proto__: dart.getMethods(scheduler.QueryScheduler.__proto__),
    fetchQueriesOnInterval: dart.fnType(dart.void, [async.Timer, core.Duration]),
    startPollingQuery: dart.fnType(dart.void, [query_options.WatchQueryOptions, core.String]),
    stopPollingQuery: dart.fnType(dart.void, [core.String])
  }));
  dart.setLibraryUri(scheduler.QueryScheduler, "package:graphql/src/scheduler/scheduler.dart");
  dart.setFieldSignature(scheduler.QueryScheduler, () => ({
    __proto__: dart.getFields(scheduler.QueryScheduler.__proto__),
    queryManager: dart.fieldType(query_manager.QueryManager),
    registeredQueries: dart.fieldType(core.Map$(core.String, query_options.WatchQueryOptions)),
    intervalQueries: dart.fieldType(core.Map$(core.Duration, core.List$(core.String))),
    [_pollingTimers]: dart.finalFieldType(core.Map$(core.Duration, async.Timer))
  }));
  const _name$1 = dart.privateName(observable_query, "_name");
  let C10;
  let C11;
  let C12;
  let C13;
  let C14;
  let C15;
  let C16;
  let C17;
  let C18;
  observable_query.QueryLifecycle = class QueryLifecycle extends core.Object {
    toString() {
      return this[_name$1];
    }
  };
  (observable_query.QueryLifecycle.new = function(index, _name) {
    this.index = index;
    this[_name$1] = _name;
    ;
  }).prototype = observable_query.QueryLifecycle.prototype;
  dart.addTypeTests(observable_query.QueryLifecycle);
  dart.setLibraryUri(observable_query.QueryLifecycle, "package:graphql/src/core/observable_query.dart");
  dart.setFieldSignature(observable_query.QueryLifecycle, () => ({
    __proto__: dart.getFields(observable_query.QueryLifecycle.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$1]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(observable_query.QueryLifecycle, ['toString']);
  observable_query.QueryLifecycle.UNEXECUTED = C10 || CT.C10;
  observable_query.QueryLifecycle.PENDING = C11 || CT.C11;
  observable_query.QueryLifecycle.POLLING = C12 || CT.C12;
  observable_query.QueryLifecycle.POLLING_STOPPED = C13 || CT.C13;
  observable_query.QueryLifecycle.SIDE_EFFECTS_PENDING = C14 || CT.C14;
  observable_query.QueryLifecycle.SIDE_EFFECTS_BLOCKING = C15 || CT.C15;
  observable_query.QueryLifecycle.COMPLETED = C16 || CT.C16;
  observable_query.QueryLifecycle.CLOSED = C17 || CT.C17;
  observable_query.QueryLifecycle.values = C18 || CT.C18;
  const _latestWasEagerlyFetched = dart.privateName(observable_query, "_latestWasEagerlyFetched");
  const _onDataSubscriptions = dart.privateName(observable_query, "_onDataSubscriptions");
  const _isRefetchSafe = dart.privateName(observable_query, "_isRefetchSafe");
  let C19;
  const queryId = dart.privateName(observable_query, "ObservableQuery.queryId");
  const queryManager$0 = dart.privateName(observable_query, "ObservableQuery.queryManager");
  const latestResult = dart.privateName(observable_query, "ObservableQuery.latestResult");
  const lifecycle = dart.privateName(observable_query, "ObservableQuery.lifecycle");
  const options$ = dart.privateName(observable_query, "ObservableQuery.options");
  const controller = dart.privateName(observable_query, "ObservableQuery.controller");
  observable_query.ObservableQuery = class ObservableQuery extends core.Object {
    get queryId() {
      return this[queryId];
    }
    set queryId(value) {
      super.queryId = value;
    }
    get queryManager() {
      return this[queryManager$0];
    }
    set queryManager(value) {
      super.queryManager = value;
    }
    get latestResult() {
      return this[latestResult];
    }
    set latestResult(value) {
      this[latestResult] = value;
    }
    get lifecycle() {
      return this[lifecycle];
    }
    set lifecycle(value) {
      this[lifecycle] = value;
    }
    get options() {
      return this[options$];
    }
    set options(value) {
      this[options$] = value;
    }
    get controller() {
      return this[controller];
    }
    set controller(value) {
      this[controller] = value;
    }
    get scheduler() {
      return this.queryManager.scheduler;
    }
    get stream() {
      return this.controller.stream;
    }
    get isCurrentlyPolling() {
      return dart.equals(this.lifecycle, observable_query.QueryLifecycle.POLLING);
    }
    get [_isRefetchSafe]() {
      switch (this.lifecycle) {
        case C16 || CT.C16:
        case C12 || CT.C12:
        case C13 || CT.C13:
        {
          return true;
        }
        case C11 || CT.C11:
        case C17 || CT.C17:
        case C10 || CT.C10:
        case C14 || CT.C14:
        case C15 || CT.C15:
        {
          return false;
        }
      }
      return false;
    }
    refetch() {
      if (dart.test(this[_isRefetchSafe])) {
        return this.queryManager.refetchQuery(this.queryId);
      }
      return FutureOfQueryResult().error(core.Exception.new("Query is not refetch safe"));
    }
    get isRebroadcastSafe() {
      switch (this.lifecycle) {
        case C11 || CT.C11:
        case C16 || CT.C16:
        case C12 || CT.C12:
        case C13 || CT.C13:
        {
          return true;
        }
        case C10 || CT.C10:
        case C17 || CT.C17:
        case C14 || CT.C14:
        case C15 || CT.C15:
        {
          return false;
        }
      }
      return false;
    }
    onListen() {
      if (dart.test(this[_latestWasEagerlyFetched])) {
        this[_latestWasEagerlyFetched] = false;
        if (!dart.test(this.controller.isClosed) && this.latestResult != null) {
          this.controller.add(this.latestResult);
        }
        return;
      }
      if (dart.test(this.options.fetchResults)) {
        this.fetchResults();
      }
    }
    fetchResults() {
      let allResults = this.queryManager.fetchQueryAsMultiSourceResult(this.queryId, this.options);
      this.latestResult == null ? this.latestResult = allResults.eagerResult : null;
      this.lifecycle = dart.test(this[_onDataSubscriptions][$isNotEmpty]) ? observable_query.QueryLifecycle.SIDE_EFFECTS_PENDING : observable_query.QueryLifecycle.PENDING;
      if (this.options.pollInterval != null && dart.notNull(this.options.pollInterval) > 0) {
        this.startPolling(this.options.pollInterval);
      }
      return allResults;
    }
    fetchMore(fetchMoreOptions) {
      return async.async(dart.void, (function* fetchMore() {
        let t3;
        if (!(fetchMoreOptions.updateQuery != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql/src/core/observable_query.dart", 143, 12, "fetchMoreOptions.updateQuery != null");
        let combinedOptions = new query_options.QueryOptions.new({fetchPolicy: query_options.FetchPolicy.noCache, errorPolicy: this.options.errorPolicy, documentNode: (t3 = fetchMoreOptions.documentNode, t3 == null ? this.options.documentNode : t3), context: this.options.context, variables: (() => {
            let t3 = new (IdentityMapOfString$dynamic()).new();
            for (let t4 of this.options.variables[$entries])
              t3[$_set](t4.key, t4.value);
            for (let t5 of fetchMoreOptions.variables[$entries])
              t3[$_set](t5.key, t5.value);
            return t3;
          })()});
        this.addResult(new query_result.QueryResult.new({data: this.latestResult.data, loading: true}));
        let fetchMoreResult = (yield this.queryManager.query(combinedOptions));
        try {
          fetchMoreResult.data = fetchMoreOptions.updateQuery(this.latestResult.data, fetchMoreResult.data);
          if (!(fetchMoreResult.data != null)) dart.assertFailed("updateQuery result cannot be null", "org-dartlang-app:///packages/graphql/src/core/observable_query.dart", 170, 14, "fetchMoreResult.data != null");
          this.queryManager.addQueryResult(this.queryId, fetchMoreResult, {writeToCache: true});
        } catch (e) {
          let error = dart.getThrown(e);
          if (dart.test(fetchMoreResult.hasException)) {
            this.latestResult.exception = operation_exception.coalesceErrors({exception: this.latestResult.exception, graphqlErrors: fetchMoreResult.exception.graphqlErrors, clientException: fetchMoreResult.exception.clientException});
            this.queryManager.addQueryResult(this.queryId, this.latestResult, {writeToCache: true});
            return;
          } else {
            dart.rethrow(e);
          }
        }
      }).bind(this));
    }
    addResult(result) {
      let t7;
      if (this.latestResult != null && dart.test(this.latestResult.timestamp.isAfter(result.timestamp))) {
        return;
      }
      if (this.latestResult != null) {
        t7 = result;
        t7.source == null ? t7.source = this.latestResult.source : null;
      }
      if (dart.equals(this.lifecycle, observable_query.QueryLifecycle.PENDING) && !dart.equals(result.optimistic, true)) {
        this.lifecycle = observable_query.QueryLifecycle.COMPLETED;
      }
      this.latestResult = result;
      if (!dart.test(this.controller.isClosed)) {
        this.controller.add(result);
      }
    }
    onData(callbacks) {
      callbacks == null ? callbacks = C19 || CT.C19 : null;
      let subscription = null;
      subscription = this.stream.listen(dart.fn(result => async.async(core.Null, (function*() {
        if (!dart.test(result.loading)) {
          for (let callback of callbacks) {
            yield callback(result);
          }
          this.queryManager.rebroadcastQueries();
          if (!dart.test(result.optimistic)) {
            yield subscription.cancel();
            this[_onDataSubscriptions].remove(subscription);
            if (dart.test(this[_onDataSubscriptions][$isEmpty])) {
              if (dart.equals(this.lifecycle, observable_query.QueryLifecycle.SIDE_EFFECTS_BLOCKING)) {
                this.lifecycle = observable_query.QueryLifecycle.COMPLETED;
                this.close();
              }
            }
          }
        }
      }).bind(this)), QueryResultToFutureOfNull()));
      this[_onDataSubscriptions].add(subscription);
    }
    startPolling(pollInterval) {
      if (dart.equals(this.options.fetchPolicy, query_options.FetchPolicy.cacheFirst) || dart.equals(this.options.fetchPolicy, query_options.FetchPolicy.cacheOnly)) {
        dart.throw(core.Exception.new("Queries that specify the cacheFirst and cacheOnly fetch policies cannot also be polling queries."));
      }
      if (dart.test(this.isCurrentlyPolling)) {
        this.scheduler.stopPollingQuery(this.queryId);
      }
      this.options.pollInterval = pollInterval;
      this.lifecycle = observable_query.QueryLifecycle.POLLING;
      this.scheduler.startPollingQuery(this.options, this.queryId);
    }
    stopPolling() {
      if (dart.test(this.isCurrentlyPolling)) {
        this.scheduler.stopPollingQuery(this.queryId);
        this.options.pollInterval = null;
        this.lifecycle = observable_query.QueryLifecycle.POLLING_STOPPED;
      }
    }
    set variables(variables) {
      this.options.variables = variables;
    }
    close(opts) {
      let force = opts && 'force' in opts ? opts.force : false;
      let fromManager = opts && 'fromManager' in opts ? opts.fromManager : false;
      return async.async(observable_query.QueryLifecycle, (function* close() {
        if (dart.equals(this.lifecycle, observable_query.QueryLifecycle.SIDE_EFFECTS_PENDING) && !dart.test(force)) {
          this.lifecycle = observable_query.QueryLifecycle.SIDE_EFFECTS_BLOCKING;
          return this.lifecycle;
        }
        if (!dart.test(fromManager)) {
          this.queryManager.closeQuery(this, {fromQuery: true});
        }
        for (let subscription of this[_onDataSubscriptions]) {
          yield subscription.cancel();
        }
        this.stopPolling();
        yield this.controller.close();
        this.lifecycle = observable_query.QueryLifecycle.CLOSED;
        return observable_query.QueryLifecycle.CLOSED;
      }).bind(this));
    }
  };
  (observable_query.ObservableQuery.new = function(opts) {
    let queryManager = opts && 'queryManager' in opts ? opts.queryManager : null;
    let options = opts && 'options' in opts ? opts.options : null;
    this[_latestWasEagerlyFetched] = false;
    this[_onDataSubscriptions] = LinkedHashSetOfStreamSubscriptionOfQueryResult().new();
    this[latestResult] = null;
    this[lifecycle] = observable_query.QueryLifecycle.UNEXECUTED;
    this[controller] = null;
    this[queryManager$0] = queryManager;
    this[options$] = options;
    this[queryId] = dart.toString(queryManager.generateQueryId());
    if (dart.test(this.options.eagerlyFetchResults)) {
      this[_latestWasEagerlyFetched] = true;
      this.fetchResults();
    }
    this.controller = StreamControllerOfQueryResult().broadcast({onListen: dart.bind(this, 'onListen')});
  }).prototype = observable_query.ObservableQuery.prototype;
  dart.addTypeTests(observable_query.ObservableQuery);
  dart.setMethodSignature(observable_query.ObservableQuery, () => ({
    __proto__: dart.getMethods(observable_query.ObservableQuery.__proto__),
    refetch: dart.fnType(async.Future$(query_result.QueryResult), []),
    onListen: dart.fnType(dart.void, []),
    fetchResults: dart.fnType(query_result.MultiSourceResult, []),
    fetchMore: dart.fnType(dart.void, [query_options.FetchMoreOptions]),
    addResult: dart.fnType(dart.void, [query_result.QueryResult]),
    onData: dart.fnType(dart.void, [core.Iterable$(dart.fnType(dart.void, [query_result.QueryResult]))]),
    startPolling: dart.fnType(dart.void, [core.int]),
    stopPolling: dart.fnType(dart.void, []),
    close: dart.fnType(async.FutureOr$(observable_query.QueryLifecycle), [], {force: core.bool, fromManager: core.bool}, {})
  }));
  dart.setGetterSignature(observable_query.ObservableQuery, () => ({
    __proto__: dart.getGetters(observable_query.ObservableQuery.__proto__),
    scheduler: scheduler.QueryScheduler,
    stream: async.Stream$(query_result.QueryResult),
    isCurrentlyPolling: core.bool,
    [_isRefetchSafe]: core.bool,
    isRebroadcastSafe: core.bool
  }));
  dart.setSetterSignature(observable_query.ObservableQuery, () => ({
    __proto__: dart.getSetters(observable_query.ObservableQuery.__proto__),
    variables: core.Map$(core.String, dart.dynamic)
  }));
  dart.setLibraryUri(observable_query.ObservableQuery, "package:graphql/src/core/observable_query.dart");
  dart.setFieldSignature(observable_query.ObservableQuery, () => ({
    __proto__: dart.getFields(observable_query.ObservableQuery.__proto__),
    [_latestWasEagerlyFetched]: dart.fieldType(core.bool),
    queryId: dart.finalFieldType(core.String),
    queryManager: dart.finalFieldType(query_manager.QueryManager),
    [_onDataSubscriptions]: dart.finalFieldType(core.Set$(async.StreamSubscription$(query_result.QueryResult))),
    latestResult: dart.fieldType(query_result.QueryResult),
    lifecycle: dart.fieldType(observable_query.QueryLifecycle),
    options: dart.fieldType(query_options.WatchQueryOptions),
    controller: dart.fieldType(async.StreamController$(query_result.QueryResult))
  }));
  const _name$2 = dart.privateName(query_result, "_name");
  let C20;
  let C21;
  let C22;
  let C23;
  let C24;
  query_result.QueryResultSource = class QueryResultSource extends core.Object {
    toString() {
      return this[_name$2];
    }
  };
  (query_result.QueryResultSource.new = function(index, _name) {
    this.index = index;
    this[_name$2] = _name;
    ;
  }).prototype = query_result.QueryResultSource.prototype;
  dart.addTypeTests(query_result.QueryResultSource);
  dart.setLibraryUri(query_result.QueryResultSource, "package:graphql/src/core/query_result.dart");
  dart.setFieldSignature(query_result.QueryResultSource, () => ({
    __proto__: dart.getFields(query_result.QueryResultSource.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$2]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(query_result.QueryResultSource, ['toString']);
  query_result.QueryResultSource.Loading = C20 || CT.C20;
  query_result.QueryResultSource.Cache = C21 || CT.C21;
  query_result.QueryResultSource.OptimisticResult = C22 || CT.C22;
  query_result.QueryResultSource.Network = C23 || CT.C23;
  query_result.QueryResultSource.values = C24 || CT.C24;
  const timestamp = dart.privateName(query_result, "QueryResult.timestamp");
  const source$ = dart.privateName(query_result, "QueryResult.source");
  const data$ = dart.privateName(query_result, "QueryResult.data");
  const exception$ = dart.privateName(query_result, "QueryResult.exception");
  query_result.QueryResult = class QueryResult extends core.Object {
    get timestamp() {
      return this[timestamp];
    }
    set timestamp(value) {
      this[timestamp] = value;
    }
    get source() {
      return this[source$];
    }
    set source(value) {
      this[source$] = value;
    }
    get data() {
      return this[data$];
    }
    set data(value) {
      this[data$] = value;
    }
    get exception() {
      return this[exception$];
    }
    set exception(value) {
      this[exception$] = value;
    }
    get loading() {
      return dart.equals(this.source, query_result.QueryResultSource.Loading);
    }
    get optimistic() {
      return dart.equals(this.source, query_result.QueryResultSource.OptimisticResult);
    }
    get hasException() {
      return this.exception != null;
    }
  };
  (query_result.QueryResult.new = function(opts) {
    let t7;
    let data = opts && 'data' in opts ? opts.data : null;
    let exception = opts && 'exception' in opts ? opts.exception : null;
    let loading = opts && 'loading' in opts ? opts.loading : null;
    let optimistic = opts && 'optimistic' in opts ? opts.optimistic : null;
    let source = opts && 'source' in opts ? opts.source : null;
    this[data$] = data;
    this[exception$] = exception;
    this[timestamp] = new core.DateTime.now();
    this[source$] = (t7 = source, t7 == null ? dart.equals(loading, true) ? query_result.QueryResultSource.Loading : dart.equals(optimistic, true) ? query_result.QueryResultSource.OptimisticResult : null : t7);
    ;
  }).prototype = query_result.QueryResult.prototype;
  dart.addTypeTests(query_result.QueryResult);
  dart.setGetterSignature(query_result.QueryResult, () => ({
    __proto__: dart.getGetters(query_result.QueryResult.__proto__),
    loading: core.bool,
    optimistic: core.bool,
    hasException: core.bool
  }));
  dart.setLibraryUri(query_result.QueryResult, "package:graphql/src/core/query_result.dart");
  dart.setFieldSignature(query_result.QueryResult, () => ({
    __proto__: dart.getFields(query_result.QueryResult.__proto__),
    timestamp: dart.fieldType(core.DateTime),
    source: dart.fieldType(query_result.QueryResultSource),
    data: dart.fieldType(dart.dynamic),
    exception: dart.fieldType(operation_exception.OperationException)
  }));
  const eagerResult$ = dart.privateName(query_result, "MultiSourceResult.eagerResult");
  const networkResult$ = dart.privateName(query_result, "MultiSourceResult.networkResult");
  query_result.MultiSourceResult = class MultiSourceResult extends core.Object {
    get eagerResult() {
      return this[eagerResult$];
    }
    set eagerResult(value) {
      this[eagerResult$] = value;
    }
    get networkResult() {
      return this[networkResult$];
    }
    set networkResult(value) {
      this[networkResult$] = value;
    }
  };
  (query_result.MultiSourceResult.new = function(opts) {
    let eagerResult = opts && 'eagerResult' in opts ? opts.eagerResult : null;
    let networkResult = opts && 'networkResult' in opts ? opts.networkResult : null;
    this[eagerResult$] = eagerResult;
    this[networkResult$] = networkResult;
    if (!!dart.equals(eagerResult.source, query_result.QueryResultSource.Network)) dart.assertFailed("An eager result cannot be gotten from the network", "org-dartlang-app:///packages/graphql/src/core/query_result.dart", 70, 11, "eagerResult.source != QueryResultSource.Network");
    this.eagerResult == null ? this.eagerResult = new query_result.QueryResult.new({loading: true}) : null;
  }).prototype = query_result.MultiSourceResult.prototype;
  dart.addTypeTests(query_result.MultiSourceResult);
  dart.setLibraryUri(query_result.MultiSourceResult, "package:graphql/src/core/query_result.dart");
  dart.setFieldSignature(query_result.MultiSourceResult, () => ({
    __proto__: dart.getFields(query_result.MultiSourceResult.__proto__),
    eagerResult: dart.fieldType(query_result.QueryResult),
    networkResult: dart.fieldType(async.FutureOr$(query_result.QueryResult))
  }));
  dart.defineLazy(query_result, {
    /*query_result.eagerSources*/get eagerSources() {
      return LinkedHashSetOfQueryResultSource().from([query_result.QueryResultSource.Cache, query_result.QueryResultSource.OptimisticResult]);
    }
  });
  exceptions.translateFailure = function translateFailure$(failure) {
    let t7;
    t7 = io_network_exception.translateNetworkFailure(failure);
    return t7 == null ? _base_exceptions.translateFailure(failure) : t7;
  };
  const wrappedException$ = dart.privateName(network_exception_stub, "NetworkException.wrappedException");
  const message$2 = dart.privateName(network_exception_stub, "NetworkException.message");
  const uri$ = dart.privateName(network_exception_stub, "NetworkException.uri");
  network_exception_stub.NetworkException = class NetworkException extends core.Object {
    get wrappedException() {
      return this[wrappedException$];
    }
    set wrappedException(value) {
      this[wrappedException$] = core.Exception._check(value);
    }
    get message() {
      return this[message$2];
    }
    set message(value) {
      this[message$2] = value;
    }
    get uri() {
      return this[uri$];
    }
    set uri(value) {
      this[uri$] = value;
    }
    toString() {
      let t7;
      return "Failed to connect to " + dart.str(this.uri) + ": " + dart.str((t7 = this.message, t7 == null ? this.wrappedException : t7));
    }
  };
  (network_exception_stub.NetworkException.new = function(opts) {
    let wrappedException = opts && 'wrappedException' in opts ? opts.wrappedException : null;
    let message = opts && 'message' in opts ? opts.message : null;
    let uri = opts && 'uri' in opts ? opts.uri : null;
    this[wrappedException$] = wrappedException;
    this[message$2] = message;
    this[uri$] = uri;
    ;
  }).prototype = network_exception_stub.NetworkException.prototype;
  dart.addTypeTests(network_exception_stub.NetworkException);
  network_exception_stub.NetworkException[dart.implements] = () => [_base_exceptions.ClientException];
  dart.setLibraryUri(network_exception_stub.NetworkException, "package:graphql/src/exceptions/network_exception_stub.dart");
  dart.setFieldSignature(network_exception_stub.NetworkException, () => ({
    __proto__: dart.getFields(network_exception_stub.NetworkException.__proto__),
    wrappedException: dart.fieldType(core.Exception),
    message: dart.fieldType(core.String),
    uri: dart.fieldType(core.Uri)
  }));
  dart.defineExtensionMethods(network_exception_stub.NetworkException, ['toString']);
  network_exception_stub.translateNetworkFailure = function translateNetworkFailure(failure) {
    if (exception.ClientException.is(failure)) {
      return new network_exception_stub.NetworkException.new({wrappedException: failure, message: failure.message, uri: failure.uri});
    }
    return null;
  };
  let C25;
  const graphqlErrors$ = dart.privateName(operation_exception, "OperationException.graphqlErrors");
  const clientException$ = dart.privateName(operation_exception, "OperationException.clientException");
  operation_exception.OperationException = class OperationException extends core.Object {
    get graphqlErrors() {
      return this[graphqlErrors$];
    }
    set graphqlErrors(value) {
      this[graphqlErrors$] = value;
    }
    get clientException() {
      return this[clientException$];
    }
    set clientException(value) {
      this[clientException$] = value;
    }
    addError(error) {
      return this.graphqlErrors[$add](error);
    }
    toString() {
      return (() => {
        let t7 = JSArrayOfString().of([]);
        if (this.clientException != null) t7[$add]("ClientException: " + dart.str(this.clientException));
        if (dart.test(this.graphqlErrors[$isNotEmpty])) t7[$add]("GraphQL Errors:");
        for (let t8 of this.graphqlErrors[$map](core.String, dart.fn(e => dart.toString(e), GraphQLErrorToString())))
          t7[$add](t8);
        return t7;
      })()[$join]("\n");
    }
  };
  (operation_exception.OperationException.new = function(opts) {
    let clientException = opts && 'clientException' in opts ? opts.clientException : null;
    let graphqlErrors = opts && 'graphqlErrors' in opts ? opts.graphqlErrors : C25 || CT.C25;
    this[graphqlErrors$] = JSArrayOfGraphQLError().of([]);
    this[clientException$] = clientException;
    this[graphqlErrors$] = graphqlErrors[$toList]();
    ;
  }).prototype = operation_exception.OperationException.prototype;
  dart.addTypeTests(operation_exception.OperationException);
  operation_exception.OperationException[dart.implements] = () => [core.Exception];
  dart.setMethodSignature(operation_exception.OperationException, () => ({
    __proto__: dart.getMethods(operation_exception.OperationException.__proto__),
    addError: dart.fnType(dart.void, [graphql_error.GraphQLError])
  }));
  dart.setLibraryUri(operation_exception.OperationException, "package:graphql/src/exceptions/operation_exception.dart");
  dart.setFieldSignature(operation_exception.OperationException, () => ({
    __proto__: dart.getFields(operation_exception.OperationException.__proto__),
    graphqlErrors: dart.fieldType(core.List$(graphql_error.GraphQLError)),
    clientException: dart.fieldType(_base_exceptions.ClientException)
  }));
  dart.defineExtensionMethods(operation_exception.OperationException, ['toString']);
  operation_exception.coalesceErrors = function coalesceErrors(opts) {
    let t10, t9, t11;
    let graphqlErrors = opts && 'graphqlErrors' in opts ? opts.graphqlErrors : null;
    let clientException = opts && 'clientException' in opts ? opts.clientException : null;
    let exception = opts && 'exception' in opts ? opts.exception : null;
    if (exception != null || clientException != null || graphqlErrors != null && dart.test(graphqlErrors[$isNotEmpty])) {
      return new operation_exception.OperationException.new({clientException: (t9 = clientException, t9 == null ? (t10 = exception, t10 == null ? null : t10.clientException) : t9), graphqlErrors: (() => {
          let t9 = JSArrayOfGraphQLError().of([]);
          if (graphqlErrors != null) for (let t10 of graphqlErrors)
            t9[$add](t10);
          if ((t11 = exception, t11 == null ? null : t11.graphqlErrors) != null) for (let t11$ of exception.graphqlErrors)
            t9[$add](t11$);
          return t9;
        })()});
    }
    return null;
  };
  io_network_exception.translateNetworkFailure = function translateNetworkFailure$(failure) {
    if (io.SocketException.is(failure)) {
      return new network_exception_stub.NetworkException.new({wrappedException: failure, message: failure.message, uri: core._Uri.new({scheme: "http", host: failure.address.host, port: failure.port})});
    }
    return network_exception_stub.translateNetworkFailure(failure);
  };
  let C26;
  let C27;
  let C28;
  let C29;
  let C30;
  let C31;
  query_options.FetchPolicy = class FetchPolicy extends core.Object {
    toString() {
      return this[_name$0];
    }
  };
  (query_options.FetchPolicy.new = function(index, _name) {
    this.index = index;
    this[_name$0] = _name;
    ;
  }).prototype = query_options.FetchPolicy.prototype;
  dart.addTypeTests(query_options.FetchPolicy);
  dart.setLibraryUri(query_options.FetchPolicy, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.FetchPolicy, () => ({
    __proto__: dart.getFields(query_options.FetchPolicy.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$0]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(query_options.FetchPolicy, ['toString']);
  query_options.FetchPolicy.cacheFirst = C26 || CT.C26;
  query_options.FetchPolicy.cacheAndNetwork = C27 || CT.C27;
  query_options.FetchPolicy.cacheOnly = C28 || CT.C28;
  query_options.FetchPolicy.noCache = C29 || CT.C29;
  query_options.FetchPolicy.networkOnly = C30 || CT.C30;
  query_options.FetchPolicy.values = C31 || CT.C31;
  let C32;
  query_options.ErrorPolicy = class ErrorPolicy extends core.Object {
    toString() {
      return this[_name$0];
    }
  };
  (query_options.ErrorPolicy.new = function(index, _name) {
    this.index = index;
    this[_name$0] = _name;
    ;
  }).prototype = query_options.ErrorPolicy.prototype;
  dart.addTypeTests(query_options.ErrorPolicy);
  dart.setLibraryUri(query_options.ErrorPolicy, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.ErrorPolicy, () => ({
    __proto__: dart.getFields(query_options.ErrorPolicy.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$0]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(query_options.ErrorPolicy, ['toString']);
  query_options.ErrorPolicy.none = C9 || CT.C9;
  query_options.ErrorPolicy.ignore = C8 || CT.C8;
  query_options.ErrorPolicy.all = C7 || CT.C7;
  query_options.ErrorPolicy.values = C32 || CT.C32;
  const fetch$ = dart.privateName(query_options, "Policies.fetch");
  const error$ = dart.privateName(query_options, "Policies.error");
  query_options.Policies = class Policies extends core.Object {
    get fetch() {
      return this[fetch$];
    }
    set fetch(value) {
      this[fetch$] = value;
    }
    get error() {
      return this[error$];
    }
    set error(value) {
      this[error$] = value;
    }
    withOverrides(overrides = null) {
      let t12, t12$, t12$0, t12$1;
      return new query_options.Policies.safe((t12$ = (t12 = overrides, t12 == null ? null : t12.fetch), t12$ == null ? this.fetch : t12$), (t12$1 = (t12$0 = overrides, t12$0 == null ? null : t12$0.error), t12$1 == null ? this.error : t12$1));
    }
  };
  (query_options.Policies.new = function(opts) {
    let fetch = opts && 'fetch' in opts ? opts.fetch : null;
    let error = opts && 'error' in opts ? opts.error : null;
    this[fetch$] = fetch;
    this[error$] = error;
    ;
  }).prototype = query_options.Policies.prototype;
  (query_options.Policies.safe = function(fetch, error) {
    this[fetch$] = fetch;
    this[error$] = error;
    if (!(fetch != null)) dart.assertFailed("fetch policy must be specified", "org-dartlang-app:///packages/graphql/src/core/query_options.dart", 65, 16, "fetch != null");
    if (!(error != null)) dart.assertFailed("error policy must be specified", "org-dartlang-app:///packages/graphql/src/core/query_options.dart", 66, 16, "error != null");
    ;
  }).prototype = query_options.Policies.prototype;
  dart.addTypeTests(query_options.Policies);
  dart.setMethodSignature(query_options.Policies, () => ({
    __proto__: dart.getMethods(query_options.Policies.__proto__),
    withOverrides: dart.fnType(query_options.Policies, [], [query_options.Policies])
  }));
  dart.setLibraryUri(query_options.Policies, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.Policies, () => ({
    __proto__: dart.getFields(query_options.Policies.__proto__),
    fetch: dart.fieldType(query_options.FetchPolicy),
    error: dart.fieldType(query_options.ErrorPolicy)
  }));
  const optimisticResult$ = dart.privateName(query_options, "BaseOptions.optimisticResult");
  const policies$ = dart.privateName(query_options, "BaseOptions.policies");
  const context$ = dart.privateName(query_options, "BaseOptions.context");
  query_options.BaseOptions = class BaseOptions extends raw_operation_data.RawOperationData {
    get optimisticResult() {
      return this[optimisticResult$];
    }
    set optimisticResult(value) {
      this[optimisticResult$] = value;
    }
    get policies() {
      return this[policies$];
    }
    set policies(value) {
      this[policies$] = value;
    }
    get context() {
      return this[context$];
    }
    set context(value) {
      this[context$] = value;
    }
    get fetchPolicy() {
      return this.policies.fetch;
    }
    get errorPolicy() {
      return this.policies.error;
    }
  };
  (query_options.BaseOptions.new = function(opts) {
    let document = opts && 'document' in opts ? opts.document : null;
    let documentNode = opts && 'documentNode' in opts ? opts.documentNode : null;
    let variables = opts && 'variables' in opts ? opts.variables : null;
    let policies = opts && 'policies' in opts ? opts.policies : null;
    let context = opts && 'context' in opts ? opts.context : null;
    let optimisticResult = opts && 'optimisticResult' in opts ? opts.optimisticResult : null;
    this[policies$] = policies;
    this[context$] = context;
    this[optimisticResult$] = optimisticResult;
    query_options.BaseOptions.__proto__.new.call(this, {document: document, documentNode: documentNode, variables: variables});
    ;
  }).prototype = query_options.BaseOptions.prototype;
  dart.addTypeTests(query_options.BaseOptions);
  dart.setGetterSignature(query_options.BaseOptions, () => ({
    __proto__: dart.getGetters(query_options.BaseOptions.__proto__),
    fetchPolicy: query_options.FetchPolicy,
    errorPolicy: query_options.ErrorPolicy
  }));
  dart.setLibraryUri(query_options.BaseOptions, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.BaseOptions, () => ({
    __proto__: dart.getFields(query_options.BaseOptions.__proto__),
    optimisticResult: dart.fieldType(core.Object),
    policies: dart.fieldType(query_options.Policies),
    context: dart.fieldType(core.Map$(core.String, dart.dynamic))
  }));
  const pollInterval$ = dart.privateName(query_options, "QueryOptions.pollInterval");
  query_options.QueryOptions = class QueryOptions extends query_options.BaseOptions {
    get pollInterval() {
      return this[pollInterval$];
    }
    set pollInterval(value) {
      this[pollInterval$] = value;
    }
  };
  (query_options.QueryOptions.new = function(opts) {
    let document = opts && 'document' in opts ? opts.document : null;
    let documentNode = opts && 'documentNode' in opts ? opts.documentNode : null;
    let variables = opts && 'variables' in opts ? opts.variables : null;
    let fetchPolicy = opts && 'fetchPolicy' in opts ? opts.fetchPolicy : null;
    let errorPolicy = opts && 'errorPolicy' in opts ? opts.errorPolicy : null;
    let optimisticResult = opts && 'optimisticResult' in opts ? opts.optimisticResult : null;
    let pollInterval = opts && 'pollInterval' in opts ? opts.pollInterval : null;
    let context = opts && 'context' in opts ? opts.context : null;
    this[pollInterval$] = pollInterval;
    query_options.QueryOptions.__proto__.new.call(this, {policies: new query_options.Policies.new({fetch: fetchPolicy, error: errorPolicy}), document: document, documentNode: documentNode, variables: variables, context: context, optimisticResult: optimisticResult});
    ;
  }).prototype = query_options.QueryOptions.prototype;
  dart.addTypeTests(query_options.QueryOptions);
  dart.setLibraryUri(query_options.QueryOptions, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.QueryOptions, () => ({
    __proto__: dart.getFields(query_options.QueryOptions.__proto__),
    pollInterval: dart.fieldType(core.int)
  }));
  const onCompleted$ = dart.privateName(query_options, "MutationOptions.onCompleted");
  const update$ = dart.privateName(query_options, "MutationOptions.update");
  const onError$ = dart.privateName(query_options, "MutationOptions.onError");
  query_options.MutationOptions = class MutationOptions extends query_options.BaseOptions {
    get onCompleted() {
      return this[onCompleted$];
    }
    set onCompleted(value) {
      this[onCompleted$] = value;
    }
    get update() {
      return this[update$];
    }
    set update(value) {
      this[update$] = value;
    }
    get onError() {
      return this[onError$];
    }
    set onError(value) {
      this[onError$] = value;
    }
  };
  (query_options.MutationOptions.new = function(opts) {
    let document = opts && 'document' in opts ? opts.document : null;
    let documentNode = opts && 'documentNode' in opts ? opts.documentNode : null;
    let variables = opts && 'variables' in opts ? opts.variables : null;
    let fetchPolicy = opts && 'fetchPolicy' in opts ? opts.fetchPolicy : null;
    let errorPolicy = opts && 'errorPolicy' in opts ? opts.errorPolicy : null;
    let context = opts && 'context' in opts ? opts.context : null;
    let onCompleted = opts && 'onCompleted' in opts ? opts.onCompleted : null;
    let update = opts && 'update' in opts ? opts.update : null;
    let onError = opts && 'onError' in opts ? opts.onError : null;
    this[onCompleted$] = onCompleted;
    this[update$] = update;
    this[onError$] = onError;
    query_options.MutationOptions.__proto__.new.call(this, {policies: new query_options.Policies.new({fetch: fetchPolicy, error: errorPolicy}), document: document, documentNode: documentNode, variables: variables, context: context});
    ;
  }).prototype = query_options.MutationOptions.prototype;
  dart.addTypeTests(query_options.MutationOptions);
  dart.setLibraryUri(query_options.MutationOptions, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.MutationOptions, () => ({
    __proto__: dart.getFields(query_options.MutationOptions.__proto__),
    onCompleted: dart.fieldType(dart.fnType(dart.void, [dart.dynamic])),
    update: dart.fieldType(dart.fnType(dart.void, [cache.Cache, query_result.QueryResult])),
    onError: dart.fieldType(dart.fnType(dart.void, [operation_exception.OperationException]))
  }));
  let C33;
  const _patchId = dart.privateName(query_options, "_patchId");
  const _optimisticUpdate = dart.privateName(query_options, "_optimisticUpdate");
  const options$0 = dart.privateName(query_options, "MutationCallbacks.options");
  const cache$0 = dart.privateName(query_options, "MutationCallbacks.cache");
  const queryId$ = dart.privateName(query_options, "MutationCallbacks.queryId");
  query_options.MutationCallbacks = class MutationCallbacks extends core.Object {
    get options() {
      return this[options$0];
    }
    set options(value) {
      super.options = value;
    }
    get cache() {
      return this[cache$0];
    }
    set cache(value) {
      super.cache = value;
    }
    get queryId() {
      return this[queryId$];
    }
    set queryId(value) {
      super.queryId = value;
    }
    get callbacks() {
      return JSArrayOfQueryResultTovoid().of([this.onCompleted, this.update, this.onError])[$where](C33 || CT.C33);
    }
    get onCompleted() {
      if (this.options.onCompleted != null) {
        return dart.fn(result => {
          if (!dart.test(result.loading) && !dart.test(result.optimistic)) {
            return this.options.onCompleted(result.data);
          }
        }, QueryResultTovoid());
      }
      return null;
    }
    get onError() {
      if (this.options.onError != null) {
        return dart.fn(result => {
          if (!dart.test(result.loading) && dart.test(result.hasException) && !dart.equals(this.options.errorPolicy, query_options.ErrorPolicy.ignore)) {
            return this.options.onError(result.exception);
          }
        }, QueryResultTovoid());
      }
      return null;
    }
    get [_patchId]() {
      return dart.str(this.queryId) + ".update";
    }
    [_optimisticUpdate](result) {
      let patchId = this[_patchId];
      if (!optimistic.OptimisticCache.is(this.cache)) dart.assertFailed("can't optimisticly update non-optimistic cache", "org-dartlang-app:///packages/graphql/src/core/query_options.dart", 216, 12, "cache is OptimisticCache");
      optimistic.OptimisticCache.as(this.cache).addOptimisiticPatch(patchId, dart.fn(cache => {
        this.options.update(cache, result);
        return cache;
      }, CacheToCache()));
    }
    get update() {
      if (this.options.update != null) {
        let widgetUpdate = this.options.update;
        let optimisticUpdate = dart.bind(this, _optimisticUpdate);
        const updateOnData = result => {
          if (dart.test(result.optimistic)) {
            return optimisticUpdate(result);
          } else {
            return widgetUpdate(this.cache, result);
          }
        };
        dart.fn(updateOnData, QueryResultTovoid());
        return updateOnData;
      }
      return null;
    }
  };
  (query_options.MutationCallbacks.new = function(opts) {
    let options = opts && 'options' in opts ? opts.options : null;
    let cache = opts && 'cache' in opts ? opts.cache : null;
    let queryId = opts && 'queryId' in opts ? opts.queryId : null;
    this[options$0] = options;
    this[cache$0] = cache;
    this[queryId$] = queryId;
    if (!(cache != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql/src/core/query_options.dart", 172, 16, "cache != null");
    if (!(options != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql/src/core/query_options.dart", 173, 16, "options != null");
    if (!(queryId != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql/src/core/query_options.dart", 174, 16, "queryId != null");
    ;
  }).prototype = query_options.MutationCallbacks.prototype;
  dart.addTypeTests(query_options.MutationCallbacks);
  dart.setMethodSignature(query_options.MutationCallbacks, () => ({
    __proto__: dart.getMethods(query_options.MutationCallbacks.__proto__),
    [_optimisticUpdate]: dart.fnType(dart.void, [query_result.QueryResult])
  }));
  dart.setGetterSignature(query_options.MutationCallbacks, () => ({
    __proto__: dart.getGetters(query_options.MutationCallbacks.__proto__),
    callbacks: core.Iterable$(dart.fnType(dart.void, [query_result.QueryResult])),
    onCompleted: dart.fnType(dart.void, [query_result.QueryResult]),
    onError: dart.fnType(dart.void, [query_result.QueryResult]),
    [_patchId]: core.String,
    update: dart.fnType(dart.void, [query_result.QueryResult])
  }));
  dart.setLibraryUri(query_options.MutationCallbacks, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.MutationCallbacks, () => ({
    __proto__: dart.getFields(query_options.MutationCallbacks.__proto__),
    options: dart.finalFieldType(query_options.MutationOptions),
    cache: dart.finalFieldType(cache.Cache),
    queryId: dart.finalFieldType(core.String)
  }));
  const _areDifferentOptions = dart.privateName(query_options, "_areDifferentOptions");
  const fetchResults$ = dart.privateName(query_options, "WatchQueryOptions.fetchResults");
  const eagerlyFetchResults$ = dart.privateName(query_options, "WatchQueryOptions.eagerlyFetchResults");
  query_options.WatchQueryOptions = class WatchQueryOptions extends query_options.QueryOptions {
    get fetchResults() {
      return this[fetchResults$];
    }
    set fetchResults(value) {
      this[fetchResults$] = value;
    }
    get eagerlyFetchResults() {
      return this[eagerlyFetchResults$];
    }
    set eagerlyFetchResults(value) {
      this[eagerlyFetchResults$] = value;
    }
    areEqualTo(otherOptions) {
      return !dart.test(this[_areDifferentOptions](this, otherOptions));
    }
    [_areDifferentOptions](a, b) {
      if (!dart.equals(a.documentNode, b.documentNode)) {
        return true;
      }
      if (!dart.equals(a.policies, b.policies)) {
        return true;
      }
      if (a.pollInterval != b.pollInterval) {
        return true;
      }
      if (!dart.equals(a.fetchResults, b.fetchResults)) {
        return true;
      }
      return helpers.areDifferentVariables(a.variables, b.variables);
    }
  };
  (query_options.WatchQueryOptions.new = function(opts) {
    let document = opts && 'document' in opts ? opts.document : null;
    let documentNode = opts && 'documentNode' in opts ? opts.documentNode : null;
    let variables = opts && 'variables' in opts ? opts.variables : null;
    let fetchPolicy = opts && 'fetchPolicy' in opts ? opts.fetchPolicy : null;
    let errorPolicy = opts && 'errorPolicy' in opts ? opts.errorPolicy : null;
    let optimisticResult = opts && 'optimisticResult' in opts ? opts.optimisticResult : null;
    let pollInterval = opts && 'pollInterval' in opts ? opts.pollInterval : null;
    let fetchResults = opts && 'fetchResults' in opts ? opts.fetchResults : false;
    let eagerlyFetchResults = opts && 'eagerlyFetchResults' in opts ? opts.eagerlyFetchResults : null;
    let context = opts && 'context' in opts ? opts.context : null;
    this[fetchResults$] = fetchResults;
    this[eagerlyFetchResults$] = eagerlyFetchResults;
    query_options.WatchQueryOptions.__proto__.new.call(this, {document: document, documentNode: documentNode, variables: variables, fetchPolicy: fetchPolicy, errorPolicy: errorPolicy, pollInterval: pollInterval, context: context, optimisticResult: optimisticResult});
    this.eagerlyFetchResults == null ? this.eagerlyFetchResults = this.fetchResults : null;
  }).prototype = query_options.WatchQueryOptions.prototype;
  dart.addTypeTests(query_options.WatchQueryOptions);
  dart.setMethodSignature(query_options.WatchQueryOptions, () => ({
    __proto__: dart.getMethods(query_options.WatchQueryOptions.__proto__),
    areEqualTo: dart.fnType(core.bool, [query_options.WatchQueryOptions]),
    [_areDifferentOptions]: dart.fnType(core.bool, [query_options.WatchQueryOptions, query_options.WatchQueryOptions])
  }));
  dart.setLibraryUri(query_options.WatchQueryOptions, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.WatchQueryOptions, () => ({
    __proto__: dart.getFields(query_options.WatchQueryOptions.__proto__),
    fetchResults: dart.fieldType(core.bool),
    eagerlyFetchResults: dart.fieldType(core.bool)
  }));
  let C34;
  const documentNode$ = dart.privateName(query_options, "FetchMoreOptions.documentNode");
  const variables$ = dart.privateName(query_options, "FetchMoreOptions.variables");
  const updateQuery$ = dart.privateName(query_options, "FetchMoreOptions.updateQuery");
  query_options.FetchMoreOptions = class FetchMoreOptions extends core.Object {
    get documentNode() {
      return this[documentNode$];
    }
    set documentNode(value) {
      this[documentNode$] = value;
    }
    get variables() {
      return this[variables$];
    }
    set variables(value) {
      super.variables = value;
    }
    get updateQuery() {
      return this[updateQuery$];
    }
    set updateQuery(value) {
      this[updateQuery$] = value;
    }
    get document() {
      return printer.printNode(this.documentNode);
    }
    set document(value) {
      this.documentNode = parser.parseString(core.String._check(value));
    }
  };
  (query_options.FetchMoreOptions.new = function(opts) {
    let t12;
    let document = opts && 'document' in opts ? opts.document : null;
    let documentNode = opts && 'documentNode' in opts ? opts.documentNode : null;
    let variables = opts && 'variables' in opts ? opts.variables : C34 || CT.C34;
    let updateQuery = opts && 'updateQuery' in opts ? opts.updateQuery : null;
    this[variables$] = variables;
    this[updateQuery$] = updateQuery;
    if (!dart.test(query_options._mutuallyExclusive(document, documentNode))) dart.assertFailed("\"document\" or \"documentNode\" options are mutually exclusive.", "org-dartlang-app:///packages/graphql/src/core/query_options.dart", 329, 11, "_mutuallyExclusive(document, documentNode)");
    if (!(updateQuery != null)) dart.assertFailed(null, "org-dartlang-app:///packages/graphql/src/core/query_options.dart", 332, 16, "updateQuery != null");
    this[documentNode$] = dart.dtest((t12 = documentNode, t12 == null ? document != null : t12)) ? parser.parseString(document) : null;
    ;
  }).prototype = query_options.FetchMoreOptions.prototype;
  dart.addTypeTests(query_options.FetchMoreOptions);
  dart.setGetterSignature(query_options.FetchMoreOptions, () => ({
    __proto__: dart.getGetters(query_options.FetchMoreOptions.__proto__),
    document: core.String
  }));
  dart.setSetterSignature(query_options.FetchMoreOptions, () => ({
    __proto__: dart.getSetters(query_options.FetchMoreOptions.__proto__),
    document: dart.dynamic
  }));
  dart.setLibraryUri(query_options.FetchMoreOptions, "package:graphql/src/core/query_options.dart");
  dart.setFieldSignature(query_options.FetchMoreOptions, () => ({
    __proto__: dart.getFields(query_options.FetchMoreOptions.__proto__),
    documentNode: dart.fieldType(ast.DocumentNode),
    variables: dart.finalFieldType(core.Map$(core.String, dart.dynamic)),
    updateQuery: dart.fieldType(dart.fnType(dart.dynamic, [dart.dynamic, dart.dynamic]))
  }));
  query_options.gql = function gql(query) {
    return parser.parseString(query);
  };
  query_options.shouldRespondEagerlyFromCache = function shouldRespondEagerlyFromCache(fetchPolicy) {
    return dart.equals(fetchPolicy, query_options.FetchPolicy.cacheFirst) || dart.equals(fetchPolicy, query_options.FetchPolicy.cacheAndNetwork) || dart.equals(fetchPolicy, query_options.FetchPolicy.cacheOnly);
  };
  query_options.shouldStopAtCache = function shouldStopAtCache(fetchPolicy) {
    return dart.equals(fetchPolicy, query_options.FetchPolicy.cacheFirst) || dart.equals(fetchPolicy, query_options.FetchPolicy.cacheOnly);
  };
  query_options._mutuallyExclusive = function _mutuallyExclusive(a, b, opts) {
    let required = opts && 'required' in opts ? opts.required : false;
    return !dart.test(required) && a == null && b == null || a != null && b == null || a == null && b != null;
  };
  const watchQuery$ = dart.privateName(graphql_client, "DefaultPolicies.watchQuery");
  const query$ = dart.privateName(graphql_client, "DefaultPolicies.query");
  const mutate$ = dart.privateName(graphql_client, "DefaultPolicies.mutate");
  graphql_client.DefaultPolicies = class DefaultPolicies extends core.Object {
    get watchQuery() {
      return this[watchQuery$];
    }
    set watchQuery(value) {
      this[watchQuery$] = value;
    }
    get query() {
      return this[query$];
    }
    set query(value) {
      this[query$] = value;
    }
    get mutate() {
      return this[mutate$];
    }
    set mutate(value) {
      this[mutate$] = value;
    }
  };
  (graphql_client.DefaultPolicies.new = function(opts) {
    let watchQuery = opts && 'watchQuery' in opts ? opts.watchQuery : null;
    let query = opts && 'query' in opts ? opts.query : null;
    let mutate = opts && 'mutate' in opts ? opts.mutate : null;
    this[watchQuery$] = graphql_client.DefaultPolicies._watchQueryDefaults.withOverrides(watchQuery);
    this[query$] = graphql_client.DefaultPolicies._queryDefaults.withOverrides(query);
    this[mutate$] = graphql_client.DefaultPolicies._mutateDefaults.withOverrides(mutate);
    ;
  }).prototype = graphql_client.DefaultPolicies.prototype;
  dart.addTypeTests(graphql_client.DefaultPolicies);
  dart.setLibraryUri(graphql_client.DefaultPolicies, "package:graphql/src/graphql_client.dart");
  dart.setFieldSignature(graphql_client.DefaultPolicies, () => ({
    __proto__: dart.getFields(graphql_client.DefaultPolicies.__proto__),
    watchQuery: dart.fieldType(query_options.Policies),
    query: dart.fieldType(query_options.Policies),
    mutate: dart.fieldType(query_options.Policies)
  }));
  dart.defineLazy(graphql_client.DefaultPolicies, {
    /*graphql_client.DefaultPolicies._watchQueryDefaults*/get _watchQueryDefaults() {
      return new query_options.Policies.safe(query_options.FetchPolicy.cacheAndNetwork, query_options.ErrorPolicy.none);
    },
    /*graphql_client.DefaultPolicies._queryDefaults*/get _queryDefaults() {
      return new query_options.Policies.safe(query_options.FetchPolicy.cacheFirst, query_options.ErrorPolicy.none);
    },
    /*graphql_client.DefaultPolicies._mutateDefaults*/get _mutateDefaults() {
      return new query_options.Policies.safe(query_options.FetchPolicy.networkOnly, query_options.ErrorPolicy.none);
    }
  });
  const defaultPolicies$ = dart.privateName(graphql_client, "GraphQLClient.defaultPolicies");
  const link$0 = dart.privateName(graphql_client, "GraphQLClient.link");
  const cache$1 = dart.privateName(graphql_client, "GraphQLClient.cache");
  const queryManager = dart.privateName(graphql_client, "GraphQLClient.queryManager");
  graphql_client.GraphQLClient = class GraphQLClient extends core.Object {
    get defaultPolicies() {
      return this[defaultPolicies$];
    }
    set defaultPolicies(value) {
      this[defaultPolicies$] = value;
    }
    get link() {
      return this[link$0];
    }
    set link(value) {
      super.link = value;
    }
    get cache() {
      return this[cache$1];
    }
    set cache(value) {
      super.cache = value;
    }
    get queryManager() {
      return this[queryManager];
    }
    set queryManager(value) {
      this[queryManager] = value;
    }
    watchQuery(options) {
      options.policies = this.defaultPolicies.watchQuery.withOverrides(options.policies);
      return this.queryManager.watchQuery(options);
    }
    query(options) {
      options.policies = this.defaultPolicies.query.withOverrides(options.policies);
      return this.queryManager.query(options);
    }
    mutate(options) {
      options.policies = this.defaultPolicies.mutate.withOverrides(options.policies);
      return this.queryManager.mutate(options);
    }
    subscribe(operation) {
      return link.execute({link: this.link, operation: operation});
    }
  };
  (graphql_client.GraphQLClient.new = function(opts) {
    let link = opts && 'link' in opts ? opts.link : null;
    let cache = opts && 'cache' in opts ? opts.cache : null;
    let defaultPolicies = opts && 'defaultPolicies' in opts ? opts.defaultPolicies : null;
    this[queryManager] = null;
    this[link$0] = link;
    this[cache$1] = cache;
    this[defaultPolicies$] = defaultPolicies;
    this.defaultPolicies == null ? this.defaultPolicies = new graphql_client.DefaultPolicies.new() : null;
    this.queryManager = new query_manager.QueryManager.new({link: this.link, cache: this.cache});
  }).prototype = graphql_client.GraphQLClient.prototype;
  dart.addTypeTests(graphql_client.GraphQLClient);
  dart.setMethodSignature(graphql_client.GraphQLClient, () => ({
    __proto__: dart.getMethods(graphql_client.GraphQLClient.__proto__),
    watchQuery: dart.fnType(observable_query.ObservableQuery, [query_options.WatchQueryOptions]),
    query: dart.fnType(async.Future$(query_result.QueryResult), [query_options.QueryOptions]),
    mutate: dart.fnType(async.Future$(query_result.QueryResult), [query_options.MutationOptions]),
    subscribe: dart.fnType(async.Stream$(fetch_result.FetchResult), [operation$.Operation])
  }));
  dart.setLibraryUri(graphql_client.GraphQLClient, "package:graphql/src/graphql_client.dart");
  dart.setFieldSignature(graphql_client.GraphQLClient, () => ({
    __proto__: dart.getFields(graphql_client.GraphQLClient.__proto__),
    defaultPolicies: dart.fieldType(graphql_client.DefaultPolicies),
    link: dart.finalFieldType(link.Link),
    cache: dart.finalFieldType(cache.Cache),
    queryManager: dart.fieldType(query_manager.QueryManager)
  }));
  link_http.HttpLink = class HttpLink extends link.Link {};
  (link_http.HttpLink.new = function(opts) {
    let uri = opts && 'uri' in opts ? opts.uri : null;
    let includeExtensions = opts && 'includeExtensions' in opts ? opts.includeExtensions : null;
    let httpClient = opts && 'httpClient' in opts ? opts.httpClient : null;
    let headers = opts && 'headers' in opts ? opts.headers : null;
    let credentials = opts && 'credentials' in opts ? opts.credentials : null;
    let fetchOptions = opts && 'fetchOptions' in opts ? opts.fetchOptions : null;
    link_http.HttpLink.__proto__.new.call(this, {request: dart.fn((operation, forward = null) => {
        let t12;
        let parsedUri = core.Uri.parse(uri);
        if (dart.test(operation.isSubscription)) {
          if (forward == null) {
            dart.throw(core.Exception.new("This link does not support subscriptions."));
          }
          return forward(operation);
        }
        let fetcher = (t12 = httpClient, t12 == null ? client.Client.new() : t12);
        let linkConfig = new http_config.HttpConfig.new({http: new http_config.HttpQueryOptions.new({includeExtensions: includeExtensions}), options: fetchOptions, credentials: credentials, headers: headers});
        let context = operation.getContext();
        let contextConfig = null;
        if (context != null) {
          contextConfig = new http_config.HttpConfig.new({http: new http_config.HttpQueryOptions.new({includeExtensions: core.bool.as(context[$_get]("includeExtensions"))}), options: MapOfString$dynamic().as(context[$_get]("fetchOptions")), credentials: MapOfString$dynamic().as(context[$_get]("credentials")), headers: MapOfString$String().as(context[$_get]("headers"))});
        }
        let httpHeadersAndBody = link_http._selectHttpOptionsAndBody(operation, fallback_http_config.fallbackHttpConfig, linkConfig, contextConfig);
        let httpHeaders = httpHeadersAndBody.headers;
        let controller = null;
        function onListen() {
          return async.async(dart.void, function* onListen() {
            let response = null;
            try {
              let request = (yield link_http._prepareRequest(parsedUri, httpHeadersAndBody.body, httpHeaders));
              response = (yield fetcher.send(request));
              operation.setContext(new (IdentityMapOfString$StreamedResponse()).from(["response", response]));
              let parsedResponse = (yield link_http._parseResponse(response));
              controller.add(parsedResponse);
            } catch (e) {
              let failure = dart.getThrown(e);
              let translated = exceptions.translateFailure(failure);
              if (network_exception_stub.NetworkException.is(translated)) {
                translated.uri = parsedUri;
              }
              controller.addError(translated);
            }
            yield controller.close();
          });
        }
        dart.fn(onListen, VoidToFutureOfvoid());
        controller = StreamControllerOfFetchResult().new({onListen: onListen});
        return controller.stream;
      }, OperationAndFnToStreamOfFetchResult())});
    ;
  }).prototype = link_http.HttpLink.prototype;
  dart.addTypeTests(link_http.HttpLink);
  dart.setLibraryUri(link_http.HttpLink, "package:graphql/src/link/http/link_http.dart");
  let C35;
  const Utf8Codec__allowMalformed = dart.privateName(convert, "Utf8Codec._allowMalformed");
  let C36;
  link_http._getFileMap = function _getFileMap(body, opts) {
    let currentMap = opts && 'currentMap' in opts ? opts.currentMap : null;
    let currentPath = opts && 'currentPath' in opts ? opts.currentPath : C35 || CT.C35;
    return async.async(MapOfString$MultipartFile(), function* _getFileMap() {
      let t13, t13$, t13$0;
      currentMap == null ? currentMap = new (IdentityMapOfString$MultipartFile()).new() : null;
      if (MapOfString$dynamic().is(body)) {
        let entries = body[$entries];
        for (let element of entries) {
          currentMap[$addAll](yield link_http._getFileMap(element.value, {currentMap: currentMap, currentPath: (t13 = ListOfString().from(currentPath), t13[$add](element.key), t13)}));
        }
        return currentMap;
      }
      if (core.List.is(body)) {
        for (let i = 0; i < dart.notNull(body[$length]); i = i + 1) {
          currentMap[$addAll](yield link_http._getFileMap(body[$_get](i), {currentMap: currentMap, currentPath: (t13$ = ListOfString().from(currentPath), t13$[$add](i[$toString]()), t13$)}));
        }
        return currentMap;
      }
      if (multipart_file.MultipartFile.is(body)) {
        t13$0 = currentMap;
        t13$0[$addAll](new (IdentityMapOfString$MultipartFile()).from([currentPath[$join]("."), body]));
        return t13$0;
      }
      if (dart.test(link_http_helper_deprecated_stub.isIoFile(body))) {
        return link_http_helper_deprecated_stub.deprecatedHelper(body, currentMap, currentPath);
      }
      return currentMap;
    });
  };
  link_http._prepareRequest = function _prepareRequest(uri, body, httpHeaders) {
    return async.async(base_request.BaseRequest, function* _prepareRequest() {
      let fileMap = (yield link_http._getFileMap(body));
      if (dart.test(fileMap[$isEmpty])) {
        let r = new request.Request.new("post", uri);
        r.headers[$addAll](httpHeaders);
        r.body = convert.json.encode(body);
        return r;
      }
      let r = new multipart_request.MultipartRequest.new("post", uri);
      r.headers[$addAll](httpHeaders);
      r.fields[$_set]("operations", convert.json.encode(body, {toEncodable: dart.fn(object => {
          if (multipart_file.MultipartFile.is(object)) {
            return null;
          }
          if (dart.test(link_http_helper_deprecated_stub.isIoFile(object))) {
            return null;
          }
          return dart.dsend(object, 'toJson', []);
        }, dynamicTodynamic())}));
      let fileMapping = new (IdentityMapOfString$ListOfString()).new();
      let fileList = JSArrayOfMultipartFile().of([]);
      let fileMapEntries = fileMap[$entries][$toList]({growable: false});
      for (let i = 0; i < dart.notNull(fileMapEntries[$length]); i = i + 1) {
        let entry = fileMapEntries[$_get](i);
        let indexString = i[$toString]();
        fileMapping[$addAll](new (IdentityMapOfString$ListOfString()).from([indexString, JSArrayOfString().of([entry.key])]));
        let f = entry.value;
        fileList[$add](new multipart_file.MultipartFile.new(indexString, f.finalize(), f.length, {contentType: f.contentType, filename: f.filename}));
      }
      r.fields[$_set]("map", convert.json.encode(fileMapping));
      r.files[$addAll](fileList);
      return r;
    });
  };
  link_http._selectHttpOptionsAndBody = function _selectHttpOptionsAndBody(operation, fallbackConfig, linkConfig = null, contextConfig = null) {
    let options = new (IdentityMapOfString$dynamic()).from(["headers", new (IdentityMapOfString$String()).new(), "credentials", new (IdentityMapOfString$dynamic()).new()]);
    let http = new http_config.HttpQueryOptions.new();
    http.addAll(fallbackConfig.http);
    if (linkConfig.http != null) {
      http.addAll(linkConfig.http);
    }
    if (contextConfig.http != null) {
      http.addAll(contextConfig.http);
    }
    options[$addAll](fallbackConfig.options);
    if (linkConfig.options != null) {
      options[$addAll](linkConfig.options);
    }
    if (contextConfig.options != null) {
      options[$addAll](contextConfig.options);
    }
    dart.dsend(options[$_get]("headers"), 'addAll', [fallbackConfig.headers]);
    if (linkConfig.headers != null) {
      dart.dsend(options[$_get]("headers"), 'addAll', [linkConfig.headers]);
    }
    if (contextConfig.headers != null) {
      dart.dsend(options[$_get]("headers"), 'addAll', [contextConfig.headers]);
    }
    dart.dsend(options[$_get]("credentials"), 'addAll', [fallbackConfig.credentials]);
    if (linkConfig.credentials != null) {
      dart.dsend(options[$_get]("credentials"), 'addAll', [linkConfig.credentials]);
    }
    if (contextConfig.credentials != null) {
      dart.dsend(options[$_get]("credentials"), 'addAll', [contextConfig.credentials]);
    }
    let body = new (IdentityMapOfString$dynamic()).from(["operationName", operation.operationName, "variables", operation.variables]);
    if (dart.test(http.includeExtensions)) {
      body[$_set]("extensions", operation.extensions);
    }
    if (dart.test(http.includeQuery)) {
      body[$_set]("query", printer.printNode(operation.documentNode));
    }
    return new http_config.HttpHeadersAndBody.new({headers: MapOfString$String().as(options[$_get]("headers")), body: body});
  };
  link_http._parseResponse = function _parseResponse(response) {
    return async.async(fetch_result.FetchResult, function* _parseResponse() {
      let statusCode = response.statusCode;
      let encoding = link_http._determineEncodingFromResponse(response);
      let responseByte = (yield response.stream.toBytes());
      let decodedBody = encoding.decode(responseByte);
      let jsonResponse = MapOfString$dynamic().as(convert.json.decode(decodedBody));
      let fetchResult = new fetch_result.FetchResult.new();
      if (jsonResponse[$_get]("errors") != null) {
        fetchResult.errors = core.List.as(jsonResponse[$_get]("errors"))[$where](C33 || CT.C33)[$toList]();
      }
      if (jsonResponse[$_get]("data") != null) {
        fetchResult.data = jsonResponse[$_get]("data");
      }
      if (fetchResult.data == null && fetchResult.errors == null) {
        if (dart.notNull(statusCode) < 200 || dart.notNull(statusCode) >= 400) {
          dart.throw(new exception.ClientException.new("Network Error: " + dart.str(statusCode) + " " + dart.str(decodedBody)));
        }
        dart.throw(new exception.ClientException.new("Invalid response body: " + dart.str(decodedBody)));
      }
      return fetchResult;
    });
  };
  link_http._determineEncodingFromResponse = function _determineEncodingFromResponse(response, fallback = C36 || CT.C36) {
    let contentType = response.headers[$_get]("content-type");
    if (contentType == null) {
      return fallback;
    }
    let mediaType = media_type.MediaType.parse(contentType);
    let charset = mediaType.parameters[$_get]("charset");
    if (charset == null) {
      return fallback;
    }
    let encoding = convert.Encoding.getByName(charset);
    return encoding == null ? fallback : encoding;
  };
  const includeQuery$ = dart.privateName(http_config, "HttpQueryOptions.includeQuery");
  const includeExtensions$ = dart.privateName(http_config, "HttpQueryOptions.includeExtensions");
  http_config.HttpQueryOptions = class HttpQueryOptions extends core.Object {
    get includeQuery() {
      return this[includeQuery$];
    }
    set includeQuery(value) {
      this[includeQuery$] = value;
    }
    get includeExtensions() {
      return this[includeExtensions$];
    }
    set includeExtensions(value) {
      this[includeExtensions$] = value;
    }
    addAll(options) {
      if (options.includeQuery != null) {
        this.includeQuery = options.includeQuery;
      }
      if (options.includeExtensions != null) {
        this.includeExtensions = options.includeExtensions;
      }
    }
  };
  (http_config.HttpQueryOptions.new = function(opts) {
    let includeQuery = opts && 'includeQuery' in opts ? opts.includeQuery : null;
    let includeExtensions = opts && 'includeExtensions' in opts ? opts.includeExtensions : null;
    this[includeQuery$] = includeQuery;
    this[includeExtensions$] = includeExtensions;
    ;
  }).prototype = http_config.HttpQueryOptions.prototype;
  dart.addTypeTests(http_config.HttpQueryOptions);
  dart.setMethodSignature(http_config.HttpQueryOptions, () => ({
    __proto__: dart.getMethods(http_config.HttpQueryOptions.__proto__),
    addAll: dart.fnType(dart.void, [http_config.HttpQueryOptions])
  }));
  dart.setLibraryUri(http_config.HttpQueryOptions, "package:graphql/src/link/http/http_config.dart");
  dart.setFieldSignature(http_config.HttpQueryOptions, () => ({
    __proto__: dart.getFields(http_config.HttpQueryOptions.__proto__),
    includeQuery: dart.fieldType(core.bool),
    includeExtensions: dart.fieldType(core.bool)
  }));
  const http$ = dart.privateName(http_config, "HttpConfig.http");
  const options$1 = dart.privateName(http_config, "HttpConfig.options");
  const credentials$ = dart.privateName(http_config, "HttpConfig.credentials");
  const headers$ = dart.privateName(http_config, "HttpConfig.headers");
  http_config.HttpConfig = class HttpConfig extends core.Object {
    get http() {
      return this[http$];
    }
    set http(value) {
      this[http$] = value;
    }
    get options() {
      return this[options$1];
    }
    set options(value) {
      this[options$1] = value;
    }
    get credentials() {
      return this[credentials$];
    }
    set credentials(value) {
      this[credentials$] = value;
    }
    get headers() {
      return this[headers$];
    }
    set headers(value) {
      this[headers$] = value;
    }
  };
  (http_config.HttpConfig.new = function(opts) {
    let http = opts && 'http' in opts ? opts.http : null;
    let options = opts && 'options' in opts ? opts.options : null;
    let credentials = opts && 'credentials' in opts ? opts.credentials : null;
    let headers = opts && 'headers' in opts ? opts.headers : null;
    this[http$] = http;
    this[options$1] = options;
    this[credentials$] = credentials;
    this[headers$] = headers;
    ;
  }).prototype = http_config.HttpConfig.prototype;
  dart.addTypeTests(http_config.HttpConfig);
  dart.setLibraryUri(http_config.HttpConfig, "package:graphql/src/link/http/http_config.dart");
  dart.setFieldSignature(http_config.HttpConfig, () => ({
    __proto__: dart.getFields(http_config.HttpConfig.__proto__),
    http: dart.fieldType(http_config.HttpQueryOptions),
    options: dart.fieldType(core.Map$(core.String, dart.dynamic)),
    credentials: dart.fieldType(core.Map$(core.String, dart.dynamic)),
    headers: dart.fieldType(core.Map$(core.String, core.String))
  }));
  const headers$0 = dart.privateName(http_config, "HttpHeadersAndBody.headers");
  const body$ = dart.privateName(http_config, "HttpHeadersAndBody.body");
  http_config.HttpHeadersAndBody = class HttpHeadersAndBody extends core.Object {
    get headers() {
      return this[headers$0];
    }
    set headers(value) {
      super.headers = value;
    }
    get body() {
      return this[body$];
    }
    set body(value) {
      super.body = value;
    }
  };
  (http_config.HttpHeadersAndBody.new = function(opts) {
    let headers = opts && 'headers' in opts ? opts.headers : null;
    let body = opts && 'body' in opts ? opts.body : null;
    this[headers$0] = headers;
    this[body$] = body;
    ;
  }).prototype = http_config.HttpHeadersAndBody.prototype;
  dart.addTypeTests(http_config.HttpHeadersAndBody);
  dart.setLibraryUri(http_config.HttpHeadersAndBody, "package:graphql/src/link/http/http_config.dart");
  dart.setFieldSignature(http_config.HttpHeadersAndBody, () => ({
    __proto__: dart.getFields(http_config.HttpHeadersAndBody.__proto__),
    headers: dart.finalFieldType(core.Map$(core.String, core.String)),
    body: dart.finalFieldType(core.Map$(core.String, dart.dynamic))
  }));
  dart.defineLazy(fallback_http_config, {
    /*fallback_http_config.defaultHttpOptions*/get defaultHttpOptions() {
      return new http_config.HttpQueryOptions.new({includeQuery: true, includeExtensions: false});
    },
    set defaultHttpOptions(_) {},
    /*fallback_http_config.defaultOptions*/get defaultOptions() {
      return new (IdentityMapOfString$dynamic()).from(["method", "POST"]);
    },
    set defaultOptions(_) {},
    /*fallback_http_config.defaultHeaders*/get defaultHeaders() {
      return new (IdentityMapOfString$String()).from(["accept", "*/*", "content-type", "application/json"]);
    },
    set defaultHeaders(_) {},
    /*fallback_http_config.defaultCredentials*/get defaultCredentials() {
      return new (IdentityMapOfString$dynamic()).new();
    },
    set defaultCredentials(_) {},
    /*fallback_http_config.fallbackHttpConfig*/get fallbackHttpConfig() {
      return new http_config.HttpConfig.new({http: fallback_http_config.defaultHttpOptions, options: fallback_http_config.defaultOptions, headers: fallback_http_config.defaultHeaders, credentials: fallback_http_config.defaultCredentials});
    },
    set fallbackHttpConfig(_) {}
  });
  const operation$0 = dart.privateName(link_error, "ErrorResponse.operation");
  const fetchResult$ = dart.privateName(link_error, "ErrorResponse.fetchResult");
  const exception$0 = dart.privateName(link_error, "ErrorResponse.exception");
  link_error.ErrorResponse = class ErrorResponse extends core.Object {
    get operation() {
      return this[operation$0];
    }
    set operation(value) {
      this[operation$0] = value;
    }
    get fetchResult() {
      return this[fetchResult$];
    }
    set fetchResult(value) {
      this[fetchResult$] = value;
    }
    get exception() {
      return this[exception$0];
    }
    set exception(value) {
      this[exception$0] = value;
    }
  };
  (link_error.ErrorResponse.new = function(opts) {
    let operation = opts && 'operation' in opts ? opts.operation : null;
    let fetchResult = opts && 'fetchResult' in opts ? opts.fetchResult : null;
    let exception = opts && 'exception' in opts ? opts.exception : null;
    this[operation$0] = operation;
    this[fetchResult$] = fetchResult;
    this[exception$0] = exception;
    ;
  }).prototype = link_error.ErrorResponse.prototype;
  dart.addTypeTests(link_error.ErrorResponse);
  dart.setLibraryUri(link_error.ErrorResponse, "package:graphql/src/link/error/link_error.dart");
  dart.setFieldSignature(link_error.ErrorResponse, () => ({
    __proto__: dart.getFields(link_error.ErrorResponse.__proto__),
    operation: dart.fieldType(operation$.Operation),
    fetchResult: dart.fieldType(fetch_result.FetchResult),
    exception: dart.fieldType(operation_exception.OperationException)
  }));
  const errorHandler$ = dart.privateName(link_error, "ErrorLink.errorHandler");
  link_error.ErrorLink = class ErrorLink extends link.Link {
    get errorHandler() {
      return this[errorHandler$];
    }
    set errorHandler(value) {
      this[errorHandler$] = value;
    }
  };
  (link_error.ErrorLink.new = function(opts) {
    let errorHandler = opts && 'errorHandler' in opts ? opts.errorHandler : null;
    this[errorHandler$] = errorHandler;
    link_error.ErrorLink.__proto__.new.call(this, {request: dart.fn((operation, forward = null) => {
        let controller = null;
        function onListen() {
          return async.async(dart.void, function* onListen() {
            let stream = forward(operation).map(fetch_result.FetchResult, dart.fn(fetchResult => {
              if (fetchResult.errors != null) {
                let errors = fetchResult.errors[$map](graphql_error.GraphQLError, dart.fn(json => new graphql_error.GraphQLError.fromJSON(json), dynamicToGraphQLError()))[$toList]();
                let response = new link_error.ErrorResponse.new({operation: operation, fetchResult: fetchResult, exception: new operation_exception.OperationException.new({graphqlErrors: errors})});
                errorHandler(response);
              }
              return fetchResult;
            }, FetchResultToFetchResult())).handleError(dart.fn(error => {
              let response = new link_error.ErrorResponse.new({operation: operation, exception: new operation_exception.OperationException.new({clientException: exceptions.translateFailure(error)})});
              errorHandler(response);
              dart.throw(error);
            }, dynamicToNull()));
            yield controller.addStream(StreamOfFetchResult()._check(stream));
            yield controller.close();
          });
        }
        dart.fn(onListen, VoidToFutureOfvoid());
        controller = StreamControllerOfFetchResult().new({onListen: onListen});
        return controller.stream;
      }, OperationAndFnToStreamOfFetchResult())});
    ;
  }).prototype = link_error.ErrorLink.prototype;
  dart.addTypeTests(link_error.ErrorLink);
  dart.setLibraryUri(link_error.ErrorLink, "package:graphql/src/link/error/link_error.dart");
  dart.setFieldSignature(link_error.ErrorLink, () => ({
    __proto__: dart.getFields(link_error.ErrorLink.__proto__),
    errorHandler: dart.fieldType(dart.fnType(dart.void, [link_error.ErrorResponse]))
  }));
  const getToken$ = dart.privateName(link_auth, "AuthLink.getToken");
  link_auth.AuthLink = class AuthLink extends link.Link {
    get getToken() {
      return this[getToken$];
    }
    set getToken(value) {
      this[getToken$] = value;
    }
  };
  (link_auth.AuthLink.new = function(opts) {
    let getToken = opts && 'getToken' in opts ? opts.getToken : null;
    this[getToken$] = getToken;
    link_auth.AuthLink.__proto__.new.call(this, {request: dart.fn((operation, forward = null) => {
        let controller = null;
        function onListen() {
          return async.async(dart.void, function* onListen() {
            try {
              let token = (yield getToken());
              operation.setContext(new (IdentityMapOfString$MapOfString$String()).from(["headers", new (IdentityMapOfString$String()).from(["Authorization", token])]));
            } catch (e) {
              let error = dart.getThrown(e);
              controller.addError(error);
            }
            yield controller.addStream(forward(operation));
            yield controller.close();
          });
        }
        dart.fn(onListen, VoidToFutureOfvoid());
        controller = StreamControllerOfFetchResult().new({onListen: onListen});
        return controller.stream;
      }, OperationAndFnToStreamOfFetchResult())});
    ;
  }).prototype = link_auth.AuthLink.prototype;
  dart.addTypeTests(link_auth.AuthLink);
  dart.setLibraryUri(link_auth.AuthLink, "package:graphql/src/link/auth/link_auth.dart");
  dart.setFieldSignature(link_auth.AuthLink, () => ({
    __proto__: dart.getFields(link_auth.AuthLink.__proto__),
    getToken: dart.fieldType(dart.fnType(async.FutureOr$(core.String), []))
  }));
  const id$ = dart.privateName(optimistic, "OptimisticPatch.id");
  const data$0 = dart.privateName(optimistic, "OptimisticPatch.data");
  optimistic.OptimisticPatch = class OptimisticPatch extends core.Object {
    get id() {
      return this[id$];
    }
    set id(value) {
      this[id$] = value;
    }
    get data() {
      return this[data$0];
    }
    set data(value) {
      this[data$0] = value;
    }
  };
  (optimistic.OptimisticPatch.new = function(id, data) {
    this[id$] = id;
    this[data$0] = data;
    ;
  }).prototype = optimistic.OptimisticPatch.prototype;
  dart.addTypeTests(optimistic.OptimisticPatch);
  dart.setLibraryUri(optimistic.OptimisticPatch, "package:graphql/src/cache/optimistic.dart");
  dart.setFieldSignature(optimistic.OptimisticPatch, () => ({
    __proto__: dart.getFields(optimistic.OptimisticPatch.__proto__),
    id: dart.fieldType(core.String),
    data: dart.fieldType(collection.HashMap$(core.String, dart.dynamic))
  }));
  const _dereference$ = dart.privateName(optimistic, "_dereference");
  const cache$2 = dart.privateName(optimistic, "OptimisticProxy.cache");
  const data$1 = dart.privateName(optimistic, "OptimisticProxy.data");
  optimistic.OptimisticProxy = class OptimisticProxy extends core.Object {
    get cache() {
      return this[cache$2];
    }
    set cache(value) {
      this[cache$2] = value;
    }
    get data() {
      return this[data$1];
    }
    set data(value) {
      this[data$1] = value;
    }
    [_dereference$](node) {
      if (core.List.is(node) && node[$length] === 2 && dart.equals(node[$_get](0), this.cache.prefix)) {
        return this.read(core.String.as(node[$_get](1)));
      }
      return null;
    }
    read(key) {
      if (dart.test(this.data[$containsKey](key))) {
        let value = this.data[$_get](key);
        return MapOfString$Object().is(value) ? new lazy_cache_map.LazyCacheMap.new(value, {dereference: dart.bind(this, _dereference$), cacheState: lazy_cache_map.CacheState.OPTIMISTIC}) : value;
      }
      return this.cache.read(key);
    }
    write(key, value) {
      this.cache.writeInto(key, value, this.data);
    }
    save() {
      return async.async(dart.void, function* save() {
      });
    }
    restore() {
    }
    reset() {
    }
  };
  (optimistic.OptimisticProxy.new = function(cache) {
    this[data$1] = new (IdentityMapOfString$dynamic()).new();
    this[cache$2] = cache;
    ;
  }).prototype = optimistic.OptimisticProxy.prototype;
  dart.addTypeTests(optimistic.OptimisticProxy);
  optimistic.OptimisticProxy[dart.implements] = () => [cache.Cache];
  dart.setMethodSignature(optimistic.OptimisticProxy, () => ({
    __proto__: dart.getMethods(optimistic.OptimisticProxy.__proto__),
    [_dereference$]: dart.fnType(core.Object, [core.Object]),
    read: dart.fnType(dart.dynamic, [core.String]),
    write: dart.fnType(dart.void, [core.String, dart.dynamic]),
    save: dart.fnType(async.Future$(dart.void), []),
    restore: dart.fnType(dart.void, []),
    reset: dart.fnType(dart.void, [])
  }));
  dart.setLibraryUri(optimistic.OptimisticProxy, "package:graphql/src/cache/optimistic.dart");
  dart.setFieldSignature(optimistic.OptimisticProxy, () => ({
    __proto__: dart.getFields(optimistic.OptimisticProxy.__proto__),
    cache: dart.fieldType(optimistic.OptimisticCache),
    data: dart.fieldType(collection.HashMap$(core.String, dart.dynamic))
  }));
  const _proxy = dart.privateName(optimistic, "_proxy");
  const _parentPatchId = dart.privateName(optimistic, "_parentPatchId");
  const _patchExistsFor = dart.privateName(optimistic, "_patchExistsFor");
  const _safeToAdd = dart.privateName(optimistic, "_safeToAdd");
  const _isReference = dart.privateName(normalized_in_memory, "_isReference");
  const _dereference$0 = dart.privateName(normalized_in_memory, "_dereference");
  const _denormalizingDereference = dart.privateName(normalized_in_memory, "_denormalizingDereference");
  const _normalizerFor = dart.privateName(normalized_in_memory, "_normalizerFor");
  const _normalize = dart.privateName(normalized_in_memory, "_normalize");
  const dataIdFromObject$ = dart.privateName(normalized_in_memory, "NormalizedInMemoryCache.dataIdFromObject");
  const prefix$ = dart.privateName(normalized_in_memory, "NormalizedInMemoryCache.prefix");
  normalized_in_memory.NormalizedInMemoryCache = class NormalizedInMemoryCache extends in_memory_html.InMemoryCache {
    get dataIdFromObject() {
      return this[dataIdFromObject$];
    }
    set dataIdFromObject(value) {
      this[dataIdFromObject$] = value;
    }
    get prefix() {
      return this[prefix$];
    }
    set prefix(value) {
      this[prefix$] = value;
    }
    [_isReference](node) {
      return core.List.is(node) && node[$length] === 2 && dart.equals(node[$_get](0), this.prefix);
    }
    [_dereference$0](node) {
      if (core.List.is(node) && dart.test(this[_isReference](node))) {
        return this.read(core.String.as(node[$_get](1)));
      }
      return null;
    }
    lazilyDenormalized(data, cacheState = null) {
      return new lazy_cache_map.LazyCacheMap.new(data, {dereference: dart.bind(this, _dereference$0), cacheState: cacheState});
    }
    [_denormalizingDereference](node) {
      if (core.List.is(node) && dart.test(this[_isReference](node))) {
        return this.denormalizedRead(core.String.as(node[$_get](1)));
      }
      return null;
    }
    denormalizedRead(key) {
      try {
        return new traverse.Traversal.new(dart.bind(this, _denormalizingDereference)).traverse(this.read(key));
      } catch (e) {
        let error = dart.getThrown(e);
        if (core.StackOverflowError.is(error)) {
          dart.throw(new _base_exceptions.NormalizationException.new("          Denormalization failed for " + dart.str(key) + " this is likely caused by a circular reference.\n          Please ensure dataIdFromObject returns a unique identifier for all possible entities in your system\n          ", error, key));
        }
      }
    }
    reset() {
      this.data[$clear]();
    }
    read(key) {
      let value = super.read(key);
      return MapOfString$Object().is(value) ? this.lazilyDenormalized(value) : value;
    }
    [_normalizerFor](into) {
      const normalizer = node => {
        let dataId = this.dataIdFromObject(node);
        if (dataId != null) {
          return JSArrayOfString().of([this.prefix, dataId]);
        }
        return null;
      };
      dart.fn(normalizer, ObjectToListOfString());
      return normalizer;
    }
    [_normalize](node) {
      let dataId = this.dataIdFromObject(node);
      if (dataId != null) {
        return JSArrayOfString().of([this.prefix, dataId]);
      }
      return null;
    }
    writeInto(key, value, into, normalizer = null) {
      normalizer == null ? normalizer = this[_normalizerFor](into) : null;
      if (MapOfString$Object().is(value)) {
        let merged = normalized_in_memory._mergedWithExisting(into, key, value);
        let traversal = new traverse.Traversal.new(normalizer, {transformSideEffect: normalized_in_memory._traversingWriteInto(into)});
        into[$_set](key, traversal.traverseValues(merged));
      } else {
        into[$_set](key, value);
      }
    }
    write(key, value) {
      this.writeInto(key, value, this.data, dart.bind(this, _normalize));
    }
  };
  (normalized_in_memory.NormalizedInMemoryCache.new = function(opts) {
    let dataIdFromObject = opts && 'dataIdFromObject' in opts ? opts.dataIdFromObject : null;
    let prefix = opts && 'prefix' in opts ? opts.prefix : "@cache/reference";
    let storagePrefix = opts && 'storagePrefix' in opts ? opts.storagePrefix : null;
    this[dataIdFromObject$] = dataIdFromObject;
    this[prefix$] = prefix;
    normalized_in_memory.NormalizedInMemoryCache.__proto__.new.call(this, {storagePrefix: core.String._check(storagePrefix)});
    ;
  }).prototype = normalized_in_memory.NormalizedInMemoryCache.prototype;
  dart.addTypeTests(normalized_in_memory.NormalizedInMemoryCache);
  dart.setMethodSignature(normalized_in_memory.NormalizedInMemoryCache, () => ({
    __proto__: dart.getMethods(normalized_in_memory.NormalizedInMemoryCache.__proto__),
    [_isReference]: dart.fnType(core.bool, [core.Object]),
    [_dereference$0]: dart.fnType(core.Object, [core.Object]),
    lazilyDenormalized: dart.fnType(lazy_cache_map.LazyCacheMap, [core.Map$(core.String, core.Object)], [lazy_cache_map.CacheState]),
    [_denormalizingDereference]: dart.fnType(core.Object, [core.Object]),
    denormalizedRead: dart.fnType(dart.dynamic, [core.String]),
    [_normalizerFor]: dart.fnType(dart.fnType(core.List$(core.String), [core.Object]), [core.Map$(core.String, core.Object)]),
    [_normalize]: dart.fnType(core.List$(core.String), [core.Object]),
    writeInto: dart.fnType(dart.void, [core.String, core.Object, core.Map$(core.String, core.Object)], [dart.fnType(core.List$(core.String), [core.Object])]),
    write: dart.fnType(dart.void, [core.String, core.Object])
  }));
  dart.setLibraryUri(normalized_in_memory.NormalizedInMemoryCache, "package:graphql/src/cache/normalized_in_memory.dart");
  dart.setFieldSignature(normalized_in_memory.NormalizedInMemoryCache, () => ({
    __proto__: dart.getFields(normalized_in_memory.NormalizedInMemoryCache.__proto__),
    dataIdFromObject: dart.fieldType(dart.fnType(core.String, [core.Object])),
    prefix: dart.fieldType(core.String)
  }));
  const optimisticPatches = dart.privateName(optimistic, "OptimisticCache.optimisticPatches");
  optimistic.OptimisticCache = class OptimisticCache extends normalized_in_memory.NormalizedInMemoryCache {
    get optimisticPatches() {
      return this[optimisticPatches];
    }
    set optimisticPatches(value) {
      this[optimisticPatches] = value;
    }
    read(key) {
      let value = super.read(key);
      let cacheState = null;
      for (let patch of this.optimisticPatches) {
        if (dart.test(patch.data[$containsKey](key))) {
          let patchData = patch.data[$_get](key);
          if (MapOfString$Object().is(value) && MapOfString$Object().is(patchData)) {
            value = helpers.deeplyMergeLeft(JSArrayOfMapOfString$dynamic().of([MapOfString$Object().as(value), patchData]));
            cacheState = lazy_cache_map.CacheState.OPTIMISTIC;
          } else {
            value = patchData;
          }
        }
      }
      return MapOfString$Object().is(value) ? this.lazilyDenormalized(value, cacheState) : value;
    }
    get [_proxy]() {
      return new optimistic.OptimisticProxy.new(this);
    }
    [_parentPatchId](id) {
      let parts = id[$split](".");
      if (dart.notNull(parts[$length]) > 1) {
        return parts[$first];
      }
      return null;
    }
    [_patchExistsFor](id) {
      return this.optimisticPatches[$firstWhere](dart.fn(patch => patch.id == id, OptimisticPatchTobool()), {orElse: dart.fn(() => null, VoidToNull())}) != null;
    }
    [_safeToAdd](id) {
      let parentId = this[_parentPatchId](id);
      return parentId == null || dart.test(this[_patchExistsFor](parentId));
    }
    addOptimisiticPatch(addId, transform) {
      let patch = optimistic.OptimisticProxy.as(transform(this[_proxy]));
      if (dart.test(this[_safeToAdd](addId))) {
        this.optimisticPatches[$add](new optimistic.OptimisticPatch.new(addId, patch.data));
      }
    }
    removeOptimisticPatch(removeId) {
      this.optimisticPatches[$removeWhere](dart.fn(patch => patch.id == removeId || this[_parentPatchId](patch.id) == removeId, OptimisticPatchTobool()));
    }
  };
  (optimistic.OptimisticCache.new = function(opts) {
    let dataIdFromObject = opts && 'dataIdFromObject' in opts ? opts.dataIdFromObject : null;
    let prefix = opts && 'prefix' in opts ? opts.prefix : "@cache/reference";
    let storagePrefix = opts && 'storagePrefix' in opts ? opts.storagePrefix : null;
    this[optimisticPatches] = JSArrayOfOptimisticPatch().of([]);
    optimistic.OptimisticCache.__proto__.new.call(this, {dataIdFromObject: dataIdFromObject, prefix: prefix, storagePrefix: storagePrefix});
    ;
  }).prototype = optimistic.OptimisticCache.prototype;
  dart.addTypeTests(optimistic.OptimisticCache);
  dart.setMethodSignature(optimistic.OptimisticCache, () => ({
    __proto__: dart.getMethods(optimistic.OptimisticCache.__proto__),
    [_parentPatchId]: dart.fnType(core.String, [core.String]),
    [_patchExistsFor]: dart.fnType(core.bool, [core.String]),
    [_safeToAdd]: dart.fnType(core.bool, [core.String]),
    addOptimisiticPatch: dart.fnType(dart.void, [core.String, dart.fnType(cache.Cache, [cache.Cache])]),
    removeOptimisticPatch: dart.fnType(dart.void, [core.String])
  }));
  dart.setGetterSignature(optimistic.OptimisticCache, () => ({
    __proto__: dart.getGetters(optimistic.OptimisticCache.__proto__),
    [_proxy]: optimistic.OptimisticProxy
  }));
  dart.setLibraryUri(optimistic.OptimisticCache, "package:graphql/src/cache/optimistic.dart");
  dart.setFieldSignature(optimistic.OptimisticCache, () => ({
    __proto__: dart.getFields(optimistic.OptimisticCache.__proto__),
    optimisticPatches: dart.fieldType(core.List$(optimistic.OptimisticPatch))
  }));
  normalized_in_memory.typenameDataIdFromObject = function typenameDataIdFromObject(object) {
    if (MapOfString$Object().is(object) && dart.test(object[$containsKey]("__typename")) && dart.test(object[$containsKey]("id"))) {
      return dart.str(object[$_get]("__typename")) + "/" + dart.str(object[$_get]("id"));
    }
    return null;
  };
  normalized_in_memory._traversingWriteInto = function _traversingWriteInto(into) {
    function sideEffect(ref, value, traversal) {
      let key = ListOfString().as(ref)[$_get](1);
      if (MapOfString$Object().is(value)) {
        let merged = normalized_in_memory._mergedWithExisting(into, key, value);
        into[$_set](key, traversal.traverseValues(merged));
      } else {
        into[$_set](key, value);
        return;
      }
    }
    dart.fn(sideEffect, ObjectAndObjectAndTraversalTovoid());
    return sideEffect;
  };
  normalized_in_memory._mergedWithExisting = function _mergedWithExisting(into, key, value) {
    let existing = into[$_get](key);
    return MapOfString$Object().is(existing) ? helpers.deeplyMergeLeft(JSArrayOfMapOfString$dynamic().of([existing, value])) : value;
  };
  dart.trackLibraries("packages/graphql/client", {
    "package:graphql/src/cache/cache.dart": cache,
    "package:graphql/src/utilities/helpers.dart": helpers,
    "package:graphql/src/cache/lazy_cache_map.dart": lazy_cache_map,
    "package:graphql/src/utilities/traverse.dart": traverse,
    "package:graphql/src/exceptions/graphql_error.dart": graphql_error,
    "package:graphql/src/exceptions/_base_exceptions.dart": _base_exceptions,
    "package:graphql/src/cache/in_memory_html.dart": in_memory_html,
    "package:graphql/src/link/web_socket/link_web_socket.dart": link_web_socket,
    "package:graphql/src/core/query_manager.dart": query_manager,
    "package:graphql/src/scheduler/scheduler.dart": scheduler,
    "package:graphql/src/core/observable_query.dart": observable_query,
    "package:graphql/src/core/query_result.dart": query_result,
    "package:graphql/src/exceptions/exceptions.dart": exceptions,
    "package:graphql/src/exceptions/network_exception_stub.dart": network_exception_stub,
    "package:graphql/src/exceptions/operation_exception.dart": operation_exception,
    "package:graphql/src/exceptions/io_network_exception.dart": io_network_exception,
    "package:graphql/src/core/query_options.dart": query_options,
    "package:graphql/internal.dart": internal,
    "package:graphql/client.dart": client$,
    "package:graphql/src/graphql_client.dart": graphql_client,
    "package:graphql/src/link/http/link_http.dart": link_http,
    "package:graphql/src/link/http/http_config.dart": http_config,
    "package:graphql/src/link/http/fallback_http_config.dart": fallback_http_config,
    "package:graphql/src/link/error/link_error.dart": link_error,
    "package:graphql/src/link/auth/link_auth.dart": link_auth,
    "package:graphql/src/cache/optimistic.dart": optimistic,
    "package:graphql/src/cache/normalized_in_memory.dart": normalized_in_memory,
    "package:graphql/src/cache/in_memory.dart": in_memory
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["src/cache/cache.dart","src/utilities/helpers.dart","src/cache/lazy_cache_map.dart","src/utilities/traverse.dart","src/exceptions/graphql_error.dart","src/exceptions/_base_exceptions.dart","src/cache/in_memory_html.dart","src/link/web_socket/link_web_socket.dart","src/core/query_manager.dart","src/scheduler/scheduler.dart","src/core/observable_query.dart","src/core/query_result.dart","src/exceptions/exceptions.dart","src/exceptions/network_exception_stub.dart","src/exceptions/operation_exception.dart","src/exceptions/io_network_exception.dart","src/core/query_options.dart","src/graphql_client.dart","src/link/http/link_http.dart","src/link/http/http_config.dart","src/link/http/fallback_http_config.dart","src/link/error/link_error.dart","src/link/auth/link_auth.dart","src/cache/optimistic.dart","src/cache/normalized_in_memory.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;SACsB;IAAM;UAGjB,KACC;IACP;;AAEc;MAAU;;;IAEX;;IAEF;;;;EAChB;;;;;;;;;;;;qCCVoB;AAClB,UAAO,AAAI,IAAD,IAAI;EAChB;iEAGuB,GACA;AAErB,QAAI,AAAE,CAAD,IAAI,QAAQ,AAAE,CAAD,IAAI;AACpB,YAAO;;AAGT,QAAI,AAAE,CAAD,IAAI,QAAQ,AAAE,CAAD,IAAI;AACpB,YAAO;;AAGT,QAAI,AAAE,CAAD,aAAW,AAAE,CAAD;AACf,YAAO;;AAGJ,uBAAe;AAMlB,IAJF,AAAE,CAAD,WAAS,SAAQ,KAAa;AAC7B,qBAAM,AAAE,CAAD,eAAa,GAAG,mBAAM,AAAC,CAAA,QAAC,GAAG,GAAK,KAAK;AACvB,QAAnB,eAAe;;;AAInB,UAAO,aAAY;EACrB;2DAGuB,QACA;AAEe,IAApC,SAAa,qCAAK,yBAAU,MAAM;AACR,IAA1B,SAAS,yBAAU,MAAM;AAavB,IAZF,AAAO,MAAD,WAAS,SAAQ,KAAa;AAClC,oBAAI,AAAO,MAAD,eAAa,GAAG,MACV,YAAZ,AAAM,MAAA,QAAC,GAAG,MACV,KAAK,IAAI,QACH,yBAAN,KAAK;AAIN,QAHD,AAAM,MAAA,QAAC,GAAG,EAAI,2BACA,yBAAZ,AAAM,MAAA,QAAC,GAAG,IACV,KAAK;;AAGY,QAAnB,AAAM,MAAA,QAAC,GAAG,EAAI,KAAK;;;AAGvB,UAAO,OAAM;EACf;qDAgBiC;;AAG/B,UAAkD,OAApB,mCAAC,6CAAK,YAAO,IAAI;EACjD;;;;;;;ICnE6B;;mDAAxB;;;;EAAwB;;;;;;;;;;;;;;;AAmEK;IAAK;aAGd;;AACR,oBAA6B,KAApB,mBAAa,KAAK,SAAlB,OAAuB,KAAK;AAElD,UAAW,aAAP,MAAM;AACR,cAAO,AAAO,AAAc,OAAf,8BAAK;;AAEpB,UAAW,wBAAP,MAAM;AACR,cAAO,6CACL,MAAM,gBACO;;AAGjB,YAAO,OAAM;IACf;SAG0B;AAAQ,2BAAS,AAAI,iBAAC,GAAG;IAAE;QAEnC;AAAQ,2BAAS,AAAI,iBAAC,GAAG;IAAE;gBAGrB;AAAQ,YAAA,AAAK,yBAAY,GAAG;IAAC;kBAG3B;AAAU,YAAA,AAAO,wBAAS,KAAK;IAAC;;AAGR,YAAA,AAAK,AAClD,sDAAI,QAA0B,SAAU,mCACnC,AAAM,KAAD,MACL,cAAS,AAAM,KAAD;IACd;YAG2C;AACnD,eAAK,cAAuC;AACjB,QAAzB,AAAC,CAAA,CAAC,AAAM,KAAD,MAAM,AAAM,KAAD;;;AAGU,MAA9B,AAAQ,uBAAQ,aAAa;IAC/B;;AAGoB,YAAA,AAAK;IAAO;;AAGT,YAAA,AAAK;IAAU;;AAGT,YAAA,AAAK;IAAI;;AAGpB,YAAA,AAAK;IAAM;gBAI2B;AACtD,eAAiB,UAAmC;AAClD,cAAO,AAAC,EAAA,CAAC,AAAM,KAAD,MAAM,AAAM,KAAD;;;AAG3B,2CAA+B,AAAQ,2CAAI,SAAS;IACtD;;AAG+B,YAAA,AAAK,AAAO,iDAAI;IAAS;SAG/B;UAAY;yBAAZ;AACE,MAAzB,AAAI,iBAAC,GAAG,EAAI,sBAAO,KAAK;;IAC1B;;kCAGgC;AACmB,MAAjD,AAAK,mBAAqB,wBAAd,sBAAO,KAAK;IAC1B;;iDAGmD;AACzB,MAAxB,AAAK,uBAAW,OAAO;IACzB;;AAIc,MAAZ,AAAK;IACP;WAGqB;AAAQ,2BAAS,AAAK,mBAAO,GAAG;IAAE;gBAGjC;AACE,MAAtB,AAAK,wBAAY,IAAI;IACvB;gBAI0B,KAAY;yBAAZ;4BAAY;AAClC,2BAAS,AAAK,wBAAY,GAAG,EAAE,QAAQ;IAAE;WAIxB,KAAY;yBAAZ;8BAAY;UAA8B;;AAC3D,2BAAS,AAAK,mBAAO,GAAG,EAAE,MAAM,aAAY,QAAQ;IAAE;;uCAGpC;AACE,MAAtB,AAAK,sBAAU,MAAM;IACvB;;AAI4D,MAA1D,WAAM,8BAAiB;IACzB;;sDA9HsB;QACE;IACZ,cAAe,wBAAb,sBAAO,IAAI;IACN,qBAAE,WAAW;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAhDf;;;;;;;AACQ,YAAW,aAAX,iBAAyB;IAAU;aAGrC;;AACR,oBAA6B,KAApB,mBAAa,KAAK,SAAlB,OAAuB,KAAK;AAClD,UAAW,aAAP,MAAM;AACR,cAAO,AAAO,AAAc,OAAf,8BAAK;;AAEpB,UAAW,wBAAP,MAAM;AACR,cAAO,qCACL,MAAM,gBACO;;AAGjB,YAAO,OAAM;IACf;;AAEoB,yBAAM,aAAO,oBAAc;IAAW;;UAElC;AACpB,YAAM,AAE6B,gCAFnC,KAAK,KACO,YAAZ,AAAM,KAAD,SAAU,gBACI,YAAnB,AAAM,KAAD,gBAAiB,uBACL,YAAjB,AAAM,KAAD,aAAe;IAAU;;8CA/BZ;;QACE;QACX;IACI,qBACI,KAAX,UAAU,QAAV,OAAoB,+BAAL,IAAI,IAAmB,AAAK,IAAD,cAAc;AAC5D,yDAAM,IAAI,gBAAe,WAAW;;EAAC;;;;;;;;;;;;;;;;;;0CA8BxB;AAAoB,UAAgB,wCAAhB,eAAe,IAClD,AAAgB,eAAD,QACf,eAAe;;gDAG6B;AAC9C,UAAgB,wCAAhB,eAAe,IACT,AAAgB,eAAD,QACf,eAAe;;;;;;ICrCb;;;;;;IAGC;;;;;;IACK;;;;;;gBAEQ;AACX,qBAAW,AAAY,qBAAI,IAAI;AAC1C,YAAO,YAAC,QAAQ;IAClB;mBAGuD;AACrD,YAAO,AAAK,KAAD,iCACT,SAAQ,KAAY,UAAU,mCAC5B,GAAG,EACH,cAAS,KAAK;IAGpB;aAIuB;;AACR,wBAAc,eAAU,IAAI;AACzC,oBAAI,iBAAY,IAAI;AAClB,aAAO,WAAW;qBAAX,OAAe,IAAI;;AAE5B,UAAI,WAAW,IAAI;AACjB,YAAI,4BAAuB;AACmB,UAA5C,yBAAoB,WAAW,EAAE,IAAI,EAAE;;AAEzC,cAAO,YAAW;;AAGpB,UAAS,kBAAL,IAAI;AACN,cAAO,AAAK,AAA6C,KAA9C,oBAAa,QAAQ,QAAS,cAAS,IAAI;;AAExD,UAAS,wBAAL,IAAI;AACN,cAAO,qBAAe,IAAI;;AAE5B,YAAO,KAAI;IACb;;qCAjDO;QACA;QACA;IAFA;IACA;IACA;AAE4B,IAAjC,AAAY,oBAAA,OAAZ,mBAAgB,iCAAJ;EACd;;;;;;;;;;;;;;;;;;ICRU;;;;;;IAGA;;;;;;;AAGW,YAAA,AAAkC,uBAAxB,aAAI,wBAAW,eAAM;IAAG;;8CAXpB;IACxB,aAAE,AAAI,IAAA,QAAC;IACL,eAAE,AAAI,IAAA,QAAC;;EAAS;;;;;;;;;;;;;;;IAsCf;;;;;;IAGD;;;;;;IAGQ;;;;;;IAGD;;;;;;IAGO;;;;;;;AAIvB,YAAqH,UAAnH,gBAAO,gBAAc,aAAV,kBAAoB,AAAU,AAAyC,kCAArC,QAAU,KAAM,AAAmB,eAAb,cAAF,CAAC,KAAY,iCAAS,MAAM;IAAsB;;;QAvClH;QACA;QACA;QACA;QACA;IAJA;IACA;IACA;IACA;IACA;;EACL;;IAGyB;IACb,iBAAiB,OAAZ,WAAH,GAAG,WAAC,0BACO,eAAZ,WAAH,GAAG,WAAC,eACJ;IACI,mBAAmB,2BAAd,WAAH,GAAG,WAAC,iBACZ,sBACsB,AAA2B,2BAAzC,WAAH,GAAG,WAAC,6CACH,QAAkB,YAAsB,oCAAS,QAAQ,oCAG7D;IACD,cAAc,aAAT,WAAH,GAAG,WAAC;IACA,oBAAoB,yBAAf,WAAH,GAAG,WAAC;;EAAqC;;;;;;;;;;;;;;;ECtC9D;;;;;;;EAMgE;;;;;;;;IAM3C;;;;;;IACZ;;;;;;IACA;;;;;;;0DAJqB,SAAc,eAAoB;IAAlC;IAAc;IAAoB;;EAAM;;;;;;;;;;;;;IAW7D;;;;;;IACA;;;;;;;sDAHiB,SAAc;IAAd;IAAc;;EAAW;;;;;;;;;;;IAahC;;;;;;;AAFK,YAAA,AAA4B,iCAAR;IAAQ;;AAM7B;IAAO;;;IAFC;;EAAQ;;;;;;;;;;;;;gEAKE;AACvC,QAAY,oCAAR,OAAO;AACT,YAAO,QAAO;;AAGhB,UAAO,kDAAwB,OAAO;EACxC;;;;;;;IC7Be;;;;;;IACN;;;;;;IAGkB;;;;;;SAIL;AAClB,oBAAI,AAAK,wBAAY,GAAG;AACtB,cAAO,AAAI,kBAAC,GAAG;;AAGjB,YAAO;IACT;UAIkB,KAAa;AAC7B,oBAAI,AAAK,wBAAY,GAAG,MACV,YAAV,AAAI,iBAAC,GAAG,MACR,KAAK,IAAI,QACH,yBAAN,KAAK;AAKL,QAHF,AAAI,iBAAC,GAAG,EAAI,wBAAsC,mCACtC,yBAAV,AAAI,iBAAC,GAAG,IACR,KAAK;;AAGU,QAAjB,AAAI,iBAAC,GAAG,EAAI,KAAK;;IAErB;;AAIiB;AACQ,QAAvB,MAAM;MACR;;;AAIoB;AACa,QAA/B,aAAO,MAAM;MACf;;;AAKc,MAAZ,AAAK;IACP;;AAE+B;AACoB,QAAjD,AAAO,AAAY,gCAAC,gBAAa,mBAAW;MAC9C;;;AAEiD;AAC/C;AACQ,wBAAU,mBAAW,AAAO,AAAY,gCAAC;AAC/C,gBAAe,gDAAK,OAAO;;cACpB;AAEK,UAAZ,WAAM,KAAK;AAEX,gBAAO;;MAEX;;;;;QAtEO;IAMA;IAGkB,aAAO;IATzB;AAE6C,IAAlD,kBAA0B,+BAAd,OAAiB,AAAG,KAAE;EACpC;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ICMa;;;;;;IACY;;;;;;mBAKkB,WAAqB;AAC9D,UAAI,AAAc,uBAAG;AACC,QAApB;;AAGF,YAAO,AAAc,AAAgD,+BAAtC,qCAAoB,SAAS,GAAG,oCACzD,QAAkB,UAAW,wCACnB,AAAO,MAAD,eACU,aAAd,AAAO,MAAD,mBACL,AAAU,SAAD,2BACN,AAAU,SAAD;IAEjC;;;AAI0B,WAAxB;0BAAe;AACkC,MAAjD,sBAAgB,mCAAa,mBAAa;IAC5C;;AAIoB;;AACY,QAA9B,8CAAM,OAAe;AACD,QAApB,sBAAgB;MAClB;;;;QArCiB;QACV;IASM;IAVI;IACV;AACF;AACmB,IAAtB,yBAAU;EACZ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ICMW;;;;;;IACC;;;;;;IAEG;;;;;;IACX;;;;;;IACyB;;;;;;eAEgB;AACrB,4BAAkB,wDACxB,eACL,OAAO;AAGO,MAAzB,cAAS,eAAe;AAExB,YAAO,gBAAe;IACxB;UAEuC;AACrC,YAAO,iBAAW,KAAK,OAAO;IAChC;WAE2C;AACzC,YAAO,AAAyB,iBAAd,KAAK,OAAO,iCAAO,QAAC;AAI9B,gCAAoB,gDACjB,qBACE,OAAO,WACP;AAGL,wBAAY,AAAkB,iBAAD;AAEnC,iBAAW,WAAY,UAAS;AACR,UAAtB,MAAM,AAAQ,QAAA,CAAC,MAAM;;AAGvB,cAAO,OAAM;MACd;IACH;eAGS,SACK;AAFgB;;AAIJ,yBACpB,mCAA8B,OAAO,EAAE,OAAO;AAClD,aAAO,AAAW,UAAD;qBAAC,OAAiB,AAAW,UAAD;MAC/C;;kCAKS,SACK;AAEM,wBAAc,2BAC9B,OAAO,EACP,OAAO;AAKT,YAAO,sDACQ,WAAW,iBAEoB,UAAvC,gCAAkB,AAAQ,OAAD,6BAAkB,AAAY,WAAD,YACjD,OACA,6BAAuB,OAAO,EAAE,OAAO;IAErD;6BAKS,SACK;AAF4B;;AAKxB,8BAAsB,iCAAY,OAAO,GACrD,cAAW,AAAQ,OAAD;AAEV;AACA;AAEZ;AAKS,UAHP,eAAc,MAAM,AAGlB,oBAFM,sBACK,SAAS;AAItB,cAAI,AAAY,WAAD,SAAS,qBACpB,AAAQ,OAAD,cAA4B;AAIpC,YAHD,AAAM,iBACJ,AAAU,SAAD,UACT,AAAY,WAAD;;AAQd,UAJD,cAAc,iCACZ,WAAW,EACX,OAAO,WACmB;;cAErB;AAEuD,UAA9D,AAAY,WAAD,IAAC,OAAZ,cAAgB,0CAAsC,2CAA1C;AAKX,UAHD,AAAY,WAAD,aAAa,+CACX,AAAY,WAAD,6BACL,4BAAiB,OAAO;;AAKZ,QAAjC,8BAAyB,OAAO;AAChC,yBAAI,AAAQ,OAAD,cAA4B,sCAC7B,gDAAN;AAE8C,UAAhD,AAAY,WAAD,QAAQ,AAAM,gBAAK,AAAU,SAAD;;AAGL,QAApC,oBAAe,OAAO,EAAE,WAAW;AAEnC,cAAO,YAAW;MACpB;;2BAKS,SACK;AAEC,qBAAW,AAAQ,OAAD;AAEnB,wBAAc,2CAAqB;AAE/C;AACE,YAAI,AAAQ,OAAD,qBAAqB;AAK7B,UAJD,cAAc,gCACZ,OAAO,aACG,QAAQ,oBACA,AAAQ,OAAD;;AAM7B,sBAAI,4CAA8B,AAAQ,OAAD,6BACpC,AAAY,WAAD;AACA,qBAAO,AAAM,gBAAK,QAAQ;AAExC,cAAI,IAAI,IAAI;AAIT,YAHD,cAAc,wCACN,IAAI,UACgB;;AAI9B,cAAwB,YAApB,AAAQ,OAAD,cAA4B,kDACnC,AAAY,WAAD;AASZ,YARD,cAAc,0CACc,iDACf,iEACQ,4CACf,uEACA,QAAQ;;;;YAMX;AAIN,QAHD,AAAY,WAAD,aAAa,+CACX,AAAY,WAAD,6BACL,4BAAiB,OAAO;;AAWT,MAApC,oBAAe,OAAO,EAAE,WAAW;AACnC,YAAO,YAAW;IACpB;iBAEwC;AACd,oBAAU,AAAO,AAAU,oBAAT,OAAO;AACjD,YAAO,iBAAW,OAAO,EAAE,OAAO;IACpC;aAEgC;AAC9B,oBAAI,AAAQ,2BAAY,OAAO;AAC7B,cAAO,AAAO,qBAAC,OAAO;;AAGxB,YAAO;IACT;mBAIS,SACK;UACP;AAEiB,4BAAkB,cAAS,OAAO;AACxD,oBAAI,YAAY;AAIb,QAHD,AAAM,iBACJ,AAAgB,AAAQ,eAAT,kBACf,AAAY,WAAD;;AAIf,UAAI,eAAe,IAAI,mBAAS,AAAgB,AAAW,eAAZ;AACP,QAAtC,AAAgB,eAAD,WAAW,WAAW;;IAEzC;gCAIS;UACU;UACA;AAEjB,WAAa,8BAAN,+BACH;AAGmE,MADhE,AAAoB,8BAA1B,gCACG,OAAO,EAAE,QAAO;;AAAU,kBAAK;QAAE,SAAM,QAAQ,EAAE,gBAAgB;;;AAEnD,wBAAc,wCACxB,AAAM,gBAAK,QAAQ,WACC;AAE5B,YAAO,YAAW;IACpB;6BAGqC;AACnC,UAAU,8BAAN;AACwD,QAAnD,AAAoB,8BAA1B,kCAAgD,QAAQ;;IAE7D;;AAOE,eAAqB,QAAS,AAAQ;AACpC,sBAAI,AAAM,KAAD;AACO,2BAAa,AAAM,gBAAK,AAAM,AAAQ,KAAT;AAC3C,cAAI,UAAU,IAAI;AAOf,YAND,AAAM,KAAD,WACH,iCACE,wCAAkB,UAAU,IAC5B,AAAM,KAAD,mBACqB;;;;IAMtC;aAE8B;AACsB,MAAlD,AAAO,oBAAC,AAAgB,eAAD,UAAY,eAAe;IACpD;eAEgC;UAAuB;AACrD,qBAAK,SAAS;AAC4B,QAAxC,AAAgB,eAAD,qBAAoB;;AAEE,MAAvC,AAAQ,sBAAO,AAAgB,eAAD;IAChC;;AAGY,sBAAY;AAEX,MAAX,iBAAS,aAAT,kBAAS;AAET,YAAO,UAAS;IAClB;gCAGc,aACA;UACgB;AAET;AACX;AAIR,UAAI,AAAY,WAAD,WAAW,kBAAQ,AAAY,AAAO,WAAR;AAC3C,gBAAQ,AAAQ,OAAD;;;AAG4B,YAAvC,SAAS,wBAAkB,WAAW;AACf,YAAvB,OAAO,AAAY,WAAD;AAClB;;;;AAGuB,YAAvB,OAAO,AAAY,WAAD;AAClB;;;;;AAKuC,YAAvC,SAAS,wBAAkB,WAAW;AACtC;;;;AAGmB,QAAvB,OAAO,AAAY,WAAD;;AAGpB,YAAO,yCACC,IAAI,UACF,MAAM,aACH,mDAA8B,MAAM;IAEnD;wBAEiD;AAC7C,YAD0D,2BAClC,AAAY,AAAO,WAAR,0CACjC,QAAS,YAA0B,wCAAS,QAAQ;IACpD;;;QAtVW;QACA;IAUF;IACX,kBAAY;IACa,gBAAmC;IAb/C;IACA;AAId,IAFD,iBAAY,gDACI;EAElB;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ICba;;;;;;IAGkB;;;;;;IAKH;;;;;;2BAMpB,OACG;AAsBR,MApBD,AAAe,AAAW,4BAAV,QAAQ,gBACtB,QAAQ;AASN,YAAI,AAAiB,AAAU,8BAAT,OAAO,KAAK;AAChC,gBAAO;;AAGM,2BACX,gCAAkB,AAAiB,AAAU,8BAAT,OAAO;AAE/C,cAA8C,WAAvC,AAAkB,qCAAY,OAAO,MACxC,AAAa,YAAD,SAAI,QAAQ;;AAKhC,oBAAI,AAAe,AAAW,4BAAV,QAAQ;AACM,QAAhC,AAAgB,8BAAO,QAAQ;AACA,QAA/B,AAAe,8BAAO,QAAQ;AAChB,QAAd,AAAM,KAAD;AACL;;AAI0D,MAA5D,AAAe,AAAW,4BAAV,QAAQ,YAAuB,UAAb;IACpC;sBAGoB,SACX;AAEP,YAAO,AAAQ,AAAqB,OAAtB,iBAAiB,QAA6B,aAArB,AAAQ,OAAD,iBAAgB;AAE1B,MAApC,AAAiB,8BAAC,OAAO,EAAI,OAAO;AAErB,qBAAW,gCACf,AAAQ,OAAD;AAGlB,oBAAI,AAAgB,mCAAY,QAAQ;AACA,QAAtC,AAAe,AAAW,4BAAV,QAAQ,QAAM,OAAO;;AAEQ,QAA7C,AAAe,4BAAC,QAAQ,EAAY,sBAAC,OAAO;AAK3C,QAHD,AAAc,4BAAC,QAAQ,EAAU,qBAC/B,QAAQ,EACR,QAAO,SAAU,4BAAuB,KAAK,EAAE,QAAQ;;IAG7D;qBAI6B;AACM,MAAjC,AAAkB,gCAAO,OAAO;IAClC;;;QAlFO;IAMwB,0BACA;IAIH,wBAA0C;IAG3C,uBAAkC;IAdtD;;EACL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ICaJ;;yDAVK;;;;EAUL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAmBe;;;;;;IACM;;;;;;IAQP;;;;;;IAEG;;;;;;IAEG;;;;;;IAEY;;;;;;;AAZE,YAAA,AAAa;IAAS;;AAcpB,YAAA,AAAW;IAAM;;AACpB,YAAU,aAAV,gBAA4B;IAAO;;AAGhE,cAAQ;;;;;AAIJ,gBAAO;;;;;;;;AAOP,gBAAO;;;AAEX,YAAO;IACT;;AAIE,oBAAI;AACF,cAAO,AAAa,gCAAa;;AAEnC,yCAAiC,mBAAU;IAC7C;;AAGE,cAAQ;;;;;;AAKJ,gBAAO;;;;;;;AAMP,gBAAO;;;AAEX,YAAO;IACT;;AAGE,oBAAI;AAC8B,QAAhC,iCAA2B;AAK3B,uBAAK,AAAW,6BAAY,qBAAgB;AACd,UAA5B,AAAW,oBAAI;;AAEjB;;AAEF,oBAAI,AAAQ;AACI,QAAd;;IAEJ;;AAG0B,uBACpB,AAAa,gDAA8B,cAAS;AACjB,MAAvC,AAAa,qBAAA,OAAb,oBAAiB,AAAW,UAAD,eAAd;AAMe,MAF5B,2BAAY,AAAqB,2CACZ,uDACA;AAErB,UAAI,AAAQ,6BAAgB,QAA6B,aAArB,AAAQ,6BAAe;AACvB,QAAlC,kBAAa,AAAQ;;AAGvB,YAAO,WAAU;IACnB;cAIgC;AAAlB;;AAEZ,cAAO,AAAiB,AAAY,gBAAb,gBAAgB;AAEjC,8BAAkB,iDACG,gDACZ,AAAQ,yCACuB,KAA9B,AAAiB,gBAAD,qBAAC,OAAgB,AAAQ,0CAC9C,AAAQ,iCACN;;AACE,0BAAR;AAAQ;AACS,0BAAjB,iBAAgB;AAAC;;;AAQtB,QAHF,eAAU,wCACF,AAAa,iCACV;AAGC,+BAAkB,MAAM,AAAa,wBAAM,eAAe;AAEtE;AAKG,UAHD,AAAgB,eAAD,QAAQ,AAAiB,gBAAD,aACrC,AAAa,wBACb,AAAgB,eAAD;AAEjB,gBAAO,AAAgB,AAAK,eAAN,SAAS,yBAAM;AAMpC,UAJD,AAAa,iCACX,cACA,eAAe,iBACD;;cAET;AACP,wBAAI,AAAgB,eAAD;AAOhB,YAJD,AAAa,8BAAY,+CACZ,AAAa,4CACT,AAAgB,AAAU,eAAX,2CACb,AAAgB,AAAU,eAAX;AAOjC,YAJD,AAAa,iCACX,cACA,kCACc;AAEhB;;AAGO,YAAP;;;MAGN;;cAK2B;;AAEzB,UAAI,qBAAgB,kBAChB,AAAa,AAAU,oCAAQ,AAAO,MAAD;AACvC;;AAGF,UAAI,qBAAgB;AACmB,aAArC,MAAM;QAAC,AAAO,aAAA,OAAP,YAAW,AAAa,2BAAjB;;AAGhB,UAAc,YAAV,gBAA4B,yDAAW,AAAO,MAAD,aAAe;AAC1B,QAApC,iBAA2B;;AAGR,MAArB,oBAAe,MAAM;AAErB,qBAAK,AAAW;AACQ,QAAtB,AAAW,oBAAI,MAAM;;IAEzB;WAK6B;AACG,MAA9B,AAAU,SAAD,IAAC,OAAV,4BAAU;AACsB;AAsB9B,MApBF,eAAe,AAAO,mBAAO,QAAa;AACxC,uBAAK,AAAO,MAAD;AACT,mBAAW,WAAY,UAAS;AACR,YAAtB,MAAM,AAAQ,QAAA,CAAC,MAAM;;AAGU,UAAjC,AAAa;AAEb,yBAAK,AAAO,MAAD;AACkB,YAA3B,MAAM,AAAa,YAAD;AACuB,YAAzC,AAAqB,kCAAO,YAAY;AAExC,0BAAI,AAAqB;AACvB,kBAAc,YAAV,gBAA4B;AACM,gBAApC,iBAA2B;AACpB,gBAAP;;;;;MAKT;AAEqC,MAAtC,AAAqB,+BAAI,YAAY;IACvC;iBAEsB;AACpB,UAAwB,YAApB,AAAQ,0BAA2B,yCACf,YAApB,AAAQ,0BAA2B;AAGpC,QAFD,WAAM,mBACJ;;AAIJ,oBAAI;AACiC,QAAnC,AAAU,gCAAiB;;AAGM,MAAnC,AAAQ,4BAAe,YAAY;AACD,MAAlC,iBAA2B;AACkB,MAA7C,AAAU,iCAAkB,cAAS;IACvC;;AAGE,oBAAI;AACiC,QAAnC,AAAU,gCAAiB;AACA,QAA3B,AAAQ,4BAAe;AACmB,QAA1C,iBAA2B;;IAE/B;kBAEmC;AACJ,MAA7B,AAAQ,yBAAY,SAAS;IAC/B;;UAWO;UACA;AAFuB;AAI5B,YAAc,YAAV,gBAA4B,oEAAyB,KAAK;AACZ,UAAhD,iBAA2B;AAE3B,gBAAO;;AAIT,uBAAK,WAAW;AACgC,UAA9C,AAAa,6BAAW,kBAAiB;;AAG3C,iBAAqC,eAAgB;AACxB,UAA3B,MAAM,AAAa,YAAD;;AAGP,QAAb;AAEwB,QAAxB,MAAM,AAAW;AAEgB,QAAjC,iBAA2B;AAC3B,cAAsB;MACxB;;;;QApSiB;QACA;IAYZ,iCAA2B;IAOW,6BACN;IAGzB;IAEG,kBAA2B;IAIZ;IA9Bb;IACA;IACJ,gBAAiC,cAA/B,AAAa,YAAD;AACzB,kBAAI,AAAQ;AACqB,MAA/B,iCAA2B;AACb,MAAd;;AAID,IAFD,kBAAW,+DACC;EAEd;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IClBF;;wDALK;;;;EAKL;;;;;;;;;;;;;;;;;;;IAsBW;;;;;;IAMS;;;;;;IAGV;;;;;;IAEW;;;;;;;AAGC,YAAO,aAAP,aAA4B;IAAO;;AAIhC,YAAO,aAAP,aAA4B;IAAgB;;AAG1C,YAAC,AAAU,mBAAG;IAAK;;;;QAlCrC;QACA;QACA;QACA;QACa;IAJb;IACA;IAIS,kBAAW;IACT,iBAAS,KAAP,MAAM,QAAN,OACA,YAAR,OAAO,EAAI,QACW,yCACN,YAAX,UAAU,EAAI,QACO,kDAClB;;EAAK;;;;;;;;;;;;;;;;;;;IAqCb;;;;;;IACU;;;;;;;;QAVf;QACA;IADA;IACA;SAEoB,aAAnB,AAAY,WAAD,SAA6B,2DACxC;AAEoC,IAA1C,AAAY,oBAAA,OAAZ,mBAAgB,2CAAqB,SAAzB;EACd;;;;;;;;;MArDI,yBAAY;YAAG,0CACD,sCACA;;;2DCXwB;;AAC1C,SAAO,6CAA2B,OAAO;iBAAlC,OAAuC,kCAAoB,OAAO;EAC3E;;;;;ICRsB;;;;;;IAEb;;;;;;IAEH;;;;;;;;AASA,YAAA,AAA2D,oCAApC,YAAG,iBAAY,yBAAR,OAAW;IAAkB;;;QANxD;QACA;QACA;IAFA;IACA;IACA;;EACL;;;;;;;;;;;oFAM6C;AAC/C,QAAY,6BAAR,OAAO;AACT,YAAO,oEACa,OAAO,WAChB,AAAQ,OAAD,eACX,AAAQ,OAAD;;AAGhB,UAAO;EACT;;;;;ICzBqB;;;;;;IAIH;;;;;;aAOW;AAAU,YAAA,AAAc,0BAAI,KAAK;IAAC;;AAExC,YAAA,AAIf;;AAHA,YAAI,wBAAmB,MAA2C,SAArC,+BAAoB;AACjD,sBAAI,AAAc,kCAAY;AAC9B,sBAAG,AAAc,uCAAI,QAAC,KAAQ,cAAF,CAAC;AAA7B;;kBACK;IAAK;;;QAVT;QACkB;IARN,uBAAgB;IAO5B;IAEiB,uBAAE,AAAc,aAAD;;EAAS;;;;;;;;;;;;;;;;QAiB7B;QACH;QACG;AAEnB,QAAI,SAAS,IAAI,QACb,eAAe,IAAI,QAClB,aAAa,IAAI,kBAAQ,AAAc,aAAD;AACzC,YAAO,mEAC4B,KAAhB,eAAe,QAAf,cAAmB,SAAS,gBAAT,OAAW,2CAChC;;AACb,cAAI,aAAa,IAAI,MAAM,eAAG,cAAa;AAAhB;AAC3B,qBAAI,SAAS,gBAAT,OAAW,sBAAiB,MAAM,gBAAG,AAAU,UAAD;AAAZ;;;;AAI5C,UAAO;EACT;mFCzCsD;AACpD,QAAY,sBAAR,OAAO;AACT,YAAY,oEACQ,OAAO,WAChB,AAAQ,OAAD,eACX,uBACK,cACF,AAAQ,AAAQ,OAAT,qBACP,AAAQ,OAAD;;AAInB,UAAO,gDAA6B,OAAO;EAC7C;;;;;;;;;;ICIA;;mDANK;;;;EAML;;;;;;;;;;;;;;;;;;;IAwBA;;mDAJK;;;;EAIL;;;;;;;;;;;;;;;;IAIc;;;;;;IAGA;;;;;;kBAaqB;;AAAe,YAAS,kCAClC,cAAjB,SAAS,gBAAT,OAAW,oBAAX,OAAoB,qBACH,iBAAjB,SAAS,kBAAT,OAAW,uBAAX,OAAoB;IACrB;;;QAbE;QACA;IADA;IACA;;EACL;0CAGK,OACA;IADA;IACA;UACM,AAAM,KAAD,IAAI,yBAAM;UACf,AAAM,KAAD,IAAI,yBAAM;;EAAiC;;;;;;;;;;;;;;;;IA0BtD;;;;;;IAGE;;;;;;IAOY;;;;;;;AALU,YAAA,AAAS;IAAK;;AAEd,YAAA,AAAS;IAAK;;;QArBhC;QACE;QACQ;QAChB;QACA;QACA;IAFA;IACA;IACA;AACF,kEAEa,QAAQ,gBACJ,YAAY,aACf,SAAS;;EACrB;;;;;;;;;;;;;;;;IAwCH;;;;;;;;QApBS;QACE;QACQ;QACT;QACA;QACL;QACF;QACgB;IADhB;AAEF,mEACa,uCAAgB,WAAW,SAAS,WAAW,cAE/C,QAAQ,gBACJ,YAAY,aACf,SAAS,WACX,OAAO,oBACE,gBAAgB;;EACnC;;;;;;;;;;;IAiCa;;;;;;IACH;;;;;;IACT;;;;;;;;QApBK;QACE;QACQ;QACT;QACA;QACS;QAChB;QACA;QACA;IAFA;IACA;IACA;AACF,sEACa,uCAAgB,WAAW,SAAS,WAAW,cAE/C,QAAQ,gBACJ,YAAY,aACf,SAAS,WACX,OAAO;;EACjB;;;;;;;;;;;;;;;;IAQe;;;;;;IACV;;;;;;IACC;;;;;;;AAaT,YAAQ,AAA+B,kCAA9B,kBAAa,aAAQ;IAAuB;;AAIvD,UAAI,AAAQ,4BAAe;AACzB,cAAO,SAAa;AAClB,yBAAK,AAAO,MAAD,wBAAa,AAAO,MAAD;AAC5B,kBAAO,AAAQ,0BAAY,AAAO,MAAD;;;;AAIvC,YAAO;IACT;;AAGE,UAAI,AAAQ,wBAAW;AACrB,cAAO,SAAa;AAClB,yBAAK,AAAO,MAAD,uBACP,AAAO,MAAD,+BACN,AAAQ,0BAA2B;AACrC,kBAAO,AAAQ,sBAAQ,AAAO,MAAD;;;;AAKnC,YAAO;IACT;;AAKuB,YAAmB,UAAhB,gBAAQ;IAAQ;wBAGP;AACpB,oBAAU;AAEvB,WAAa,8BAAN,+BACH;AAIF,MAHK,AAAoB,8BAA1B,gCAA8C,OAAO,EAAE,QAAO;AAChC,QAA7B,AAAQ,oBAAO,KAAK,EAAE,MAAM;AAC5B,cAAO,MAAK;;IAEhB;;AAQE,UAAI,AAAQ,uBAAU;AAEG,2BAAe,AAAQ;AACjC,yCAAmB;AAGhC,cAAK,eAAyB;AAC5B,wBAAI,AAAO,MAAD;AACR,kBAAO,AAAgB,iBAAA,CAAC,MAAM;;AAE9B,kBAAO,AAAY,aAAA,CAAC,YAAO,MAAM;;;;AAIrC,cAAO,aAAY;;AAErB,YAAO;IACT;;;QA9EO;QACA;QACA;IAFA;IACA;IACA;UACM,AAAM,KAAD,IAAI;UACT,AAAQ,OAAD,IAAI;UACX,AAAQ,OAAD,IAAI;;EAAK;;;;;;;;;;;;;;;;;;;;;;;;;IAyGxB;;;;;;IACA;;;;;;eAG6B;AAChC,YAAO,YAAC,2BAAqB,MAAM,YAAY;IACjD;2BAIoB,GACA;AAElB,uBAAI,AAAE,CAAD,eAAiB,AAAE,CAAD;AACrB,cAAO;;AAGT,uBAAI,AAAE,CAAD,WAAa,AAAE,CAAD;AACjB,cAAO;;AAGT,UAAI,AAAE,CAAD,iBAAiB,AAAE,CAAD;AACrB,cAAO;;AAGT,uBAAI,AAAE,CAAD,eAAiB,AAAE,CAAD;AACrB,cAAO;;AAIT,YAAO,+BAAsB,AAAE,CAAD,YAAY,AAAE,CAAD;IAC7C;;;QAxDa;QACE;QACQ;QACT;QACA;QACL;QACH;QACC;QACA;QACgB;IAFhB;IACA;AAEF,wEAEa,QAAQ,gBACJ,YAAY,aACf,SAAS,eACP,WAAW,eACX,WAAW,gBACV,YAAY,WACjB,OAAO,oBACE,gBAAgB;AAEC,IAApC,AAAoB,4BAAA,OAApB,2BAAwB,oBAAJ;EAC3B;;;;;;;;;;;;;;;;;;IA6Da;;;;;;IAac;;;;;;IAIf;;;;;;;AAZW,+BAAU;IAAa;iBAIjC;AACsB,MAAjC,oBAAe,sCAAY,KAAK;IAClC;;;;QAzBa;QACE;QACR;QACU;IADV;IACU;mBAGT,iCAAmB,QAAQ,EAAE,YAAY,sBACzC;UAEK,AAAY,WAAD,IAAI;IACJ,kCAED,MAAb,YAAY,SAAZ,OAAgB,AAAS,QAAD,IAAI,eAAO,mBAAY,QAAQ,IAAI;;EAAI;;;;;;;;;;;;;;;;;mCArUnD;AAAU,8BAAY,KAAK;EAAC;uFAkBL;AAC3C,UAAY,AAC+B,aAD3C,WAAW,EAAgB,yCACf,YAAZ,WAAW,EAAgB,8CACf,YAAZ,WAAW,EAAgB;EAAS;+DAEL;AAC/B,UAAY,AAA0B,aAAtC,WAAW,EAAgB,yCACf,YAAZ,WAAW,EAAgB;EAAS;iEAmU/B,GACA;QACF;AAEH,UACyB,YADvB,QAAQ,KAAI,AAAE,CAAD,IAAI,QAAQ,AAAE,CAAD,IAAI,QAC/B,CAAC,IAAI,QAAQ,AAAE,CAAD,IAAI,QAClB,AAAE,CAAD,IAAI,QAAQ,CAAC,IAAI;EAAK;;;;;ICrVjB;;;;;;IAUA;;;;;;IAUA;;;;;;;;QAEE;QACA;QACA;IACW,oBAAE,AAAoB,iEAAc,UAAU;IACnD,eAAE,AAAe,4DAAc,KAAK;IACnC,gBAAE,AAAgB,6DAAc,MAAM;;EAAC;;;;;;;;;;MAE5C,kDAAmB;YAAY,iCAC9B,2CACA;;MAGD,6CAAc;YAAY,iCACzB,sCACA;;MAED,8CAAe;YAAY,iCAC1B,uCACA;;;;;;;;IAqBE;;;;;;IAGL;;;;;;IAGC;;;;;;IAEC;;;;;;eAIgC;AAEmB,MAD9D,AAAQ,OAAD,YACH,AAAgB,AAAW,8CAAc,AAAQ,OAAD;AACpD,YAAO,AAAa,8BAAW,OAAO;IACxC;UAIuC;AACmC,MAAxE,AAAQ,OAAD,YAAY,AAAgB,AAAM,yCAAc,AAAQ,OAAD;AAC9D,YAAO,AAAa,yBAAM,OAAO;IACnC;WAI2C;AACgC,MAAzE,AAAQ,OAAD,YAAY,AAAgB,AAAO,0CAAc,AAAQ,OAAD;AAC/D,YAAO,AAAa,0BAAO,OAAO;IACpC;cAIwC;AACtC,YAAO,qBAAc,sBAAiB,SAAS;IACjD;;;QAhDiB;QACA;QACV;IAkBM;IApBI;IACA;IACV;AAEgC,IAArC,AAAgB,wBAAA,OAAhB,uBAAoB,2CAAJ;AAIf,IAHD,oBAAe,0CACP,kBACC;EAEX;;;;;;;;;;;;;;;;;;;QC3DmB;QACZ;QAGE;QACa;QACC;QACA;AAClB,0DAGY,SACG,WACD;;AAEH,wBAAgB,eAAM,GAAG;AAE/B,sBAAI,AAAU,SAAD;AACX,cAAI,AAAQ,OAAD,IAAI;AAC+C,YAA5D,WAAM,mBAAU;;AAElB,gBAAO,AAAO,QAAA,CAAC,SAAS;;AAGb,uBAAqB,MAAX,UAAU,SAAV,OAAc;AAEpB,yBAAa,sCACtB,yDACe,iBAAiB,aAE7B,YAAY,eACR,WAAW,WACf,OAAO;AAGS,sBAAU,AAAU,SAAD;AACnC;AAEX,YAAI,OAAO,IAAI;AASZ,UAPD,gBAAgB,sCACR,yDAC4C,aAA7B,AAAO,OAAA,QAAC,kCAEI,yBAAxB,AAAO,OAAA,QAAC,+BACmB,yBAAvB,AAAO,OAAA,QAAC,0BACO,wBAAnB,AAAO,OAAA,QAAC;;AAII,iCACrB,oCACF,SAAS,EACT,yCACA,UAAU,EACV,aAAa;AAGW,0BAAc,AAAmB,kBAAD;AAE5B;AAE9B,iBAAa;AAAQ;AACF;AAEjB;AAEoB,6BAAU,MAAM,0BAC9B,SAAS,EAAE,AAAmB,kBAAD,OAAO,WAAW;AAEb,cAAtC,YAAW,MAAM,AAAQ,OAAD,MAAM,OAAO;AAInC,cAFF,AAAU,SAAD,YAAsC,mDAC7C,YAAY,QAAQ;AAEJ,oCACd,MAAM,yBAAe,QAAQ;AAEH,cAA9B,AAAW,UAAD,KAAK,cAAc;;kBACtB;AAGC,+BAAa,4BAAoB,OAAO;AAChD,kBAAe,2CAAX,UAAU;AACc,gBAA1B,AAAW,UAAD,OAAO,SAAS;;AAEG,cAA/B,AAAW,UAAD,UAAU,UAAU;;AAGR,YAAxB,MAAM,AAAW,UAAD;UAClB;;;AAE8D,QAA9D,aAAa,+CAAwC,QAAQ;AAE7D,cAAO,AAAW,WAAD;;;EAEpB;;;;;;+CAIC;QACmB;QACd;AAH+B;;AAKJ,MAAxC,AAAW,UAAD,IAAC,OAAX,aAAsC,kDAA3B;AACX,UAAS,yBAAL,IAAI;AACoC,sBAAU,AAAK,IAAD;AACxD,iBAA+B,UAAW,QAAO;AAK7C,UAJF,AAAW,UAAD,UAAQ,MAAM,sBACtB,AAAQ,OAAD,qBACK,UAAU,sBACX,oBAAoB,WAAW,GAAG,UAAI,AAAQ,OAAD;;AAG5D,cAAO,WAAU;;AAEnB,UAAS,aAAL,IAAI;AACN,iBAAS,IAAI,GAAG,AAAE,CAAD,gBAAG,AAAK,IAAD,YAAS,IAAA,AAAC,CAAA;AAK9B,UAJF,AAAW,UAAD,UAAQ,MAAM,sBACtB,AAAI,IAAA,QAAC,CAAC,gBACM,UAAU,uBACX,oBAAoB,WAAW,GAAG,WAAI,AAAE,CAAD;;AAGtD,cAAO,WAAU;;AAEnB,UAAS,gCAAL,IAAI;AACN,gBAAO,UAAU;QACb,eAA8B,gDAAC,AAAY,WAAD,QAAM,MAAM,IAAI;;;AAMhE,oBAAI,0CAAS,IAAI;AACf,cAAO,mDAAiB,IAAI,EAAE,UAAU,EAAE,WAAW;;AAIvD,YAAO,WAAU;IACnB;;uDAGM,KACiB,MACD;AAHa;AAKA,qBAAU,MAAM,sBAAY,IAAI;AACjE,oBAAI,AAAQ,OAAD;AACK,gBAAI,wBAAQ,QAAQ,GAAG;AACR,QAA7B,AAAE,AAAQ,CAAT,kBAAgB,WAAW;AACF,QAA1B,AAAE,CAAD,QAAQ,AAAK,oBAAO,IAAI;AACzB,cAAO,EAAC;;AAGa,cAAI,2CAAiB,QAAQ,GAAG;AAC1B,MAA7B,AAAE,AAAQ,CAAT,kBAAgB,WAAW;AAY1B,MAXF,AAAE,AAAM,CAAP,eAAQ,cAAgB,AAAK,oBAAO,IAAI,gBAAe,QAAS;AAC/D,cAAW,gCAAP,MAAM;AACR,kBAAO;;AAKT,wBAAI,0CAAS,MAAM;AACjB,kBAAO;;AAET,gBAAc,YAAP,MAAM;;AAGiB,wBAAoC;AAC1C,qBAA0B;AAER,2BACxC,AAAQ,AAAQ,OAAT,+BAA0B;AAErC,eAAS,IAAI,GAAG,AAAE,CAAD,gBAAG,AAAe,cAAD,YAAS,IAAA,AAAC,CAAA;AACJ,oBAAQ,AAAc,cAAA,QAAC,CAAC;AACjD,0BAAc,AAAE,CAAD;AAG1B,QAFF,AAAY,WAAD,UAA8B,+CACvC,WAAW,EAAU,sBAAC,AAAM,KAAD;AAET,gBAAI,AAAM,KAAD;AAO3B,QANF,AAAS,QAAD,OAAK,qCACX,WAAW,EACX,AAAE,CAAD,aACD,AAAE,CAAD,uBACY,AAAE,CAAD,wBACJ,AAAE,CAAD;;AAI2B,MAA1C,AAAE,AAAM,CAAP,eAAQ,OAAS,AAAK,oBAAO,WAAW;AAEjB,MAAxB,AAAE,AAAM,CAAP,gBAAc,QAAQ;AACvB,YAAO,EAAC;IACV;;2EAGY,WACC,gBACA,mBACA;AAEgB,kBAA2B,0CACpD,WAA2B,0CAC3B,eAAgC;AAEX,eAAO;AAKE,IAAhC,AAAK,IAAD,QAAQ,AAAe,cAAD;AAG1B,QAAI,AAAW,UAAD,SAAS;AACO,MAA5B,AAAK,IAAD,QAAQ,AAAW,UAAD;;AAIxB,QAAI,AAAc,aAAD,SAAS;AACO,MAA/B,AAAK,IAAD,QAAQ,AAAc,aAAD;;AAMW,IAAtC,AAAQ,OAAD,UAAQ,AAAe,cAAD;AAG7B,QAAI,AAAW,UAAD,YAAY;AACU,MAAlC,AAAQ,OAAD,UAAQ,AAAW,UAAD;;AAI3B,QAAI,AAAc,aAAD,YAAY;AACU,MAArC,AAAQ,OAAD,UAAQ,AAAc,aAAD;;AAMmB,IAA9B,WAAnB,AAAO,OAAA,QAAC,uBAAkB,AAAe,cAAD;AAGxC,QAAI,AAAW,UAAD,YAAY;AACqB,MAA1B,WAAnB,AAAO,OAAA,QAAC,uBAAkB,AAAW,UAAD;;AAItC,QAAI,AAAc,aAAD,YAAY;AACqB,MAA7B,WAAnB,AAAO,OAAA,QAAC,uBAAkB,AAAc,aAAD;;AAMgB,IAAlC,WAAvB,AAAO,OAAA,QAAC,2BAAsB,AAAe,cAAD;AAG5C,QAAI,AAAW,UAAD,gBAAgB;AACyB,MAA9B,WAAvB,AAAO,OAAA,QAAC,2BAAsB,AAAW,UAAD;;AAI1C,QAAI,AAAc,aAAD,gBAAgB;AACyB,MAAjC,WAAvB,AAAO,OAAA,QAAC,2BAAsB,AAAc,aAAD;;AAIlB,eAAwB,0CACjD,iBAAiB,AAAU,SAAD,gBAC1B,aAAa,AAAU,SAAD;AAIxB,kBAAI,AAAK,IAAD;AACmC,MAAzC,AAAI,IAAA,QAAC,cAAgB,AAAU,SAAD;;AAGhC,kBAAI,AAAK,IAAD;AAC2C,MAAjD,AAAI,IAAA,QAAC,SAAW,kBAAU,AAAU,SAAD;;AAGrC,UAAO,kDACuB,wBAAnB,AAAO,OAAA,QAAC,mBACX,IAAI;EAEd;qDAEoD;AAAlB;AACtB,uBAAa,AAAS,QAAD;AAEhB,qBAAW,yCAA+B,QAAQ;AAEjD,0BAAe,MAAM,AAAS,AAAO,QAAR;AAChC,wBAAc,AAAS,QAAD,QAAQ,YAAY;AAE5B,yBACE,yBAAzB,AAAK,oBAAO,WAAW;AACT,wBAAc;AAEhC,UAAI,AAAY,YAAA,QAAC,aAAa;AAEyC,QADrE,AAAY,WAAD,UACiB,AAAkB,AAAe,aAAxD,AAAY,YAAA,QAAC;;AAGpB,UAAI,AAAY,YAAA,QAAC,WAAW;AACa,QAAvC,AAAY,WAAD,QAAQ,AAAY,YAAA,QAAC;;AAGlC,UAAI,AAAY,AAAK,WAAN,SAAS,QAAQ,AAAY,AAAO,WAAR,WAAW;AACpD,YAAe,aAAX,UAAU,IAAG,OAAkB,aAAX,UAAU,KAAI;AAGnC,UAFD,WAAM,kCACJ,AAAyC,6BAAxB,UAAU,mBAAE,WAAW;;AAGgB,QAA5D,WAAM,kCAAgB,AAAqC,qCAAZ,WAAW;;AAG5D,YAAO,YAAW;IACpB;;qFAOqD,UACvC;AACC,sBAAc,AAAS,AAAO,QAAR,gBAAS;AAE5C,QAAI,AAAY,WAAD,IAAI;AACjB,YAAO,SAAQ;;AAGD,oBAAsB,2BAAM,WAAW;AAC1C,kBAAU,AAAU,AAAU,SAAX,mBAAY;AAE5C,QAAI,AAAQ,OAAD,IAAI;AACb,YAAO,SAAQ;;AAGF,mBAAoB,2BAAU,OAAO;AAEpD,UAAO,AAAS,SAAD,IAAI,OAAO,QAAQ,GAAG,QAAQ;EAC/C;;;;IC1WO;;;;;;IACA;;;;;;WAEwB;AAC3B,UAAI,AAAQ,OAAD,iBAAiB;AACS,QAAnC,oBAAe,AAAQ,OAAD;;AAGxB,UAAI,AAAQ,OAAD,sBAAsB;AACc,QAA7C,yBAAoB,AAAQ,OAAD;;IAE/B;;;QAfO;QACA;IADA;IACA;;EACL;;;;;;;;;;;;;;;;;IAwBe;;;;;;IACI;;;;;;IACA;;;;;;IACD;;;;;;;;QATb;QACA;QACA;QACA;IAHA;IACA;IACA;IACA;;EACL;;;;;;;;;;;;;IAcwB;;;;;;IACC;;;;;;;;QALpB;QACA;IADA;IACA;;EACL;;;;;;;;;MCpCa,uCAAkB;YAAG,qDACtB,yBACK;;;MAGA,mCAAc;YAAoB,2CACrD,UAAU;;;MAGQ,mCAAc;YAAmB,0CACnD,UAAU,OACV,gBAAgB;;;MAGG,uCAAkB;YAAoB;;;MAEhD,uCAAkB;YAAG,uCACxB,kDACG,8CACA,kDACI;;;;;;;;ICJH;;;;;;IACE;;;;;;IACO;;;;;;;;QAPZ;QACA;QACA;IAFA;IACA;IACA;;EACL;;;;;;;;;;;IAoDW;;;;;;;;QA3CN;;AACF,4DACY,SAAW,WAAqB;AACT;AAE9B,iBAAa;AAAQ;AACZ,yBAAS,AAAO,AAAY,AAehC,OAfoB,CAAC,SAAS,gCAAM,QAAa;AAClD,kBAAI,AAAY,WAAD,WAAW;AACL,6BAAS,AAAY,AACnC,AACA,WAFkC,0CAC9B,QAAC,QAAsB,wCAAS,IAAI;AAG/B,+BAAW,6CACZ,SAAS,eACP,WAAW,aACb,+DAAkC,MAAM;AAG/B,gBAAtB,AAAY,YAAA,CAAC,QAAQ;;AAEvB,oBAAO,YAAW;wDACL,QAAC;AACA,6BAAW,6CACZ,SAAS,aACT,iEACQ,4BAAiB,KAAK;AAIrB,cAAtB,AAAY,YAAA,CAAC,QAAQ;AACV,cAAX,WAAM,KAAK;;AAGqB,YAAlC,MAAM,AAAW,UAAD,wCAAW,MAAM;AACT,YAAxB,MAAM,AAAW,UAAD;UAClB;;;AAE8D,QAA9D,aAAa,+CAAwC,QAAQ;AAE7D,cAAO,AAAW,WAAD;;;EAEpB;;;;;;;;;IC9BE;;;;;;;;QA1BF;;AACF,0DACY,SAAW,WAAqB;AACT;AAE9B,iBAAa;AAAQ;AACnB;AACe,2BAAQ,MAAM,AAAQ,QAAA;AAIjC,cAFF,AAAU,SAAD,YAAyC,qDAChD,WAA2B,yCAAC,iBAAiB,KAAK;;kBAE7C;AACmB,cAA1B,AAAW,UAAD,UAAU,KAAK;;AAGmB,YAA9C,MAAM,AAAW,UAAD,WAAW,AAAO,OAAA,CAAC,SAAS;AACpB,YAAxB,MAAM,AAAW,UAAD;UAClB;;;AAE8D,QAA9D,aAAa,+CAAwC,QAAQ;AAE7D,cAAO,AAAW,WAAD;;;EAEpB;;;;;;;;;;ICtBA;;;;;;IACkB;;;;;;;6CAFJ,IAAS;IAAT;IAAS;;EAAK;;;;;;;;;;;;IAOnB;;;;;;IACS;;;;;;oBAEE;AACzB,UAAS,aAAL,IAAI,KAAY,AAAK,AAAO,IAAR,cAAW,KAAa,YAAR,AAAI,IAAA,QAAC,IAAM,AAAM;AACvD,cAAO,WAAa,eAAR,AAAI,IAAA,QAAC;;AAGnB,YAAO;IACT;SAGoB;AAClB,oBAAI,AAAK,wBAAY,GAAG;AACT,oBAAQ,AAAI,iBAAC,GAAG;AAC7B,cAAa,yBAAN,KAAK,IACN,oCACE,KAAK,0BACQ,kCACU,yCAEzB,KAAK;;AAEb,YAAO,AAAM,iBAAK,GAAG;IACvB;UAGkB,KAAa;AACI,MAAjC,AAAM,qBAAU,GAAG,EAAE,KAAK,EAAE;IAC9B;;AAIiB;MAAU;;;IAEX;;IAEF;;6CAtCO;IAEI,eAAO;IAFX;;EAAM;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ICKV;;;;;;IAEV;;;;;;mBAEkB;AACrB,YAAK,AAA4B,cAAjC,IAAI,KAAY,AAAK,AAAO,IAAR,cAAW,KAAa,YAAR,AAAI,IAAA,QAAC,IAAM;IAAM;qBAE9B;AACzB,UAAS,aAAL,IAAI,eAAY,mBAAa,IAAI;AACnC,cAAO,WAAa,eAAR,AAAI,IAAA,QAAC;;AAGnB,YAAO;IACT;uBAGsB,MACT;AAEX,YAAO,qCACL,IAAI,0BACS,mCACD,UAAU;IAE1B;gCAEwC;AACtC,UAAS,aAAL,IAAI,eAAY,mBAAa,IAAI;AACnC,cAAO,uBAAyB,eAAR,AAAI,IAAA,QAAC;;AAG/B,YAAO;IACT;qBAMgC;AAC9B;AACE,cAAO,AAAqC,sCAA3B,2CAAoC,UAAK,GAAG;;YACtD;AACP,YAAU,2BAAN,KAAK;AAQN,UAPD,WAAM,gDACJ,AAGG,mDAFyB,GAAG,kLAG/B,KAAK,EACL,GAAG;;;IAIX;;AAIc,MAAZ,AAAK;IACP;SAOoB;AACL,kBAAc,WAAK,GAAG;AACnC,YAAa,yBAAN,KAAK,IAA0B,wBAAmB,KAAK,IAAI,KAAK;IACzE;qBAG8C;AAC5C,YAAa,aAAkB;AACvB,qBAAS,sBAAiB,IAAI;AACpC,YAAI,MAAM,IAAI;AACZ,gBAAe,uBAAC,aAAQ,MAAM;;AAEhC,cAAO;;;AAGT,YAAO,WAAU;IACnB;iBAG+B;AAChB,mBAAS,sBAAiB,IAAI;AAC3C,UAAI,MAAM,IAAI;AACZ,cAAe,uBAAC,aAAQ,MAAM;;AAEhC,YAAO;IACT;cAKS,KACA,OACa,MACT;AAEwB,MAAnC,AAAW,UAAD,IAAC,OAAX,aAAe,qBAAe,IAAI,IAAvB;AACX,UAAU,wBAAN,KAAK;AACD,qBAAS,yCAAoB,IAAI,EAAE,GAAG,EAAE,KAAK;AACnC,wBAAY,2BAC1B,UAAU,wBACW,0CAAqB,IAAI;AAGJ,QAA5C,AAAI,IAAA,QAAC,GAAG,EAAI,AAAU,SAAD,gBAAgB,MAAM;;AAI1B,QAAjB,AAAI,IAAA,QAAC,GAAG,EAAI,KAAK;;IAErB;UAKkB,KAAY;AACW,MAAvC,eAAU,GAAG,EAAE,KAAK,EAAE,qBAAM;IAC9B;;;QA9HiB;QACV;QACY;IAFF;IACV;AAEF,6GAAqB,aAAa;;EAAC;;;;;;;;;;;;;;;;;;;;;;IDoDlB;;;;;;SAKF;AACX,kBAAc,WAAK,GAAG;AAClB;AACX,eAAqB,QAAS;AAC5B,sBAAI,AAAM,AAAK,KAAN,oBAAkB,GAAG;AACf,0BAAY,AAAM,AAAI,KAAL,aAAM,GAAG;AACvC,cAAU,wBAAN,KAAK,KAAqC,wBAAV,SAAS;AAIzC,YAHF,QAAQ,wBAAgB,mCAChB,wBAAN,KAAK,GACL,SAAS;AAEuB,YAAlC,aAAwB;;AAGP,YAAjB,QAAQ,SAAS;;;;AAIvB,YAAa,yBAAN,KAAK,IACN,wBAAmB,KAAK,EAAE,UAAU,IACpC,KAAK;IACb;;AAE8B,gDAAgB;IAAK;qBAEtB;AACR,kBAAQ,AAAG,EAAD,SAAO;AACpC,UAAiB,aAAb,AAAM,KAAD,aAAU;AACjB,cAAO,AAAM,MAAD;;AAEd,YAAO;IACT;sBAE4B;AACxB,YAAA,AAAkB,AACM,qCADK,QAAiB,SAAU,AAAM,AAAG,KAAJ,OAAO,EAAE,qCAC1D,cAAM,yBAClB;IAAI;iBAMe;AACR,qBAAW,qBAAe,EAAE;AACzC,YAAO,AAAS,AAAQ,SAAT,IAAI,kBAAQ,sBAAgB,QAAQ;IACrD;wBAYS,OACQ;AAEO,kBAA0B,8BAAlB,AAAS,SAAA,CAAC;AACxC,oBAAI,iBAAW,KAAK;AACuC,QAAzD,AAAkB,6BAAI,mCAAgB,KAAK,EAAE,AAAM,KAAD;;IAEtD;0BAOkC;AAI/B,MAHD,AAAkB,qCAChB,QAAiB,SACb,AAAM,AAAG,AAAY,KAAhB,OAAO,QAAQ,IAAI,AAAyB,qBAAV,AAAM,KAAD,QAAQ,QAAQ;IAEpE;;;QA3F6B;QACpB;QACU;IAQG,0BAAqC;AAPtD,2EACqB,gBAAgB,UAC1B,MAAM,iBACC,aAAa;;EAC7B;;;;;;;;;;;;;;;;;;;oFC6E8B;AACrC,QAAW,wBAAP,MAAM,eACN,AAAO,MAAD,eAAa,4BACnB,AAAO,MAAD,eAAa;AACrB,YAAgD,UAAtC,AAAM,MAAA,QAAC,iBAAc,eAAG,AAAM,MAAA,QAAC;;AAE3C,UAAO;EACT;4EAOoD;AAClD,aAAK,WAAkB,KAAY,OAAiB;AACrC,gBAAW,AAAgB,kBAApB,GAAG,SAAkB;AACzC,UAAU,wBAAN,KAAK;AACD,qBAAS,yCAAoB,IAAI,EAAE,GAAG,EAAE,KAAK;AACP,QAA5C,AAAI,IAAA,QAAC,GAAG,EAAI,AAAU,SAAD,gBAAgB,MAAM;;AAI1B,QAAjB,AAAI,IAAA,QAAC,GAAG,EAAI,KAAK;AACjB;;;;AAIJ,UAAO,WAAU;EACnB;0EAIwB,MAAa,KAAyB;AACtD,mBAAW,AAAI,IAAA,QAAC,GAAG;AACzB,UAAiB,yBAAT,QAAQ,IACV,wBAAgB,mCAAC,QAAQ,EAAE,KAAK,MAChC,KAAK;EACb","file":"client.ddc.js"}');
  // Exports:
  return {
    src__cache__cache: cache,
    src__utilities__helpers: helpers,
    src__cache__lazy_cache_map: lazy_cache_map,
    src__utilities__traverse: traverse,
    src__exceptions__graphql_error: graphql_error,
    src__exceptions___base_exceptions: _base_exceptions,
    src__cache__in_memory_html: in_memory_html,
    src__link__web_socket__link_web_socket: link_web_socket,
    src__core__query_manager: query_manager,
    src__scheduler__scheduler: scheduler,
    src__core__observable_query: observable_query,
    src__core__query_result: query_result,
    src__exceptions__exceptions: exceptions,
    src__exceptions__network_exception_stub: network_exception_stub,
    src__exceptions__operation_exception: operation_exception,
    src__exceptions__io_network_exception: io_network_exception,
    src__core__query_options: query_options,
    internal: internal,
    client: client$,
    src__graphql_client: graphql_client,
    src__link__http__link_http: link_http,
    src__link__http__http_config: http_config,
    src__link__http__fallback_http_config: fallback_http_config,
    src__link__error__link_error: link_error,
    src__link__auth__link_auth: link_auth,
    src__cache__optimistic: optimistic,
    src__cache__normalized_in_memory: normalized_in_memory,
    src__cache__in_memory: in_memory
  };
});

//# sourceMappingURL=client.ddc.js.map
