define(['dart_sdk', 'packages/graphql/src/core/raw_operation_data', 'packages/rxdart/futures', 'packages/websocket/src/websocket_browser', 'packages/uuid_enhanced/uuid'], function(dart_sdk, packages__graphql__src__core__raw_operation_data, packages__rxdart__futures, packages__websocket__src__websocket_browser, packages__uuid_enhanced__uuid) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const _js_helper = dart_sdk._js_helper;
  const _interceptors = dart_sdk._interceptors;
  const convert = dart_sdk.convert;
  const typed_data = dart_sdk.typed_data;
  const collection = dart_sdk.collection;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const operation = packages__graphql__src__core__raw_operation_data.src__link__operation;
  const messages = packages__graphql__src__core__raw_operation_data.src__websocket__messages;
  const behavior_subject = packages__rxdart__futures.src__subjects__behavior_subject;
  const websocket_browser = packages__websocket__src__websocket_browser.src__websocket_browser;
  const uuid = packages__uuid_enhanced__uuid.uuid;
  const link = Object.create(dart.library);
  const fetch_result = Object.create(dart.library);
  const socket_client = Object.create(dart.library);
  const $isNotEmpty = dartx.isNotEmpty;
  const $reduce = dartx.reduce;
  const $values = dartx.values;
  const $_get = dartx._get;
  const $remove = dartx.remove;
  const $_set = dartx._set;
  let LinkAndLinkToLink = () => (LinkAndLinkToLink = dart.constFn(dart.fnType(link.Link, [link.Link, link.Link])))();
  let StreamOfFetchResult = () => (StreamOfFetchResult = dart.constFn(async.Stream$(fetch_result.FetchResult)))();
  let OperationToStreamOfFetchResult = () => (OperationToStreamOfFetchResult = dart.constFn(dart.fnType(StreamOfFetchResult(), [operation.Operation])))();
  let OperationAndFnToStreamOfFetchResult = () => (OperationAndFnToStreamOfFetchResult = dart.constFn(dart.fnType(StreamOfFetchResult(), [operation.Operation], [OperationToStreamOfFetchResult()])))();
  let BehaviorSubjectOfSocketConnectionState = () => (BehaviorSubjectOfSocketConnectionState = dart.constFn(behavior_subject.BehaviorSubject$(socket_client.SocketConnectionState)))();
  let IdentityMapOfString$Function = () => (IdentityMapOfString$Function = dart.constFn(_js_helper.IdentityMap$(core.String, core.Function)))();
  let dynamicToGraphQLSocketMessage = () => (dynamicToGraphQLSocketMessage = dart.constFn(dart.fnType(messages.GraphQLSocketMessage, [dart.dynamic])))();
  let EventSinkOfConnectionKeepAlive = () => (EventSinkOfConnectionKeepAlive = dart.constFn(async.EventSink$(messages.ConnectionKeepAlive)))();
  let EventSinkOfConnectionKeepAliveToNull = () => (EventSinkOfConnectionKeepAliveToNull = dart.constFn(dart.fnType(core.Null, [EventSinkOfConnectionKeepAlive()])))();
  let dynamicToNull = () => (dynamicToNull = dart.constFn(dart.fnType(core.Null, [dart.dynamic])))();
  let VoidToNull = () => (VoidToNull = dart.constFn(dart.fnType(core.Null, [])))();
  let FutureOfvoid = () => (FutureOfvoid = dart.constFn(async.Future$(dart.void)))();
  let VoidToFutureOfvoid = () => (VoidToFutureOfvoid = dart.constFn(dart.fnType(FutureOfvoid(), [])))();
  let JSArrayOfFuture = () => (JSArrayOfFuture = dart.constFn(_interceptors.JSArray$(async.Future)))();
  let MapOfString$dynamic = () => (MapOfString$dynamic = dart.constFn(core.Map$(core.String, dart.dynamic)))();
  let IdentityMapOfString$dynamic = () => (IdentityMapOfString$dynamic = dart.constFn(_js_helper.IdentityMap$(core.String, dart.dynamic)))();
  let dynamicTodynamic = () => (dynamicTodynamic = dart.constFn(dart.fnType(dart.dynamic, [dart.dynamic])))();
  let StreamControllerOfSubscriptionData = () => (StreamControllerOfSubscriptionData = dart.constFn(async.StreamController$(messages.SubscriptionData)))();
  let SocketConnectionStateTobool = () => (SocketConnectionStateTobool = dart.constFn(dart.fnType(core.bool, [socket_client.SocketConnectionState])))();
  let EventSinkOfSocketConnectionState = () => (EventSinkOfSocketConnectionState = dart.constFn(async.EventSink$(socket_client.SocketConnectionState)))();
  let EventSinkOfSocketConnectionStateToNull = () => (EventSinkOfSocketConnectionStateToNull = dart.constFn(dart.fnType(core.Null, [EventSinkOfSocketConnectionState()])))();
  let GraphQLSocketMessageTobool = () => (GraphQLSocketMessageTobool = dart.constFn(dart.fnType(core.bool, [messages.GraphQLSocketMessage])))();
  let EventSinkOfGraphQLSocketMessage = () => (EventSinkOfGraphQLSocketMessage = dart.constFn(async.EventSink$(messages.GraphQLSocketMessage)))();
  let EventSinkOfGraphQLSocketMessageToNull = () => (EventSinkOfGraphQLSocketMessageToNull = dart.constFn(dart.fnType(core.Null, [EventSinkOfGraphQLSocketMessage()])))();
  let GraphQLSocketMessageToFuture = () => (GraphQLSocketMessageToFuture = dart.constFn(dart.fnType(async.Future, [messages.GraphQLSocketMessage])))();
  let SubscriptionDataTovoid = () => (SubscriptionDataTovoid = dart.constFn(dart.fnType(dart.void, [messages.SubscriptionData])))();
  let GraphQLSocketMessageTovoid = () => (GraphQLSocketMessageTovoid = dart.constFn(dart.fnType(dart.void, [messages.GraphQLSocketMessage])))();
  let SocketConnectionStateToNull = () => (SocketConnectionStateToNull = dart.constFn(dart.fnType(core.Null, [socket_client.SocketConnectionState])))();
  const CT = Object.create(null);
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.const({
        __proto__: core.Duration.prototype,
        [Duration__duration]: 10000000
      });
    },
    get C1() {
      return C1 = dart.const({
        __proto__: core.Duration.prototype,
        [Duration__duration]: 30000000
      });
    },
    get C2() {
      return C2 = dart.const({
        __proto__: core.Duration.prototype,
        [Duration__duration]: 5000000
      });
    },
    get C3() {
      return C3 = dart.const({
        __proto__: socket_client.SocketConnectionState.prototype,
        [_name$]: "SocketConnectionState.NOT_CONNECTED",
        index: 0
      });
    },
    get C4() {
      return C4 = dart.const({
        __proto__: socket_client.SocketConnectionState.prototype,
        [_name$]: "SocketConnectionState.CONNECTING",
        index: 1
      });
    },
    get C5() {
      return C5 = dart.const({
        __proto__: socket_client.SocketConnectionState.prototype,
        [_name$]: "SocketConnectionState.CONNECTED",
        index: 2
      });
    },
    get C6() {
      return C6 = dart.constList([C3 || CT.C3, C4 || CT.C4, C5 || CT.C5], socket_client.SocketConnectionState);
    },
    get C7() {
      return C7 = dart.constList(["graphql-ws"], core.String);
    },
    get C8() {
      return C8 = dart.const({
        __proto__: socket_client.SocketClientConfig.prototype,
        [initPayload$]: null,
        [queryAndMutationTimeout$]: C0 || CT.C0,
        [delayBetweenReconnectionAttempts$]: C2 || CT.C2,
        [inactivityTimeout$]: C1 || CT.C1,
        [autoReconnect$]: true
      });
    },
    get C9() {
      return C9 = dart.fn(socket_client.SocketClient._parseSocketMessage, dynamicToGraphQLSocketMessage());
    }
  });
  const request$ = dart.privateName(link, "Link.request");
  link.Link = class Link extends core.Object {
    get request() {
      return this[request$];
    }
    set request(value) {
      this[request$] = value;
    }
    static from(links) {
      if (!dart.test(links[$isNotEmpty])) dart.assertFailed(null, "org-dartlang-app:///packages/graphql/src/link/link.dart", 35, 12, "links.isNotEmpty");
      return links[$reduce](dart.fn((first, second) => first.concat(second), LinkAndLinkToLink()));
    }
    concat(next) {
      return link._concat(this, next);
    }
  };
  (link.Link.new = function(opts) {
    let request = opts && 'request' in opts ? opts.request : null;
    this[request$] = request;
    ;
  }).prototype = link.Link.prototype;
  dart.addTypeTests(link.Link);
  dart.setMethodSignature(link.Link, () => ({
    __proto__: dart.getMethods(link.Link.__proto__),
    concat: dart.fnType(link.Link, [link.Link])
  }));
  dart.setLibraryUri(link.Link, "package:graphql/src/link/link.dart");
  dart.setFieldSignature(link.Link, () => ({
    __proto__: dart.getFields(link.Link.__proto__),
    request: dart.fieldType(dart.fnType(async.Stream$(fetch_result.FetchResult), [operation.Operation], [dart.fnType(async.Stream$(fetch_result.FetchResult), [operation.Operation])]))
  }));
  link._concat = function _concat(first, second) {
    return new link.Link.new({request: dart.fn((operation, forward = null) => first.request(operation, dart.fn(op => second.request(op, forward), OperationToStreamOfFetchResult())), OperationAndFnToStreamOfFetchResult())});
  };
  link.execute = function execute(opts) {
    let link = opts && 'link' in opts ? opts.link : null;
    let operation = opts && 'operation' in opts ? opts.operation : null;
    return link.request(operation);
  };
  const statusCode$ = dart.privateName(fetch_result, "FetchResult.statusCode");
  const reasonPhrase$ = dart.privateName(fetch_result, "FetchResult.reasonPhrase");
  const errors$ = dart.privateName(fetch_result, "FetchResult.errors");
  const data$ = dart.privateName(fetch_result, "FetchResult.data");
  const extensions$ = dart.privateName(fetch_result, "FetchResult.extensions");
  const context$ = dart.privateName(fetch_result, "FetchResult.context");
  fetch_result.FetchResult = class FetchResult extends core.Object {
    get statusCode() {
      return this[statusCode$];
    }
    set statusCode(value) {
      this[statusCode$] = value;
    }
    get reasonPhrase() {
      return this[reasonPhrase$];
    }
    set reasonPhrase(value) {
      this[reasonPhrase$] = value;
    }
    get errors() {
      return this[errors$];
    }
    set errors(value) {
      this[errors$] = value;
    }
    get data() {
      return this[data$];
    }
    set data(value) {
      this[data$] = value;
    }
    get extensions() {
      return this[extensions$];
    }
    set extensions(value) {
      this[extensions$] = value;
    }
    get context() {
      return this[context$];
    }
    set context(value) {
      this[context$] = value;
    }
  };
  (fetch_result.FetchResult.new = function(opts) {
    let statusCode = opts && 'statusCode' in opts ? opts.statusCode : null;
    let reasonPhrase = opts && 'reasonPhrase' in opts ? opts.reasonPhrase : null;
    let errors = opts && 'errors' in opts ? opts.errors : null;
    let data = opts && 'data' in opts ? opts.data : null;
    let extensions = opts && 'extensions' in opts ? opts.extensions : null;
    let context = opts && 'context' in opts ? opts.context : null;
    this[statusCode$] = statusCode;
    this[reasonPhrase$] = reasonPhrase;
    this[errors$] = errors;
    this[data$] = data;
    this[extensions$] = extensions;
    this[context$] = context;
    ;
  }).prototype = fetch_result.FetchResult.prototype;
  dart.addTypeTests(fetch_result.FetchResult);
  dart.setLibraryUri(fetch_result.FetchResult, "package:graphql/src/link/fetch_result.dart");
  dart.setFieldSignature(fetch_result.FetchResult, () => ({
    __proto__: dart.getFields(fetch_result.FetchResult.__proto__),
    statusCode: dart.fieldType(core.int),
    reasonPhrase: dart.fieldType(core.String),
    errors: dart.fieldType(core.List),
    data: dart.fieldType(dart.dynamic),
    extensions: dart.fieldType(core.Map$(core.String, dart.dynamic)),
    context: dart.fieldType(core.Map$(core.String, dart.dynamic))
  }));
  const Duration__duration = dart.privateName(core, "Duration._duration");
  let C0;
  let C1;
  let C2;
  const autoReconnect$ = dart.privateName(socket_client, "SocketClientConfig.autoReconnect");
  const inactivityTimeout$ = dart.privateName(socket_client, "SocketClientConfig.inactivityTimeout");
  const delayBetweenReconnectionAttempts$ = dart.privateName(socket_client, "SocketClientConfig.delayBetweenReconnectionAttempts");
  const queryAndMutationTimeout$ = dart.privateName(socket_client, "SocketClientConfig.queryAndMutationTimeout");
  const initPayload$ = dart.privateName(socket_client, "SocketClientConfig.initPayload");
  socket_client.SocketClientConfig = class SocketClientConfig extends core.Object {
    get autoReconnect() {
      return this[autoReconnect$];
    }
    set autoReconnect(value) {
      super.autoReconnect = value;
    }
    get inactivityTimeout() {
      return this[inactivityTimeout$];
    }
    set inactivityTimeout(value) {
      super.inactivityTimeout = value;
    }
    get delayBetweenReconnectionAttempts() {
      return this[delayBetweenReconnectionAttempts$];
    }
    set delayBetweenReconnectionAttempts(value) {
      super.delayBetweenReconnectionAttempts = value;
    }
    get queryAndMutationTimeout() {
      return this[queryAndMutationTimeout$];
    }
    set queryAndMutationTimeout(value) {
      super.queryAndMutationTimeout = value;
    }
    get initPayload() {
      return this[initPayload$];
    }
    set initPayload(value) {
      super.initPayload = value;
    }
    get initOperation() {
      return new messages.InitOperation.new(this.initPayload);
    }
  };
  (socket_client.SocketClientConfig.new = function(opts) {
    let autoReconnect = opts && 'autoReconnect' in opts ? opts.autoReconnect : true;
    let queryAndMutationTimeout = opts && 'queryAndMutationTimeout' in opts ? opts.queryAndMutationTimeout : C0 || CT.C0;
    let inactivityTimeout = opts && 'inactivityTimeout' in opts ? opts.inactivityTimeout : C1 || CT.C1;
    let delayBetweenReconnectionAttempts = opts && 'delayBetweenReconnectionAttempts' in opts ? opts.delayBetweenReconnectionAttempts : C2 || CT.C2;
    let initPayload = opts && 'initPayload' in opts ? opts.initPayload : null;
    this[autoReconnect$] = autoReconnect;
    this[queryAndMutationTimeout$] = queryAndMutationTimeout;
    this[inactivityTimeout$] = inactivityTimeout;
    this[delayBetweenReconnectionAttempts$] = delayBetweenReconnectionAttempts;
    this[initPayload$] = initPayload;
    ;
  }).prototype = socket_client.SocketClientConfig.prototype;
  dart.addTypeTests(socket_client.SocketClientConfig);
  dart.setGetterSignature(socket_client.SocketClientConfig, () => ({
    __proto__: dart.getGetters(socket_client.SocketClientConfig.__proto__),
    initOperation: messages.InitOperation
  }));
  dart.setLibraryUri(socket_client.SocketClientConfig, "package:graphql/src/socket_client.dart");
  dart.setFieldSignature(socket_client.SocketClientConfig, () => ({
    __proto__: dart.getFields(socket_client.SocketClientConfig.__proto__),
    autoReconnect: dart.finalFieldType(core.bool),
    inactivityTimeout: dart.finalFieldType(core.Duration),
    delayBetweenReconnectionAttempts: dart.finalFieldType(core.Duration),
    queryAndMutationTimeout: dart.finalFieldType(core.Duration),
    initPayload: dart.finalFieldType(dart.dynamic)
  }));
  const _name$ = dart.privateName(socket_client, "_name");
  let C3;
  let C4;
  let C5;
  let C6;
  socket_client.SocketConnectionState = class SocketConnectionState extends core.Object {
    toString() {
      return this[_name$];
    }
  };
  (socket_client.SocketConnectionState.new = function(index, _name) {
    this.index = index;
    this[_name$] = _name;
    ;
  }).prototype = socket_client.SocketConnectionState.prototype;
  dart.addTypeTests(socket_client.SocketConnectionState);
  dart.setLibraryUri(socket_client.SocketConnectionState, "package:graphql/src/socket_client.dart");
  dart.setFieldSignature(socket_client.SocketConnectionState, () => ({
    __proto__: dart.getFields(socket_client.SocketConnectionState.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(socket_client.SocketConnectionState, ['toString']);
  socket_client.SocketConnectionState.NOT_CONNECTED = C3 || CT.C3;
  socket_client.SocketConnectionState.CONNECTING = C4 || CT.C4;
  socket_client.SocketConnectionState.CONNECTED = C5 || CT.C5;
  socket_client.SocketConnectionState.values = C6 || CT.C6;
  let C7;
  let C8;
  const _connectionStateController = dart.privateName(socket_client, "_connectionStateController");
  const _subscriptionInitializers = dart.privateName(socket_client, "_subscriptionInitializers");
  const _connectionWasLost = dart.privateName(socket_client, "_connectionWasLost");
  const _reconnectTimer = dart.privateName(socket_client, "_reconnectTimer");
  const _socket = dart.privateName(socket_client, "_socket");
  const _messageStream = dart.privateName(socket_client, "_messageStream");
  const _keepAliveSubscription = dart.privateName(socket_client, "_keepAliveSubscription");
  const _messageSubscription = dart.privateName(socket_client, "_messageSubscription");
  const _connect = dart.privateName(socket_client, "_connect");
  const _write = dart.privateName(socket_client, "_write");
  let C9;
  const _messagesOfType = dart.privateName(socket_client, "_messagesOfType");
  const randomBytesForUuid$ = dart.privateName(socket_client, "SocketClient.randomBytesForUuid");
  const url$ = dart.privateName(socket_client, "SocketClient.url");
  const config$ = dart.privateName(socket_client, "SocketClient.config");
  const protocols$ = dart.privateName(socket_client, "SocketClient.protocols");
  socket_client.SocketClient = class SocketClient extends core.Object {
    get randomBytesForUuid() {
      return this[randomBytesForUuid$];
    }
    set randomBytesForUuid(value) {
      this[randomBytesForUuid$] = value;
    }
    get url() {
      return this[url$];
    }
    set url(value) {
      super.url = value;
    }
    get config() {
      return this[config$];
    }
    set config(value) {
      super.config = value;
    }
    get protocols() {
      return this[protocols$];
    }
    set protocols(value) {
      super.protocols = value;
    }
    get socket() {
      return this[_socket];
    }
    [_connect]() {
      return async.async(dart.void, (function* _connect() {
        if (dart.test(this[_connectionStateController].isClosed)) {
          return;
        }
        this[_connectionStateController].value = socket_client.SocketConnectionState.CONNECTING;
        core.print("Connecting to websocket: " + dart.str(this.url) + "...");
        try {
          this[_socket] = (yield websocket_browser.WebSocket.connect(this.url, {protocols: this.protocols}));
          this[_connectionStateController].value = socket_client.SocketConnectionState.CONNECTED;
          core.print("Connected to websocket.");
          this[_write](this.config.initOperation);
          this[_messageStream] = this[_socket].stream.map(messages.GraphQLSocketMessage, C9 || CT.C9);
          if (this.config.inactivityTimeout != null) {
            this[_keepAliveSubscription] = this[_messagesOfType](messages.ConnectionKeepAlive).timeout(this.config.inactivityTimeout, {onTimeout: dart.fn(event => {
                core.print("Haven't received keep alive message for " + dart.str(this.config.inactivityTimeout.inSeconds) + " seconds. Disconnecting..");
                event.close();
                this[_socket].close(1001);
                this[_connectionStateController].value = socket_client.SocketConnectionState.NOT_CONNECTED;
              }, EventSinkOfConnectionKeepAliveToNull())}).listen(null);
          }
          this[_messageSubscription] = this[_messageStream].listen(dart.fn(data => {
          }, dynamicToNull()), {onDone: dart.fn(() => {
              this.onConnectionLost();
            }, VoidToNull()), cancelOnError: true, onError: dart.fn(e => {
              core.print("error: " + dart.str(e));
            }, dynamicToNull())});
          if (dart.test(this[_connectionWasLost])) {
            for (let callback of this[_subscriptionInitializers][$values]) {
              dart.dcall(callback, []);
            }
            this[_connectionWasLost] = false;
          }
        } catch (e$) {
          let e = dart.getThrown(e$);
          this.onConnectionLost(e);
        }
      }).bind(this));
    }
    onConnectionLost(e = null) {
      let t1, t1$, t1$0;
      if (e != null) {
        core.print("There was an error causing connection lost: " + dart.str(e));
      }
      core.print("Disconnected from websocket.");
      t1 = this[_reconnectTimer];
      t1 == null ? null : t1.cancel();
      t1$ = this[_keepAliveSubscription];
      t1$ == null ? null : t1$.cancel();
      t1$0 = this[_messageSubscription];
      t1$0 == null ? null : t1$0.cancel();
      if (dart.test(this[_connectionStateController].isClosed)) {
        return;
      }
      this[_connectionWasLost] = true;
      if (!dart.equals(this[_connectionStateController].value, socket_client.SocketConnectionState.NOT_CONNECTED)) {
        this[_connectionStateController].value = socket_client.SocketConnectionState.NOT_CONNECTED;
      }
      if (dart.test(this.config.autoReconnect) && !dart.test(this[_connectionStateController].isClosed)) {
        if (this.config.delayBetweenReconnectionAttempts != null) {
          core.print("Scheduling to connect in " + dart.str(this.config.delayBetweenReconnectionAttempts.inSeconds) + " seconds...");
          this[_reconnectTimer] = async.Timer.new(this.config.delayBetweenReconnectionAttempts, dart.fn(() => {
            this[_connect]();
          }, VoidToNull()));
        } else {
          async.Timer.run(dart.fn(() => this[_connect](), VoidToFutureOfvoid()));
        }
      }
    }
    dispose() {
      return async.async(dart.void, (function* dispose() {
        let t1, t1$, t1$0, t1$1, t1$2;
        core.print("Disposing socket client..");
        t1 = this[_reconnectTimer];
        t1 == null ? null : t1.cancel();
        yield async.Future.wait(dart.dynamic, JSArrayOfFuture().of([(t1$ = this[_socket], t1$ == null ? null : t1$.close()), (t1$0 = this[_keepAliveSubscription], t1$0 == null ? null : t1$0.cancel()), (t1$1 = this[_messageSubscription], t1$1 == null ? null : t1$1.cancel()), (t1$2 = this[_connectionStateController], t1$2 == null ? null : t1$2.close())]));
      }).bind(this));
    }
    static _parseSocketMessage(message) {
      let t1, t1$, t1$0;
      let map = MapOfString$dynamic().as(convert.json.decode(core.String.as(message)));
      let type = core.String.as((t1 = map[$_get]("type"), t1 == null ? "unknown" : t1));
      let payload = (t1$ = map[$_get]("payload"), t1$ == null ? new (IdentityMapOfString$dynamic()).new() : t1$);
      let id = core.String.as((t1$0 = map[$_get]("id"), t1$0 == null ? "none" : t1$0));
      switch (type) {
        case "connection_ack":
        {
          return new messages.ConnectionAck.new();
        }
        case "connection_error":
        {
          return new messages.ConnectionError.new(payload);
        }
        case "ka":
        {
          return new messages.ConnectionKeepAlive.new();
        }
        case "data":
        {
          let data = dart.dsend(payload, '_get', ["data"]);
          let errors = dart.dsend(payload, '_get', ["errors"]);
          return new messages.SubscriptionData.new(id, data, errors);
        }
        case "error":
        {
          return new messages.SubscriptionError.new(id, payload);
        }
        case "complete":
        {
          return new messages.SubscriptionComplete.new(id);
        }
        default:
        {
          return new messages.UnknownData.new(map);
        }
      }
    }
    [_write](message) {
      if (dart.equals(this[_connectionStateController].value, socket_client.SocketConnectionState.CONNECTED)) {
        this[_socket].add(convert.json.encode(message, {toEncodable: dart.fn(m => dart.dsend(m, 'toJson', []), dynamicTodynamic())}));
      }
    }
    subscribe(payload, waitForConnection) {
      let id = dart.toString(uuid.Uuid.randomUuid({random: this.randomBytesForUuid}));
      let response = StreamControllerOfSubscriptionData().new();
      let sub = null;
      let addTimeout = !dart.test(payload.operation.isSubscription) && this.config.queryAndMutationTimeout != null;
      let onListen = dart.fn(() => {
        let waitForConnectedStateWithoutTimeout = this[_connectionStateController].startWith(dart.test(waitForConnection) ? null : socket_client.SocketConnectionState.CONNECTED).where(dart.fn(state => dart.equals(state, socket_client.SocketConnectionState.CONNECTED), SocketConnectionStateTobool())).take(1);
        let waitForConnectedState = addTimeout ? waitForConnectedStateWithoutTimeout.timeout(this.config.queryAndMutationTimeout, {onTimeout: dart.fn(event => {
            core.print("Connection timed out.");
            response.addError(new async.TimeoutException.new("Connection timed out."));
            event.close();
            response.close();
          }, EventSinkOfSocketConnectionStateToNull())}) : waitForConnectedStateWithoutTimeout;
        sub = waitForConnectedState.listen(dart.fn(_ => {
          let dataErrorComplete = this[_messageStream].where(dart.fn(message => {
            if (messages.SubscriptionData.is(message)) {
              return message.id == id;
            }
            if (messages.SubscriptionError.is(message)) {
              return message.id == id;
            }
            if (messages.SubscriptionComplete.is(message)) {
              return message.id == id;
            }
            return false;
          }, GraphQLSocketMessageTobool())).takeWhile(dart.fn(_ => !dart.test(response.isClosed), GraphQLSocketMessageTobool()));
          let subscriptionComplete = addTimeout ? dataErrorComplete.where(dart.fn(message => messages.SubscriptionComplete.is(message), GraphQLSocketMessageTobool())).take(1).timeout(this.config.queryAndMutationTimeout, {onTimeout: dart.fn(event => {
              core.print("Request timed out.");
              response.addError(new async.TimeoutException.new("Request timed out."));
              event.close();
              response.close();
            }, EventSinkOfGraphQLSocketMessageToNull())}) : dataErrorComplete.where(dart.fn(message => messages.SubscriptionComplete.is(message), GraphQLSocketMessageTobool())).take(1);
          subscriptionComplete.listen(dart.fn(_ => response.close(), GraphQLSocketMessageToFuture()));
          dataErrorComplete.where(dart.fn(message => messages.SubscriptionData.is(message), GraphQLSocketMessageTobool())).cast(messages.SubscriptionData).listen(dart.fn(message => response.add(message), SubscriptionDataTovoid()));
          dataErrorComplete.where(dart.fn(message => messages.SubscriptionError.is(message), GraphQLSocketMessageTobool())).listen(dart.fn(message => response.addError(message), GraphQLSocketMessageTovoid()));
          this[_write](new messages.StartOperation.new(id, payload));
        }, SocketConnectionStateToNull()));
      }, VoidToNull());
      response.onListen = onListen;
      response.onCancel = dart.fn(() => {
        let t1;
        this[_subscriptionInitializers][$remove](id);
        t1 = sub;
        t1 == null ? null : t1.cancel();
        if (dart.equals(this[_connectionStateController].value, socket_client.SocketConnectionState.CONNECTED) && this[_socket] != null) {
          this[_write](new messages.StopOperation.new(id));
        }
      }, VoidToNull());
      this[_subscriptionInitializers][$_set](id, onListen);
      return response.stream;
    }
    get connectionState() {
      return this[_connectionStateController].stream;
    }
    [_messagesOfType](M) {
      return this[_messageStream].where(dart.fn(message => M.is(message), GraphQLSocketMessageTobool())).cast(M);
    }
  };
  (socket_client.SocketClient.new = function(url, opts) {
    let protocols = opts && 'protocols' in opts ? opts.protocols : C7 || CT.C7;
    let config = opts && 'config' in opts ? opts.config : C8 || CT.C8;
    let randomBytesForUuid = opts && 'randomBytesForUuid' in opts ? opts.randomBytesForUuid : null;
    this[_connectionStateController] = BehaviorSubjectOfSocketConnectionState().new();
    this[_subscriptionInitializers] = new (IdentityMapOfString$Function()).new();
    this[_connectionWasLost] = false;
    this[_reconnectTimer] = null;
    this[_socket] = null;
    this[_messageStream] = null;
    this[_keepAliveSubscription] = null;
    this[_messageSubscription] = null;
    this[url$] = url;
    this[protocols$] = protocols;
    this[config$] = config;
    this[randomBytesForUuid$] = randomBytesForUuid;
    this[_connect]();
  }).prototype = socket_client.SocketClient.prototype;
  dart.addTypeTests(socket_client.SocketClient);
  dart.setMethodSignature(socket_client.SocketClient, () => ({
    __proto__: dart.getMethods(socket_client.SocketClient.__proto__),
    [_connect]: dart.fnType(async.Future$(dart.void), []),
    onConnectionLost: dart.fnType(dart.void, [], [dart.dynamic]),
    dispose: dart.fnType(async.Future$(dart.void), []),
    [_write]: dart.fnType(dart.void, [messages.GraphQLSocketMessage]),
    subscribe: dart.fnType(async.Stream$(messages.SubscriptionData), [messages.SubscriptionRequest, core.bool]),
    [_messagesOfType]: dart.gFnType(M => [async.Stream$(M), []], M => [messages.GraphQLSocketMessage])
  }));
  dart.setGetterSignature(socket_client.SocketClient, () => ({
    __proto__: dart.getGetters(socket_client.SocketClient.__proto__),
    socket: websocket_browser.WebSocket,
    connectionState: async.Stream$(socket_client.SocketConnectionState)
  }));
  dart.setLibraryUri(socket_client.SocketClient, "package:graphql/src/socket_client.dart");
  dart.setFieldSignature(socket_client.SocketClient, () => ({
    __proto__: dart.getFields(socket_client.SocketClient.__proto__),
    randomBytesForUuid: dart.fieldType(typed_data.Uint8List),
    url: dart.finalFieldType(core.String),
    config: dart.finalFieldType(socket_client.SocketClientConfig),
    protocols: dart.finalFieldType(core.Iterable$(core.String)),
    [_connectionStateController]: dart.finalFieldType(behavior_subject.BehaviorSubject$(socket_client.SocketConnectionState)),
    [_subscriptionInitializers]: dart.finalFieldType(collection.HashMap$(core.String, core.Function)),
    [_connectionWasLost]: dart.fieldType(core.bool),
    [_reconnectTimer]: dart.fieldType(async.Timer),
    [_socket]: dart.fieldType(websocket_browser.WebSocket),
    [_messageStream]: dart.fieldType(async.Stream$(messages.GraphQLSocketMessage)),
    [_keepAliveSubscription]: dart.fieldType(async.StreamSubscription$(messages.ConnectionKeepAlive)),
    [_messageSubscription]: dart.fieldType(async.StreamSubscription$(messages.GraphQLSocketMessage))
  }));
  dart.trackLibraries("packages/graphql/src/link/fetch_result", {
    "package:graphql/src/link/link.dart": link,
    "package:graphql/src/link/fetch_result.dart": fetch_result,
    "package:graphql/src/socket_client.dart": socket_client
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["link.dart","fetch_result.dart","../socket_client.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IA+BiB;;;;;;gBAEa;AAC1B,qBAAO,AAAM,KAAD;AACZ,YAAO,AAAM,MAAD,UAAQ,SAAC,OAAO,WAAW,AAAM,KAAD,QAAQ,MAAM;IAC5D;WAEiB;AAAS,0BAAQ,MAAM,IAAI;IAAC;;;QATlC;;;EAAS;;;;;;;;;;;kCAdf,OACA;AAEL,UAAO,6BAAc,SACT,WACD,mBAEF,AAAM,KAAD,SAAS,SAAS,EAAE,QAAW,MAClC,AAAO,MAAD,SAAS,EAAE,EAAE,OAAO;EAGvC;;QAekC;QAAgB;AAC9C,UAAA,AAAK,KAAD,SAAS,SAAS;EAAC;;;;;;;;ICjCrB;;;;;;IACG;;;;;;IAEO;;;;;;IAGN;;;;;;IACa;;;;;;IACA;;;;;;;;QAfd;QACA;QACA;QACA;QACA;QACA;IALA;IACA;IACA;IACA;IACA;IACA;;EACL;;;;;;;;;;;;;;;;;;;;;;ICcS;;;;;;IAOI;;;;;;IAMA;;;;;;IAIA;;;;;;IAID;;;;;;;AAEqB,4CAAc;IAAY;;;QA/BtD;QACA;QACA;QACA;QACA;IAJA;IACA;IACA;IACA;IACA;;EACL;;;;;;;;;;;;;;;;;;;;;;;IA6B8D;;6DAA7D;;;;EAA6D;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAsBtD;;;;;;IACG;;;;;;IACY;;;;;;IACF;;;;;;;AAUC;IAAO;;AASV;AACnB,sBAAI,AAA2B;AAC7B;;AAGiE,QAAnE,AAA2B,yCAA8B;AAChB,QAAzC,WAAM,AAAkC,uCAAP,YAAG;AAEpC;AAIG,UAHD,iBAAU,MAAgB,oCACxB,sBACW;AAEqD,UAAlE,AAA2B,yCAA8B;AACzB,UAAhC,WAAM;AACsB,UAA5B,aAAO,AAAO;AAGmD,UADjE,uBACI,AAAQ,AAAO;AAEnB,cAAI,AAAO,iCAAqB;AAWhB,YAVd,+BAAyB,AAAuC,AAU9D,4DATA,AAAO,2CACI,QAAgC;AAEoE,gBAD7G,WACI,AAAwG,sDAA7D,AAAO,AAAkB,2CAAU;AACrE,gBAAb,AAAM,KAAD;AACmC,gBAAxC,AAAQ;AAE+B,gBADvC,AAA2B,yCACD;kEAErB;;AAcL,UAXN,6BAAuB,AAAe,4BAClC,QAAS;wCAGD;AAEY,cAAlB;6CAEa,eACN,QAAS;AACE,cAAlB,WAAM,AAAW,qBAAF,CAAC;;AAGtB,wBAAI;AACF,qBAAc,WAAY,AAA0B;AACxC,cAAF,WAAR,QAAQ;;AAGgB,YAA1B,2BAAqB;;;cAEhB;AACY,UAAnB,sBAAiB,CAAC;;MAEtB;;qBAEuB;;AACrB,UAAI,CAAC,IAAI;AACgD,QAAvD,WAAM,AAAgD,0DAAF,CAAC;;AAElB,MAArC,WAAM;AACmB,WAAzB;0BAAiB;AACe,YAAhC;2BAAwB;AACM,aAA9B;4BAAsB;AAEtB,oBAAI,AAA2B;AAC7B;;AAGuB,MAAzB,2BAAqB;AAErB,uBAAI,AAA2B,wCACL;AAC8C,QAAtE,AAA2B,yCAA8B;;AAG3D,oBAAI,AAAO,yCAAkB,AAA2B;AACtD,YAAI,AAAO,gDAAoC;AAEkD,UAD/F,WACI,AAA0F,uCAA9D,AAAO,AAAiC,0DAAU;AAOjF,UALD,wBAAkB,gBAChB,AAAO,8CACP;AACY,YAAV;;;AAIuB,UAArB,gBAAI,cAAM;;;IAGtB;;AAQoB;;AACgB,QAAlC,WAAM;AACmB,aAAzB;4BAAiB;AAMf,QALF,MAAa,gCAAK,0DAChB,OAAS,mEACT,OAAwB,mEACxB,OAAsB,yEACtB,OAA4B;MAEhC;;+BAEwD;;AAC3B,gBACQ,yBAA/B,AAAK,oBAAe,eAAR,OAAO;AACV,iBAAkC,gBAAd,KAAZ,AAAG,GAAA,QAAC,eAAD,OAAY;AACtB,qBAAyB,MAAf,AAAG,GAAA,QAAC,mBAAD,OAAgC;AAC9C,eAA2B,gBAAX,OAAV,AAAG,GAAA,QAAC,eAAD,OAAU;AAEhC,cAAQ,IAAI;;;AAER,gBAAO;;;;AAEP,gBAAO,kCAAgB,OAAO;;;;AAE9B,gBAAO;;;;AAEO,qBAAc,WAAP,OAAO,WAAC;AACf,uBAAgB,WAAP,OAAO,WAAC;AAC/B,gBAAO,mCAAiB,EAAE,EAAE,IAAI,EAAE,MAAM;;;;AAExC,gBAAO,oCAAkB,EAAE,EAAE,OAAO;;;;AAEpC,gBAAO,uCAAqB,EAAE;;;;AAE9B,gBAAO,8BAAY,GAAG;;;IAE5B;aAEuC;AACrC,UAAqC,YAAjC,AAA2B,wCAA+B;AAM3D,QALD,AAAQ,kBACN,AAAK,oBACH,OAAO,gBACM,QAAS,KAAQ,WAAF,CAAC;;IAIrC;cAW8B,SAAoB;AACnC,eAAiD,cAAvC,8BAAmB;AACD,qBACrC;AACsC;AAC/B,uBAA+C,WAAjC,AAAQ,AAAU,OAAX,8BAC5B,AAAO,uCAA2B;AAEhC,qBAAW;AAEX,kDAAsC,AACjC,AAEA,AAEA,qDAHG,iBAAiB,IAAG,OAA6B,qDAC9C,QAAuB,SACpB,YAAN,KAAK,EAA0B,qFAC7B;AAE0B,oCAAwB,UAAU,GACpE,AAAoC,mCAAD,SACjC,AAAO,iDACI,QAAkC;AACb,YAA9B,WAAM;AACsD,YAA5D,AAAS,QAAD,UAAU,+BAAiB;AACtB,YAAb,AAAM,KAAD;AACW,YAAhB,AAAS,QAAD;2DAGZ,mCAAmC;AAwDvC,QAtDF,MAAM,AAAsB,qBAAD,QAAQ,QAAC;AACC,kCAC/B,AAAe,AAgBjB,2BAfA,QAAsB;AACpB,gBAAY,6BAAR,OAAO;AACT,oBAAO,AAAQ,AAAG,QAAJ,OAAO,EAAE;;AAGzB,gBAAY,8BAAR,OAAO;AACT,oBAAO,AAAQ,AAAG,QAAJ,OAAO,EAAE;;AAGzB,gBAAY,iCAAR,OAAO;AACT,oBAAO,AAAQ,AAAG,QAAJ,OAAO,EAAE;;AAGzB,kBAAO;sDAEC,QAAC,KAAM,WAAC,AAAS,QAAD;AAEO,qCAAuB,UAAU,GAC9D,AACG,AAEA,AACA,iBAJc,OACR,QAAsB,WACjB,iCAAR,OAAO,uCACL,WAEN,AAAO,iDACI,QAAiC;AACf,cAA3B,WAAM;AACmD,cAAzD,AAAS,QAAD,UAAU,+BAAiB;AACtB,cAAb,AAAM,KAAD;AACW,cAAhB,AAAS,QAAD;4DAGZ,AACG,AAEA,iBAHc,OACR,QAAsB,WACjB,iCAAR,OAAO,uCACL;AAEsC,UAApD,AAAqB,oBAAD,QAAQ,QAAC,KAAM,AAAS,QAAD;AAMqB,UAJhE,AACK,AAEA,AACA,iBAJY,OAET,QAAsB,WAAoB,6BAAR,OAAO,yEAErC,QAAkB,WAAY,AAAS,QAAD,KAAK,OAAO;AAMO,UAJrE,AACK,AAEA,iBAHY,OAET,QAAsB,WAAoB,8BAAR,OAAO,yCAEzC,QAAsB,WAAY,AAAS,QAAD,UAAU,OAAO;AAEhC,UAAnC,aAAO,gCAAe,EAAE,EAAE,OAAO;;;AAIT,MAA5B,AAAS,QAAD,YAAY,QAAQ;AAU3B,MARD,AAAS,QAAD,YAAY;;AACkB,QAApC,AAA0B,yCAAO,EAAE;AAEtB,aAAb,GAAG;qBAAH,OAAK;AACL,YAAqC,YAAjC,AAA2B,wCAA+B,kDAC1D,iBAAW;AACY,UAAzB,aAAO,+BAAc,EAAE;;;AAIa,MAAxC,AAAyB,uCAAC,EAAE,EAAI,QAAQ;AAExC,YAAO,AAAS,SAAD;IACjB;;AAMI,YAAA,AAA2B;IAAM;;AAQ0B,YAAA,AAC1D,AACA,4BADM,QAAsB,WAAoB,KAAR,OAAO;IACtC;;6CArTP;QACA;QAGA;QACmB;IASmB,mCACzC;IAE4B,kCAA4B;IACvD,2BAAqB;IAEpB;IACI;IAGmB;IAEW;IACC;IA3BlC;IACA;IAGA;IACmB;AAEd,IAAV;EACF","file":"fetch_result.ddc.js"}');
  // Exports:
  return {
    src__link__link: link,
    src__link__fetch_result: fetch_result,
    src__socket_client: socket_client
  };
});

//# sourceMappingURL=fetch_result.ddc.js.map
