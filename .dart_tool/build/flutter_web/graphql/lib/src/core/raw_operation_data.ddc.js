define(['dart_sdk', 'packages/gql/src/ast/ast', 'packages/gql/language', 'packages/http/http'], function(dart_sdk, packages__gql__src__ast__ast, packages__gql__language, packages__http__http) {
  'use strict';
  const core = dart_sdk.core;
  const _js_helper = dart_sdk._js_helper;
  const collection = dart_sdk.collection;
  const convert = dart_sdk.convert;
  const _interceptors = dart_sdk._interceptors;
  const async = dart_sdk.async;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const ast = packages__gql__src__ast__ast.src__ast__ast;
  const parser = packages__gql__language.src__language__parser;
  const printer = packages__gql__language.src__language__printer;
  const multipart_file = packages__http__http.src__multipart_file;
  const operation = Object.create(dart.library);
  const get_from_ast = Object.create(dart.library);
  const raw_operation_data = Object.create(dart.library);
  const link_http_helper_deprecated_stub = Object.create(dart.library);
  const messages = Object.create(dart.library);
  const $addAll = dartx.addAll;
  const $toString = dartx.toString;
  const $whereType = dartx.whereType;
  const $toList = dartx.toList;
  const $isEmpty = dartx.isEmpty;
  const $last = dartx.last;
  const $length = dartx.length;
  const $any = dartx.any;
  const $_set = dartx._set;
  const $hashCode = dartx.hashCode;
  let IdentityMapOfString$dynamic = () => (IdentityMapOfString$dynamic = dart.constFn(_js_helper.IdentityMap$(core.String, dart.dynamic)))();
  let SplayTreeMapOfString$dynamic = () => (SplayTreeMapOfString$dynamic = dart.constFn(collection.SplayTreeMap$(core.String, dart.dynamic)))();
  let dynamicTodynamic = () => (dynamicTodynamic = dart.constFn(dart.fnType(dart.dynamic, [dart.dynamic])))();
  let JSArrayOfOperationDefinitionNode = () => (JSArrayOfOperationDefinitionNode = dart.constFn(_interceptors.JSArray$(ast.OperationDefinitionNode)))();
  let OperationDefinitionNodeTobool = () => (OperationDefinitionNodeTobool = dart.constFn(dart.fnType(core.bool, [ast.OperationDefinitionNode])))();
  let MapOfString$MultipartFile = () => (MapOfString$MultipartFile = dart.constFn(core.Map$(core.String, multipart_file.MultipartFile)))();
  const CT = Object.create(null);
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.constMap(core.String, dart.dynamic, []);
    }
  });
  const _context = dart.privateName(operation, "_context");
  const _documentIdentifier = dart.privateName(raw_operation_data, "_documentIdentifier");
  const _operationName = dart.privateName(raw_operation_data, "_operationName");
  let C0;
  const _identifier = dart.privateName(raw_operation_data, "_identifier");
  const documentNode$ = dart.privateName(raw_operation_data, "RawOperationData.documentNode");
  const variables$ = dart.privateName(raw_operation_data, "RawOperationData.variables");
  raw_operation_data.RawOperationData = class RawOperationData extends core.Object {
    get documentNode() {
      return this[documentNode$];
    }
    set documentNode(value) {
      this[documentNode$] = value;
    }
    get variables() {
      return this[variables$];
    }
    set variables(value) {
      this[variables$] = value;
    }
    get document() {
      return printer.printNode(this.documentNode);
    }
    set document(value) {
      this.documentNode = parser.parseString(core.String._check(value));
    }
    get operationName() {
      this[_operationName] == null ? this[_operationName] = get_from_ast.getLastOperationName(this.documentNode) : null;
      return this[_operationName];
    }
    get [_identifier]() {
      let t0;
      this[_documentIdentifier] == null ? this[_documentIdentifier] = (t0 = this.operationName, t0 == null ? "UNNAMED/" + dart.toString(dart.hashCode(this.documentNode)) : t0) : null;
      return this[_documentIdentifier];
    }
    toKey() {
      let encodedVariables = convert.json.encode(this.variables, {toEncodable: dart.fn(object => {
          if (multipart_file.MultipartFile.is(object)) {
            return object.filename;
          }
          if (dart.test(link_http_helper_deprecated_stub.isIoFile(object))) {
            return dart.dload(object, 'path');
          }
          return dart.dsend(object, 'toJson', []);
        }, dynamicTodynamic())});
      return dart.str(this.document) + "|" + dart.str(encodedVariables) + "|" + dart.str(this[_identifier]);
    }
  };
  (raw_operation_data.RawOperationData.new = function(opts) {
    let t0, t0$;
    let document = opts && 'document' in opts ? opts.document : null;
    let documentNode = opts && 'documentNode' in opts ? opts.documentNode : null;
    let variables = opts && 'variables' in opts ? opts.variables : null;
    let operationName = opts && 'operationName' in opts ? opts.operationName : null;
    this[_documentIdentifier] = null;
    if (!(document != null || documentNode != null)) dart.assertFailed("Either a \"document\"  or \"documentNode\" option is required. " + "You must specify your GraphQL document in the query options.", "org-dartlang-app:///packages/graphql/src/core/raw_operation_data.dart", 20, 11, "document != null || documentNode != null");
    this[documentNode$] = (t0 = documentNode, t0 == null ? parser.parseString(document) : t0);
    this[_operationName] = operationName;
    this[variables$] = SplayTreeMapOfString$dynamic().of((t0$ = variables, t0$ == null ? C0 || CT.C0 : t0$));
    ;
  }).prototype = raw_operation_data.RawOperationData.prototype;
  dart.addTypeTests(raw_operation_data.RawOperationData);
  dart.setMethodSignature(raw_operation_data.RawOperationData, () => ({
    __proto__: dart.getMethods(raw_operation_data.RawOperationData.__proto__),
    toKey: dart.fnType(core.String, [])
  }));
  dart.setGetterSignature(raw_operation_data.RawOperationData, () => ({
    __proto__: dart.getGetters(raw_operation_data.RawOperationData.__proto__),
    document: core.String,
    operationName: core.String,
    [_identifier]: core.String
  }));
  dart.setSetterSignature(raw_operation_data.RawOperationData, () => ({
    __proto__: dart.getSetters(raw_operation_data.RawOperationData.__proto__),
    document: dart.dynamic
  }));
  dart.setLibraryUri(raw_operation_data.RawOperationData, "package:graphql/src/core/raw_operation_data.dart");
  dart.setFieldSignature(raw_operation_data.RawOperationData, () => ({
    __proto__: dart.getFields(raw_operation_data.RawOperationData.__proto__),
    documentNode: dart.fieldType(ast.DocumentNode),
    variables: dart.fieldType(core.Map$(core.String, dart.dynamic)),
    [_operationName]: dart.fieldType(core.String),
    [_documentIdentifier]: dart.fieldType(core.String)
  }));
  const extensions$ = dart.privateName(operation, "Operation.extensions");
  operation.Operation = class Operation extends raw_operation_data.RawOperationData {
    get extensions() {
      return this[extensions$];
    }
    set extensions(value) {
      super.extensions = value;
    }
    static fromOptions(options) {
      return new operation.Operation.new({documentNode: options.documentNode, variables: options.variables});
    }
    setContext(next) {
      if (next != null) {
        this[_context][$addAll](next);
      }
    }
    getContext() {
      let result = new (IdentityMapOfString$dynamic()).new();
      result[$addAll](this[_context]);
      return result;
    }
    get isSubscription() {
      return get_from_ast.isOfType(ast.OperationType.subscription, this.documentNode, this.operationName);
    }
  };
  (operation.Operation.new = function(opts) {
    let document = opts && 'document' in opts ? opts.document : null;
    let documentNode = opts && 'documentNode' in opts ? opts.documentNode : null;
    let variables = opts && 'variables' in opts ? opts.variables : null;
    let extensions = opts && 'extensions' in opts ? opts.extensions : null;
    let operationName = opts && 'operationName' in opts ? opts.operationName : null;
    this[_context] = new (IdentityMapOfString$dynamic()).new();
    this[extensions$] = extensions;
    operation.Operation.__proto__.new.call(this, {document: document, documentNode: documentNode, variables: variables, operationName: operationName});
    ;
  }).prototype = operation.Operation.prototype;
  dart.addTypeTests(operation.Operation);
  dart.setMethodSignature(operation.Operation, () => ({
    __proto__: dart.getMethods(operation.Operation.__proto__),
    setContext: dart.fnType(dart.void, [core.Map$(core.String, dart.dynamic)]),
    getContext: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setGetterSignature(operation.Operation, () => ({
    __proto__: dart.getGetters(operation.Operation.__proto__),
    isSubscription: core.bool
  }));
  dart.setLibraryUri(operation.Operation, "package:graphql/src/link/operation.dart");
  dart.setFieldSignature(operation.Operation, () => ({
    __proto__: dart.getFields(operation.Operation.__proto__),
    extensions: dart.finalFieldType(core.Map$(core.String, dart.dynamic)),
    [_context]: dart.finalFieldType(core.Map$(core.String, dart.dynamic))
  }));
  get_from_ast.getOperationNodes = function getOperationNodes(doc) {
    if (doc.definitions == null) return JSArrayOfOperationDefinitionNode().of([]);
    return doc.definitions[$whereType](ast.OperationDefinitionNode)[$toList]();
  };
  get_from_ast.getLastOperationName = function getLastOperationName(doc) {
    let t0, t0$;
    let operations = get_from_ast.getOperationNodes(doc);
    if (dart.test(operations[$isEmpty])) return null;
    t0$ = (t0 = operations[$last], t0 == null ? null : t0.name);
    return t0$ == null ? null : t0$.value;
  };
  get_from_ast.isOfType = function isOfType(operationType, doc, operationName) {
    let operations = get_from_ast.getOperationNodes(doc);
    if (operationName == null) {
      if (dart.notNull(operations[$length]) > 1) return false;
      return operations[$any](dart.fn(op => dart.equals(op.type, operationType), OperationDefinitionNodeTobool()));
    }
    return operations[$any](dart.fn(op => {
      let t0;
      return (t0 = op.name, t0 == null ? null : t0.value) == operationName && dart.equals(op.type, operationType);
    }, OperationDefinitionNodeTobool()));
  };
  link_http_helper_deprecated_stub.deprecatedHelper = function deprecatedHelper(body, currentMap, currentPath) {
    return async.async(MapOfString$MultipartFile(), function* deprecatedHelper() {
      return null;
    });
  };
  link_http_helper_deprecated_stub.isIoFile = function isIoFile(object) {
    return false;
  };
  messages.MessageTypes = class MessageTypes extends core.Object {};
  (messages.MessageTypes.__ = function() {
    ;
  }).prototype = messages.MessageTypes.prototype;
  dart.addTypeTests(messages.MessageTypes);
  dart.setLibraryUri(messages.MessageTypes, "package:graphql/src/websocket/messages.dart");
  dart.defineLazy(messages.MessageTypes, {
    /*messages.MessageTypes.GQL_CONNECTION_INIT*/get GQL_CONNECTION_INIT() {
      return "connection_init";
    },
    /*messages.MessageTypes.GQL_CONNECTION_TERMINATE*/get GQL_CONNECTION_TERMINATE() {
      return "connection_terminate";
    },
    /*messages.MessageTypes.GQL_CONNECTION_ACK*/get GQL_CONNECTION_ACK() {
      return "connection_ack";
    },
    /*messages.MessageTypes.GQL_CONNECTION_ERROR*/get GQL_CONNECTION_ERROR() {
      return "connection_error";
    },
    /*messages.MessageTypes.GQL_CONNECTION_KEEP_ALIVE*/get GQL_CONNECTION_KEEP_ALIVE() {
      return "ka";
    },
    /*messages.MessageTypes.GQL_START*/get GQL_START() {
      return "start";
    },
    /*messages.MessageTypes.GQL_STOP*/get GQL_STOP() {
      return "stop";
    },
    /*messages.MessageTypes.GQL_DATA*/get GQL_DATA() {
      return "data";
    },
    /*messages.MessageTypes.GQL_ERROR*/get GQL_ERROR() {
      return "error";
    },
    /*messages.MessageTypes.GQL_COMPLETE*/get GQL_COMPLETE() {
      return "complete";
    },
    /*messages.MessageTypes.GQL_UNKNOWN*/get GQL_UNKNOWN() {
      return "unknown";
    }
  });
  messages.JsonSerializable = class JsonSerializable extends core.Object {
    toString() {
      return dart.toString(this.toJson());
    }
  };
  (messages.JsonSerializable.new = function() {
    ;
  }).prototype = messages.JsonSerializable.prototype;
  dart.addTypeTests(messages.JsonSerializable);
  dart.setLibraryUri(messages.JsonSerializable, "package:graphql/src/websocket/messages.dart");
  dart.defineExtensionMethods(messages.JsonSerializable, ['toString']);
  const type$ = dart.privateName(messages, "GraphQLSocketMessage.type");
  messages.GraphQLSocketMessage = class GraphQLSocketMessage extends messages.JsonSerializable {
    get type() {
      return this[type$];
    }
    set type(value) {
      super.type = value;
    }
  };
  (messages.GraphQLSocketMessage.new = function(type) {
    this[type$] = type;
    ;
  }).prototype = messages.GraphQLSocketMessage.prototype;
  dart.addTypeTests(messages.GraphQLSocketMessage);
  dart.setLibraryUri(messages.GraphQLSocketMessage, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.GraphQLSocketMessage, () => ({
    __proto__: dart.getFields(messages.GraphQLSocketMessage.__proto__),
    type: dart.finalFieldType(core.String)
  }));
  const payload$ = dart.privateName(messages, "InitOperation.payload");
  messages.InitOperation = class InitOperation extends messages.GraphQLSocketMessage {
    get payload() {
      return this[payload$];
    }
    set payload(value) {
      super.payload = value;
    }
    toJson() {
      let jsonMap = new (IdentityMapOfString$dynamic()).new();
      jsonMap[$_set]("type", this.type);
      if (this.payload != null) {
        jsonMap[$_set]("payload", this.payload);
      }
      return jsonMap;
    }
  };
  (messages.InitOperation.new = function(payload) {
    this[payload$] = payload;
    messages.InitOperation.__proto__.new.call(this, "connection_init");
    ;
  }).prototype = messages.InitOperation.prototype;
  dart.addTypeTests(messages.InitOperation);
  dart.setMethodSignature(messages.InitOperation, () => ({
    __proto__: dart.getMethods(messages.InitOperation.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.InitOperation, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.InitOperation, () => ({
    __proto__: dart.getFields(messages.InitOperation.__proto__),
    payload: dart.finalFieldType(dart.dynamic)
  }));
  const operation$ = dart.privateName(messages, "SubscriptionRequest.operation");
  messages.SubscriptionRequest = class SubscriptionRequest extends messages.JsonSerializable {
    get operation() {
      return this[operation$];
    }
    set operation(value) {
      super.operation = value;
    }
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["operationName", this.operation.operationName, "query", printer.printNode(this.operation.documentNode), "variables", this.operation.variables]);
    }
  };
  (messages.SubscriptionRequest.new = function(operation) {
    this[operation$] = operation;
    ;
  }).prototype = messages.SubscriptionRequest.prototype;
  dart.addTypeTests(messages.SubscriptionRequest);
  dart.setMethodSignature(messages.SubscriptionRequest, () => ({
    __proto__: dart.getMethods(messages.SubscriptionRequest.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.SubscriptionRequest, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.SubscriptionRequest, () => ({
    __proto__: dart.getFields(messages.SubscriptionRequest.__proto__),
    operation: dart.finalFieldType(operation.Operation)
  }));
  const id$ = dart.privateName(messages, "StartOperation.id");
  const payload$0 = dart.privateName(messages, "StartOperation.payload");
  messages.StartOperation = class StartOperation extends messages.GraphQLSocketMessage {
    get id() {
      return this[id$];
    }
    set id(value) {
      super.id = value;
    }
    get payload() {
      return this[payload$0];
    }
    set payload(value) {
      super.payload = value;
    }
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type, "id", this.id, "payload", this.payload]);
    }
  };
  (messages.StartOperation.new = function(id, payload) {
    this[id$] = id;
    this[payload$0] = payload;
    messages.StartOperation.__proto__.new.call(this, "start");
    ;
  }).prototype = messages.StartOperation.prototype;
  dart.addTypeTests(messages.StartOperation);
  dart.setMethodSignature(messages.StartOperation, () => ({
    __proto__: dart.getMethods(messages.StartOperation.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.StartOperation, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.StartOperation, () => ({
    __proto__: dart.getFields(messages.StartOperation.__proto__),
    id: dart.finalFieldType(core.String),
    payload: dart.finalFieldType(messages.SubscriptionRequest)
  }));
  const id$0 = dart.privateName(messages, "StopOperation.id");
  messages.StopOperation = class StopOperation extends messages.GraphQLSocketMessage {
    get id() {
      return this[id$0];
    }
    set id(value) {
      super.id = value;
    }
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type, "id", this.id]);
    }
  };
  (messages.StopOperation.new = function(id) {
    this[id$0] = id;
    messages.StopOperation.__proto__.new.call(this, "stop");
    ;
  }).prototype = messages.StopOperation.prototype;
  dart.addTypeTests(messages.StopOperation);
  dart.setMethodSignature(messages.StopOperation, () => ({
    __proto__: dart.getMethods(messages.StopOperation.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.StopOperation, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.StopOperation, () => ({
    __proto__: dart.getFields(messages.StopOperation.__proto__),
    id: dart.finalFieldType(core.String)
  }));
  messages.ConnectionAck = class ConnectionAck extends messages.GraphQLSocketMessage {
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type]);
    }
  };
  (messages.ConnectionAck.new = function() {
    messages.ConnectionAck.__proto__.new.call(this, "connection_ack");
    ;
  }).prototype = messages.ConnectionAck.prototype;
  dart.addTypeTests(messages.ConnectionAck);
  dart.setMethodSignature(messages.ConnectionAck, () => ({
    __proto__: dart.getMethods(messages.ConnectionAck.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.ConnectionAck, "package:graphql/src/websocket/messages.dart");
  const payload$1 = dart.privateName(messages, "ConnectionError.payload");
  messages.ConnectionError = class ConnectionError extends messages.GraphQLSocketMessage {
    get payload() {
      return this[payload$1];
    }
    set payload(value) {
      super.payload = value;
    }
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type, "payload", this.payload]);
    }
  };
  (messages.ConnectionError.new = function(payload) {
    this[payload$1] = payload;
    messages.ConnectionError.__proto__.new.call(this, "connection_error");
    ;
  }).prototype = messages.ConnectionError.prototype;
  dart.addTypeTests(messages.ConnectionError);
  dart.setMethodSignature(messages.ConnectionError, () => ({
    __proto__: dart.getMethods(messages.ConnectionError.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.ConnectionError, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.ConnectionError, () => ({
    __proto__: dart.getFields(messages.ConnectionError.__proto__),
    payload: dart.finalFieldType(dart.dynamic)
  }));
  messages.ConnectionKeepAlive = class ConnectionKeepAlive extends messages.GraphQLSocketMessage {
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type]);
    }
  };
  (messages.ConnectionKeepAlive.new = function() {
    messages.ConnectionKeepAlive.__proto__.new.call(this, "ka");
    ;
  }).prototype = messages.ConnectionKeepAlive.prototype;
  dart.addTypeTests(messages.ConnectionKeepAlive);
  dart.setMethodSignature(messages.ConnectionKeepAlive, () => ({
    __proto__: dart.getMethods(messages.ConnectionKeepAlive.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.ConnectionKeepAlive, "package:graphql/src/websocket/messages.dart");
  const id$1 = dart.privateName(messages, "SubscriptionData.id");
  const data$ = dart.privateName(messages, "SubscriptionData.data");
  const errors$ = dart.privateName(messages, "SubscriptionData.errors");
  messages.SubscriptionData = class SubscriptionData extends messages.GraphQLSocketMessage {
    get id() {
      return this[id$1];
    }
    set id(value) {
      super.id = value;
    }
    get data() {
      return this[data$];
    }
    set data(value) {
      super.data = value;
    }
    get errors() {
      return this[errors$];
    }
    set errors(value) {
      super.errors = value;
    }
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type, "data", this.data, "errors", this.errors]);
    }
    get hashCode() {
      return dart.hashCode(this.toJson());
    }
    _equals(other) {
      if (other == null) return false;
      return messages.SubscriptionData.is(other) && convert.jsonEncode(other) == convert.jsonEncode(this);
    }
  };
  (messages.SubscriptionData.new = function(id, data, errors) {
    this[id$1] = id;
    this[data$] = data;
    this[errors$] = errors;
    messages.SubscriptionData.__proto__.new.call(this, "data");
    ;
  }).prototype = messages.SubscriptionData.prototype;
  dart.addTypeTests(messages.SubscriptionData);
  dart.setMethodSignature(messages.SubscriptionData, () => ({
    __proto__: dart.getMethods(messages.SubscriptionData.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.SubscriptionData, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.SubscriptionData, () => ({
    __proto__: dart.getFields(messages.SubscriptionData.__proto__),
    id: dart.finalFieldType(core.String),
    data: dart.finalFieldType(dart.dynamic),
    errors: dart.finalFieldType(dart.dynamic)
  }));
  dart.defineExtensionMethods(messages.SubscriptionData, ['_equals']);
  dart.defineExtensionAccessors(messages.SubscriptionData, ['hashCode']);
  const id$2 = dart.privateName(messages, "SubscriptionError.id");
  const payload$2 = dart.privateName(messages, "SubscriptionError.payload");
  messages.SubscriptionError = class SubscriptionError extends messages.GraphQLSocketMessage {
    get id() {
      return this[id$2];
    }
    set id(value) {
      super.id = value;
    }
    get payload() {
      return this[payload$2];
    }
    set payload(value) {
      super.payload = value;
    }
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type, "id", this.id, "payload", this.payload]);
    }
  };
  (messages.SubscriptionError.new = function(id, payload) {
    this[id$2] = id;
    this[payload$2] = payload;
    messages.SubscriptionError.__proto__.new.call(this, "error");
    ;
  }).prototype = messages.SubscriptionError.prototype;
  dart.addTypeTests(messages.SubscriptionError);
  dart.setMethodSignature(messages.SubscriptionError, () => ({
    __proto__: dart.getMethods(messages.SubscriptionError.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.SubscriptionError, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.SubscriptionError, () => ({
    __proto__: dart.getFields(messages.SubscriptionError.__proto__),
    id: dart.finalFieldType(core.String),
    payload: dart.finalFieldType(dart.dynamic)
  }));
  const id$3 = dart.privateName(messages, "SubscriptionComplete.id");
  messages.SubscriptionComplete = class SubscriptionComplete extends messages.GraphQLSocketMessage {
    get id() {
      return this[id$3];
    }
    set id(value) {
      super.id = value;
    }
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type, "id", this.id]);
    }
  };
  (messages.SubscriptionComplete.new = function(id) {
    this[id$3] = id;
    messages.SubscriptionComplete.__proto__.new.call(this, "complete");
    ;
  }).prototype = messages.SubscriptionComplete.prototype;
  dart.addTypeTests(messages.SubscriptionComplete);
  dart.setMethodSignature(messages.SubscriptionComplete, () => ({
    __proto__: dart.getMethods(messages.SubscriptionComplete.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.SubscriptionComplete, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.SubscriptionComplete, () => ({
    __proto__: dart.getFields(messages.SubscriptionComplete.__proto__),
    id: dart.finalFieldType(core.String)
  }));
  const payload$3 = dart.privateName(messages, "UnknownData.payload");
  messages.UnknownData = class UnknownData extends messages.GraphQLSocketMessage {
    get payload() {
      return this[payload$3];
    }
    set payload(value) {
      super.payload = value;
    }
    toJson() {
      return new (IdentityMapOfString$dynamic()).from(["type", this.type, "payload", this.payload]);
    }
  };
  (messages.UnknownData.new = function(payload) {
    this[payload$3] = payload;
    messages.UnknownData.__proto__.new.call(this, "unknown");
    ;
  }).prototype = messages.UnknownData.prototype;
  dart.addTypeTests(messages.UnknownData);
  dart.setMethodSignature(messages.UnknownData, () => ({
    __proto__: dart.getMethods(messages.UnknownData.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), [])
  }));
  dart.setLibraryUri(messages.UnknownData, "package:graphql/src/websocket/messages.dart");
  dart.setFieldSignature(messages.UnknownData, () => ({
    __proto__: dart.getFields(messages.UnknownData.__proto__),
    payload: dart.finalFieldType(dart.dynamic)
  }));
  dart.trackLibraries("packages/graphql/src/core/raw_operation_data", {
    "package:graphql/src/link/operation.dart": operation,
    "package:graphql/src/utilities/get_from_ast.dart": get_from_ast,
    "package:graphql/src/core/raw_operation_data.dart": raw_operation_data,
    "package:graphql/src/link/http/link_http_helper_deprecated_stub.dart": link_http_helper_deprecated_stub,
    "package:graphql/src/websocket/messages.dart": messages
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["raw_operation_data.dart","../link/operation.dart","../utilities/get_from_ast.dart","../link/http/link_http_helper_deprecated_stub.dart","../websocket/messages.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAqCe;;;;;;IAiBQ;;;;;;;AAXE,+BAAU;IAAa;iBAKjC;AACsB,MAAjC,oBAAe,sCAAY,KAAK;IAClC;;AAUuD,MAArD,AAAe,wBAAA,OAAf,uBAAmB,kCAAqB,qBAAzB;AACf,YAAO;IACT;;;AAQoE,MADlE,AAAoB,6BAAA,OAApB,6BACkB,+BAAd,OAAiB,AAAW,aAAwB,cAAT,cAAb,4BADd;AAEpB,YAAO;IACT;;AAIe,6BACT,AAAK,oBAAO,8BAAwB,QAAS;AAC/C,cAAW,gCAAP,MAAM;AACR,kBAAO,AAAO,OAAD;;AAKf,wBAAI,0CAAS,MAAM;AACjB,kBAAc,YAAP,MAAM;;AAGf,gBAAc,YAAP,MAAM;;AAKf,YAAiD,UAAxC,iBAAQ,eAAE,gBAAgB,mBAAE;IACvC;;;;QAjFa;QACE;QACQ;QACd;IAgDF;UA7CC,AAAiB,QAAT,IAAI,QAAQ,YAAY,IAAI,yBACpC,oEACA;IASW,uBAAe,KAAb,YAAY,QAAZ,OAAgB,mBAAY,QAAQ;IACpC,uBAAE,aAAa;uBACpB,mCACE,MAAV,SAAS,SAAT;;EACD;;;;;;;;;;;;;;;;;;;;;;;;;;ICPoB;;;;;;uBAPoB;AAC7C,YAAO,4CACS,AAAQ,OAAD,0BACV,AAAQ,OAAD;IAEtB;eAOqC;AACnC,UAAI,IAAI,IAAI;AACW,QAArB,AAAS,wBAAO,IAAI;;IAExB;;AAG6B,mBAA0B;AAC9B,MAAvB,AAAO,MAAD,UAAQ;AAEd,YAAO,OAAM;IACf;;AAE2B,mCACP,gCACd,mBACA;IACD;;;QAzCQ;QACE;QACQ;QAChB;QACE;IAiBkB,iBAA4B;IAlBhD;AAEF,4DAEe,QAAQ,gBACJ,YAAY,aACf,SAAS,iBACL,aAAa;;EAAC;;;;;;;;;;;;;;;;;8DChBoB;AAC3D,QAAI,AAAI,AAAY,GAAb,gBAAgB,MAAM,MAAO;AAEpC,UAAO,AAAI,AAAY,AAAqC,IAAlD;EACZ;oEAEyC;;AACjC,qBAAa,+BAAkB,GAAG;AAExC,kBAAI,AAAW,UAAD,aAAU,MAAO;AAE/B,gBAAO,AAAW,UAAD,sBAAC,OAAM;yBAAN,OAAY;EAChC;4CAGgB,eACD,KACN;AAED,qBAAa,+BAAkB,GAAG;AAExC,QAAI,AAAc,aAAD,IAAI;AACnB,UAAsB,aAAlB,AAAW,UAAD,aAAU,GAAG,MAAO;AAElC,YAAO,AAAW,WAAD,OACf,QAAC,MAAe,YAAR,AAAG,EAAD,OAAS,aAAa;;AAIpC,UAAO,AAAW,WAAD,OACf,QAAC;;AAAO,YAAe,AAAiB,OAAhC,AAAG,EAAD,oBAAC,OAAM,aAAS,aAAa,IAAY,YAAR,AAAG,EAAD,OAAS,aAAa;;EAEvE;gFC3BQ,MAAM,YAAY;AADyB;AAE/C;IAAI;;gEAMM;AAAW;EAAK;;;;ECHZ;;;;MAGI,yCAAmB;;;MACnB,8CAAwB;;;MAGxB,wCAAkB;;;MAClB,0CAAoB;;;MACpB,+CAAyB;;;MAGzB,+BAAS;;;MACT,8BAAQ;;;MAGR,8BAAQ;;;MACR,+BAAS;;;MACT,kCAAY;;;MAGZ,iCAAW;;;;;;AAOV,YAAS,eAAT;IAAmB;;;;EAC1C;;;;;;IAMe;;;;;;;;IAFa;;EAAK;;;;;;;;;IAWjB;;;;;;;AAIe,oBAA2B;AAChC,MAAtB,AAAO,OAAA,QAAC,QAAU;AAElB,UAAI,gBAAW;AACe,QAA5B,AAAO,OAAA,QAAC,WAAa;;AAGvB,YAAO,QAAO;IAChB;;;IAdmB;AAAW;;EAAuC;;;;;;;;;;;;;IAuBrD;;;;;;;AAGiB,YAAiB,2CAC5C,iBAAiB,AAAU,8BAC3B,SAAS,kBAAU,AAAU,8BAC7B,aAAa,AAAU;IACxB;;;IARoB;;EAAU;;;;;;;;;;;;;;IAmBtB;;;;;;IACa;;;;;;;AAGO,YAAiB,2CAC5C,QAAQ,WACR,MAAM,SACN,WAAW;IACZ;;0CAVe,IAAS;IAAT;IAAS;AAAW;;EAA6B;;;;;;;;;;;;;;IAkBxD;;;;;;;AAGoB,YAAiB,2CAC5C,QAAQ,WACR,MAAM;IACP;;;IARc;AAAM;;EAA4B;;;;;;;;;;;;;AAiBpB,YAAiB,2CAC5C,QAAQ;IACT;;;AALa;;EAAsC;;;;;;;;;IAa1C;;;;;;;AAGmB,YAAiB,2CAC5C,QAAQ,WACR,WAAW;IACZ;;;IARgB;AAAW;;EAAwC;;;;;;;;;;;;;AAgBvC,YAAiB,2CAC5C,QAAQ;IACT;;;AALmB;;EAA6C;;;;;;;;;;;IAexD;;;;;;IACC;;;;;;IACA;;;;;;;AAGmB,YAAiB,2CAC5C,QAAQ,WACR,QAAQ,WACR,UAAU;IACX;;AAGe,YAAS,eAAT;IAAiB;;UAGZ;AACrB,YAAM,AAAoB,8BAA1B,KAAK,KAAwB,AAAkB,mBAAP,KAAK,KAAK,mBAAW;IAAK;;4CAnBhD,IAAS,MAAW;IAApB;IAAS;IAAW;AACpC;;EAA4B;;;;;;;;;;;;;;;;;;IA0BrB;;;;;;IACC;;;;;;;AAGmB,YAAiB,2CAC5C,QAAQ,WACR,MAAM,SACN,WAAW;IACZ;;6CAVkB,IAAS;IAAT;IAAS;AAAW;;EAA6B;;;;;;;;;;;;;;IAkB3D;;;;;;;AAGoB,YAAiB,2CAC5C,QAAQ,WACR,MAAM;IACP;;;IARqB;AAAM;;EAAgC;;;;;;;;;;;;;IAiBlD;;;;;;;AAGmB,YAAiB,2CAC5C,QAAQ,WACR,WAAW;IACZ;;;IARY;AAAW;;EAA+B","file":"raw_operation_data.ddc.js"}');
  // Exports:
  return {
    src__link__operation: operation,
    src__utilities__get_from_ast: get_from_ast,
    src__core__raw_operation_data: raw_operation_data,
    src__link__http__link_http_helper_deprecated_stub: link_http_helper_deprecated_stub,
    src__websocket__messages: messages
  };
});

//# sourceMappingURL=raw_operation_data.ddc.js.map
