define(['dart_sdk'], function(dart_sdk) {
  'use strict';
  const core = dart_sdk.core;
  const _native_typed_data = dart_sdk._native_typed_data;
  const math = dart_sdk.math;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const uuid_util = Object.create(dart.library);
  const $floor = dartx.floor;
  const $toInt = dartx.toInt;
  const $rightShift = dartx['>>'];
  const $_set = dartx._set;
  const CT = Object.create(null);
  uuid_util.UuidUtil = class UuidUtil extends core.Object {
    static mathRNG(opts) {
      let seed = opts && 'seed' in opts ? opts.seed : -1;
      let rand = null;
      let b = _native_typed_data.NativeUint8List.new(16);
      let _rand = seed === -1 ? math.Random.new() : math.Random.new(seed);
      for (let i = 0; i < 16; i = i + 1) {
        if ((i & 3) === 0) {
          rand = (dart.notNull(_rand.nextDouble()) * 4294967296)[$floor]()[$toInt]();
        }
        b[$_set](i, rand[$rightShift]((i & 3) << 3) & 255);
      }
      return b;
    }
    static cryptoRNG() {
      let b = _native_typed_data.NativeUint8List.new(16);
      let rand = math.Random.secure();
      for (let i = 0; i < 16; i = i + 1) {
        b[$_set](i, rand.nextInt(1 << 8));
      }
      return b;
    }
  };
  (uuid_util.UuidUtil.new = function() {
    ;
  }).prototype = uuid_util.UuidUtil.prototype;
  dart.addTypeTests(uuid_util.UuidUtil);
  dart.setLibraryUri(uuid_util.UuidUtil, "package:uuid_enhanced/uuid_util.dart");
  dart.trackLibraries("packages/uuid_enhanced/uuid_util", {
    "package:uuid_enhanced/uuid_util.dart": uuid_util
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["uuid_util.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;UAOgC;AACxB;AACY,cAAI,uCAAU;AAEjB,kBAAS,AAAK,IAAD,KAAI,CAAC,IAAK,oBAAW,gBAAO,IAAI;AAC1D,eAAS,IAAI,GAAG,AAAE,CAAD,GAAG,IAAI,IAAA,AAAC,CAAA;AACvB,YAAe,CAAV,AAAE,CAAD,GAAG,OAAS;AACyC,UAAzD,OAA0C,AAAQ,CAAvB,aAAnB,AAAM,KAAD,iBAAgB;;AAEQ,QAAvC,AAAC,CAAA,QAAC,CAAC,EAAI,AAAK,AAAqB,IAAtB,cAAgB,CAAV,AAAE,CAAD,GAAG,MAAS,KAAK;;AAGrC,YAAO,EAAC;IACV;;AAIkB,cAAI,uCAAU;AACjB,iBAAc;AAC3B,eAAS,IAAI,GAAG,AAAE,CAAD,GAAG,IAAI,IAAA,AAAC,CAAA;AACI,QAA3B,AAAC,CAAA,QAAC,CAAC,EAAI,AAAK,IAAD,SAAS,AAAE,KAAG;;AAE3B,YAAO,EAAC;IACV;;;;EACF","file":"uuid_util.ddc.js"}');
  // Exports:
  return {
    uuid_util: uuid_util
  };
});

//# sourceMappingURL=uuid_util.ddc.js.map
