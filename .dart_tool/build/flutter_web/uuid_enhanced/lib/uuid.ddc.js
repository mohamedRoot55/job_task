define(['dart_sdk', 'packages/convert/convert', 'packages/uuid_enhanced/uuid_util', 'packages/crypto/crypto', 'packages/collection/src/comparators'], function(dart_sdk, packages__convert__convert, packages__uuid_enhanced__uuid_util, packages__crypto__crypto, packages__collection__src__comparators) {
  'use strict';
  const core = dart_sdk.core;
  const _native_typed_data = dart_sdk._native_typed_data;
  const _interceptors = dart_sdk._interceptors;
  const typed_data = dart_sdk.typed_data;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const hex = packages__convert__convert.src__hex;
  const uuid_util = packages__uuid_enhanced__uuid_util.uuid_util;
  const sha1 = packages__crypto__crypto.src__sha1;
  const equality = packages__collection__src__comparators.src__equality;
  const uuid = Object.create(dart.library);
  const $toLowerCase = dartx.toLowerCase;
  const $toList = dartx.toList;
  const $length = dartx.length;
  const $take = dartx.take;
  const $substring = dartx.substring;
  const $_get = dartx._get;
  const $_set = dartx._set;
  const $modulo = dartx['%'];
  const $rightShift = dartx['>>'];
  const $truncate = dartx.truncate;
  const $codeUnits = dartx.codeUnits;
  const $plus = dartx['+'];
  const $leftShift = dartx['<<'];
  const $map = dartx.map;
  const $join = dartx.join;
  let ListOfint = () => (ListOfint = dart.constFn(core.List$(core.int)))();
  let JSArrayOfListOfint = () => (JSArrayOfListOfint = dart.constFn(_interceptors.JSArray$(ListOfint())))();
  let ListOfintToString = () => (ListOfintToString = dart.constFn(dart.fnType(core.String, [ListOfint()])))();
  let ListEqualityOfint = () => (ListEqualityOfint = dart.constFn(equality.ListEquality$(core.int)))();
  let DefaultEqualityOfNull = () => (DefaultEqualityOfNull = dart.constFn(equality.DefaultEquality$(core.Null)))();
  let JSArrayOfint = () => (JSArrayOfint = dart.constFn(_interceptors.JSArray$(core.int)))();
  const CT = Object.create(null);
  dart.defineLazy(CT, {
    get C1() {
      return C1 = dart.const({
        __proto__: DefaultEqualityOfNull().prototype
      });
    },
    get C0() {
      return C0 = dart.const({
        __proto__: ListEqualityOfint().prototype,
        [ListEquality__elementEquality]: C1 || CT.C1
      });
    }
  });
  let C1;
  const ListEquality__elementEquality = dart.privateName(equality, "ListEquality._elementEquality");
  let C0;
  uuid.Uuid = class Uuid extends typed_data.UnmodifiableUint8ListView {
    static empty() {
      return new uuid.Uuid.fromBytes(_native_typed_data.NativeUint8List.new(16));
    }
    static fromString(input) {
      if (!(input != null)) dart.assertFailed(null, "org-dartlang-app:///packages/uuid_enhanced/uuid.dart", 16, 12, "input != null");
      let bytes = _native_typed_data.NativeUint8List.new(16);
      input = input[$toLowerCase]();
      let regex = core.RegExp.new("[0-9a-f]{2}");
      let matches = regex.allMatches(input)[$toList]();
      if (!(dart.notNull(matches[$length]) >= 16)) dart.assertFailed(null, "org-dartlang-app:///packages/uuid_enhanced/uuid.dart", 23, 12, "matches.length >= 16");
      let i = 0;
      for (let match of matches[$take](16)) {
        let hexString = input[$substring](match.start, match.end);
        bytes[$_set](i, hex.hex.decode(hexString)[$_get](0));
        i = i + 1;
      }
      return new uuid.Uuid.fromBytes(bytes);
    }
    static fromTime(opts) {
      let t0;
      let mSecs = opts && 'mSecs' in opts ? opts.mSecs : null;
      let nSecs = opts && 'nSecs' in opts ? opts.nSecs : null;
      let clockSequence = opts && 'clockSequence' in opts ? opts.clockSequence : null;
      let node = opts && 'node' in opts ? opts.node : null;
      let bytes = _native_typed_data.NativeUint8List.new(16);
      let clockSeq = (t0 = clockSequence, t0 == null ? uuid.Uuid._clockSeq : t0);
      mSecs == null ? mSecs = new core.DateTime.now().millisecondsSinceEpoch : null;
      nSecs == null ? nSecs = dart.notNull(uuid.Uuid._lastNSecs) + 1 : null;
      let dt = (dart.notNull(mSecs) - dart.notNull(uuid.Uuid._lastMSecs)) * 10000 + (dart.notNull(nSecs) - dart.notNull(uuid.Uuid._lastNSecs));
      if (dt < 0 && clockSequence == null) {
        clockSeq = dart.notNull(clockSeq) + 1 & 16383;
      }
      if ((dt < 0 || dart.notNull(mSecs) > dart.notNull(uuid.Uuid._lastMSecs)) && nSecs == null) {
        nSecs = 0;
      }
      if (dart.notNull(nSecs) >= 10000) {
        dart.throw(core.Exception.new("uuid.fromTime(): Can't create more than 10M uuids/sec"));
      }
      uuid.Uuid._lastMSecs = mSecs;
      uuid.Uuid._lastNSecs = nSecs;
      uuid.Uuid._clockSeq = clockSeq;
      mSecs = dart.notNull(mSecs) + 12219292800000;
      let timeLow = ((dart.notNull(mSecs) & 268435455) * 10000 + dart.notNull(nSecs))[$modulo](4294967296);
      bytes[$_set](0, timeLow[$rightShift](8 * 3) & 255);
      bytes[$_set](1, timeLow[$rightShift](8 * 2) & 255);
      bytes[$_set](2, timeLow[$rightShift](8 * 1) & 255);
      bytes[$_set](3, timeLow[$rightShift](8 * 0) & 255);
      let timeMidHigh = (dart.notNull(mSecs) / 4294967296)[$truncate]() * 10000 & 268435455;
      bytes[$_set](4, timeMidHigh[$rightShift](8 * 1) & 255);
      bytes[$_set](5, timeMidHigh[$rightShift](8 * 0) & 255);
      bytes[$_set](6, timeMidHigh[$rightShift](8 * 3) & 15 | 16);
      bytes[$_set](7, timeMidHigh[$rightShift](8 * 2) & 255);
      bytes[$_set](8, (clockSeq[$rightShift](8 * 1) | 128) >>> 0);
      bytes[$_set](9, clockSeq[$rightShift](8 * 0) & 255);
      node == null ? node = uuid.Uuid._nodeId : null;
      for (let n = 0; n < 6; n = n + 1) {
        bytes[$_set](10 + n, node[$_get](n));
      }
      return new uuid.Uuid.fromBytes(bytes);
    }
    static randomUuid(opts) {
      let random = opts && 'random' in opts ? opts.random : null;
      random == null ? random = uuid_util.UuidUtil.mathRNG() : null;
      random[$_set](6, dart.notNull(random[$_get](6)) & 15 | 64);
      random[$_set](8, dart.notNull(random[$_get](8)) & 63 | 128);
      return new uuid.Uuid.fromBytes(random);
    }
    static fromName(name, opts) {
      let namespace = opts && 'namespace' in opts ? opts.namespace : null;
      let randomNamespace = opts && 'randomNamespace' in opts ? opts.randomNamespace : true;
      namespace == null ? namespace = dart.test(randomNamespace) ? dart.toString(uuid.Uuid.randomUuid()) : uuid.Uuid.NAMESPACE_NIL : null;
      let bytes = uuid.Uuid.fromString(namespace);
      let nameBytes = _native_typed_data.NativeUint8List.fromList(name[$codeUnits]);
      let hashBytes = _native_typed_data.NativeUint8List.fromList(sha1.sha1.convert(bytes[$plus](nameBytes)).bytes);
      hashBytes[$_set](6, dart.notNull(hashBytes[$_get](6)) & 15 | 80);
      hashBytes[$_set](8, dart.notNull(hashBytes[$_get](8)) & 63 | 128);
      return new uuid.Uuid.fromBytes(hashBytes);
    }
    _equals(other) {
      if (other == null) return false;
      return uuid.Uuid.is(other) && dart.test(uuid.Uuid._equality.equals(other, this));
    }
    get hashCode() {
      return uuid.Uuid._equality.hash(this);
    }
    get isFromTime() {
      return true;
    }
    get millisecondsSinceEpoch() {
      return this._get(0)[$leftShift](8 * 3) + this._get(1)[$leftShift](8 * 2) + this._get(2)[$leftShift](8 * 1) + this._get(3)[$leftShift](8 * 0);
    }
    get clockSequence() {
      return (dart.notNull(this._get(8)) << 8 >>> 0) + dart.notNull(this._get(9));
    }
    toString() {
      return JSArrayOfListOfint().of([this.sublist(0, 4), this.sublist(4, 6), this.sublist(6, 8), this.sublist(8, 10), this.sublist(10, 16)])[$map](core.String, dart.fn(e => hex.hex.encode(e), ListOfintToString()))[$join]("-");
    }
  };
  (uuid.Uuid.fromBytes = function(list) {
    uuid.Uuid.__proto__.new.call(this, list);
    ;
  }).prototype = uuid.Uuid.prototype;
  dart.addTypeTests(uuid.Uuid);
  dart.setGetterSignature(uuid.Uuid, () => ({
    __proto__: dart.getGetters(uuid.Uuid.__proto__),
    isFromTime: core.bool,
    millisecondsSinceEpoch: core.int,
    clockSequence: core.int
  }));
  dart.setLibraryUri(uuid.Uuid, "package:uuid_enhanced/uuid.dart");
  dart.defineExtensionMethods(uuid.Uuid, ['_equals', 'toString']);
  dart.defineExtensionAccessors(uuid.Uuid, ['hashCode']);
  dart.defineLazy(uuid.Uuid, {
    /*uuid.Uuid.NAMESPACE_DNS*/get NAMESPACE_DNS() {
      return "6ba7b810-9dad-11d1-80b4-00c04fd430c8";
    },
    /*uuid.Uuid.NAMESPACE_URL*/get NAMESPACE_URL() {
      return "6ba7b811-9dad-11d1-80b4-00c04fd430c8";
    },
    /*uuid.Uuid.NAMESPACE_OID*/get NAMESPACE_OID() {
      return "6ba7b812-9dad-11d1-80b4-00c04fd430c8";
    },
    /*uuid.Uuid.NAMESPACE_X500*/get NAMESPACE_X500() {
      return "6ba7b814-9dad-11d1-80b4-00c04fd430c8";
    },
    /*uuid.Uuid.NAMESPACE_NIL*/get NAMESPACE_NIL() {
      return "00000000-0000-0000-0000-000000000000";
    },
    /*uuid.Uuid._equality*/get _equality() {
      return C0 || CT.C0;
    },
    /*uuid.Uuid._seedBytes*/get _seedBytes() {
      return uuid_util.UuidUtil.cryptoRNG();
    },
    /*uuid.Uuid._lastMSecs*/get _lastMSecs() {
      return 0;
    },
    set _lastMSecs(_) {},
    /*uuid.Uuid._lastNSecs*/get _lastNSecs() {
      return 0;
    },
    set _lastNSecs(_) {},
    /*uuid.Uuid._clockSeq*/get _clockSeq() {
      return (dart.notNull(uuid.Uuid._seedBytes[$_get](6)) << 8 | dart.notNull(uuid.Uuid._seedBytes[$_get](7))) & 262143;
    },
    set _clockSeq(_) {},
    /*uuid.Uuid._nodeId*/get _nodeId() {
      return _native_typed_data.NativeUint8List.fromList(JSArrayOfint().of([(dart.notNull(uuid.Uuid._seedBytes[$_get](0)) | 1) >>> 0, uuid.Uuid._seedBytes[$_get](1), uuid.Uuid._seedBytes[$_get](2), uuid.Uuid._seedBytes[$_get](3), uuid.Uuid._seedBytes[$_get](4), uuid.Uuid._seedBytes[$_get](5)]));
    }
  });
  dart.trackLibraries("packages/uuid_enhanced/uuid", {
    "package:uuid_enhanced/uuid.dart": uuid
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["uuid.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAY0B,YAAK,yBAAU,uCAAU;IAAI;sBAEtB;AAC7B,YAAO,AAAM,KAAD,IAAI;AAEA,kBAAQ,uCAAU;AACP,MAA3B,QAAQ,AAAM,KAAD;AAEA,kBAAQ,gBAAO;AACV,oBAAU,AAAM,AAAkB,KAAnB,YAAY,KAAK;AAClD,YAAsB,aAAf,AAAQ,OAAD,cAAW;AAErB,cAAI;AACR,eAAW,QAAS,AAAQ,QAAD,QAAM;AAClB,wBAAY,AAAM,KAAD,aAAW,AAAM,KAAD,QAAQ,AAAM,KAAD;AACxB,QAAnC,AAAK,KAAA,QAAC,CAAC,EAAI,AAAI,AAAiB,eAAV,SAAS,SAAE;AAC9B,QAAH,IAAA,AAAC,CAAA;;AAEH,YAAY,yBAAU,KAAK;IAC7B;;;UAUM;UACA;UACM;UACA;AAEM,kBAAQ,uCAAU;AAE9B,sBAAyB,KAAd,aAAa,QAAb,OAAiB;AAMe,MAA/C,AAAM,KAAD,IAAC,OAAN,QAAmB,AAAM,iDAAnB;AAIkB,MAAxB,AAAM,KAAD,IAAC,OAAN,QAAqB,aAAX,wBAAa,IAAjB;AAGI,eAA0B,AAAQ,CAAtB,aAAN,KAAK,iBAAG,yBAAc,SAAe,aAAN,KAAK,iBAAG;AAGvD,UAAI,AAAG,EAAD,GAAG,KAAK,AAAc,aAAD,IAAI;AACG,QAAhC,WAAoB,AAAI,aAAb,QAAQ,IAAG,IAAI;;AAK5B,WAAK,AAAG,EAAD,GAAG,KAAW,aAAN,KAAK,iBAAG,0BAAe,AAAM,KAAD,IAAI;AACpC,QAAT,QAAQ;;AAIV,UAAU,aAAN,KAAK,KAAI;AAC8D,QAAzE,WAAM,mBAAU;;AAGA,MAAlB,uBAAa,KAAK;AACA,MAAlB,uBAAa,KAAK;AACE,MAApB,sBAAY,QAAQ;AAGG,MAAvB,QAAM,aAAN,KAAK,IAAI;AAGC,oBAAgD,CAAjB,AAAQ,CAArB,aAAN,KAAK,IAAG,aAAa,qBAAQ,KAAK,YAAI;AAC1B,MAAlC,AAAK,KAAA,QAAC,GAAK,AAAQ,AAAS,OAAV,cAAI,AAAE,IAAE,KAAI;AACI,MAAlC,AAAK,KAAA,QAAC,GAAK,AAAQ,AAAS,OAAV,cAAI,AAAE,IAAE,KAAI;AACI,MAAlC,AAAK,KAAA,QAAC,GAAK,AAAQ,AAAS,OAAV,cAAI,AAAE,IAAE,KAAI;AACI,MAAlC,AAAK,KAAA,QAAC,GAAK,AAAQ,AAAS,OAAV,cAAI,AAAE,IAAE,KAAI;AAGpB,wBAAqB,AAAe,AAAS,cAA9B,KAAK,IAAI,2BAAc,QAAS;AACnB,MAAtC,AAAK,KAAA,QAAC,GAAK,AAAY,AAAS,WAAV,cAAI,AAAE,IAAE,KAAI;AACI,MAAtC,AAAK,KAAA,QAAC,GAAK,AAAY,AAAS,WAAV,cAAI,AAAE,IAAE,KAAI;AAGU,MAA5C,AAAK,KAAA,QAAC,GAAK,AAAY,AAAS,AAAM,WAAhB,cAAI,AAAE,IAAE,KAAI,KAAM;AACF,MAAtC,AAAK,KAAA,QAAC,GAAK,AAAY,AAAS,WAAV,cAAI,AAAE,IAAE,KAAI;AAGC,MAAnC,AAAK,KAAA,QAAC,GAAuB,CAAlB,AAAS,QAAD,cAAI,AAAE,IAAE,KAAI;AAGI,MAAnC,AAAK,KAAA,QAAC,GAAK,AAAS,AAAS,QAAV,cAAI,AAAE,IAAE,KAAI;AAGf,MAAhB,AAAK,IAAD,IAAC,OAAL,OAAS,oBAAJ;AACL,eAAS,IAAI,GAAG,AAAE,CAAD,GAAG,GAAG,IAAA,AAAC,CAAA;AACC,QAAvB,AAAK,KAAA,QAAC,AAAG,KAAE,CAAC,EAAI,AAAI,IAAA,QAAC,CAAC;;AAGxB,YAAY,yBAAU,KAAK;IAC7B;;UASY;AAGmB,MAA7B,AAAO,MAAD,IAAC,OAAP,SAAoB,+BAAb;AAG8B,MAArC,AAAM,MAAA,QAAC,GAAgB,AAAQ,aAAlB,AAAM,MAAA,QAAC,MAAK,KAAQ;AACI,MAArC,AAAM,MAAA,QAAC,GAAgB,AAAQ,aAAlB,AAAM,MAAA,QAAC,MAAK,KAAQ;AACjC,YAAY,yBAAU,MAAM;IAC9B;oBAaS;UACA;UAGF;AAK6D,MADlE,AAAU,SAAD,IAAC,OAAV,sBACI,eAAe,IAAqB,cAAb,0BAA0B,0BAD3C;AAIM,kBAAa,qBAAW,SAAS;AAGjC,sBAAsB,4CAAS,AAAK,IAAD;AAGnC,sBACF,4CAAS,AAAK,AAA2B,kBAAnB,AAAM,KAAD,QAAG,SAAS;AAGV,MAA3C,AAAS,SAAA,QAAC,GAAmB,AAAQ,aAArB,AAAS,SAAA,QAAC,MAAK,KAAQ;AACI,MAA3C,AAAS,SAAA,QAAC,GAAmB,AAAQ,aAArB,AAAS,SAAA,QAAC,MAAK,KAAQ;AAEvC,YAAY,yBAAU,SAAS;IACjC;;UAKyB;AACrB,YAAM,AAAQ,cAAd,KAAK,eAAY,AAAU,2BAAO,KAAK,EAAE;IAAK;;AAG9B,YAAA,AAAU,0BAAK;IAAK;;AA+BtC,YAAO;IACT;;AAGE,YAAY,AAAI,AAAU,AACH,AACA,WAFV,eAAM,AAAE,IAAE,KACd,AAAI,UAAH,eAAM,AAAE,IAAE,KACX,AAAI,UAAH,eAAM,AAAE,IAAE,KACX,AAAI,UAAH,eAAM,AAAE,IAAE;IACtB;;AAGE,YAAsB,EAAN,aAAJ,UAAC,OAAM,wBAAS,UAAC;IAC/B;;AAKE,YAAkB,AAMhB,AAAoC,0BALpC,aAAQ,GAAG,IACX,aAAQ,GAAG,IACX,aAAQ,GAAG,IACX,aAAQ,GAAG,KACX,aAAQ,IAAI,yBACR,QAAW,KAAM,AAAI,eAAO,CAAC,gCAAQ;IAC7C;;kCAnOyB;AAAQ,uCAAM,IAAI;;EAAC;;;;;;;;;;;;MA+KxB,uBAAa;;;MACb,uBAAa;;;MACb,uBAAa;;;MACb,wBAAc;;;MACd,uBAAa;;;MAEF,mBAAS;;;MAGjB,oBAAU;YAAY;;MAElC,oBAAU;YAAG;;;MACb,oBAAU;YAAG;;;MAEb,mBAAS;YAAwC,EAAtB,AAAK,aAAnB,AAAU,4BAAC,OAAM,iBAAI,AAAU,4BAAC,OAAM;;;MAGvC,iBAAO;YAAa,6CAAc,mBACzC,cAAd,AAAU,4BAAC,MAAK,UAChB,AAAU,4BAAC,IACX,AAAU,4BAAC,IACX,AAAU,4BAAC,IACX,AAAU,4BAAC,IACX,AAAU,4BAAC","file":"uuid.ddc.js"}');
  // Exports:
  return {
    uuid: uuid
  };
});

//# sourceMappingURL=uuid.ddc.js.map
