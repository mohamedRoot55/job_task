define(['dart_sdk'], function(dart_sdk) {
  'use strict';
  const core = dart_sdk.core;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const ast = Object.create(dart.library);
  const CT = Object.create(null);
  dart.trackLibraries("packages/gql/ast", {
    "package:gql/ast.dart": ast
  }, {
  }, '{"version":3,"sourceRoot":"","sources":[],"names":[],"mappings":"","file":"ast.ddc.js"}');
  // Exports:
  return {
    ast: ast
  };
});

//# sourceMappingURL=ast.ddc.js.map
