define(['dart_sdk', 'packages/gql/src/ast/ast', 'packages/source_span/source_span'], function(dart_sdk, packages__gql__src__ast__ast, packages__source_span__source_span) {
  'use strict';
  const core = dart_sdk.core;
  const _interceptors = dart_sdk._interceptors;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const ast = packages__gql__src__ast__ast.src__ast__ast;
  const visitor = packages__gql__src__ast__ast.src__ast__visitor;
  const span_exception = packages__source_span__source_span.src__span_exception;
  const file = packages__source_span__source_span.src__file;
  const language = Object.create(dart.library);
  const printer = Object.create(dart.library);
  const parser$ = Object.create(dart.library);
  const lexer$ = Object.create(dart.library);
  const $join = dartx.join;
  const $map = dartx.map;
  const $add = dartx.add;
  const $expand = dartx.expand;
  const $skip = dartx.skip;
  const $isNotEmpty = dartx.isNotEmpty;
  const $replaceAll = dartx.replaceAll;
  const $isEmpty = dartx.isEmpty;
  const $length = dartx.length;
  const $_get = dartx._get;
  const $_equals = dartx._equals;
  const $substring = dartx.substring;
  const $codeUnitAt = dartx.codeUnitAt;
  const $replaceAllMapped = dartx.replaceAllMapped;
  const $split = dartx.split;
  const $toList = dartx.toList;
  const $skipWhile = dartx.skipWhile;
  const $first = dartx.first;
  const $removeAt = dartx.removeAt;
  const $last = dartx.last;
  const $removeLast = dartx.removeLast;
  let ListOfString = () => (ListOfString = dart.constFn(core.List$(core.String)))();
  let DefinitionNodeToString = () => (DefinitionNodeToString = dart.constFn(dart.fnType(core.String, [ast.DefinitionNode])))();
  let DirectiveNodeToString = () => (DirectiveNodeToString = dart.constFn(dart.fnType(core.String, [ast.DirectiveNode])))();
  let JSArrayOfString = () => (JSArrayOfString = dart.constFn(_interceptors.JSArray$(core.String)))();
  let OperationTypeDefinitionNodeToListOfString = () => (OperationTypeDefinitionNodeToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.OperationTypeDefinitionNode])))();
  let VariableDefinitionNodeToString = () => (VariableDefinitionNodeToString = dart.constFn(dart.fnType(core.String, [ast.VariableDefinitionNode])))();
  let ArgumentNodeToString = () => (ArgumentNodeToString = dart.constFn(dart.fnType(core.String, [ast.ArgumentNode])))();
  let ObjectFieldNodeToListOfString = () => (ObjectFieldNodeToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.ObjectFieldNode])))();
  let ValueNodeToListOfString = () => (ValueNodeToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.ValueNode])))();
  let SelectionNodeToListOfString = () => (SelectionNodeToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.SelectionNode])))();
  let NamedTypeNodeToListOfString = () => (NamedTypeNodeToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.NamedTypeNode])))();
  let FieldDefinitionNodeToListOfString = () => (FieldDefinitionNodeToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.FieldDefinitionNode])))();
  let InputValueDefinitionNodeToListOfString = () => (InputValueDefinitionNodeToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.InputValueDefinitionNode])))();
  let EnumValueDefinitionNodeToListOfString = () => (EnumValueDefinitionNodeToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.EnumValueDefinitionNode])))();
  let DirectiveLocationToListOfString = () => (DirectiveLocationToListOfString = dart.constFn(dart.fnType(ListOfString(), [ast.DirectiveLocation])))();
  let JSArrayOfDirectiveNode = () => (JSArrayOfDirectiveNode = dart.constFn(_interceptors.JSArray$(ast.DirectiveNode)))();
  let JSArrayOfNamedTypeNode = () => (JSArrayOfNamedTypeNode = dart.constFn(_interceptors.JSArray$(ast.NamedTypeNode)))();
  let JSArrayOfDirectiveLocation = () => (JSArrayOfDirectiveLocation = dart.constFn(_interceptors.JSArray$(ast.DirectiveLocation)))();
  let MatchToString = () => (MatchToString = dart.constFn(dart.fnType(core.String, [core.Match])))();
  let JSArrayOfToken = () => (JSArrayOfToken = dart.constFn(_interceptors.JSArray$(lexer$.Token)))();
  let StringToString = () => (StringToString = dart.constFn(dart.fnType(core.String, [core.String])))();
  let StringTobool = () => (StringTobool = dart.constFn(dart.fnType(core.bool, [core.String])))();
  const CT = Object.create(null);
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.const({
        __proto__: ast.OperationType.prototype,
        [_name]: "OperationType.query",
        index: 0
      });
    },
    get C1() {
      return C1 = dart.const({
        __proto__: ast.OperationType.prototype,
        [_name]: "OperationType.mutation",
        index: 1
      });
    },
    get C2() {
      return C2 = dart.const({
        __proto__: ast.OperationType.prototype,
        [_name]: "OperationType.subscription",
        index: 2
      });
    },
    get C3() {
      return C3 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.query",
        index: 0
      });
    },
    get C4() {
      return C4 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.mutation",
        index: 1
      });
    },
    get C5() {
      return C5 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.subscription",
        index: 2
      });
    },
    get C6() {
      return C6 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.field",
        index: 3
      });
    },
    get C7() {
      return C7 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.fragmentDefinition",
        index: 4
      });
    },
    get C8() {
      return C8 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.fragmentSpread",
        index: 5
      });
    },
    get C9() {
      return C9 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.inlineFragment",
        index: 6
      });
    },
    get C10() {
      return C10 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.schema",
        index: 7
      });
    },
    get C11() {
      return C11 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.scalar",
        index: 8
      });
    },
    get C12() {
      return C12 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.object",
        index: 9
      });
    },
    get C13() {
      return C13 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.fieldDefinition",
        index: 10
      });
    },
    get C14() {
      return C14 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.argumentDefinition",
        index: 11
      });
    },
    get C15() {
      return C15 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.interface",
        index: 12
      });
    },
    get C16() {
      return C16 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.union",
        index: 13
      });
    },
    get C17() {
      return C17 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.enumDefinition",
        index: 14
      });
    },
    get C18() {
      return C18 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.enumValue",
        index: 15
      });
    },
    get C19() {
      return C19 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.inputObject",
        index: 16
      });
    },
    get C20() {
      return C20 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name]: "DirectiveLocation.inputFieldDefinition",
        index: 17
      });
    },
    get C21() {
      return C21 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.bracketL",
        index: 11
      });
    },
    get C22() {
      return C22 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.braceL",
        index: 13
      });
    },
    get C23() {
      return C23 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.int",
        index: 17
      });
    },
    get C24() {
      return C24 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.float",
        index: 18
      });
    },
    get C25() {
      return C25 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.string",
        index: 19
      });
    },
    get C26() {
      return C26 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.blockString",
        index: 20
      });
    },
    get C27() {
      return C27 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.name",
        index: 16
      });
    },
    get C28() {
      return C28 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.dollar",
        index: 3
      });
    },
    get C29() {
      return C29 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.sof",
        index: 0
      });
    },
    get C30() {
      return C30 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.eof",
        index: 1
      });
    },
    get C31() {
      return C31 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.bang",
        index: 2
      });
    },
    get C32() {
      return C32 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.amp",
        index: 4
      });
    },
    get C33() {
      return C33 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.parenL",
        index: 5
      });
    },
    get C34() {
      return C34 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.parenR",
        index: 6
      });
    },
    get C35() {
      return C35 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.spread",
        index: 7
      });
    },
    get C36() {
      return C36 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.colon",
        index: 8
      });
    },
    get C37() {
      return C37 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.equals",
        index: 9
      });
    },
    get C38() {
      return C38 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.at",
        index: 10
      });
    },
    get C39() {
      return C39 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.bracketR",
        index: 12
      });
    },
    get C40() {
      return C40 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.pipe",
        index: 14
      });
    },
    get C41() {
      return C41 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.braceR",
        index: 15
      });
    },
    get C42() {
      return C42 = dart.const({
        __proto__: lexer$.TokenKind.prototype,
        [_name$]: "TokenKind.comment",
        index: 21
      });
    },
    get C43() {
      return C43 = dart.constList([C29 || CT.C29, C30 || CT.C30, C31 || CT.C31, C28 || CT.C28, C32 || CT.C32, C33 || CT.C33, C34 || CT.C34, C35 || CT.C35, C36 || CT.C36, C37 || CT.C37, C38 || CT.C38, C21 || CT.C21, C39 || CT.C39, C22 || CT.C22, C40 || CT.C40, C41 || CT.C41, C27 || CT.C27, C23 || CT.C23, C24 || CT.C24, C25 || CT.C25, C26 || CT.C26, C42 || CT.C42], lexer$.TokenKind);
    },
    get C44() {
      return C44 = dart.fn(lexer$.isBlank, StringTobool());
    }
  });
  const _tabs = dart.privateName(printer, "_tabs");
  const _indent = dart.privateName(printer, "_indent");
  printer._PrintVisitor = class _PrintVisitor extends visitor.Visitor$(core.String) {
    [_indent](tabs) {
      return ListOfString().filled(tabs, "  ")[$join]();
    }
    visitDocumentNode(doc) {
      return doc.definitions[$map](core.String, dart.fn(def => def.accept(core.String, this), DefinitionNodeToString()))[$join]("\n\n");
    }
    visitDirectiveSetNode(directives) {
      return directives[$map](core.String, dart.fn(directive => directive.accept(core.String, this), DirectiveNodeToString()))[$join](" ");
    }
    visitOperationTypeDefinitionSetNode(operationTypes) {
      return (() => {
        let t0 = JSArrayOfString().of([]);
        t0[$add]("{");
        t0[$add]("\n");
        t0[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) + 1));
        for (let t1 of operationTypes[$expand](core.String, dart.fn(operationType => JSArrayOfString().of([this[_indent](this[_tabs]), operationType.accept(core.String, this), "\n"]), OperationTypeDefinitionNodeToListOfString()))[$skip](1))
          t0[$add](t1);
        t0[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) - 1));
        t0[$add]("}");
        return t0;
      })()[$join]();
    }
    visitVariableDefinitionSetNode(defs) {
      return "(" + dart.notNull(defs[$map](core.String, dart.fn(def => def.accept(core.String, this), VariableDefinitionNodeToString()))[$join](", ")) + ")";
    }
    visitArgumentSetNode(args) {
      return "(" + dart.notNull(args[$map](core.String, dart.fn(arg => arg.accept(core.String, this), ArgumentNodeToString()))[$join](", ")) + ")";
    }
    visitVariableDefinitionNode(def) {
      return JSArrayOfString().of([def.variable.accept(core.String, this), ": ", def.type.accept(core.String, this), def.defaultValue.accept(core.String, this)])[$join]();
    }
    visitDefaultValueNode(defaultValueNode) {
      if (defaultValueNode.value == null) return "";
      return " = " + dart.str(defaultValueNode.value.accept(core.String, this));
    }
    visitVariableNode(variable) {
      return "$" + dart.notNull(variable.name.accept(core.String, this));
    }
    visitOperationDefinitionNode(op) {
      return (() => {
        let t2 = JSArrayOfString().of([]);
        t2[$add](printer._opType(op.type));
        if (op.name != null) for (let t3 of JSArrayOfString().of([" ", op.name.accept(core.String, this)]))
          t2[$add](t3);
        if (op.variableDefinitions != null && dart.test(op.variableDefinitions[$isNotEmpty])) t2[$add](this.visitVariableDefinitionSetNode(op.variableDefinitions));
        if (op.directives != null && dart.test(op.directives[$isNotEmpty])) for (let t4 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(op.directives)]))
          t2[$add](t4);
        t2[$add](" ");
        t2[$add](this.visitSelectionSetNode(op.selectionSet));
        return t2;
      })()[$join]();
    }
    visitNameNode(name) {
      return name.value;
    }
    visitDirectiveNode(directiveNode) {
      return (() => {
        let t5 = JSArrayOfString().of([]);
        t5[$add]("@");
        t5[$add](directiveNode.name.accept(core.String, this));
        if (directiveNode.arguments != null && dart.test(directiveNode.arguments[$isNotEmpty])) t5[$add](this.visitArgumentSetNode(directiveNode.arguments));
        return t5;
      })()[$join]();
    }
    visitListTypeNode(listTypeNode) {
      return (() => {
        let t6 = JSArrayOfString().of([]);
        t6[$add]("[");
        t6[$add](listTypeNode.type.accept(core.String, this));
        t6[$add]("]");
        if (dart.test(listTypeNode.isNonNull)) t6[$add]("!");
        return t6;
      })()[$join]();
    }
    visitNamedTypeNode(namedTypeNode) {
      return (() => {
        let t7 = JSArrayOfString().of([]);
        t7[$add](namedTypeNode.name.accept(core.String, this));
        if (dart.test(namedTypeNode.isNonNull)) t7[$add]("!");
        return t7;
      })()[$join]();
    }
    visitObjectFieldNode(objectFieldNode) {
      return JSArrayOfString().of([objectFieldNode.name.accept(core.String, this), ": ", objectFieldNode.value.accept(core.String, this)])[$join]();
    }
    visitObjectValueNode(objectValueNode) {
      return (() => {
        let t8 = JSArrayOfString().of([]);
        t8[$add]("{");
        for (let t9 of objectValueNode.fields[$expand](core.String, dart.fn(field => JSArrayOfString().of([", ", field.accept(core.String, this)]), ObjectFieldNodeToListOfString()))[$skip](1))
          t8[$add](t9);
        t8[$add]("}");
        return t8;
      })()[$join]();
    }
    visitListValueNode(listValueNode) {
      return (() => {
        let t10 = JSArrayOfString().of([]);
        t10[$add]("[");
        for (let t11 of listValueNode.values[$expand](core.String, dart.fn(value => JSArrayOfString().of([", ", value.accept(core.String, this)]), ValueNodeToListOfString()))[$skip](1))
          t10[$add](t11);
        t10[$add]("]");
        return t10;
      })()[$join]();
    }
    visitEnumValueNode(enumValueNode) {
      return enumValueNode.name.accept(core.String, this);
    }
    visitNullValueNode(nullValueNode) {
      return "null";
    }
    visitBooleanValueNode(booleanValueNode) {
      return dart.test(booleanValueNode.value) ? "true" : "false";
    }
    visitStringValueNode(stringValueNode) {
      if (!dart.test(stringValueNode.isBlock)) {
        return JSArrayOfString().of(["\"", stringValueNode.value, "\""])[$join]();
      }
      return JSArrayOfString().of(["\"\"\"", "\n", this[_indent](this[_tabs]), stringValueNode.value[$replaceAll]("\"\"\"", "\\\"\"\""), "\n", this[_indent](this[_tabs]), "\"\"\""])[$join]();
    }
    visitFloatValueNode(floatValueNode) {
      return floatValueNode.value;
    }
    visitIntValueNode(intValueNode) {
      return intValueNode.value;
    }
    visitTypeConditionNode(typeConditionNode) {
      return "on " + dart.str(typeConditionNode.on.accept(core.String, this));
    }
    visitFragmentDefinitionNode(fragmentDefinitionNode) {
      return (() => {
        let t12 = JSArrayOfString().of([]);
        t12[$add]("fragment ");
        t12[$add](fragmentDefinitionNode.name.accept(core.String, this));
        if (fragmentDefinitionNode.typeCondition != null) for (let t13 of JSArrayOfString().of([" ", fragmentDefinitionNode.typeCondition.accept(core.String, this)]))
          t12[$add](t13);
        if (fragmentDefinitionNode.directives != null && dart.test(fragmentDefinitionNode.directives[$isNotEmpty])) for (let t14 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(fragmentDefinitionNode.directives)]))
          t12[$add](t14);
        if (fragmentDefinitionNode.selectionSet != null) for (let t15 of JSArrayOfString().of([" ", fragmentDefinitionNode.selectionSet.accept(core.String, this)]))
          t12[$add](t15);
        return t12;
      })()[$join]();
    }
    visitInlineFragmentNode(inlineFragmentNode) {
      return (() => {
        let t16 = JSArrayOfString().of([]);
        t16[$add]("...");
        if (inlineFragmentNode.typeCondition != null) for (let t17 of JSArrayOfString().of([" ", inlineFragmentNode.typeCondition.accept(core.String, this)]))
          t16[$add](t17);
        if (inlineFragmentNode.directives != null && dart.test(inlineFragmentNode.directives[$isNotEmpty])) for (let t18 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(inlineFragmentNode.directives)]))
          t16[$add](t18);
        if (inlineFragmentNode.selectionSet != null) for (let t19 of JSArrayOfString().of([" ", inlineFragmentNode.selectionSet.accept(core.String, this)]))
          t16[$add](t19);
        return t16;
      })()[$join]();
    }
    visitFragmentSpreadNode(fragmentSpreadNode) {
      return (() => {
        let t20 = JSArrayOfString().of([]);
        t20[$add]("...");
        t20[$add](fragmentSpreadNode.name.accept(core.String, this));
        if (fragmentSpreadNode.directives != null && dart.test(fragmentSpreadNode.directives[$isNotEmpty])) for (let t21 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(fragmentSpreadNode.directives)]))
          t20[$add](t21);
        return t20;
      })()[$join]();
    }
    visitArgumentNode(argumentNode) {
      return JSArrayOfString().of([argumentNode.name.accept(core.String, this), ": ", argumentNode.value.accept(core.String, this)])[$join]();
    }
    visitFieldNode(fieldNode) {
      return (() => {
        let t22 = JSArrayOfString().of([]);
        if (fieldNode.alias != null) for (let t23 of JSArrayOfString().of([fieldNode.alias.accept(core.String, this), ": "]))
          t22[$add](t23);
        t22[$add](fieldNode.name.accept(core.String, this));
        if (fieldNode.arguments != null && dart.test(fieldNode.arguments[$isNotEmpty])) t22[$add](this.visitArgumentSetNode(fieldNode.arguments));
        if (fieldNode.directives != null && dart.test(fieldNode.directives[$isNotEmpty])) for (let t24 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(fieldNode.directives)]))
          t22[$add](t24);
        if (fieldNode.selectionSet != null) for (let t25 of JSArrayOfString().of([" ", this.visitSelectionSetNode(fieldNode.selectionSet)]))
          t22[$add](t25);
        return t22;
      })()[$join]();
    }
    visitSelectionSetNode(selectionSetNode) {
      return (() => {
        let t26 = JSArrayOfString().of([]);
        t26[$add]("{");
        t26[$add]("\n");
        t26[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) + 1));
        for (let t27 of selectionSetNode.selections[$expand](core.String, dart.fn(selection => JSArrayOfString().of(["\n", this[_indent](this[_tabs]), selection.accept(core.String, this)]), SelectionNodeToListOfString()))[$skip](2))
          t26[$add](t27);
        t26[$add]("\n");
        t26[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) - 1));
        t26[$add]("}");
        return t26;
      })()[$join]();
    }
    visitSchemaDefinitionNode(node) {
      return JSArrayOfString().of(["schema", " ", this.visitDirectiveSetNode(node.directives), this.visitOperationTypeDefinitionSetNode(node.operationTypes)])[$join]();
    }
    visitOperationTypeDefinitionNode(node) {
      return JSArrayOfString().of([printer._opType(node.operation), ": ", node.type.accept(core.String, this)])[$join]();
    }
    visitScalarTypeDefinitionNode(node) {
      return (() => {
        let t28 = JSArrayOfString().of([]);
        if (node.description != null) for (let t29 of JSArrayOfString().of([node.description.accept(core.String, this), "\n"]))
          t28[$add](t29);
        t28[$add]("scalar");
        t28[$add](" ");
        t28[$add](node.name.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) for (let t30 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(node.directives)]))
          t28[$add](t30);
        return t28;
      })()[$join]();
    }
    visitObjectTypeDefinitionNode(node) {
      return (() => {
        let t31 = JSArrayOfString().of([]);
        if (node.description != null) for (let t32 of JSArrayOfString().of([node.description.accept(core.String, this), "\n"]))
          t31[$add](t32);
        t31[$add]("type");
        t31[$add](" ");
        t31[$add](node.name.accept(core.String, this));
        t31[$add](" ");
        t31[$add](this.visitImplementsSetNode(node.interfaces));
        t31[$add](this.visitDirectiveSetNode(node.directives));
        t31[$add](this.visitFieldSetNode(node.fields));
        return t31;
      })()[$join]();
    }
    visitImplementsSetNode(interfaces) {
      return dart.test(interfaces[$isEmpty]) ? "" : (() => {
        let t33 = JSArrayOfString().of([]);
        t33[$add]("implements");
        t33[$add](" ");
        for (let t34 of interfaces[$expand](core.String, dart.fn($interface => JSArrayOfString().of(["&", " ", $interface.accept(core.String, this), " "]), NamedTypeNodeToListOfString()))[$skip](2))
          t33[$add](t34);
        return t33;
      })()[$join]();
    }
    visitFieldSetNode(fields) {
      return dart.test(fields[$isEmpty]) ? "" : (() => {
        let t35 = JSArrayOfString().of([]);
        t35[$add]("{");
        t35[$add]("\n");
        t35[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) + 1));
        for (let t36 of fields[$expand](core.String, dart.fn(field => JSArrayOfString().of(["\n", this[_indent](this[_tabs]), field.accept(core.String, this)]), FieldDefinitionNodeToListOfString()))[$skip](2))
          t35[$add](t36);
        t35[$add]("\n");
        t35[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) - 1));
        t35[$add]("}");
        return t35;
      })()[$join]();
    }
    visitFieldDefinitionNode(node) {
      return (() => {
        let t37 = JSArrayOfString().of([]);
        if (node.description != null) for (let t38 of JSArrayOfString().of([node.description.accept(core.String, this), "\n", this[_indent](this[_tabs])]))
          t37[$add](t38);
        t37[$add](node.name.accept(core.String, this));
        t37[$add](this.visitArgumentDefinitionSetNode(node.args));
        t37[$add](":");
        t37[$add](" ");
        t37[$add](node.type.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) t37[$add](" ");
        t37[$add](this.visitDirectiveSetNode(node.directives));
        return t37;
      })()[$join]();
    }
    visitArgumentDefinitionSetNode(args) {
      return dart.test(args[$isEmpty]) ? "" : (() => {
        let t39 = JSArrayOfString().of([]);
        t39[$add]("(");
        for (let t40 of args[$expand](core.String, dart.fn(arg => JSArrayOfString().of([",", " ", arg.accept(core.String, this)]), InputValueDefinitionNodeToListOfString()))[$skip](2))
          t39[$add](t40);
        t39[$add](")");
        return t39;
      })()[$join]();
    }
    visitInputValueDefinitionNode(node) {
      return (() => {
        let t41 = JSArrayOfString().of([]);
        if (node.description != null) for (let t42 of JSArrayOfString().of([node.description.accept(core.String, this), "\n", this[_indent](this[_tabs])]))
          t41[$add](t42);
        t41[$add](node.name.accept(core.String, this));
        t41[$add](":");
        t41[$add](" ");
        t41[$add](node.type.accept(core.String, this));
        if (node.defaultValue != null) for (let t43 of JSArrayOfString().of([" ", "=", " ", node.defaultValue.accept(core.String, this)]))
          t41[$add](t43);
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) t41[$add](" ");
        t41[$add](this.visitDirectiveSetNode(node.directives));
        return t41;
      })()[$join]();
    }
    visitInterfaceTypeDefinitionNode(node) {
      return (() => {
        let t44 = JSArrayOfString().of([]);
        if (node.description != null) for (let t45 of JSArrayOfString().of([node.description.accept(core.String, this), "\n"]))
          t44[$add](t45);
        t44[$add]("interface");
        t44[$add](" ");
        t44[$add](node.name.accept(core.String, this));
        t44[$add](" ");
        t44[$add](this.visitDirectiveSetNode(node.directives));
        t44[$add](this.visitFieldSetNode(node.fields));
        return t44;
      })()[$join]();
    }
    visitUnionTypeDefinitionNode(node) {
      return (() => {
        let t46 = JSArrayOfString().of([]);
        if (node.description != null) for (let t47 of JSArrayOfString().of([node.description.accept(core.String, this), "\n"]))
          t46[$add](t47);
        t46[$add]("union");
        t46[$add](" ");
        t46[$add](node.name.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) for (let t48 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(node.directives)]))
          t46[$add](t48);
        t46[$add](this.visitUnionTypeSetNode(node.types));
        return t46;
      })()[$join]();
    }
    visitUnionTypeSetNode(types) {
      return dart.test(types[$isEmpty]) ? "" : (() => {
        let t49 = JSArrayOfString().of([]);
        t49[$add](" ");
        t49[$add]("=");
        t49[$add]("\n");
        t49[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) + 1));
        for (let t50 of types[$expand](core.String, dart.fn(type => JSArrayOfString().of(["\n", this[_indent](this[_tabs]), "|", " ", type.accept(core.String, this)]), NamedTypeNodeToListOfString()))[$skip](2))
          t49[$add](t50);
        for (let t51 of JSArrayOfString().of([this[_indent](this[_tabs] = dart.notNull(this[_tabs]) - 1)])[$skip](1))
          t49[$add](t51);
        return t49;
      })()[$join]();
    }
    visitEnumTypeDefinitionNode(node) {
      return (() => {
        let t52 = JSArrayOfString().of([]);
        if (node.description != null) for (let t53 of JSArrayOfString().of([node.description.accept(core.String, this), "\n"]))
          t52[$add](t53);
        t52[$add]("enum");
        t52[$add](" ");
        t52[$add](node.name.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) for (let t54 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(node.directives)]))
          t52[$add](t54);
        t52[$add](this.visitEnumValueDefinitionSetNode(node.values));
        return t52;
      })()[$join]();
    }
    visitEnumValueDefinitionSetNode(values) {
      return dart.test(values[$isEmpty]) ? "" : (() => {
        let t55 = JSArrayOfString().of([]);
        t55[$add](" ");
        t55[$add]("{");
        t55[$add]("\n");
        t55[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) + 1));
        for (let t56 of values[$expand](core.String, dart.fn(value => JSArrayOfString().of(["\n", this[_indent](this[_tabs]), value.accept(core.String, this)]), EnumValueDefinitionNodeToListOfString()))[$skip](2))
          t55[$add](t56);
        t55[$add]("\n");
        t55[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) - 1));
        t55[$add]("}");
        return t55;
      })()[$join]();
    }
    visitEnumValueDefinitionNode(node) {
      return (() => {
        let t57 = JSArrayOfString().of([]);
        if (node.description != null) for (let t58 of JSArrayOfString().of([node.description.accept(core.String, this), "\n", this[_indent](this[_tabs])]))
          t57[$add](t58);
        t57[$add](node.name.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) t57[$add](" ");
        t57[$add](this.visitDirectiveSetNode(node.directives));
        return t57;
      })()[$join]();
    }
    visitInputObjectTypeDefinitionNode(node) {
      return (() => {
        let t59 = JSArrayOfString().of([]);
        if (node.description != null) for (let t60 of JSArrayOfString().of([node.description.accept(core.String, this), "\n"]))
          t59[$add](t60);
        t59[$add]("input");
        t59[$add](" ");
        t59[$add](node.name.accept(core.String, this));
        t59[$add](" ");
        t59[$add](this.visitDirectiveSetNode(node.directives));
        t59[$add](this.visitInputValueDefinitionSetNode(node.fields));
        return t59;
      })()[$join]();
    }
    visitInputValueDefinitionSetNode(fields) {
      return dart.test(fields[$isEmpty]) ? "" : (() => {
        let t61 = JSArrayOfString().of([]);
        t61[$add]("{");
        t61[$add]("\n");
        t61[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) + 1));
        for (let t62 of fields[$expand](core.String, dart.fn(field => JSArrayOfString().of(["\n", this[_indent](this[_tabs]), field.accept(core.String, this)]), InputValueDefinitionNodeToListOfString()))[$skip](2))
          t61[$add](t62);
        t61[$add]("\n");
        t61[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) - 1));
        t61[$add]("}");
        return t61;
      })()[$join]();
    }
    visitDirectiveDefinitionNode(node) {
      return (() => {
        let t63 = JSArrayOfString().of([]);
        if (node.description != null) for (let t64 of JSArrayOfString().of([node.description.accept(core.String, this), "\n"]))
          t63[$add](t64);
        t63[$add]("directive");
        t63[$add](" ");
        t63[$add]("@");
        t63[$add](node.name.accept(core.String, this));
        t63[$add](this.visitArgumentDefinitionSetNode(node.args));
        if (dart.test(node.repeatable)) for (let t65 of JSArrayOfString().of([" ", "repeatable"]))
          t63[$add](t65);
        t63[$add](this.visitDirectiveLocationSetNode(node.locations));
        return t63;
      })()[$join]();
    }
    visitDirectiveLocationSetNode(locations) {
      return dart.test(locations[$isEmpty]) ? "" : (() => {
        let t66 = JSArrayOfString().of([]);
        t66[$add](" ");
        t66[$add]("on");
        t66[$add]("\n");
        t66[$add](this[_indent](this[_tabs] = dart.notNull(this[_tabs]) + 1));
        for (let t67 of locations[$expand](core.String, dart.fn(location => JSArrayOfString().of(["\n", this[_indent](this[_tabs]), "|", " ", printer._directiveLocation(location)]), DirectiveLocationToListOfString()))[$skip](2))
          t66[$add](t67);
        for (let t68 of JSArrayOfString().of([this[_indent](this[_tabs] = dart.notNull(this[_tabs]) - 1)])[$skip](1))
          t66[$add](t68);
        return t66;
      })()[$join]();
    }
    visitSchemaExtensionNode(node) {
      return (() => {
        let t69 = JSArrayOfString().of([]);
        t69[$add]("extend");
        t69[$add](" ");
        t69[$add]("schema");
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) for (let t70 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(node.directives)]))
          t69[$add](t70);
        if (node.operationTypes != null && dart.test(node.operationTypes[$isNotEmpty])) for (let t71 of JSArrayOfString().of([" ", this.visitOperationTypeDefinitionSetNode(node.operationTypes)]))
          t69[$add](t71);
        return t69;
      })()[$join]();
    }
    visitScalarTypeExtensionNode(node) {
      return (() => {
        let t72 = JSArrayOfString().of([]);
        t72[$add]("extend");
        t72[$add](" ");
        t72[$add]("scalar");
        t72[$add](" ");
        t72[$add](node.name.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) for (let t73 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(node.directives)]))
          t72[$add](t73);
        return t72;
      })()[$join]();
    }
    visitObjectTypeExtensionNode(node) {
      return JSArrayOfString().of(["extend", " ", "type", " ", node.name.accept(core.String, this), " ", this.visitImplementsSetNode(node.interfaces), this.visitDirectiveSetNode(node.directives), this.visitFieldSetNode(node.fields)])[$join]();
    }
    visitInterfaceTypeExtensionNode(node) {
      return JSArrayOfString().of(["extend", " ", "interface", " ", node.name.accept(core.String, this), " ", this.visitDirectiveSetNode(node.directives), this.visitFieldSetNode(node.fields)])[$join]();
    }
    visitUnionTypeExtensionNode(node) {
      return (() => {
        let t74 = JSArrayOfString().of([]);
        t74[$add]("extend");
        t74[$add](" ");
        t74[$add]("union");
        t74[$add](" ");
        t74[$add](node.name.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) for (let t75 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(node.directives)]))
          t74[$add](t75);
        t74[$add](this.visitUnionTypeSetNode(node.types));
        return t74;
      })()[$join]();
    }
    visitEnumTypeExtensionNode(node) {
      return (() => {
        let t76 = JSArrayOfString().of([]);
        t76[$add]("extend");
        t76[$add](" ");
        t76[$add]("enum");
        t76[$add](" ");
        t76[$add](node.name.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) for (let t77 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(node.directives)]))
          t76[$add](t77);
        t76[$add](this.visitEnumValueDefinitionSetNode(node.values));
        return t76;
      })()[$join]();
    }
    visitInputObjectTypeExtensionNode(node) {
      return (() => {
        let t78 = JSArrayOfString().of([]);
        t78[$add]("extend");
        t78[$add](" ");
        t78[$add]("input");
        t78[$add](" ");
        t78[$add](node.name.accept(core.String, this));
        if (node.directives != null && dart.test(node.directives[$isNotEmpty])) for (let t79 of JSArrayOfString().of([" ", this.visitDirectiveSetNode(node.directives)]))
          t78[$add](t79);
        t78[$add](this.visitInputValueDefinitionSetNode(node.fields));
        return t78;
      })()[$join]();
    }
  };
  (printer._PrintVisitor.new = function() {
    this[_tabs] = 0;
    ;
  }).prototype = printer._PrintVisitor.prototype;
  dart.addTypeTests(printer._PrintVisitor);
  dart.setMethodSignature(printer._PrintVisitor, () => ({
    __proto__: dart.getMethods(printer._PrintVisitor.__proto__),
    [_indent]: dart.fnType(core.String, [core.int]),
    visitDocumentNode: dart.fnType(core.String, [ast.DocumentNode]),
    visitDirectiveSetNode: dart.fnType(core.String, [core.Iterable$(ast.DirectiveNode)]),
    visitOperationTypeDefinitionSetNode: dart.fnType(core.String, [core.Iterable$(ast.OperationTypeDefinitionNode)]),
    visitVariableDefinitionSetNode: dart.fnType(core.String, [core.Iterable$(ast.VariableDefinitionNode)]),
    visitArgumentSetNode: dart.fnType(core.String, [core.Iterable$(ast.ArgumentNode)]),
    visitVariableDefinitionNode: dart.fnType(core.String, [ast.VariableDefinitionNode]),
    visitDefaultValueNode: dart.fnType(core.String, [ast.DefaultValueNode]),
    visitVariableNode: dart.fnType(core.String, [ast.VariableNode]),
    visitOperationDefinitionNode: dart.fnType(core.String, [ast.OperationDefinitionNode]),
    visitNameNode: dart.fnType(core.String, [ast.NameNode]),
    visitDirectiveNode: dart.fnType(core.String, [ast.DirectiveNode]),
    visitListTypeNode: dart.fnType(core.String, [ast.ListTypeNode]),
    visitNamedTypeNode: dart.fnType(core.String, [ast.NamedTypeNode]),
    visitObjectFieldNode: dart.fnType(core.String, [ast.ObjectFieldNode]),
    visitObjectValueNode: dart.fnType(core.String, [ast.ObjectValueNode]),
    visitListValueNode: dart.fnType(core.String, [ast.ListValueNode]),
    visitEnumValueNode: dart.fnType(core.String, [ast.EnumValueNode]),
    visitNullValueNode: dart.fnType(core.String, [ast.NullValueNode]),
    visitBooleanValueNode: dart.fnType(core.String, [ast.BooleanValueNode]),
    visitStringValueNode: dart.fnType(core.String, [ast.StringValueNode]),
    visitFloatValueNode: dart.fnType(core.String, [ast.FloatValueNode]),
    visitIntValueNode: dart.fnType(core.String, [ast.IntValueNode]),
    visitTypeConditionNode: dart.fnType(core.String, [ast.TypeConditionNode]),
    visitFragmentDefinitionNode: dart.fnType(core.String, [ast.FragmentDefinitionNode]),
    visitInlineFragmentNode: dart.fnType(core.String, [ast.InlineFragmentNode]),
    visitFragmentSpreadNode: dart.fnType(core.String, [ast.FragmentSpreadNode]),
    visitArgumentNode: dart.fnType(core.String, [ast.ArgumentNode]),
    visitFieldNode: dart.fnType(core.String, [ast.FieldNode]),
    visitSelectionSetNode: dart.fnType(core.String, [ast.SelectionSetNode]),
    visitSchemaDefinitionNode: dart.fnType(core.String, [ast.SchemaDefinitionNode]),
    visitOperationTypeDefinitionNode: dart.fnType(core.String, [ast.OperationTypeDefinitionNode]),
    visitScalarTypeDefinitionNode: dart.fnType(core.String, [ast.ScalarTypeDefinitionNode]),
    visitObjectTypeDefinitionNode: dart.fnType(core.String, [ast.ObjectTypeDefinitionNode]),
    visitImplementsSetNode: dart.fnType(core.String, [core.Iterable$(ast.NamedTypeNode)]),
    visitFieldSetNode: dart.fnType(core.String, [core.Iterable$(ast.FieldDefinitionNode)]),
    visitFieldDefinitionNode: dart.fnType(core.String, [ast.FieldDefinitionNode]),
    visitArgumentDefinitionSetNode: dart.fnType(core.String, [core.Iterable$(ast.InputValueDefinitionNode)]),
    visitInputValueDefinitionNode: dart.fnType(core.String, [ast.InputValueDefinitionNode]),
    visitInterfaceTypeDefinitionNode: dart.fnType(core.String, [ast.InterfaceTypeDefinitionNode]),
    visitUnionTypeDefinitionNode: dart.fnType(core.String, [ast.UnionTypeDefinitionNode]),
    visitUnionTypeSetNode: dart.fnType(core.String, [core.Iterable$(ast.NamedTypeNode)]),
    visitEnumTypeDefinitionNode: dart.fnType(core.String, [ast.EnumTypeDefinitionNode]),
    visitEnumValueDefinitionSetNode: dart.fnType(core.String, [core.Iterable$(ast.EnumValueDefinitionNode)]),
    visitEnumValueDefinitionNode: dart.fnType(core.String, [ast.EnumValueDefinitionNode]),
    visitInputObjectTypeDefinitionNode: dart.fnType(core.String, [ast.InputObjectTypeDefinitionNode]),
    visitInputValueDefinitionSetNode: dart.fnType(core.String, [core.Iterable$(ast.InputValueDefinitionNode)]),
    visitDirectiveDefinitionNode: dart.fnType(core.String, [ast.DirectiveDefinitionNode]),
    visitDirectiveLocationSetNode: dart.fnType(core.String, [core.Iterable$(ast.DirectiveLocation)]),
    visitSchemaExtensionNode: dart.fnType(core.String, [ast.SchemaExtensionNode]),
    visitScalarTypeExtensionNode: dart.fnType(core.String, [ast.ScalarTypeExtensionNode]),
    visitObjectTypeExtensionNode: dart.fnType(core.String, [ast.ObjectTypeExtensionNode]),
    visitInterfaceTypeExtensionNode: dart.fnType(core.String, [ast.InterfaceTypeExtensionNode]),
    visitUnionTypeExtensionNode: dart.fnType(core.String, [ast.UnionTypeExtensionNode]),
    visitEnumTypeExtensionNode: dart.fnType(core.String, [ast.EnumTypeExtensionNode]),
    visitInputObjectTypeExtensionNode: dart.fnType(core.String, [ast.InputObjectTypeExtensionNode])
  }));
  dart.setLibraryUri(printer._PrintVisitor, "package:gql/src/language/printer.dart");
  dart.setFieldSignature(printer._PrintVisitor, () => ({
    __proto__: dart.getFields(printer._PrintVisitor.__proto__),
    [_tabs]: dart.fieldType(core.int)
  }));
  const _name = dart.privateName(ast, "_name");
  let C0;
  let C1;
  let C2;
  let C3;
  let C4;
  let C5;
  let C6;
  let C7;
  let C8;
  let C9;
  let C10;
  let C11;
  let C12;
  let C13;
  let C14;
  let C15;
  let C16;
  let C17;
  let C18;
  let C19;
  let C20;
  printer.printNode = function printNode(node) {
    return node.accept(core.String, new printer._PrintVisitor.new());
  };
  printer._opType = function _opType(t) {
    switch (t) {
      case C0 || CT.C0:
      {
        return "query";
      }
      case C1 || CT.C1:
      {
        return "mutation";
      }
      case C2 || CT.C2:
      {
        return "subscription";
      }
    }
    return null;
  };
  printer._directiveLocation = function _directiveLocation(location) {
    switch (location) {
      case C3 || CT.C3:
      {
        return "QUERY";
      }
      case C4 || CT.C4:
      {
        return "MUTATION";
      }
      case C5 || CT.C5:
      {
        return "SUBSCRIPTION";
      }
      case C6 || CT.C6:
      {
        return "FIELD";
      }
      case C7 || CT.C7:
      {
        return "FRAGMENT_DEFINITION";
      }
      case C8 || CT.C8:
      {
        return "FRAGMENT_SPREAD";
      }
      case C9 || CT.C9:
      {
        return "INLINE_FRAGMENT";
      }
      case C10 || CT.C10:
      {
        return "SCHEMA";
      }
      case C11 || CT.C11:
      {
        return "SCALAR";
      }
      case C12 || CT.C12:
      {
        return "OBJECT";
      }
      case C13 || CT.C13:
      {
        return "FIELD_DEFINITION";
      }
      case C14 || CT.C14:
      {
        return "ARGUMENT_DEFINITION";
      }
      case C15 || CT.C15:
      {
        return "INTERFACE";
      }
      case C16 || CT.C16:
      {
        return "UNION";
      }
      case C17 || CT.C17:
      {
        return "ENUM";
      }
      case C18 || CT.C18:
      {
        return "ENUM_VALUE";
      }
      case C19 || CT.C19:
      {
        return "INPUT_OBJECT";
      }
      case C20 || CT.C20:
      {
        return "INPUT_FIELD_DEFINITION";
      }
    }
    return null;
  };
  const _position = dart.privateName(parser$, "_position");
  const _tokens$ = dart.privateName(parser$, "_tokens");
  const _length = dart.privateName(parser$, "_length");
  const _parseDocument = dart.privateName(parser$, "_parseDocument");
  const _next = dart.privateName(parser$, "_next");
  const _peek = dart.privateName(parser$, "_peek");
  const _advance = dart.privateName(parser$, "_advance");
  const _expectToken = dart.privateName(parser$, "_expectToken");
  const _expectOptionalToken = dart.privateName(parser$, "_expectOptionalToken");
  const _expectKeyword = dart.privateName(parser$, "_expectKeyword");
  const _expectOptionalKeyword = dart.privateName(parser$, "_expectOptionalKeyword");
  const _parseMany = dart.privateName(parser$, "_parseMany");
  const _maybeParseMany = dart.privateName(parser$, "_maybeParseMany");
  const _parseDefinition = dart.privateName(parser$, "_parseDefinition");
  const _parseExecutableDefinition = dart.privateName(parser$, "_parseExecutableDefinition");
  const _parseTypeSystemDefinition = dart.privateName(parser$, "_parseTypeSystemDefinition");
  const _parseTypeSystemExtension = dart.privateName(parser$, "_parseTypeSystemExtension");
  const _parseOperationDefinition = dart.privateName(parser$, "_parseOperationDefinition");
  const _parseFragmentDefinition = dart.privateName(parser$, "_parseFragmentDefinition");
  const _parseFragmentName = dart.privateName(parser$, "_parseFragmentName");
  const _parseNamedType = dart.privateName(parser$, "_parseNamedType");
  const _parseDirectives = dart.privateName(parser$, "_parseDirectives");
  const _parseSelectionSet = dart.privateName(parser$, "_parseSelectionSet");
  const _parseOperationType = dart.privateName(parser$, "_parseOperationType");
  const _parseName = dart.privateName(parser$, "_parseName");
  const _parseVariableDefinitions = dart.privateName(parser$, "_parseVariableDefinitions");
  const _parseVariableDefinition = dart.privateName(parser$, "_parseVariableDefinition");
  const _parseVariable = dart.privateName(parser$, "_parseVariable");
  const _parseType = dart.privateName(parser$, "_parseType");
  const _parseValue = dart.privateName(parser$, "_parseValue");
  const _parseObjectField = dart.privateName(parser$, "_parseObjectField");
  const _parseConstObjectField = dart.privateName(parser$, "_parseConstObjectField");
  const _parseNonConstObjectField = dart.privateName(parser$, "_parseNonConstObjectField");
  const _parseConstValue = dart.privateName(parser$, "_parseConstValue");
  const _parseNonConstValue = dart.privateName(parser$, "_parseNonConstValue");
  const _parseList = dart.privateName(parser$, "_parseList");
  const _name$ = dart.privateName(lexer$, "_name");
  let C21;
  const _parseObject = dart.privateName(parser$, "_parseObject");
  let C22;
  let C23;
  let C24;
  const _parseStringValue = dart.privateName(parser$, "_parseStringValue");
  let C25;
  let C26;
  let C27;
  let C28;
  const _parseDirective = dart.privateName(parser$, "_parseDirective");
  const _parseArguments = dart.privateName(parser$, "_parseArguments");
  const _parseConstArgument = dart.privateName(parser$, "_parseConstArgument");
  const _parseNonConstArgument = dart.privateName(parser$, "_parseNonConstArgument");
  const _parseArgument = dart.privateName(parser$, "_parseArgument");
  const _parseSelection = dart.privateName(parser$, "_parseSelection");
  const _parseFragment = dart.privateName(parser$, "_parseFragment");
  const _parseField = dart.privateName(parser$, "_parseField");
  const _parseSchemaDefinition = dart.privateName(parser$, "_parseSchemaDefinition");
  const _parseScalarTypeDefinition = dart.privateName(parser$, "_parseScalarTypeDefinition");
  const _parseObjectTypeDefinition = dart.privateName(parser$, "_parseObjectTypeDefinition");
  const _parseInterfaceTypeDefinition = dart.privateName(parser$, "_parseInterfaceTypeDefinition");
  const _parseUnionTypeDefinition = dart.privateName(parser$, "_parseUnionTypeDefinition");
  const _parseEnumTypeDefinition = dart.privateName(parser$, "_parseEnumTypeDefinition");
  const _parseInputObjectTypeDefinition = dart.privateName(parser$, "_parseInputObjectTypeDefinition");
  const _parseDirectiveDefinition = dart.privateName(parser$, "_parseDirectiveDefinition");
  const _parseSchemaExtension = dart.privateName(parser$, "_parseSchemaExtension");
  const _parseScalarTypeExtension = dart.privateName(parser$, "_parseScalarTypeExtension");
  const _parseObjectTypeExtension = dart.privateName(parser$, "_parseObjectTypeExtension");
  const _parseInterfaceTypeExtension = dart.privateName(parser$, "_parseInterfaceTypeExtension");
  const _parseUnionTypeExtension = dart.privateName(parser$, "_parseUnionTypeExtension");
  const _parseEnumTypeExtension = dart.privateName(parser$, "_parseEnumTypeExtension");
  const _parseInputObjectTypeExtension = dart.privateName(parser$, "_parseInputObjectTypeExtension");
  const _parseOperationTypeDefinition = dart.privateName(parser$, "_parseOperationTypeDefinition");
  const _parseDescription = dart.privateName(parser$, "_parseDescription");
  const _parseImplementsInterfaces = dart.privateName(parser$, "_parseImplementsInterfaces");
  const _parseFieldsDefinition = dart.privateName(parser$, "_parseFieldsDefinition");
  const _parseFieldDefinition = dart.privateName(parser$, "_parseFieldDefinition");
  const _parseArgumentDefinitions = dart.privateName(parser$, "_parseArgumentDefinitions");
  const _parseInputValueDefinition = dart.privateName(parser$, "_parseInputValueDefinition");
  const _parseUnionMemberTypes = dart.privateName(parser$, "_parseUnionMemberTypes");
  const _parseEnumValuesDefinition = dart.privateName(parser$, "_parseEnumValuesDefinition");
  const _parseEnumValueDefinition = dart.privateName(parser$, "_parseEnumValueDefinition");
  const _parseInputFieldsDefinition = dart.privateName(parser$, "_parseInputFieldsDefinition");
  const _parseDirectiveLocations = dart.privateName(parser$, "_parseDirectiveLocations");
  const _parseDirectiveLocation = dart.privateName(parser$, "_parseDirectiveLocation");
  parser$._Parser = class _Parser extends core.Object {
    parse() {
      return this[_parseDocument]();
    }
    [_peek](kind, opts) {
      let offset = opts && 'offset' in opts ? opts.offset : 0;
      let next = this[_next]({offset: offset});
      if (next == null) return false;
      return dart.equals(next.kind, kind);
    }
    [_advance]() {
      this[_position] = dart.notNull(this[_position]) + 1;
    }
    [_expectToken](kind, errorMessage = null) {
      let t80;
      let next = this[_next]();
      if (dart.equals(next.kind, kind)) {
        this[_advance]();
        return next;
      }
      dart.throw(new span_exception.SourceSpanException.new((t80 = errorMessage, t80 == null ? "Expected " + dart.str(kind) : t80), this[_next]().span));
    }
    [_expectOptionalToken](kind) {
      let next = this[_next]();
      if (dart.equals(next.kind, kind)) {
        this[_advance]();
        return next;
      }
      return null;
    }
    [_expectKeyword](value, errorMessage = null) {
      let t80;
      let next = this[_next]();
      if (dart.equals(next.kind, lexer$.TokenKind.name_) && next.value == value) {
        this[_advance]();
        return next;
      }
      dart.throw(new span_exception.SourceSpanException.new((t80 = errorMessage, t80 == null ? "Expected keyword '" + dart.str(value) + "'" : t80), this[_next]().span));
    }
    [_expectOptionalKeyword](value) {
      let next = this[_next]();
      if (dart.equals(next.kind, lexer$.TokenKind.name_) && next.value == value) {
        this[_advance]();
        return next;
      }
      return null;
    }
    [_next](opts) {
      let offset = opts && 'offset' in opts ? opts.offset : 0;
      if (dart.notNull(this[_position]) + dart.notNull(offset) >= dart.notNull(this[_length])) return null;
      return this[_tokens$][$_get](dart.notNull(this[_position]) + dart.notNull(offset));
    }
    [_parseMany](N, open, parse, close, errorMessage = null) {
      this[_expectToken](open, errorMessage);
      let nodes = _interceptors.JSArray$(N).of([]);
      while (this[_expectOptionalToken](close) == null) {
        nodes[$add](parse());
      }
      return nodes;
    }
    [_maybeParseMany](N, open, parse, close, errorMessage = null) {
      if (dart.test(this[_peek](open))) {
        return this[_parseMany](N, open, parse, close, errorMessage);
      }
      return _interceptors.JSArray$(N).of([]);
    }
    [_parseDocument]() {
      return new ast.DocumentNode.new({definitions: this[_parseMany](ast.DefinitionNode, lexer$.TokenKind.sof, dart.bind(this, _parseDefinition), lexer$.TokenKind.eof)});
    }
    [_parseDefinition]() {
      if (dart.test(this[_peek](lexer$.TokenKind.name_))) {
        switch (this[_next]().value) {
          case "query":
          case "mutation":
          case "subscription":
          case "fragment":
          {
            return this[_parseExecutableDefinition]();
          }
          case "schema":
          case "scalar":
          case "type":
          case "interface":
          case "union":
          case "enum":
          case "input":
          case "directive":
          {
            return this[_parseTypeSystemDefinition]();
          }
          case "extend":
          {
            return this[_parseTypeSystemExtension]();
          }
        }
      } else if (dart.test(this[_peek](lexer$.TokenKind.braceL))) {
        return this[_parseExecutableDefinition]();
      } else if (dart.test(this[_peek](lexer$.TokenKind.string)) || dart.test(this[_peek](lexer$.TokenKind.blockString))) {
        return this[_parseTypeSystemDefinition]();
      }
      dart.throw(new span_exception.SourceSpanException.new("Unknown definition type '" + dart.str(this[_next]().value) + "'", this[_next]().span));
    }
    [_parseExecutableDefinition]() {
      if (dart.test(this[_peek](lexer$.TokenKind.name_))) {
        switch (this[_next]().value) {
          case "query":
          case "mutation":
          case "subscription":
          {
            return this[_parseOperationDefinition]();
          }
          case "fragment":
          {
            return this[_parseFragmentDefinition]();
          }
        }
      } else if (dart.test(this[_peek](lexer$.TokenKind.braceL))) {
        return this[_parseOperationDefinition]();
      }
      dart.throw(new span_exception.SourceSpanException.new("Unknown executable definition '" + dart.str(this[_next]().value) + "'", this[_next]().span));
    }
    [_parseFragmentDefinition]() {
      this[_expectKeyword]("fragment");
      let name = this[_parseFragmentName]();
      this[_expectKeyword]("on");
      return new ast.FragmentDefinitionNode.new({name: name, typeCondition: new ast.TypeConditionNode.new({on: this[_parseNamedType]()}), directives: this[_parseDirectives](), selectionSet: this[_parseSelectionSet]()});
    }
    [_parseOperationDefinition]() {
      if (dart.test(this[_peek](lexer$.TokenKind.braceL))) {
        return new ast.OperationDefinitionNode.new({type: ast.OperationType.query, selectionSet: this[_parseSelectionSet]()});
      }
      let operationType = this[_parseOperationType]();
      let name = null;
      if (dart.test(this[_peek](lexer$.TokenKind.name_))) name = this[_parseName]();
      return new ast.OperationDefinitionNode.new({type: operationType, name: name, variableDefinitions: this[_parseVariableDefinitions](), directives: this[_parseDirectives](), selectionSet: this[_parseSelectionSet]()});
    }
    [_parseVariableDefinitions]() {
      return this[_maybeParseMany](ast.VariableDefinitionNode, lexer$.TokenKind.parenL, dart.bind(this, _parseVariableDefinition), lexer$.TokenKind.parenR);
    }
    [_parseVariableDefinition]() {
      let variable = this[_parseVariable]();
      this[_expectToken](lexer$.TokenKind.colon, "Expected ':' followed by variable type");
      let type = this[_parseType]();
      let defaultValue = null;
      if (this[_expectOptionalToken](lexer$.TokenKind.equals) != null) {
        defaultValue = this[_parseValue]({isConst: true});
      }
      return new ast.VariableDefinitionNode.new({variable: variable, type: type, defaultValue: new ast.DefaultValueNode.new({value: defaultValue}), directives: this[_parseDirectives]({isConst: true})});
    }
    [_parseVariable]() {
      this[_expectToken](lexer$.TokenKind.dollar, "Variable name must be start with '$'");
      return new ast.VariableNode.new({name: this[_parseName]("Expected a variable name")});
    }
    [_parseType]() {
      if (this[_expectOptionalToken](lexer$.TokenKind.bracketL) != null) {
        let type = this[_parseType]();
        this[_expectToken](lexer$.TokenKind.bracketR, "Expected ']'");
        let isNonNull = this[_expectOptionalToken](lexer$.TokenKind.bang) != null;
        return new ast.ListTypeNode.new({isNonNull: isNonNull, type: type});
      } else {
        return this[_parseNamedType]();
      }
    }
    [_parseNamedType]() {
      let name = this[_parseName]("Expected a named type");
      let isNonNull = this[_expectOptionalToken](lexer$.TokenKind.bang) != null;
      return new ast.NamedTypeNode.new({isNonNull: isNonNull, name: name});
    }
    [_parseConstObjectField]() {
      return this[_parseObjectField]({isConst: true});
    }
    [_parseNonConstObjectField]() {
      return this[_parseObjectField]({isConst: false});
    }
    [_parseObjectField](opts) {
      let isConst = opts && 'isConst' in opts ? opts.isConst : null;
      let name = this[_parseName]("Expected an object field name");
      this[_expectToken](lexer$.TokenKind.colon, "Missing ':' before object field value");
      return new ast.ObjectFieldNode.new({name: name, value: this[_parseValue]({isConst: isConst})});
    }
    [_parseConstValue]() {
      return this[_parseValue]({isConst: true});
    }
    [_parseNonConstValue]() {
      return this[_parseValue]({isConst: false});
    }
    [_parseValue](opts) {
      let isConst = opts && 'isConst' in opts ? opts.isConst : null;
      let token = this[_next]();
      switch (token.kind) {
        case C21 || CT.C21:
        {
          return this[_parseList]({isConst: isConst});
        }
        case C22 || CT.C22:
        {
          return this[_parseObject]({isConst: isConst});
        }
        case C23 || CT.C23:
        {
          this[_advance]();
          return new ast.IntValueNode.new({value: token.value});
        }
        case C24 || CT.C24:
        {
          this[_advance]();
          return new ast.FloatValueNode.new({value: token.value});
        }
        case C25 || CT.C25:
        case C26 || CT.C26:
        {
          return this[_parseStringValue]();
        }
        case C27 || CT.C27:
        {
          if (token.value === "true" || token.value === "false") {
            this[_advance]();
            return new ast.BooleanValueNode.new({value: token.value === "true"});
          } else if (token.value === "null") {
            this[_advance]();
            return new ast.NullValueNode.new();
          }
          return new ast.EnumValueNode.new({name: this[_parseName]()});
        }
        case C28 || CT.C28:
        {
          if (!dart.test(isConst)) {
            return this[_parseVariable]();
          }
          dart.throw(new span_exception.SourceSpanException.new("Cannot use variable in a constant context", token.span));
        }
        default:
        {
          dart.throw(new span_exception.SourceSpanException.new("Unexpected token found when parsing a value", token.span));
        }
      }
    }
    [_parseStringValue]() {
      let valueToken = this[_next]();
      this[_advance]();
      return new ast.StringValueNode.new({value: valueToken.value, isBlock: dart.equals(valueToken.kind, lexer$.TokenKind.blockString)});
    }
    [_parseList](opts) {
      let isConst = opts && 'isConst' in opts ? opts.isConst : null;
      return new ast.ListValueNode.new({values: this[_parseMany](ast.ValueNode, lexer$.TokenKind.bracketL, dart.test(isConst) ? dart.bind(this, _parseConstValue) : dart.bind(this, _parseNonConstValue), lexer$.TokenKind.bracketR)});
    }
    [_parseObject](opts) {
      let isConst = opts && 'isConst' in opts ? opts.isConst : null;
      return new ast.ObjectValueNode.new({fields: this[_parseMany](ast.ObjectFieldNode, lexer$.TokenKind.braceL, dart.test(isConst) ? dart.bind(this, _parseConstObjectField) : dart.bind(this, _parseNonConstObjectField), lexer$.TokenKind.braceR)});
    }
    [_parseDirectives](opts) {
      let isConst = opts && 'isConst' in opts ? opts.isConst : false;
      let directives = JSArrayOfDirectiveNode().of([]);
      while (dart.test(this[_peek](lexer$.TokenKind.at))) {
        directives[$add](this[_parseDirective]({isConst: isConst}));
      }
      return directives;
    }
    [_parseDirective](opts) {
      let isConst = opts && 'isConst' in opts ? opts.isConst : null;
      this[_expectToken](lexer$.TokenKind.at, "Expected directive name starting with '@'");
      return new ast.DirectiveNode.new({name: this[_parseName]("Expected a directive name"), arguments: this[_parseArguments]({isConst: isConst})});
    }
    [_parseArguments](opts) {
      let isConst = opts && 'isConst' in opts ? opts.isConst : null;
      return this[_maybeParseMany](ast.ArgumentNode, lexer$.TokenKind.parenL, dart.test(isConst) ? dart.bind(this, _parseConstArgument) : dart.bind(this, _parseNonConstArgument), lexer$.TokenKind.parenR);
    }
    [_parseConstArgument]() {
      return this[_parseArgument]({isConst: true});
    }
    [_parseNonConstArgument]() {
      return this[_parseArgument]({isConst: false});
    }
    [_parseArgument](opts) {
      let isConst = opts && 'isConst' in opts ? opts.isConst : null;
      let name = this[_parseName]("Expected an argument name");
      this[_expectToken](lexer$.TokenKind.colon, "Expected ':' followed by argument value");
      return new ast.ArgumentNode.new({name: name, value: this[_parseValue]({isConst: isConst})});
    }
    [_parseOperationType]() {
      let token = this[_expectToken](lexer$.TokenKind.name_, "Expected operation name");
      switch (token.value) {
        case "query":
        {
          return ast.OperationType.query;
        }
        case "mutation":
        {
          return ast.OperationType.mutation;
        }
        case "subscription":
        {
          return ast.OperationType.subscription;
        }
      }
      dart.throw(new span_exception.SourceSpanException.new("Unknown operation '" + dart.str(token.value) + "'", token.span));
    }
    [_parseSelectionSet]() {
      return new ast.SelectionSetNode.new({selections: this[_parseMany](ast.SelectionNode, lexer$.TokenKind.braceL, dart.bind(this, _parseSelection), lexer$.TokenKind.braceR, "Expected a selection set starting with '{'")});
    }
    [_parseSelection]() {
      if (dart.test(this[_peek](lexer$.TokenKind.spread))) {
        return this[_parseFragment]();
      }
      return this[_parseField]();
    }
    [_parseFragment]() {
      this[_expectToken](lexer$.TokenKind.spread, "Expected '...' spread before fragment");
      let hasTypeCondition = this[_expectOptionalKeyword]("on") != null;
      if (!hasTypeCondition && dart.test(this[_peek](lexer$.TokenKind.name_))) {
        return new ast.FragmentSpreadNode.new({name: this[_parseFragmentName](), directives: this[_parseDirectives]({isConst: false})});
      }
      return new ast.InlineFragmentNode.new({typeCondition: hasTypeCondition ? new ast.TypeConditionNode.new({on: this[_parseNamedType]()}) : null, directives: this[_parseDirectives]({isConst: false}), selectionSet: this[_parseSelectionSet]()});
    }
    [_parseFragmentName]() {
      let token = this[_next]();
      if (token.value === "on") {
        dart.throw(new span_exception.SourceSpanException.new("Invalid fragment name 'on'", token.span));
      }
      return this[_parseName]("Expected a fragment name");
    }
    [_parseField]() {
      let nameOrAlias = this[_parseName]("Expected a field or field alias name");
      let name = null;
      let alias = null;
      if (this[_expectOptionalToken](lexer$.TokenKind.colon) != null) {
        alias = nameOrAlias;
        name = this[_parseName]("Expected a field name");
      } else {
        name = nameOrAlias;
      }
      let $arguments = this[_parseArguments]({isConst: false});
      let directives = this[_parseDirectives]({isConst: false});
      let selectionSet = null;
      if (dart.test(this[_peek](lexer$.TokenKind.braceL))) {
        selectionSet = this[_parseSelectionSet]();
      }
      return new ast.FieldNode.new({alias: alias, name: name, arguments: $arguments, directives: directives, selectionSet: selectionSet});
    }
    [_parseName](errorMessage = null) {
      let t80;
      let token = this[_expectToken](lexer$.TokenKind.name_, (t80 = errorMessage, t80 == null ? "Expected a name" : t80));
      return new ast.NameNode.new({value: token.value, span: token.span});
    }
    [_parseTypeSystemDefinition]() {
      let keywordOffset = dart.test(this[_peek](lexer$.TokenKind.string)) || dart.test(this[_peek](lexer$.TokenKind.blockString)) ? 1 : 0;
      let token = this[_next]({offset: keywordOffset});
      if (dart.test(this[_peek](lexer$.TokenKind.name_, {offset: keywordOffset}))) {
        switch (token.value) {
          case "schema":
          {
            return this[_parseSchemaDefinition]();
          }
          case "scalar":
          {
            return this[_parseScalarTypeDefinition]();
          }
          case "type":
          {
            return this[_parseObjectTypeDefinition]();
          }
          case "interface":
          {
            return this[_parseInterfaceTypeDefinition]();
          }
          case "union":
          {
            return this[_parseUnionTypeDefinition]();
          }
          case "enum":
          {
            return this[_parseEnumTypeDefinition]();
          }
          case "input":
          {
            return this[_parseInputObjectTypeDefinition]();
          }
          case "directive":
          {
            return this[_parseDirectiveDefinition]();
          }
        }
      }
      dart.throw(new span_exception.SourceSpanException.new("Unknown type system definition type '" + dart.str(token.value) + "'", token.span));
    }
    [_parseTypeSystemExtension]() {
      this[_expectKeyword]("extend");
      let token = this[_next]();
      if (this[_peek](lexer$.TokenKind.name_) != null) {
        switch (token.value) {
          case "schema":
          {
            return this[_parseSchemaExtension]();
          }
          case "scalar":
          {
            return this[_parseScalarTypeExtension]();
          }
          case "type":
          {
            return this[_parseObjectTypeExtension]();
          }
          case "interface":
          {
            return this[_parseInterfaceTypeExtension]();
          }
          case "union":
          {
            return this[_parseUnionTypeExtension]();
          }
          case "enum":
          {
            return this[_parseEnumTypeExtension]();
          }
          case "input":
          {
            return this[_parseInputObjectTypeExtension]();
          }
        }
      }
      dart.throw(new span_exception.SourceSpanException.new("Unknown type system extension type '" + dart.str(token.value) + "'", token.span));
    }
    [_parseSchemaDefinition]() {
      this[_expectKeyword]("schema");
      return new ast.SchemaDefinitionNode.new({directives: this[_parseDirectives]({isConst: true}), operationTypes: this[_parseMany](ast.OperationTypeDefinitionNode, lexer$.TokenKind.braceL, dart.bind(this, _parseOperationTypeDefinition), lexer$.TokenKind.braceR, "Expected a operation type definitions starting with '{'")});
    }
    [_parseOperationTypeDefinition]() {
      let operation = this[_parseOperationType]();
      this[_expectToken](lexer$.TokenKind.colon, "Expected ':' before operation type");
      let type = this[_parseNamedType]();
      return new ast.OperationTypeDefinitionNode.new({operation: operation, type: type});
    }
    [_parseScalarTypeDefinition]() {
      let description = this[_parseDescription]();
      this[_expectKeyword]("scalar");
      let name = this[_parseName]("Expected a scalar name");
      let directives = this[_parseDirectives]({isConst: true});
      return new ast.ScalarTypeDefinitionNode.new({description: description, name: name, directives: directives});
    }
    [_parseDescription]() {
      if (dart.test(this[_peek](lexer$.TokenKind.string)) || dart.test(this[_peek](lexer$.TokenKind.blockString))) {
        return this[_parseStringValue]();
      }
      return null;
    }
    [_parseImplementsInterfaces]() {
      let types = JSArrayOfNamedTypeNode().of([]);
      if (this[_expectOptionalKeyword]("implements") != null) {
        this[_expectOptionalToken](lexer$.TokenKind.amp);
        do {
          types[$add](this[_parseNamedType]());
        } while (this[_expectOptionalToken](lexer$.TokenKind.amp) != null);
      }
      return types;
    }
    [_parseObjectTypeDefinition]() {
      let description = this[_parseDescription]();
      this[_expectKeyword]("type");
      let name = this[_parseName]("Expected an object type name");
      let interfaces = this[_parseImplementsInterfaces]();
      let directives = this[_parseDirectives]({isConst: true});
      let fields = this[_parseFieldsDefinition]();
      return new ast.ObjectTypeDefinitionNode.new({description: description, name: name, interfaces: interfaces, directives: directives, fields: fields});
    }
    [_parseFieldsDefinition]() {
      return this[_maybeParseMany](ast.FieldDefinitionNode, lexer$.TokenKind.braceL, dart.bind(this, _parseFieldDefinition), lexer$.TokenKind.braceR);
    }
    [_parseFieldDefinition]() {
      let description = this[_parseDescription]();
      let name = this[_parseName]("Expected a field name");
      let args = this[_parseArgumentDefinitions]();
      this[_expectToken](lexer$.TokenKind.colon, "Expected ':' followed by field type");
      let type = this[_parseType]();
      let directives = this[_parseDirectives]({isConst: true});
      return new ast.FieldDefinitionNode.new({description: description, name: name, args: args, type: type, directives: directives});
    }
    [_parseArgumentDefinitions]() {
      return this[_maybeParseMany](ast.InputValueDefinitionNode, lexer$.TokenKind.parenL, dart.bind(this, _parseInputValueDefinition), lexer$.TokenKind.parenR);
    }
    [_parseInputValueDefinition]() {
      let description = this[_parseDescription]();
      let name = this[_parseName]("Expected an input value name");
      this[_expectToken](lexer$.TokenKind.colon, "Expected ':' followed by input value type");
      let type = this[_parseType]();
      let defaultValue = null;
      if (this[_expectOptionalToken](lexer$.TokenKind.equals) != null) {
        defaultValue = this[_parseConstValue]();
      }
      let directives = this[_parseDirectives]({isConst: true});
      return new ast.InputValueDefinitionNode.new({description: description, name: name, type: type, defaultValue: defaultValue, directives: directives});
    }
    [_parseInterfaceTypeDefinition]() {
      let description = this[_parseDescription]();
      this[_expectKeyword]("interface");
      let name = this[_parseName]("Expected an interface name");
      let directives = this[_parseDirectives]({isConst: true});
      let fields = this[_parseFieldsDefinition]();
      return new ast.InterfaceTypeDefinitionNode.new({description: description, name: name, directives: directives, fields: fields});
    }
    [_parseUnionTypeDefinition]() {
      let description = this[_parseDescription]();
      this[_expectKeyword]("union");
      let name = this[_parseName]("Expected a union name");
      let directives = this[_parseDirectives]({isConst: true});
      let types = this[_parseUnionMemberTypes]();
      return new ast.UnionTypeDefinitionNode.new({description: description, name: name, directives: directives, types: types});
    }
    [_parseUnionMemberTypes]() {
      let types = JSArrayOfNamedTypeNode().of([]);
      if (this[_expectOptionalToken](lexer$.TokenKind.equals) != null) {
        this[_expectOptionalToken](lexer$.TokenKind.pipe);
        do {
          types[$add](this[_parseNamedType]());
        } while (this[_expectOptionalToken](lexer$.TokenKind.pipe) != null);
      }
      return types;
    }
    [_parseEnumTypeDefinition]() {
      let description = this[_parseDescription]();
      this[_expectKeyword]("enum");
      let name = this[_parseName]("Expected an enum name");
      let directives = this[_parseDirectives]({isConst: true});
      let values = this[_parseEnumValuesDefinition]();
      return new ast.EnumTypeDefinitionNode.new({description: description, name: name, directives: directives, values: values});
    }
    [_parseEnumValuesDefinition]() {
      return this[_maybeParseMany](ast.EnumValueDefinitionNode, lexer$.TokenKind.braceL, dart.bind(this, _parseEnumValueDefinition), lexer$.TokenKind.braceR);
    }
    [_parseEnumValueDefinition]() {
      let description = this[_parseDescription]();
      let name = this[_parseName]("Expected an enum value");
      let directives = this[_parseDirectives]({isConst: true});
      return new ast.EnumValueDefinitionNode.new({description: description, name: name, directives: directives});
    }
    [_parseInputObjectTypeDefinition]() {
      let description = this[_parseDescription]();
      this[_expectKeyword]("input");
      let name = this[_parseName]("Expected an input object type name");
      let directives = this[_parseDirectives]({isConst: true});
      let fields = this[_parseInputFieldsDefinition]();
      return new ast.InputObjectTypeDefinitionNode.new({description: description, name: name, directives: directives, fields: fields});
    }
    [_parseInputFieldsDefinition]() {
      return this[_maybeParseMany](ast.InputValueDefinitionNode, lexer$.TokenKind.braceL, dart.bind(this, _parseInputValueDefinition), lexer$.TokenKind.braceR);
    }
    [_parseDirectiveDefinition]() {
      let description = this[_parseDescription]();
      this[_expectKeyword]("directive");
      this[_expectToken](lexer$.TokenKind.at, "Directive name must be start with '@'");
      let name = this[_parseName]("Expected a directive name");
      let args = this[_parseArgumentDefinitions]();
      let repeatable = this[_expectOptionalKeyword]("repeatable") != null;
      this[_expectKeyword]("on");
      let locations = this[_parseDirectiveLocations]();
      return new ast.DirectiveDefinitionNode.new({description: description, name: name, args: args, repeatable: repeatable, locations: locations});
    }
    [_parseDirectiveLocations]() {
      this[_expectOptionalToken](lexer$.TokenKind.pipe);
      let locations = JSArrayOfDirectiveLocation().of([]);
      do {
        locations[$add](this[_parseDirectiveLocation]());
      } while (this[_expectOptionalToken](lexer$.TokenKind.pipe) != null);
      return locations;
    }
    [_parseDirectiveLocation]() {
      let name = this[_parseName]("Expected a directive location");
      switch (name.value) {
        case "QUERY":
        {
          return ast.DirectiveLocation.query;
        }
        case "MUTATION":
        {
          return ast.DirectiveLocation.mutation;
        }
        case "SUBSCRIPTION":
        {
          return ast.DirectiveLocation.subscription;
        }
        case "FIELD":
        {
          return ast.DirectiveLocation.field;
        }
        case "FRAGMENT_DEFINITION":
        {
          return ast.DirectiveLocation.fragmentDefinition;
        }
        case "FRAGMENT_SPREAD":
        {
          return ast.DirectiveLocation.fragmentSpread;
        }
        case "INLINE_FRAGMENT":
        {
          return ast.DirectiveLocation.inlineFragment;
        }
        case "SCHEMA":
        {
          return ast.DirectiveLocation.schema;
        }
        case "SCALAR":
        {
          return ast.DirectiveLocation.scalar;
        }
        case "OBJECT":
        {
          return ast.DirectiveLocation.object;
        }
        case "FIELD_DEFINITION":
        {
          return ast.DirectiveLocation.fieldDefinition;
        }
        case "ARGUMENT_DEFINITION":
        {
          return ast.DirectiveLocation.argumentDefinition;
        }
        case "INTERFACE":
        {
          return ast.DirectiveLocation.interface;
        }
        case "UNION":
        {
          return ast.DirectiveLocation.union;
        }
        case "ENUM":
        {
          return ast.DirectiveLocation.enumDefinition;
        }
        case "ENUM_VALUE":
        {
          return ast.DirectiveLocation.enumValue;
        }
        case "INPUT_OBJECT":
        {
          return ast.DirectiveLocation.inputObject;
        }
        case "INPUT_FIELD_DEFINITION":
        {
          return ast.DirectiveLocation.inputFieldDefinition;
        }
      }
      dart.throw(new span_exception.SourceSpanException.new("Unknown directive location '" + dart.str(name.value) + "'", name.span));
    }
    [_parseSchemaExtension]() {
      let errorToken = this[_next]({offset: -1});
      this[_expectKeyword]("schema");
      let directives = this[_parseDirectives]({isConst: true});
      let operationTypes = this[_maybeParseMany](ast.OperationTypeDefinitionNode, lexer$.TokenKind.braceL, dart.bind(this, _parseOperationTypeDefinition), lexer$.TokenKind.braceR);
      if (dart.test(directives[$isEmpty]) && dart.test(operationTypes[$isEmpty])) {
        dart.throw(new span_exception.SourceSpanException.new("Schema extension must have either directives or operation types defined", errorToken.span.expand(this[_next]().span)));
      }
      return new ast.SchemaExtensionNode.new({directives: directives, operationTypes: operationTypes});
    }
    [_parseScalarTypeExtension]() {
      let errorToken = this[_next]({offset: -1});
      this[_expectKeyword]("scalar");
      let name = this[_parseName]("Expected a scalar name");
      let directives = this[_parseDirectives]({isConst: true});
      if (dart.test(directives[$isEmpty])) {
        dart.throw(new span_exception.SourceSpanException.new("Scalar extension must have either directives defined", errorToken.span.expand(this[_next]().span)));
      }
      return new ast.ScalarTypeExtensionNode.new({name: name, directives: directives});
    }
    [_parseObjectTypeExtension]() {
      let errorToken = this[_next]({offset: -1});
      this[_expectKeyword]("type");
      let name = this[_parseName]("Expected an object type name");
      let interfaces = this[_parseImplementsInterfaces]();
      let directives = this[_parseDirectives]({isConst: true});
      let fields = this[_parseFieldsDefinition]();
      if (dart.test(interfaces[$isEmpty]) && dart.test(directives[$isEmpty]) && dart.test(fields[$isEmpty])) {
        dart.throw(new span_exception.SourceSpanException.new("Object type extension must define at least one directive or field, or implement at lease one interface", errorToken.span.expand(this[_next]().span)));
      }
      return new ast.ObjectTypeExtensionNode.new({name: name, interfaces: interfaces, directives: directives, fields: fields});
    }
    [_parseInterfaceTypeExtension]() {
      let errorToken = this[_next]({offset: -1});
      this[_expectKeyword]("interface");
      let name = this[_parseName]("Expected an interface name");
      let directives = this[_parseDirectives]({isConst: true});
      let fields = this[_parseFieldsDefinition]();
      if (dart.test(directives[$isEmpty]) && dart.test(fields[$isEmpty])) {
        dart.throw(new span_exception.SourceSpanException.new("Interface type extension must define at least one directive or field", errorToken.span.expand(this[_next]().span)));
      }
      return new ast.InterfaceTypeExtensionNode.new({name: name, directives: directives, fields: fields});
    }
    [_parseUnionTypeExtension]() {
      let errorToken = this[_next]({offset: -1});
      this[_expectKeyword]("union");
      let name = this[_parseName]("Expected a union name");
      let directives = this[_parseDirectives]({isConst: true});
      let types = this[_parseUnionMemberTypes]();
      if (dart.test(directives[$isEmpty]) && dart.test(types[$isEmpty])) {
        dart.throw(new span_exception.SourceSpanException.new("Union type extension must define at least one directive or type", errorToken.span.expand(this[_next]().span)));
      }
      return new ast.UnionTypeExtensionNode.new({name: name, directives: directives, types: types});
    }
    [_parseEnumTypeExtension]() {
      let errorToken = this[_next]({offset: -1});
      this[_expectKeyword]("enum");
      let name = this[_parseName]("Expected an enum name");
      let directives = this[_parseDirectives]({isConst: true});
      let values = this[_parseEnumValuesDefinition]();
      if (dart.test(directives[$isEmpty]) && dart.test(values[$isEmpty])) {
        dart.throw(new span_exception.SourceSpanException.new("Enum type extension must define at least one directive or value", errorToken.span.expand(this[_next]().span)));
      }
      return new ast.EnumTypeExtensionNode.new({name: name, directives: directives, values: values});
    }
    [_parseInputObjectTypeExtension]() {
      let errorToken = this[_next]({offset: -1});
      this[_expectKeyword]("input");
      let name = this[_parseName]("Expected an input object type name");
      let directives = this[_parseDirectives]({isConst: true});
      let fields = this[_parseInputFieldsDefinition]();
      if (dart.test(directives[$isEmpty]) && dart.test(fields[$isEmpty])) {
        dart.throw(new span_exception.SourceSpanException.new("Input type extension must define at least one directive or field, or implement at lease one interface", errorToken.span.expand(this[_next]().span)));
      }
      return new ast.InputObjectTypeExtensionNode.new({name: name, directives: directives, fields: fields});
    }
  };
  (parser$._Parser.new = function(_tokens) {
    this[_position] = 0;
    this[_tokens$] = _tokens;
    this[_length] = _tokens[$length];
    ;
  }).prototype = parser$._Parser.prototype;
  dart.addTypeTests(parser$._Parser);
  dart.setMethodSignature(parser$._Parser, () => ({
    __proto__: dart.getMethods(parser$._Parser.__proto__),
    parse: dart.fnType(ast.DocumentNode, []),
    [_peek]: dart.fnType(core.bool, [lexer$.TokenKind], {offset: core.int}, {}),
    [_advance]: dart.fnType(dart.void, []),
    [_expectToken]: dart.fnType(lexer$.Token, [lexer$.TokenKind], [core.String]),
    [_expectOptionalToken]: dart.fnType(lexer$.Token, [lexer$.TokenKind]),
    [_expectKeyword]: dart.fnType(lexer$.Token, [core.String], [core.String]),
    [_expectOptionalKeyword]: dart.fnType(lexer$.Token, [core.String]),
    [_next]: dart.fnType(lexer$.Token, [], {offset: core.int}, {}),
    [_parseMany]: dart.gFnType(N => [core.List$(N), [lexer$.TokenKind, dart.fnType(N, []), lexer$.TokenKind], [core.String]]),
    [_maybeParseMany]: dart.gFnType(N => [core.List$(N), [lexer$.TokenKind, dart.fnType(N, []), lexer$.TokenKind], [core.String]]),
    [_parseDocument]: dart.fnType(ast.DocumentNode, []),
    [_parseDefinition]: dart.fnType(ast.DefinitionNode, []),
    [_parseExecutableDefinition]: dart.fnType(ast.ExecutableDefinitionNode, []),
    [_parseFragmentDefinition]: dart.fnType(ast.FragmentDefinitionNode, []),
    [_parseOperationDefinition]: dart.fnType(ast.OperationDefinitionNode, []),
    [_parseVariableDefinitions]: dart.fnType(core.List$(ast.VariableDefinitionNode), []),
    [_parseVariableDefinition]: dart.fnType(ast.VariableDefinitionNode, []),
    [_parseVariable]: dart.fnType(ast.VariableNode, []),
    [_parseType]: dart.fnType(ast.TypeNode, []),
    [_parseNamedType]: dart.fnType(ast.NamedTypeNode, []),
    [_parseConstObjectField]: dart.fnType(ast.ObjectFieldNode, []),
    [_parseNonConstObjectField]: dart.fnType(ast.ObjectFieldNode, []),
    [_parseObjectField]: dart.fnType(ast.ObjectFieldNode, [], {isConst: core.bool}, {}),
    [_parseConstValue]: dart.fnType(ast.ValueNode, []),
    [_parseNonConstValue]: dart.fnType(ast.ValueNode, []),
    [_parseValue]: dart.fnType(ast.ValueNode, [], {isConst: core.bool}, {}),
    [_parseStringValue]: dart.fnType(ast.StringValueNode, []),
    [_parseList]: dart.fnType(ast.ListValueNode, [], {isConst: core.bool}, {}),
    [_parseObject]: dart.fnType(ast.ObjectValueNode, [], {isConst: core.bool}, {}),
    [_parseDirectives]: dart.fnType(core.List$(ast.DirectiveNode), [], {isConst: core.bool}, {}),
    [_parseDirective]: dart.fnType(ast.DirectiveNode, [], {isConst: core.bool}, {}),
    [_parseArguments]: dart.fnType(core.List$(ast.ArgumentNode), [], {isConst: core.bool}, {}),
    [_parseConstArgument]: dart.fnType(ast.ArgumentNode, []),
    [_parseNonConstArgument]: dart.fnType(ast.ArgumentNode, []),
    [_parseArgument]: dart.fnType(ast.ArgumentNode, [], {isConst: core.bool}, {}),
    [_parseOperationType]: dart.fnType(ast.OperationType, []),
    [_parseSelectionSet]: dart.fnType(ast.SelectionSetNode, []),
    [_parseSelection]: dart.fnType(ast.SelectionNode, []),
    [_parseFragment]: dart.fnType(ast.SelectionNode, []),
    [_parseFragmentName]: dart.fnType(ast.NameNode, []),
    [_parseField]: dart.fnType(ast.FieldNode, []),
    [_parseName]: dart.fnType(ast.NameNode, [], [core.String]),
    [_parseTypeSystemDefinition]: dart.fnType(ast.TypeSystemDefinitionNode, []),
    [_parseTypeSystemExtension]: dart.fnType(ast.TypeSystemExtensionNode, []),
    [_parseSchemaDefinition]: dart.fnType(ast.SchemaDefinitionNode, []),
    [_parseOperationTypeDefinition]: dart.fnType(ast.OperationTypeDefinitionNode, []),
    [_parseScalarTypeDefinition]: dart.fnType(ast.ScalarTypeDefinitionNode, []),
    [_parseDescription]: dart.fnType(ast.StringValueNode, []),
    [_parseImplementsInterfaces]: dart.fnType(core.List$(ast.NamedTypeNode), []),
    [_parseObjectTypeDefinition]: dart.fnType(ast.ObjectTypeDefinitionNode, []),
    [_parseFieldsDefinition]: dart.fnType(core.List$(ast.FieldDefinitionNode), []),
    [_parseFieldDefinition]: dart.fnType(ast.FieldDefinitionNode, []),
    [_parseArgumentDefinitions]: dart.fnType(core.List$(ast.InputValueDefinitionNode), []),
    [_parseInputValueDefinition]: dart.fnType(ast.InputValueDefinitionNode, []),
    [_parseInterfaceTypeDefinition]: dart.fnType(ast.InterfaceTypeDefinitionNode, []),
    [_parseUnionTypeDefinition]: dart.fnType(ast.UnionTypeDefinitionNode, []),
    [_parseUnionMemberTypes]: dart.fnType(core.List$(ast.NamedTypeNode), []),
    [_parseEnumTypeDefinition]: dart.fnType(ast.EnumTypeDefinitionNode, []),
    [_parseEnumValuesDefinition]: dart.fnType(core.List$(ast.EnumValueDefinitionNode), []),
    [_parseEnumValueDefinition]: dart.fnType(ast.EnumValueDefinitionNode, []),
    [_parseInputObjectTypeDefinition]: dart.fnType(ast.InputObjectTypeDefinitionNode, []),
    [_parseInputFieldsDefinition]: dart.fnType(core.List$(ast.InputValueDefinitionNode), []),
    [_parseDirectiveDefinition]: dart.fnType(ast.DirectiveDefinitionNode, []),
    [_parseDirectiveLocations]: dart.fnType(core.List$(ast.DirectiveLocation), []),
    [_parseDirectiveLocation]: dart.fnType(ast.DirectiveLocation, []),
    [_parseSchemaExtension]: dart.fnType(ast.SchemaExtensionNode, []),
    [_parseScalarTypeExtension]: dart.fnType(ast.ScalarTypeExtensionNode, []),
    [_parseObjectTypeExtension]: dart.fnType(ast.ObjectTypeExtensionNode, []),
    [_parseInterfaceTypeExtension]: dart.fnType(ast.InterfaceTypeExtensionNode, []),
    [_parseUnionTypeExtension]: dart.fnType(ast.UnionTypeExtensionNode, []),
    [_parseEnumTypeExtension]: dart.fnType(ast.EnumTypeExtensionNode, []),
    [_parseInputObjectTypeExtension]: dart.fnType(ast.InputObjectTypeExtensionNode, [])
  }));
  dart.setLibraryUri(parser$._Parser, "package:gql/src/language/parser.dart");
  dart.setFieldSignature(parser$._Parser, () => ({
    __proto__: dart.getFields(parser$._Parser.__proto__),
    [_tokens$]: dart.finalFieldType(core.List$(lexer$.Token)),
    [_length]: dart.finalFieldType(core.int),
    [_position]: dart.fieldType(core.int)
  }));
  parser$.parse = function parse(source) {
    let lexer = new lexer$.Lexer.new();
    let tokens = lexer.tokenize(source);
    let parser = new parser$._Parser.new(tokens);
    return parser.parse();
  };
  parser$.parseString = function parseString(source, opts) {
    let url = opts && 'url' in opts ? opts.url : null;
    return parser$.parse(new file.SourceFile.fromString(source, {url: url}));
  };
  let C29;
  let C30;
  let C31;
  let C32;
  let C33;
  let C34;
  let C35;
  let C36;
  let C37;
  let C38;
  let C39;
  let C40;
  let C41;
  let C42;
  let C43;
  lexer$.TokenKind = class TokenKind extends core.Object {
    toString() {
      return this[_name$];
    }
  };
  (lexer$.TokenKind.new = function(index, _name) {
    this.index = index;
    this[_name$] = _name;
    ;
  }).prototype = lexer$.TokenKind.prototype;
  dart.addTypeTests(lexer$.TokenKind);
  dart.setLibraryUri(lexer$.TokenKind, "package:gql/src/language/lexer.dart");
  dart.setFieldSignature(lexer$.TokenKind, () => ({
    __proto__: dart.getFields(lexer$.TokenKind.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(lexer$.TokenKind, ['toString']);
  lexer$.TokenKind.sof = C29 || CT.C29;
  lexer$.TokenKind.eof = C30 || CT.C30;
  lexer$.TokenKind.bang = C31 || CT.C31;
  lexer$.TokenKind.dollar = C28 || CT.C28;
  lexer$.TokenKind.amp = C32 || CT.C32;
  lexer$.TokenKind.parenL = C33 || CT.C33;
  lexer$.TokenKind.parenR = C34 || CT.C34;
  lexer$.TokenKind.spread = C35 || CT.C35;
  lexer$.TokenKind.colon = C36 || CT.C36;
  lexer$.TokenKind.equals = C37 || CT.C37;
  lexer$.TokenKind.at = C38 || CT.C38;
  lexer$.TokenKind.bracketL = C21 || CT.C21;
  lexer$.TokenKind.bracketR = C39 || CT.C39;
  lexer$.TokenKind.braceL = C22 || CT.C22;
  lexer$.TokenKind.pipe = C40 || CT.C40;
  lexer$.TokenKind.braceR = C41 || CT.C41;
  lexer$.TokenKind.name_ = C27 || CT.C27;
  lexer$.TokenKind.int = C23 || CT.C23;
  lexer$.TokenKind.float = C24 || CT.C24;
  lexer$.TokenKind.string = C25 || CT.C25;
  lexer$.TokenKind.blockString = C26 || CT.C26;
  lexer$.TokenKind.comment = C42 || CT.C42;
  lexer$.TokenKind.values = C43 || CT.C43;
  lexer$.Token = class Token extends core.Object {
    _equals(other) {
      if (other == null) return false;
      return lexer$.Token.is(other) && dart.equals(other.kind, this.kind) && dart.equals(other.span, this.span);
    }
  };
  (lexer$.Token.new = function() {
    ;
  }).prototype = lexer$.Token.prototype;
  dart.addTypeTests(lexer$.Token);
  dart.setMethodSignature(lexer$.Token, () => ({
    __proto__: dart.getMethods(lexer$.Token.__proto__),
    _equals: dart.fnType(core.bool, [core.Object]),
    [$_equals]: dart.fnType(core.bool, [core.Object])
  }));
  dart.setLibraryUri(lexer$.Token, "package:gql/src/language/lexer.dart");
  dart.defineExtensionMethods(lexer$.Token, ['_equals']);
  const _text = dart.privateName(lexer$, "_text");
  const _getValue = dart.privateName(lexer$, "_getValue");
  const kind$ = dart.privateName(lexer$, "_Token.kind");
  const span$ = dart.privateName(lexer$, "_Token.span");
  lexer$._Token = class _Token extends core.Object {
    get kind() {
      return this[kind$];
    }
    set kind(value) {
      super.kind = value;
    }
    get span() {
      return this[span$];
    }
    set span(value) {
      super.span = value;
    }
    get [_text]() {
      return this.span.text;
    }
    [_getValue]() {
      switch (this.kind) {
        case C25 || CT.C25:
        {
          return this[_text][$substring](1, this[_text].length - 1)[$replaceAllMapped](core.RegExp.new("\\\\[nrbtf\\\\/\"]"), dart.fn(match => {
            switch (match.group(0)[$codeUnitAt](1)) {
              case 34:
              case 47:
              case 92:
              {
                return match.group(0)[$_get](1);
              }
              case 98:
              {
                return "\b";
              }
              case 102:
              {
                return "\f";
              }
              case 110:
              {
                return "\n";
              }
              case 114:
              {
                return "\r";
              }
              case 116:
              {
                return "\t";
              }
              default:
              {
                return match.group(0);
              }
            }
          }, MatchToString()))[$replaceAllMapped](core.RegExp.new("\\\\u[0-9A-Za-z]{4}"), dart.fn(match => core.String.fromCharCode(lexer$.uniCharCode(match.group(0)[$codeUnitAt](2), match.group(0)[$codeUnitAt](3), match.group(0)[$codeUnitAt](4), match.group(0)[$codeUnitAt](5))), MatchToString()));
        }
        case C26 || CT.C26:
        {
          return lexer$.dedentBlockStringValue(this[_text][$substring](3, this[_text].length - 3)[$replaceAll]("\\\"\"\"", "\"\"\""));
        }
        default:
        {
          return this.span.text;
        }
      }
    }
    get value() {
      return this[_getValue]();
    }
    toString() {
      return "<" + dart.str(this.kind) + " in " + dart.str(this.span) + " \"" + dart.str(this.value) + "\">";
    }
  };
  (lexer$._Token.new = function(opts) {
    let kind = opts && 'kind' in opts ? opts.kind : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[kind$] = kind;
    this[span$] = span;
    ;
  }).prototype = lexer$._Token.prototype;
  dart.addTypeTests(lexer$._Token);
  lexer$._Token[dart.implements] = () => [lexer$.Token];
  dart.setMethodSignature(lexer$._Token, () => ({
    __proto__: dart.getMethods(lexer$._Token.__proto__),
    [_getValue]: dart.fnType(core.String, [])
  }));
  dart.setGetterSignature(lexer$._Token, () => ({
    __proto__: dart.getGetters(lexer$._Token.__proto__),
    [_text]: core.String,
    value: core.String
  }));
  dart.setLibraryUri(lexer$._Token, "package:gql/src/language/lexer.dart");
  dart.setFieldSignature(lexer$._Token, () => ({
    __proto__: dart.getFields(lexer$._Token.__proto__),
    kind: dart.finalFieldType(lexer$.TokenKind),
    span: dart.finalFieldType(file.FileSpan)
  }));
  dart.defineExtensionMethods(lexer$._Token, ['toString']);
  lexer$.Lexer = class Lexer extends core.Object {
    tokenize(src, opts) {
      let skipComments = opts && 'skipComments' in opts ? opts.skipComments : true;
      let scanner = new lexer$._Scanner.new(src);
      let tokens = JSArrayOfToken().of([]);
      let token = null;
      do {
        token = scanner.scanToken();
        if (dart.test(skipComments) && !dart.equals(token.kind, lexer$.TokenKind.comment)) {
          tokens[$add](token);
        }
      } while (!dart.equals(token.kind, lexer$.TokenKind.eof));
      return tokens;
    }
  };
  (lexer$.Lexer.new = function() {
    ;
  }).prototype = lexer$.Lexer.prototype;
  dart.addTypeTests(lexer$.Lexer);
  dart.setMethodSignature(lexer$.Lexer, () => ({
    __proto__: dart.getMethods(lexer$.Lexer.__proto__),
    tokenize: dart.fnType(core.List$(lexer$.Token), [file.SourceFile], {skipComments: core.bool}, {})
  }));
  dart.setLibraryUri(lexer$.Lexer, "package:gql/src/language/lexer.dart");
  const _scanDigits = dart.privateName(lexer$, "_scanDigits");
  lexer$._Scanner = class _Scanner extends core.Object {
    peek(opts) {
      let offset = opts && 'offset' in opts ? opts.offset : 0;
      if (dart.notNull(this.position) + dart.notNull(offset) >= dart.notNull(this.src.length)) {
        return null;
      }
      let start = dart.notNull(this.position) + dart.notNull(offset);
      return this.src.getText(start, start + 1)[$codeUnitAt](0);
    }
    createToken(kind, opts) {
      let length = opts && 'length' in opts ? opts.length : 1;
      let value = opts && 'value' in opts ? opts.value : false;
      let token = new lexer$._Token.new({kind: kind, span: this.src.span(this.position, dart.notNull(this.position) + dart.notNull(length))});
      this.position = dart.notNull(this.position) + dart.notNull(length);
      return token;
    }
    scanToken() {
      if (this.position === -1) {
        this.position = dart.notNull(this.position) + 1;
        return new lexer$._Token.new({kind: lexer$.TokenKind.sof, span: this.src.span(0, 0)});
      }
      this.consumeWhitespace();
      if (dart.notNull(this.position) >= dart.notNull(this.src.length)) {
        return new lexer$._Token.new({kind: lexer$.TokenKind.eof, span: this.src.span(this.src.length)});
      }
      let code = this.peek();
      if (dart.notNull(code) >= 65 && dart.notNull(code) <= 90 || code === 95 || dart.notNull(code) >= 97 && dart.notNull(code) <= 122) {
        return this.scanName();
      }
      if (dart.notNull(code) >= 48 && dart.notNull(code) <= 57 || code === 45) {
        return this.scanNumber();
      }
      switch (code) {
        case 33:
        {
          return this.createToken(lexer$.TokenKind.bang);
        }
        case 35:
        {
          return this.scanComment();
        }
        case 36:
        {
          return this.createToken(lexer$.TokenKind.dollar);
        }
        case 38:
        {
          return this.createToken(lexer$.TokenKind.amp);
        }
        case 40:
        {
          return this.createToken(lexer$.TokenKind.parenL);
        }
        case 41:
        {
          return this.createToken(lexer$.TokenKind.parenR);
        }
        case 46:
        {
          if (this.peek({offset: 1}) === 46 && this.peek({offset: 2}) === 46) {
            return this.createToken(lexer$.TokenKind.spread, {length: 3});
          }
          break;
        }
        case 58:
        {
          return this.createToken(lexer$.TokenKind.colon);
        }
        case 61:
        {
          return this.createToken(lexer$.TokenKind.equals);
        }
        case 64:
        {
          return this.createToken(lexer$.TokenKind.at);
        }
        case 91:
        {
          return this.createToken(lexer$.TokenKind.bracketL);
        }
        case 93:
        {
          return this.createToken(lexer$.TokenKind.bracketR);
        }
        case 123:
        {
          return this.createToken(lexer$.TokenKind.braceL);
        }
        case 124:
        {
          return this.createToken(lexer$.TokenKind.pipe);
        }
        case 125:
        {
          return this.createToken(lexer$.TokenKind.braceR);
        }
        case 34:
        {
          if (this.peek({offset: 1}) === 34 && this.peek({offset: 2}) === 34) {
            return this.scanBlockString();
          }
          return this.scanString();
        }
      }
      dart.throw(new span_exception.SourceSpanException.new("Syntax Error", this.src.span(this.position, this.position)));
    }
    scanComment() {
      this.position = dart.notNull(this.position) + 1;
      let length = 0;
      let code = null;
      do {
        code = this.peek({offset: length = length + 1});
      } while (code != null && (dart.notNull(code) > 31 || code === 9));
      return this.createToken(lexer$.TokenKind.comment, {length: length, value: true});
    }
    scanNumber() {
      let length = 0;
      let code = this.peek();
      let isFloat = false;
      if (code === 45) {
        code = this.peek({offset: length = dart.notNull(length) + 1});
      }
      if (code === 48) {
        code = this.peek({offset: length = dart.notNull(length) + 1});
        if (code != null && dart.notNull(code) >= 48 && dart.notNull(code) <= 57) {
          dart.throw(new span_exception.SourceSpanException.new("Syntax Error", this.src.span(this.position, this.position)));
        }
      } else {
        length = this[_scanDigits](length);
        code = this.peek({offset: length});
      }
      if (code === 46) {
        isFloat = true;
        code = this.peek({offset: length = dart.notNull(length) + 1});
        length = this[_scanDigits](length);
        code = this.peek({offset: length});
      }
      if (code === 69 || code === 101) {
        isFloat = true;
        code = this.peek({offset: length = dart.notNull(length) + 1});
        if (code === 43 || code === 45) {
          code = this.peek({offset: length = dart.notNull(length) + 1});
        }
        length = this[_scanDigits](length);
      }
      return this.createToken(isFloat ? lexer$.TokenKind.float : lexer$.TokenKind.int, {length: length, value: true});
    }
    [_scanDigits](offset) {
      let digitOffset = offset;
      let code = this.peek({offset: digitOffset});
      if (dart.notNull(code) >= 48 && dart.notNull(code) <= 57) {
        do {
          code = this.peek({offset: digitOffset = dart.notNull(digitOffset) + 1});
        } while (code != null && dart.notNull(code) >= 48 && dart.notNull(code) <= 57);
        return digitOffset;
      }
      dart.throw(new span_exception.SourceSpanException.new("Expected a digit", this.src.span(this.position, this.position)));
    }
    scanName() {
      let length = 0;
      let code = this.peek();
      while (dart.notNull(this.position) + length !== this.src.length && code != null && (code === 95 || dart.notNull(code) >= 48 && dart.notNull(code) <= 57 || dart.notNull(code) >= 65 && dart.notNull(code) <= 90 || dart.notNull(code) >= 97 && dart.notNull(code) <= 122)) {
        code = this.peek({offset: length = length + 1});
      }
      return this.createToken(lexer$.TokenKind.name_, {length: length, value: true});
    }
    scanString() {
      let start = this.position;
      this.position = dart.notNull(this.position) + 1;
      let code = this.peek();
      while (dart.notNull(this.position) < dart.notNull(this.src.length) && code != null && code !== 10 && code !== 13) {
        if (code === 34) {
          return new lexer$._Token.new({kind: lexer$.TokenKind.string, span: this.src.span(start, this.position = dart.notNull(this.position) + 1)});
        }
        if (code == null || dart.notNull(code) < 32 && code !== 9) {
          dart.throw(new span_exception.SourceSpanException.new("Unexpected character in a string literal", this.src.span(this.position, this.position)));
        }
        this.position = dart.notNull(this.position) + 1;
        if (code === 92) {
          code = this.peek();
          switch (code) {
            case 34:
            case 47:
            case 92:
            case 98:
            case 102:
            case 110:
            case 114:
            case 116:
            {
              break;
            }
            case 117:
            {
              let isUnicode = dart.test(lexer$.isHex(this.peek({offset: 1}))) && dart.test(lexer$.isHex(this.peek({offset: 2}))) && dart.test(lexer$.isHex(this.peek({offset: 3}))) && dart.test(lexer$.isHex(this.peek({offset: 4})));
              if (!isUnicode) {
                dart.throw(new span_exception.SourceSpanException.new("Expected hexadecimal number", this.src.span(dart.notNull(this.position) + 1, dart.notNull(this.position) + 4)));
              }
              this.position = dart.notNull(this.position) + 4;
              break;
            }
            default:
            {
              dart.throw(new span_exception.SourceSpanException.new("Unknown escape character", this.src.span(this.position, this.position)));
            }
          }
          this.position = dart.notNull(this.position) + 1;
        }
        code = this.peek();
      }
      dart.throw(new span_exception.SourceSpanException.new("Unexpected character in a string literal", this.src.span(this.position, this.position)));
    }
    scanBlockString() {
      let start = this.position;
      this.position = dart.notNull(this.position) + 3;
      let code = this.peek();
      while (dart.notNull(this.position) < dart.notNull(this.src.length) && code != null) {
        if (code === 34 && this.peek({offset: 1}) === 34 && this.peek({offset: 2}) === 34) {
          this.position = dart.notNull(this.position) + 3;
          return new lexer$._Token.new({kind: lexer$.TokenKind.blockString, span: this.src.span(start, this.position)});
        }
        if (code == null || dart.notNull(code) < 32 && code !== 9 && code !== 10 && code !== 13) {
          dart.throw(new span_exception.SourceSpanException.new("Unexpected character in a string literal", this.src.span(this.position, this.position)));
        }
        if (code === 10) {
          this.position = dart.notNull(this.position) + 1;
          this.line = dart.notNull(this.line) + 1;
          this.lineStart = this.position;
        } else if (code === 13) {
          if (this.peek({offset: 1}) === 10) {
            this.position = dart.notNull(this.position) + 2;
          } else {
            this.position = dart.notNull(this.position) + 1;
          }
          this.line = dart.notNull(this.line) + 1;
          this.lineStart = this.position;
        } else if (code === 92 && this.peek({offset: 1}) === 34 && this.peek({offset: 2}) === 34 && this.peek({offset: 3}) === 34) {
          this.position = dart.notNull(this.position) + 4;
        } else {
          this.position = dart.notNull(this.position) + 1;
        }
        code = this.peek();
      }
      dart.throw(new span_exception.SourceSpanException.new("Unexpected character in a string literal", this.src.span(this.position, this.position)));
    }
    consumeWhitespace() {
      while (dart.notNull(this.position) < dart.notNull(this.src.length)) {
        let next = this.peek();
        if (next === 9 || next === 32 || next === 44 || next === 65279) {
          this.position = dart.notNull(this.position) + 1;
        } else if (next === 10) {
          this.position = dart.notNull(this.position) + 1;
          this.line = dart.notNull(this.line) + 1;
          this.lineStart = this.position;
        } else if (next === 13) {
          if (this.peek({offset: 1}) === 10) {
            this.position = dart.notNull(this.position) + 2;
          } else {
            this.position = dart.notNull(this.position) + 1;
          }
          this.line = dart.notNull(this.line) + 1;
          this.lineStart = this.position;
        } else {
          break;
        }
      }
    }
  };
  (lexer$._Scanner.new = function(src) {
    this.position = -1;
    this.line = 1;
    this.lineStart = 0;
    this.src = src;
    ;
  }).prototype = lexer$._Scanner.prototype;
  dart.addTypeTests(lexer$._Scanner);
  dart.setMethodSignature(lexer$._Scanner, () => ({
    __proto__: dart.getMethods(lexer$._Scanner.__proto__),
    peek: dart.fnType(core.int, [], {offset: core.int}, {}),
    createToken: dart.fnType(lexer$.Token, [lexer$.TokenKind], {length: core.int, value: core.bool}, {}),
    scanToken: dart.fnType(lexer$.Token, []),
    scanComment: dart.fnType(lexer$.Token, []),
    scanNumber: dart.fnType(lexer$.Token, []),
    [_scanDigits]: dart.fnType(core.int, [core.int]),
    scanName: dart.fnType(lexer$.Token, []),
    scanString: dart.fnType(lexer$.Token, []),
    scanBlockString: dart.fnType(lexer$.Token, []),
    consumeWhitespace: dart.fnType(dart.void, [])
  }));
  dart.setLibraryUri(lexer$._Scanner, "package:gql/src/language/lexer.dart");
  dart.setFieldSignature(lexer$._Scanner, () => ({
    __proto__: dart.getFields(lexer$._Scanner.__proto__),
    src: dart.finalFieldType(file.SourceFile),
    position: dart.fieldType(core.int),
    line: dart.fieldType(core.int),
    lineStart: dart.fieldType(core.int)
  }));
  let C44;
  lexer$.isHex = function isHex(a) {
    return dart.notNull(a) >= 48 && dart.notNull(a) <= 57 || dart.notNull(a) >= 65 && dart.notNull(a) <= 70 || dart.notNull(a) >= 97 && dart.notNull(a) <= 102;
  };
  lexer$.uniCharCode = function uniCharCode(a, b, c, d) {
    return (dart.notNull(lexer$.char2Hex(a)) << 12 | dart.notNull(lexer$.char2Hex(b)) << 8 >>> 0 | dart.notNull(lexer$.char2Hex(c)) << 4 >>> 0 | dart.notNull(lexer$.char2Hex(d))) >>> 0;
  };
  lexer$.char2Hex = function char2Hex(a) {
    if (dart.notNull(a) >= 48 && dart.notNull(a) <= 57) {
      return dart.notNull(a) - 48;
    }
    if (dart.notNull(a) >= 65 && dart.notNull(a) <= 70) {
      return dart.notNull(a) - 55;
    }
    if (dart.notNull(a) >= 97 && dart.notNull(a) <= 102) {
      return dart.notNull(a) - 87;
    }
    return -1;
  };
  lexer$.dedentBlockStringValue = function dedentBlockStringValue(value) {
    let lines = value[$split](core.RegExp.new("\\r\\n|[\\n\\r]"));
    let commonIndent = null;
    for (let i = 1; i < dart.notNull(lines[$length]); i = i + 1) {
      let line = lines[$_get](i);
      let indent = lexer$.leadingWhitespace(line);
      if (dart.notNull(indent) < line.length && (commonIndent == null || dart.notNull(indent) < dart.notNull(commonIndent))) {
        commonIndent = indent;
        if (commonIndent === 0) {
          break;
        }
      }
    }
    if (commonIndent != null && commonIndent !== 0) {
      lines = lines[$map](core.String, dart.fn(line => {
        if (line.length < dart.notNull(commonIndent)) {
          return "";
        } else {
          return line[$substring](commonIndent);
        }
      }, StringToString()))[$toList]();
    }
    lines[$skipWhile](C44 || CT.C44);
    while (dart.test(lines[$isNotEmpty]) && dart.test(lexer$.isBlank(lines[$first]))) {
      lines[$removeAt](0);
    }
    while (dart.test(lines[$isNotEmpty]) && dart.test(lexer$.isBlank(lines[$last]))) {
      lines[$removeLast]();
    }
    return lines[$join]("\n");
  };
  lexer$.leadingWhitespace = function leadingWhitespace(str) {
    let i = 0;
    while (i < str.length && (str[$_get](i) === " " || str[$_get](i) === "\t")) {
      i = i + 1;
    }
    return i;
  };
  lexer$.isBlank = function isBlank(str) {
    return lexer$.leadingWhitespace(str) === str.length;
  };
  dart.trackLibraries("packages/gql/language", {
    "package:gql/language.dart": language,
    "package:gql/src/language/printer.dart": printer,
    "package:gql/src/language/parser.dart": parser$,
    "package:gql/src/language/lexer.dart": lexer$
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["src/language/printer.dart","src/language/parser.dart","src/language/lexer.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;cAYqB;AAAS,YAAK,AAAmB,uBAAZ,IAAI,EAAE;IAAY;sBAGpB;AAAQ,YAAA,AAAI,AAC7C,AAGA,IAJ4C,gCAE3C,QAAC,OAAQ,AAAI,GAAD,qBAAQ,yCAEhB;IAAO;0BAEoC;AAAe,YAAA,AAC/D,AAGA,WAJyE,oBAExE,QAAC,aAAc,AAAU,SAAD,qBAAQ,wCAE5B;IAAI;wCAGgC;AAC1C,YAAA,AAeE;;AAdA;AACA;AACA,+BAAU,cAAF,aAAE,eAAF;AACR,sBAAG,AACE,AAOA,eARY,uBAEX,QAAC,iBAAkB,sBACjB,cAAQ,cACR,AAAc,aAAD,qBAAQ,OACrB,6DAGE;AARV;AASA,+BAAU,cAAF,aAAE,eAAF;AACR;;;IACM;mCAG6B;AACrC,YAAA,AAAI,AAKY,oBAJhB,AACK,AAGA,IAJD,oBAEE,QAAC,OAAQ,AAAI,GAAD,qBAAQ,iDAEhB,SACV;IAAG;yBAE4C;AAC/C,YAAA,AAAI,AAKY,oBAJhB,AACK,AAGA,IAJD,oBAEE,QAAC,OAAQ,AAAI,GAAD,qBAAQ,uCAEhB,SACV;IAAG;gCAGmD;AAAQ,YAAA,AAK5D,uBAJA,AAAI,AAAS,GAAV,8BAAiB,OACpB,MACA,AAAI,AAAK,GAAN,0BAAa,OAChB,AAAI,AAAa,GAAd,kCAAqB;IAClB;0BAGkC;AAC5C,UAAI,AAAiB,AAAM,gBAAP,UAAU,MAAM,MAAO;AAE3C,YAAO,AAA2C,kBAArC,AAAiB,AAAM,gBAAP,2BAAc;IAC7C;sBAGsC;AAClC,YAAA,AAAK,oBAAE,AAAS,AAAK,QAAN,0BAAa;IAAK;iCAGuB;AAAO,YAAA,AAc7D;;AAbA,iCAAQ,AAAG,EAAD;AACV,YAAI,AAAG,EAAD,SAAS,MAAM,cAAG,uBACtB,KACA,AAAG,AAAK,EAAN,0BAAa;AAFI;AAIrB,YAAI,AAAG,EAAD,wBAAwB,kBAAQ,AAAG,AAAoB,EAArB,oCACtC,6CAA+B,AAAG,EAAD;AACnC,YAAI,AAAG,EAAD,eAAe,kBAAQ,AAAG,AAAW,EAAZ,2BAAwB,cAAG,uBACxD,KACA,2BAAsB,AAAG,EAAD;AAF6B;AAIvD;AACA,4CAAsB,AAAG,EAAD;;;IAClB;kBAGkB;AAAS,YAAA,AAAK,KAAD;IAAM;uBAGT;AAAkB,YAAA,AAMpD;;AALA;AACmB,iBAAnB,AAAc,aAAD,0BAAa;AAC1B,YAAI,AAAc,aAAD,cAAc,kBAC3B,AAAc,AAAU,aAAX,0BACf,mCAAqB,AAAc,aAAD;;;IAC9B;sBAG0B;AAAiB,YAAA,AAKjD;;AAJA;AACkB,iBAAlB,AAAa,YAAD,0BAAa;AACzB;AACA,sBAAI,AAAa,YAAD,aAAY;;;IACtB;uBAG4B;AAAkB,YAAA,AAGpD;;AAFmB,iBAAnB,AAAc,aAAD,0BAAa;AAC1B,sBAAI,AAAc,aAAD,aAAY;;;IACvB;yBAGgC;AAAoB,YAAA,AAI1D,uBAHA,AAAgB,AAAK,eAAN,0BAAa,OAC5B,MACA,AAAgB,AAAM,eAAP,2BAAc;IACvB;yBAGgC;AAAoB,YAAA,AAW1D;;AAVA;AACA,sBAAG,AAAgB,AACd,AAMA,gBAPa,8BAEZ,QAAC,SAAU,sBACT,MACA,AAAM,KAAD,qBAAQ,kDAGX;AAPV;AAQA;;;IACM;uBAG4B;AAAkB,YAAA,AAWpD;;AAVA;AACA,uBAAG,AAAc,AACZ,AAMA,cAPW,8BAEV,QAAC,SAAU,sBACT,MACA,AAAM,KAAD,qBAAQ,4CAGX;AAPV;AAQA;;;IACM;uBAG4B;AACpC,YAAA,AAAc,AAAK,cAAN,0BAAa;IAAK;uBAGK;AAAkB;IAAM;0BAGlB;AAC1C,uBAAA,AAAiB,gBAAD,UAAS,SAAS;IAAO;yBAGD;AAC1C,qBAAK,AAAgB,eAAD;AAClB,cAAO,AAIL,uBAHA,MACA,AAAgB,eAAD,QACf;;AAIJ,YAAO,AAQL,uBAPA,UACA,MACA,cAAQ,cACR,AAAgB,AAAM,eAAP,oBAAkB,UAAO,aACxC,MACA,cAAQ,cACR;IAEJ;wBAG0C;AACtC,YAAA,AAAe,eAAD;IAAM;sBAGc;AAAiB,YAAA,AAAa,aAAD;IAAM;2BAGzB;AAC5C,YAAA,AAAyC,kBAAnC,AAAkB,AAAG,iBAAJ,wBAAW;IAAO;gCAId;AAC3B,YAAA,AAgBE;;AAfA;AAC4B,kBAA5B,AAAuB,sBAAD,0BAAa;AACnC,YAAI,AAAuB,sBAAD,kBAAkB,MAAM,eAAG,uBACnD,KACA,AAAuB,AAAc,sBAAf,mCAAsB;AAFI;AAIlD,YAAI,AAAuB,sBAAD,eAAe,kBACrC,AAAuB,AAAW,sBAAZ,2BAAwB,eAAG,uBACnD,KACA,2BAAsB,AAAuB,sBAAD;AAFI;AAIlD,YAAI,AAAuB,sBAAD,iBAAiB,MAAM,eAAG,uBAClD,KACA,AAAuB,AAAa,sBAAd,kCAAqB;AAFI;;;IAI3C;4BAGsC;AAAuB,YAAA,AAenE;;AAdA;AACA,YAAI,AAAmB,kBAAD,kBAAkB,MAAM,eAAG,uBAC/C,KACA,AAAmB,AAAc,kBAAf,mCAAsB;AAFI;AAI9C,YAAI,AAAmB,kBAAD,eAAe,kBACjC,AAAmB,AAAW,kBAAZ,2BAAwB,eAAG,uBAC/C,KACA,2BAAsB,AAAmB,kBAAD;AAFI;AAI9C,YAAI,AAAmB,kBAAD,iBAAiB,MAAM,eAAG,uBAC9C,KACA,AAAmB,AAAa,kBAAd,kCAAqB;AAFI;;;IAIvC;4BAGsC;AAAuB,YAAA,AAQnE;;AAPA;AACwB,kBAAxB,AAAmB,kBAAD,0BAAa;AAC/B,YAAI,AAAmB,kBAAD,eAAe,kBACjC,AAAmB,AAAW,kBAAZ,2BAAwB,eAAG,uBAC/C,KACA,2BAAsB,AAAmB,kBAAD;AAFI;;;IAIxC;sBAG0B;AAAiB,YAAA,AAIjD,uBAHA,AAAa,AAAK,YAAN,0BAAa,OACzB,MACA,AAAa,AAAM,YAAP,2BAAc;IACpB;mBAGoB;AAAc,YAAA,AAiBxC;;AAhBA,YAAI,AAAU,SAAD,UAAU,MAAM,eAAG,uBAC9B,AAAU,AAAM,SAAP,2BAAc,OACvB;AAF2B;AAId,kBAAf,AAAU,SAAD,0BAAa;AACtB,YAAI,AAAU,SAAD,cAAc,kBAAQ,AAAU,AAAU,SAAX,0BAC1C,oCAAqB,AAAU,SAAD;AAChC,YAAI,AAAU,SAAD,eAAe,kBACxB,AAAU,AAAW,SAAZ,2BAAwB,eAAG,uBACtC,KACA,2BAAsB,AAAU,SAAD;AAFI;AAIrC,YAAI,AAAU,SAAD,iBAAiB,MAAM,eAAG,uBACrC,KACA,2BAAsB,AAAU,SAAD;AAFG;;;IAI9B;0BAGkC;AAAqB,YAAA,AAgB7D;;AAfA;AACA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR,uBAAG,AAAiB,AACf,AAOA,iBARc,kCAEb,QAAC,aAAc,sBACb,MACA,cAAQ,cACR,AAAU,SAAD,qBAAQ,gDAGf;AARV;AASA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR;;;IACM;8BAG0C;AAAS,YAAA,AAKzD,uBAJA,UACA,KACA,2BAAsB,AAAK,IAAD,cAC1B,yCAAoC,AAAK,IAAD;IAClC;qCAGwD;AAAS,YAAA,AAIvE,uBAHA,gBAAQ,AAAK,IAAD,aACZ,MACA,AAAK,AAAK,IAAN,0BAAa;IACX;kCAGkD;AAAS,YAAA,AAYjE;;AAXA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB;AAF4B;AAI9B;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB,eAAG,uBAC5D,KACA,2BAAsB,AAAK,IAAD;AAF+B;;;IAIrD;kCAGkD;AAAS,YAAA,AAYjE;;AAXA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB;AAF4B;AAI9B;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB;AACA,8CAAuB,AAAK,IAAD;AAC3B,6CAAsB,AAAK,IAAD;AAC1B,yCAAkB,AAAK,IAAD;;;IAChB;2BAE0C;AAClD,uBAAA,AAAW,UAAD,cACJ,KACA,AAaE;;AAZA;AACA;AACA,uBAAG,AACE,AAQA,WATQ,uBAEP,QAAC,cAAc,sBACb,KACA,KACA,AAAU,+BAAO,OACjB,8CAGE;AATV;;;IAUM;sBAEqC;AACnD,uBAAA,AAAO,MAAD,cACA,KACA,AAgBE;;AAfA;AACA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR,uBAAG,AACE,AAOA,OARI,uBAEH,QAAC,SAAU,sBACT,MACA,cAAQ,cACR,AAAM,KAAD,qBAAQ,sDAGX;AARV;AASA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR;;;IACM;6BAGkC;AAAS,YAAA,AAavD;;AAZA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB,MACA,cAAQ;AAHoB;AAKpB,kBAAV,AAAK,IAAD,0BAAa;AACjB,sDAA+B,AAAK,IAAD;AACnC;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB;AAC3D,6CAAsB,AAAK,IAAD;;;IACpB;mCAG+B;AACvC,uBAAA,AAAK,IAAD,cACE,KACA,AAYE;;AAXA;AACA,uBAAG,AACE,AAOA,KARE,uBAED,QAAC,OAAQ,sBACP,KACA,KACA,AAAI,GAAD,qBAAQ,2DAGT;AARV;AASA;;;IACM;kCAG4C;AAAS,YAAA,AAkBjE;;AAjBA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB,MACA,cAAQ;AAHoB;AAKpB,kBAAV,AAAK,IAAD,0BAAa;AACjB;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,iBAAiB,MAAM,eAAG,uBAChC,KACA,KACA,KACA,AAAK,AAAa,IAAd,kCAAqB;AAJI;AAM/B,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB;AAC3D,6CAAsB,AAAK,IAAD;;;IACpB;qCAGwD;AAAS,YAAA,AAWvE;;AAVA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB;AAF4B;AAI9B;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB;AACA,6CAAsB,AAAK,IAAD;AAC1B,yCAAkB,AAAK,IAAD;;;IAChB;iCAGgD;AAAS,YAAA,AAa/D;;AAZA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB;AAF4B;AAI9B;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB,eAAG,uBAC5D,KACA,2BAAsB,AAAK,IAAD;AAF+B;AAI3D,6CAAsB,AAAK,IAAD;;;IACpB;0BAEyC;AAAU,uBAAA,AAAM,KAAD,cAC9D,KACA,AAmBE;;AAlBA;AACA;AACA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR,uBAAG,AACE,AASA,MAVG,uBAEF,QAAC,QAAS,sBACR,MACA,cAAQ,cACR,KACA,KACA,AAAK,IAAD,qBAAQ,gDAGV;AAVV;AAWA,uBAAG,AAED,uBADA,cAAU,cAAF,aAAE,eAAF,YACH;AAFP;;;IAGM;gCAG4C;AAAS,YAAA,AAa7D;;AAZA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB;AAF4B;AAI9B;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB,eAAG,uBAC5D,KACA,2BAAsB,AAAK,IAAD;AAF+B;AAI3D,uDAAgC,AAAK,IAAD;;;IAC9B;oCAG8B;AACtC,uBAAA,AAAO,MAAD,cACA,KACA,AAiBE;;AAhBA;AACA;AACA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR,uBAAG,AACE,AAOA,OARI,uBAEH,QAAC,SAAU,sBACT,MACA,cAAQ,cACR,AAAM,KAAD,qBAAQ,0DAGX;AARV;AASA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR;;;IACM;iCAG0C;AAAS,YAAA,AAS/D;;AARA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB,MACA,cAAQ;AAHoB;AAKpB,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB;AAC3D,6CAAsB,AAAK,IAAD;;;IACpB;uCAI0B;AAClC,YAAA,AAWE;;AAVA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB;AAF4B;AAI9B;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB;AACA,6CAAsB,AAAK,IAAD;AAC1B,wDAAiC,AAAK,IAAD;;;IAC/B;qCAG+B;AACvC,uBAAA,AAAO,MAAD,cACA,KACA,AAgBE;;AAfA;AACA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR,uBAAG,AACE,AAOA,OARI,uBAEH,QAAC,SAAU,sBACT,MACA,cAAQ,cACR,AAAM,KAAD,qBAAQ,2DAGX;AARV;AASA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR;;;IACM;iCAG0C;AAAS,YAAA,AAe/D;;AAdA,YAAI,AAAK,IAAD,gBAAgB,MAAM,eAAG,uBAC/B,AAAK,AAAY,IAAb,iCAAoB,OACxB;AAF4B;AAI9B;AACA;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,sDAA+B,AAAK,IAAD;AACnC,sBAAI,AAAK,IAAD,cAAa,eAAG,uBACtB,KACA;AAFmB;AAIrB,qDAA8B,AAAK,IAAD;;;IAC5B;kCAEqD;AAC7D,uBAAA,AAAU,SAAD,cACH,KACA,AAmBE;;AAlBA;AACA;AACA;AACA,gCAAU,cAAF,aAAE,eAAF;AACR,uBAAG,AACE,AASA,UAVO,uBAEN,QAAC,YAAa,sBACZ,MACA,cAAQ,cACR,KACA,KACA,2BAAmB,QAAQ,gDAGzB;AAVV;AAWA,uBAAG,AAED,uBADA,cAAU,cAAF,aAAE,eAAF,YACH;AAFP;;;IAGM;6BAGkC;AAAS,YAAA,AAYvD;;AAXA;AACA;AACA;AACA,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB,eAAG,uBAC5D,KACA,2BAAsB,AAAK,IAAD;AAF+B;AAI3D,YAAI,AAAK,IAAD,mBAAmB,kBAAQ,AAAK,AAAe,IAAhB,+BAA4B,eAAG,uBACpE,KACA,yCAAoC,AAAK,IAAD;AAFyB;;;IAI7D;iCAGgD;AAAS,YAAA,AAU/D;;AATA;AACA;AACA;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB,eAAG,uBAC5D,KACA,2BAAsB,AAAK,IAAD;AAF+B;;;IAIrD;iCAGgD;AAAS,YAAA,AAU/D,uBATA,UACA,KACA,QACA,KACA,AAAK,AAAK,IAAN,0BAAa,OACjB,KACA,4BAAuB,AAAK,IAAD,cAC3B,2BAAsB,AAAK,IAAD,cAC1B,uBAAkB,AAAK,IAAD;IAChB;oCAGsD;AAAS,YAAA,AASrE,uBARA,UACA,KACA,aACA,KACA,AAAK,AAAK,IAAN,0BAAa,OACjB,KACA,2BAAsB,AAAK,IAAD,cAC1B,uBAAkB,AAAK,IAAD;IAChB;gCAG8C;AAAS,YAAA,AAW7D;;AAVA;AACA;AACA;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB,eAAG,uBAC5D,KACA,2BAAsB,AAAK,IAAD;AAF+B;AAI3D,6CAAsB,AAAK,IAAD;;;IACpB;+BAG4C;AAAS,YAAA,AAW3D;;AAVA;AACA;AACA;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB,eAAG,uBAC5D,KACA,2BAAsB,AAAK,IAAD;AAF+B;AAI3D,uDAAgC,AAAK,IAAD;;;IAC9B;sCAG0D;AAClE,YAAA,AAWE;;AAVA;AACA;AACA;AACA;AACU,kBAAV,AAAK,IAAD,0BAAa;AACjB,YAAI,AAAK,IAAD,eAAe,kBAAQ,AAAK,AAAW,IAAZ,2BAAwB,eAAG,uBAC5D,KACA,2BAAsB,AAAK,IAAD;AAF+B;AAI3D,wDAAiC,AAAK,IAAD;;;IAC/B;;;IApsBR,cAAQ;;EAqsBd;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;yCA1sBsB;AAAS,UAAA,AAAK,KAAD,qBAC7B;EACD;qCA0sBwB;AAC3B,YAAQ,CAAC;;;AAEL,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;AAIX,UAAO;EACT;2DAE4C;AAC1C,YAAQ,QAAQ;;;AAEZ,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;;AAEP,cAAO;;;AAIX,UAAO;EACT;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ACjuB0B;IAAgB;YAEnB;UAAW;AACxB,iBAAO,qBAAc,MAAM;AACjC,UAAI,AAAK,IAAD,IAAI,MAAM,MAAO;AAEzB,YAAiB,aAAV,AAAK,IAAD,OAAS,IAAI;IAC1B;;AAGa,MAAT,kBAAF,aAAE,mBAAF;IACF;mBAE6B,MAAc;;AACnC,iBAAO;AACb,UAAc,YAAV,AAAK,IAAD,OAAS,IAAI;AACT,QAAV;AACA,cAAO,KAAI;;AAMZ,MAHD,WAAM,4CACS,MAAb,YAAY,SAAZ,OAAgB,AAAgB,uBAAL,IAAI,UAC/B,AAAQ;IAEZ;2BAEqC;AAC7B,iBAAO;AACb,UAAc,YAAV,AAAK,IAAD,OAAS,IAAI;AACT,QAAV;AACA,cAAO,KAAI;;AAGb,YAAO;IACT;qBAE4B,OAAe;;AACnC,iBAAO;AACb,UAAc,YAAV,AAAK,IAAD,OAAmB,2BAAQ,AAAK,AAAM,IAAP,UAAU,KAAK;AAC1C,QAAV;AACA,cAAO,KAAI;;AAMZ,MAHD,WAAM,4CACS,MAAb,YAAY,SAAZ,OAAgB,AAA2B,gCAAP,KAAK,gBACzC,AAAQ;IAEZ;6BAEoC;AAC5B,iBAAO;AACb,UAAc,YAAV,AAAK,IAAD,OAAmB,2BAAQ,AAAK,AAAM,IAAP,UAAU,KAAK;AAC1C,QAAV;AACA,cAAO,KAAI;;AAGb,YAAO;IACT;;UAEiB;AACf,UAAc,AAAS,aAAnB,gCAAY,MAAM,kBAAI,gBAAS,MAAO;AAE1C,YAAO,AAAO,uBAAW,aAAV,gCAAY,MAAM;IACnC;oBAGY,MACQ,OACR,OACH;AAEyB,MAAhC,mBAAa,IAAI,EAAE,YAAY;AAEzB,kBAAW;AAEjB,aAAO,AAA4B,2BAAP,KAAK,KAAK;AAClB,QAAlB,AAAM,KAAD,OAAK,AAAK,KAAA;;AAGjB,YAAO,MAAK;IACd;yBAGY,MACQ,OACR,OACH;AAEP,oBAAI,YAAM,IAAI;AACZ,cAAO,qBACL,IAAI,EACJ,KAAK,EACL,KAAK,EACL,YAAY;;AAIhB,YAAU;IACZ;;AAEiC,oDACd,qCACD,gCACV,yBACU;IAEb;;AAGH,oBAAI,YAAgB;AAClB,gBAAQ,AAAQ;;;;;;AAKZ,kBAAO;;;;;;;;;;;AASP,kBAAO;;;;AAEP,kBAAO;;;YAEN,eAAI,YAAgB;AACzB,cAAO;YACF,eAAI,YAAgB,uCAAW,YAAgB;AACpD,cAAO;;AAMR,MAHD,WAAM,2CACJ,AAA4C,uCAAhB,AAAQ,uBAAM,KAC1C,AAAQ;IAEZ;;AAGE,oBAAI,YAAgB;AAClB,gBAAQ,AAAQ;;;;;AAIZ,kBAAO;;;;AAEP,kBAAO;;;YAEN,eAAI,YAAgB;AACzB,cAAO;;AAMR,MAHD,WAAM,2CACJ,AAAkD,6CAAhB,AAAQ,uBAAM,KAChD,AAAQ;IAEZ;;AAG4B,MAA1B,qBAAe;AAET,iBAAO;AAEO,MAApB,qBAAe;AAEf,YAAO,2CACC,IAAI,iBACK,mCACT,uCAEM,wCACE;IAElB;;AAGE,oBAAI,YAAgB;AAClB,cAAO,4CACe,uCACN;;AAIZ,0BAAgB;AACb;AACT,oBAAI,YAAgB,0BAAO,AAAmB,OAAZ;AAElC,YAAO,4CACC,aAAa,QACb,IAAI,uBACW,+CACT,wCACE;IAElB;;AAE4D,+DAC5C,mCACV,iCACU;IACX;;AAGG,qBAAW;AAEsD,MAAvE,mBAAuB,wBAAO;AAExB,iBAAO;AAEH;AACV,UAAI,2BAA+B,4BAAW;AACH,QAAzC,eAAe,4BAAqB;;AAGtC,YAAO,+CACK,QAAQ,QACZ,IAAI,gBACI,qCACL,YAAY,gBAET,iCAA0B;IAE1C;;AAGyE,MAAvE,mBAAuB,yBAAQ;AAE/B,YAAO,iCACC,iBAAW;IAErB;;AAGE,UAAI,2BAA+B,8BAAa;AACxC,mBAAO;AACmC,QAAhD,mBAAuB,2BAAU;AAE3B,wBAAY,AAAqC,2BAAN,0BAAS;AAE1D,cAAO,sCACM,SAAS,QACd,IAAI;;AAGZ,cAAO;;IAEX;;AAGQ,iBAAO,iBAAW;AAClB,sBAAY,AAAqC,2BAAN,0BAAS;AAE1D,YAAO,uCACM,SAAS,QACd,IAAI;IAEd;;AAE4C,+CAA2B;IAAK;;AAGxE,+CAA2B;IAAM;;UAEG;AAChC,iBAAO,iBAAW;AAE8C,MAAtE,mBAAuB,wBAAO;AAE9B,YAAO,oCAAsB,IAAI,SAAS,4BAAqB,OAAO;IACxE;;AAEgC,yCAAqB;IAAK;;AAEvB,yCAAqB;IAAM;;UAElC;AACpB,kBAAQ;AACd,cAAQ,AAAM,KAAD;;;AAET,gBAAO,4BAAoB,OAAO;;;;AAElC,gBAAO,8BAAsB,OAAO;;;;AAE1B,UAAV;AAEA,gBAAO,kCACE,AAAM,KAAD;;;;AAGJ,UAAV;AAEA,gBAAO,oCACE,AAAM,KAAD;;;;;AAId,gBAAO;;;;AAEP,cAAI,AAAM,AAAM,KAAP,WAAU,UAAU,AAAM,AAAM,KAAP,WAAU;AAChC,YAAV;AAEA,kBAAO,sCACE,AAAM,AAAM,KAAP,WAAU;gBAEnB,KAAI,AAAM,AAAM,KAAP,WAAU;AACd,YAAV;AAEA,kBAAO;;AAGT,gBAAO,kCACC;;;;AAGR,yBAAK,OAAO;AACV,kBAAO;;AAMR,UAHD,WAAM,2CACJ,6CACA,AAAM,KAAD;;;;AAMN,UAHD,WAAM,2CACJ,+CACA,AAAM,KAAD;;;IAGb;;AAGQ,uBAAa;AACT,MAAV;AAEA,YAAO,qCACE,AAAW,UAAD,iBACQ,YAAhB,AAAW,UAAD,OAAmB;IAE1C;;UAE+B;AAAa,gDAC9B,gCACI,qCACV,OAAO,cAAG,oCAAmB,4BACnB;IAEb;;UAE8B;AAAa,kDAClC,sCACI,mCACV,OAAO,cAAG,0CAAyB,kCACzB;IAEb;;UAEsC;AACnC,uBAA4B;AAClC,uBAAO,YAAgB;AAC4B,QAAjD,AAAW,UAAD,OAAK,gCAAyB,OAAO;;AAGjD,YAAO,WAAU;IACnB;;UAEoC;AACqC,MAAvE,mBAAuB,qBAAI;AAE3B,YAAO,kCACC,iBAAW,yCACN,gCAAyB,OAAO;IAE/C;;UAEyC;AAAa,qDACtC,mCACV,OAAO,cAAG,uCAAsB,+BACtB;IACX;;AAEiC,4CAAwB;IAAK;;AAE1B,4CAAwB;IAAM;;UAErC;AAC1B,iBAAO,iBAAW;AAEgD,MAAxE,mBAAuB,wBAAO;AAE9B,YAAO,iCACC,IAAI,SACH,4BAAqB,OAAO;IAEvC;;AAGQ,kBAAQ,mBAAuB,wBAAM;AAE3C,cAAQ,AAAM,KAAD;;;AAET,gBAAqB;;;;AAErB,gBAAqB;;;;AAErB,gBAAqB;;;AAMxB,MAHD,WAAM,2CACJ,AAAoC,iCAAd,AAAM,KAAD,UAAO,KAClC,AAAM,KAAD;IAET;;AAEyC,uDACvB,oCACA,mCACV,wBACU,yBACV;IAEH;;AAGH,oBAAI,YAAgB;AAClB,cAAO;;AAGT,YAAO;IACT;;AAGyE,MAAvE,mBAAuB,yBAAQ;AAEzB,6BAAmB,AAA6B,6BAAN,SAAS;AAEzD,WAAK,gBAAgB,cAAI,YAAgB;AACvC,cAAO,uCACC,wCACM,iCAA0B;;AAI1C,YAAO,gDAED,gBAAgB,GAAG,mCAAsB,4BAAqB,kBACtD,iCAA0B,uBACxB;IAElB;;AAGQ,kBAAQ;AACd,UAAI,AAAM,AAAM,KAAP,WAAU;AAIhB,QAHD,WAAM,2CACJ,8BACA,AAAM,KAAD;;AAIT,YAAO,kBAAW;IACpB;;AAGQ,wBAAc,iBAAW;AAEtB;AACA;AAET,UAAI,2BAA+B,2BAAU;AACxB,QAAnB,QAAQ,WAAW;AACuB,QAA1C,OAAO,iBAAW;;AAEA,QAAlB,OAAO,WAAW;;AAGd,uBAAY,gCAAyB;AACrC,uBAAa,iCAA0B;AAE5B;AACjB,oBAAI,YAAgB;AACiB,QAAnC,eAAe;;AAGjB,YAAO,+BACE,KAAK,QACN,IAAI,aACC,wBACC,UAAU,gBACR,YAAY;IAE9B;iBAE4B;;AACpB,kBAAQ,mBACF,yBACG,MAAb,YAAY,SAAZ,OAAgB;AAGlB,YAAO,8BACE,AAAM,KAAD,cACN,AAAM,KAAD;IAEf;;AAGQ,0BACuB,UAAxB,YAAgB,uCAAW,YAAgB,iCAAgB,IAAI;AAE9D,kBAAQ,qBAAc,aAAa;AAEzC,oBAAI,YAAgB,iCAAc,aAAa;AAC7C,gBAAQ,AAAM,KAAD;;;AAET,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAOZ,MAHD,WAAM,2CACJ,AAAsD,mDAAd,AAAM,KAAD,UAAO,KACpD,AAAM,KAAD;IAET;;AAG0B,MAAxB,qBAAe;AAET,kBAAQ;AAEd,UAAI,YAAgB,2BAAS;AAC3B,gBAAQ,AAAM,KAAD;;;AAET,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAEP,kBAAO;;;;AAOZ,MAHD,WAAM,2CACJ,AAAqD,kDAAd,AAAM,KAAD,UAAO,KACnD,AAAM,KAAD;IAET;;AAG0B,MAAxB,qBAAe;AAEf,YAAO,+CACO,iCACD,wBAEK,kDACJ,mCACV,sCACU,yBACV;IAGN;;AAGQ,sBAAY;AACiD,MAAnE,mBAAuB,wBAAO;AACxB,iBAAO;AAEb,YAAO,qDACM,SAAS,QACd,IAAI;IAEd;;AAGQ,wBAAc;AACI,MAAxB,qBAAe;AACT,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AAE7C,YAAO,oDACQ,WAAW,QAClB,IAAI,cACE,UAAU;IAE1B;;AAGE,oBAAI,YAAgB,uCAAW,YAAgB;AAC7C,cAAO;;AAGT,YAAO;IACT;;AAGQ,kBAAuB;AAE7B,UAAI,6BAAuB,iBAAiB;AACP,QAAnC,2BAA+B;AAC/B;AAC8B,UAA5B,AAAM,KAAD,OAAK;iBACH,2BAA+B,yBAAQ;;AAGlD,YAAO,MAAK;IACd;;AAGQ,wBAAc;AACE,MAAtB,qBAAe;AACT,iBAAO,iBAAW;AAClB,uBAAa;AACb,uBAAa,iCAA0B;AACvC,mBAAS;AAEf,YAAO,oDACQ,WAAW,QAClB,IAAI,cACE,UAAU,cACV,UAAU,UACd,MAAM;IAElB;;AAEsD,4DACtC,mCACV,8BACU;IACX;;AAGG,wBAAc;AACd,iBAAO,iBAAW;AAClB,iBAAO;AACuD,MAApE,mBAAuB,wBAAO;AACxB,iBAAO;AACP,uBAAa,iCAA0B;AAE7C,YAAO,+CACQ,WAAW,QAClB,IAAI,QACJ,IAAI,QACJ,IAAI,cACE,UAAU;IAE1B;;AAGI,iEACY,mCACV,mCACU;IACX;;AAGG,wBAAc;AACd,iBAAO,iBAAW;AACkD,MAA1E,mBAAuB,wBAAO;AACxB,iBAAO;AACH;AACV,UAAI,2BAA+B,4BAAW;AACX,QAAjC,eAAe;;AAEX,uBAAa,iCAA0B;AAE7C,YAAO,oDACQ,WAAW,QAClB,IAAI,QACJ,IAAI,gBACI,YAAY,cACd,UAAU;IAE1B;;AAGQ,wBAAc;AACO,MAA3B,qBAAe;AACT,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AACvC,mBAAS;AAEf,YAAO,uDACQ,WAAW,QAClB,IAAI,cACE,UAAU,UACd,MAAM;IAElB;;AAGQ,wBAAc;AACG,MAAvB,qBAAe;AACT,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AACvC,kBAAQ;AAEd,YAAO,mDACQ,WAAW,QAClB,IAAI,cACE,UAAU,SACf,KAAK;IAEhB;;AAGQ,kBAAuB;AAE7B,UAAI,2BAA+B,4BAAW;AACR,QAApC,2BAA+B;AAC/B;AAC8B,UAA5B,AAAM,KAAD,OAAK;iBACH,2BAA+B,0BAAS;;AAGnD,YAAO,MAAK;IACd;;AAGQ,wBAAc;AACE,MAAtB,qBAAe;AACT,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AACvC,mBAAS;AAEf,YAAO,kDACQ,WAAW,QAClB,IAAI,cACE,UAAU,UACd,MAAM;IAElB;;AAE8D,gEAC9C,mCACV,kCACU;IACX;;AAGG,wBAAc;AACd,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AAE7C,YAAO,mDACQ,WAAW,QAClB,IAAI,cACE,UAAU;IAE1B;;AAGQ,wBAAc;AACG,MAAvB,qBAAe;AACT,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AACvC,mBAAS;AAEf,YAAO,yDACQ,WAAW,QAClB,IAAI,cACE,UAAU,UACd,MAAM;IAElB;;AAGI,iEACY,mCACV,mCACU;IACX;;AAGG,wBAAc;AACO,MAA3B,qBAAe;AACoD,MAAnE,mBAAuB,qBAAI;AACrB,iBAAO,iBAAW;AAClB,iBAAO;AACP,uBAAa,AAAqC,6BAAd,iBAAiB;AACvC,MAApB,qBAAe;AACT,sBAAY;AAElB,YAAO,mDACQ,WAAW,QAClB,IAAI,QACJ,IAAI,cACE,UAAU,aACX,SAAS;IAExB;;AAGsC,MAApC,2BAA+B;AACzB,sBAA+B;AACrC;AAC0C,QAAxC,AAAU,SAAD,OAAK;eACP,2BAA+B,0BAAS;AAEjD,YAAO,UAAS;IAClB;;AAGQ,iBAAO,iBAAW;AAExB,cAAQ,AAAK,IAAD;;;AAER,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;;AAEzB,gBAAyB;;;AAM5B,MAHD,WAAM,2CACJ,AAA4C,0CAAb,AAAK,IAAD,UAAO,KAC1C,AAAK,IAAD;IAER;;AAGQ,uBAAa,qBAAc,CAAC;AAEV,MAAxB,qBAAe;AAET,uBAAa,iCAA0B;AACvC,2BAAiB,uDACX,mCACV,sCACU;AAGZ,oBAAI,AAAW,UAAD,yBAAY,AAAe,cAAD;AAIrC,QAHD,WAAM,2CACJ,2EACA,AAAW,AAAK,UAAN,aAAa,AAAQ;;AAInC,YAAO,8CACO,UAAU,kBACN,cAAc;IAElC;;AAGQ,uBAAa,qBAAc,CAAC;AAEV,MAAxB,qBAAe;AAET,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AAE7C,oBAAI,AAAW,UAAD;AAIX,QAHD,WAAM,2CACJ,wDACA,AAAW,AAAK,UAAN,aAAa,AAAQ;;AAInC,YAAO,4CACC,IAAI,cACE,UAAU;IAE1B;;AAGQ,uBAAa,qBAAc,CAAC;AAEZ,MAAtB,qBAAe;AAET,iBAAO,iBAAW;AAClB,uBAAa;AACb,uBAAa,iCAA0B;AACvC,mBAAS;AAEf,oBAAI,AAAW,UAAD,yBAAY,AAAW,UAAD,yBAAY,AAAO,MAAD;AAInD,QAHD,WAAM,2CACJ,0GACA,AAAW,AAAK,UAAN,aAAa,AAAQ;;AAInC,YAAO,4CACC,IAAI,cACE,UAAU,cACV,UAAU,UACd,MAAM;IAElB;;AAGQ,uBAAa,qBAAc,CAAC;AAEP,MAA3B,qBAAe;AAET,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AACvC,mBAAS;AAEf,oBAAI,AAAW,UAAD,yBAAY,AAAO,MAAD;AAI7B,QAHD,WAAM,2CACJ,wEACA,AAAW,AAAK,UAAN,aAAa,AAAQ;;AAInC,YAAO,+CACC,IAAI,cACE,UAAU,UACd,MAAM;IAElB;;AAGQ,uBAAa,qBAAc,CAAC;AAEX,MAAvB,qBAAe;AAET,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AACvC,kBAAQ;AAEd,oBAAI,AAAW,UAAD,yBAAY,AAAM,KAAD;AAI5B,QAHD,WAAM,2CACJ,mEACA,AAAW,AAAK,UAAN,aAAa,AAAQ;;AAInC,YAAO,2CACC,IAAI,cACE,UAAU,SACf,KAAK;IAEhB;;AAGQ,uBAAa,qBAAc,CAAC;AAEZ,MAAtB,qBAAe;AAET,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AACvC,mBAAS;AAEf,oBAAI,AAAW,UAAD,yBAAY,AAAO,MAAD;AAI7B,QAHD,WAAM,2CACJ,mEACA,AAAW,AAAK,UAAN,aAAa,AAAQ;;AAInC,YAAO,0CACC,IAAI,cACE,UAAU,UACd,MAAM;IAElB;;AAGQ,uBAAa,qBAAc,CAAC;AAEX,MAAvB,qBAAe;AAET,iBAAO,iBAAW;AAClB,uBAAa,iCAA0B;AACvC,mBAAS;AAEf,oBAAI,AAAW,UAAD,yBAAY,AAAO,MAAD;AAI7B,QAHD,WAAM,2CACJ,yGACA,AAAW,AAAK,UAAN,aAAa,AAAQ;;AAInC,YAAO,iDACC,IAAI,cACE,UAAU,UACd,MAAM;IAElB;;kCA3gCa;IAFT,kBAAY;IAEH;IAAmB,gBAAE,AAAQ,OAAD;;EAAO;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iCA5BrC;AAEL,gBAAQ;AACR,iBAAS,AAAM,KAAD,UAAU,MAAM;AAC9B,iBAAS,wBAAQ,MAAM;AAE7B,UAAO,AAAO,OAAD;EACf;6CAMS;QACC;AAEN,yBACa,+BACT,MAAM,QACD,GAAG;EAEX;;;;;;;;;;;;;;;;;;;ICNL;;0CAvBK;;;;EAuBL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;UAU0B;AACpB,YAAM,AAA+B,iBAArC,KAAK,KAAwB,YAAX,AAAM,KAAD,OAAS,cAAmB,YAAX,AAAM,KAAD,OAAS;IAAI;;;;EAChE;;;;;;;;;;;;;;IAIkB;;;;;;IAED;;;;;;;AAOK,YAAA,AAAK;IAAI;;AAG3B,cAAQ;;;AAEJ,gBAAO,AAAM,AAA+B,AAsB1C,yBAtBqB,GAAG,AAAM,AAAO,qBAAE,sBACvC,gBAAO,uBACP,QAAC;AACC,oBAAQ,AAAM,AAAS,KAAV,OAAO,gBAAc;;;;;AAI9B,sBAAO,AAAM,AAAQ,MAAT,OAAO,UAAG;;;;AAEtB,sBAAO;;;;AAEP,sBAAO;;;;AAEP,sBAAO;;;;AAEP,sBAAO;;;;AAEP,sBAAO;;;;AAEP,sBAAO,AAAM,MAAD,OAAO;;;kDAIzB,gBAAO,wBACP,QAAC,SAAiB,yBAChB,mBACE,AAAM,AAAS,KAAV,OAAO,gBAAc,IAC1B,AAAM,AAAS,KAAV,OAAO,gBAAc,IAC1B,AAAM,AAAS,KAAV,OAAO,gBAAc,IAC1B,AAAM,AAAS,KAAV,OAAO,gBAAc;;;;AAKhC,gBAAO,+BACL,AAAM,AAA+B,wBAArB,GAAG,AAAM,AAAO,qBAAE,gBAAc,YAAS;;;;AAG3D,gBAAO,AAAK;;;IAElB;;AAGoB;IAAW;;AAGV,YAAA,AAAiC,gBAA7B,aAAK,kBAAK,aAAI,iBAAK,cAAM;IAAI;;;QAvD/C;QACA;IADA;IACA;;EACL;;;;;;;;;;;;;;;;;;;;aAyD8B;UAAW;AACnC,oBAAU,wBAAS,GAAG;AACtB,mBAAgB;AAEhB;AACN;AAC6B,QAA3B,QAAQ,AAAQ,OAAD;AACf,sBAAI,YAAY,kBAAI,AAAM,KAAD,OAAmB;AACzB,UAAjB,AAAO,MAAD,OAAK,KAAK;;4BAEX,AAAM,KAAD,OAAmB;AAEjC,YAAO,OAAM;IACf;;;;EACF;;;;;;;;;;UAWQ;AAEJ,UAAa,AAAS,aAAlB,8BAAW,MAAM,kBAAI,AAAI;AAC3B,cAAO;;AAGH,kBAAiB,aAAT,8BAAW,MAAM;AAE/B,YAAO,AAAI,AAA0B,kBAAlB,KAAK,EAAE,AAAM,KAAD,GAAG,gBAAc;IAClD;gBAGY;UACN;UACC;AAEC,kBAAQ,6BACN,IAAI,QACJ,AAAI,cAAK,eAAmB,aAAT,8BAAW,MAAM;AAE1B,MAAlB,gBAAS,aAAT,8BAAY,MAAM;AAClB,YAAO,MAAK;IACd;;AAGE,UAAI,AAAS,kBAAG,CAAC;AACL,QAAR,gBAAF,aAAE,iBAAF;AACA,cAAO,8BACW,4BACV,AAAI,cAAK,GAAG;;AAGH,MAAnB;AAEA,UAAa,aAAT,+BAAY,AAAI;AAClB,cAAO,8BACW,4BACV,AAAI,cAAK,AAAI;;AAIjB,iBAAO;AAEb,UAAU,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI,MACvB,AAAK,IAAD,KAAI,MACF,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI;AACzB,cAAO;;AAGT,UAAU,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI,MAAO,AAAK,IAAD,KAAI;AACxC,cAAO;;AAGT,cAAQ,IAAI;;;AAER,gBAAO,kBAAsB;;;;AAE7B,gBAAO;;;;AAEP,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,cAAI,AAAgB,mBAAH,QAAM,MAAM,AAAgB,mBAAH,QAAM;AAC9C,kBAAO,kBACK,kCACF;;AAGZ;;;;AAEA,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,gBAAO,kBAAsB;;;;AAE7B,cAAI,AAAgB,mBAAH,QAAM,MAAM,AAAgB,mBAAH,QAAM;AAC9C,kBAAO;;AAGT,gBAAO;;;AAMV,MAHD,WAAM,2CACJ,gBACA,AAAI,cAAK,eAAU;IAEvB;;AAGY,MAAR,gBAAF,aAAE,iBAAF;AACI,mBAAS;AACT;AAEJ;AAC+B,QAA7B,OAAO,mBAAe,SAAF,AAAE,MAAM,GAAR;eACb,IAAI,IAAI,SAAc,aAAL,IAAI,IAAG,MAAU,AAAK,IAAD,KAAI;AAEnD,YAAO,kBACK,mCACF,MAAM,SACP;IAEX;;AAGM,mBAAS;AACT,iBAAO;AACP,oBAAU;AAEd,UAAI,AAAK,IAAD,KAAI;AAEmB,QAA7B,OAAO,mBAAe,SAAF,aAAE,MAAM,IAAR;;AAGtB,UAAI,AAAK,IAAD,KAAI;AAEmB,QAA7B,OAAO,mBAAe,SAAF,aAAE,MAAM,IAAR;AAEpB,YAAI,IAAI,IAAI,QAAa,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI;AAIvC,UAHD,WAAM,2CACJ,gBACA,AAAI,cAAK,eAAU;;;AAIK,QAA5B,SAAS,kBAAY,MAAM;AACA,QAA3B,OAAO,mBAAa,MAAM;;AAG5B,UAAI,AAAK,IAAD,KAAI;AACI,QAAd,UAAU;AAEmB,QAA7B,OAAO,mBAAe,SAAF,aAAE,MAAM,IAAR;AACQ,QAA5B,SAAS,kBAAY,MAAM;AACA,QAA3B,OAAO,mBAAa,MAAM;;AAG5B,UAAI,AAAK,IAAD,KAAI,MAAM,AAAK,IAAD,KAAI;AACV,QAAd,UAAU;AAEmB,QAA7B,OAAO,mBAAe,SAAF,aAAE,MAAM,IAAR;AACpB,YAAI,AAAK,IAAD,KAAI,MAAM,AAAK,IAAD,KAAI;AACK,UAA7B,OAAO,mBAAe,SAAF,aAAE,MAAM,IAAR;;AAEM,QAA5B,SAAS,kBAAY,MAAM;;AAG7B,YAAO,kBACL,OAAO,GAAa,yBAAkB,+BAC9B,MAAM,SACP;IAEX;kBAEoB;AACd,wBAAc,MAAM;AACpB,iBAAO,mBAAa,WAAW;AAEnC,UAAS,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI;AACxB;AACoC,UAAlC,OAAO,mBAAe,cAAF,aAAE,WAAW,IAAb;iBACb,IAAI,IAAI,QAAa,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI;AAE/C,cAAO,YAAW;;AAMnB,MAHD,WAAM,2CACJ,oBACA,AAAI,cAAK,eAAU;IAEvB;;AAGM,mBAAS;AACT,iBAAO;AAEX,aAAgB,aAAT,iBAAW,MAAM,KAAI,AAAI,mBAC5B,IAAI,IAAI,SACP,AAAK,IAAD,KAAI,MACC,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI,MACjB,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI,MACjB,aAAL,IAAI,KAAI,MAAW,aAAL,IAAI,KAAI;AACA,QAA7B,OAAO,mBAAe,SAAF,AAAE,MAAM,GAAR;;AAGtB,YAAO,kBACK,iCACF,MAAM,SACP;IAEX;;AAGQ,kBAAQ;AACJ,MAAR,gBAAF,aAAE,iBAAF;AACI,iBAAO;AAEX,aAAgB,aAAT,8BAAW,AAAI,oBAClB,IAAI,IAAI,QACR,IAAI,KAAI,MACR,IAAI,KAAI;AACV,YAAI,AAAK,IAAD,KAAI;AAEV,gBAAO,8BACW,+BACV,AAAI,cAAK,KAAK,EAAI,gBAAF,aAAE,iBAAF;;AAI1B,YAAI,AAAK,IAAD,IAAI,QAAc,aAAL,IAAI,IAAG,MAAU,IAAI,KAAI;AAO3C,UAND,WAAM,2CACJ,4CACA,AAAI,cACF,eACA;;AAKI,QAAR,gBAAF,aAAE,iBAAF;AACA,YAAI,AAAK,IAAD,KAAI;AAEG,UAAb,OAAO;AAEP,kBAAQ,IAAI;;;;;;;;;;AASR;;;;AAEM,8BAEqB,UAFT,aAAM,mBAAa,mBACjC,aAAM,mBAAa,mBACnB,aAAM,mBAAa,mBACnB,aAAM,mBAAa;AAEvB,mBAAK,SAAS;AAOX,gBAND,WAAM,2CACJ,+BACA,AAAI,cACO,aAAT,iBAAW,GACF,aAAT,iBAAW;;AAKJ,cAAb,gBAAS,aAAT,iBAAY;AACZ;;;;AAKC,cAHD,WAAM,2CACJ,4BACA,AAAI,cAAK,eAAU;;;AAIf,UAAR,gBAAF,aAAE,iBAAF;;AAGW,QAAb,OAAO;;AAMR,MAHD,WAAM,2CACJ,4CACA,AAAI,cAAK,eAAU;IAEvB;;AAGQ,kBAAQ;AACD,MAAb,gBAAS,aAAT,iBAAY;AACR,iBAAO;AAEX,aAAgB,aAAT,8BAAW,AAAI,oBAAU,IAAI,IAAI;AACtC,YAAI,AAAK,IAAD,KAAI,MAAM,AAAgB,mBAAH,QAAM,MAAM,AAAgB,mBAAH,QAAM;AAC/C,UAAb,gBAAS,aAAT,iBAAY;AAEZ,gBAAO,8BACW,oCACV,AAAI,cAAK,KAAK,EAAE;;AAI1B,YAAI,AAAK,IAAD,IAAI,QACF,aAAL,IAAI,IAAG,MACJ,IAAI,KAAI,KACR,IAAI,KAAI,MACR,IAAI,KAAI;AAIb,UAHD,WAAM,2CACJ,4CACA,AAAI,cAAK,eAAU;;AAIvB,YAAI,AAAK,IAAD,KAAI;AAEA,UAAR,gBAAF,aAAE,iBAAF;AACM,UAAJ,YAAF,aAAE,aAAF;AACoB,UAApB,iBAAY;cACP,KAAI,AAAK,IAAD,KAAI;AACjB,cAAI,AAAgB,mBAAH,QAAM;AACR,YAAb,gBAAS,aAAT,iBAAY;;AAEF,YAAR,gBAAF,aAAE,iBAAF;;AAEI,UAAJ,YAAF,aAAE,aAAF;AACoB,UAApB,iBAAY;cACP,KAAI,AAAK,IAAD,KAAI,MACf,AAAgB,mBAAH,QAAM,MACnB,AAAgB,mBAAH,QAAM,MACnB,AAAgB,mBAAH,QAAM;AACR,UAAb,gBAAS,aAAT,iBAAY;;AAEF,UAAR,gBAAF,aAAE,iBAAF;;AAGW,QAAb,OAAO;;AAMR,MAHD,WAAM,2CACJ,4CACA,AAAI,cAAK,eAAU;IAEvB;;AAGE,aAAgB,aAAT,8BAAW,AAAI;AACd,mBAAO;AACb,YAAI,AAAK,IAAD,KAAI,KACJ,AAAK,IAAD,KAAI,MACR,AAAK,IAAD,KAAI,MACR,AAAK,IAAD,KAAI;AAEJ,UAAR,gBAAF,aAAE,iBAAF;cACK,KAAI,AAAK,IAAD,KAAI;AACP,UAAR,gBAAF,aAAE,iBAAF;AACM,UAAJ,YAAF,aAAE,aAAF;AACoB,UAApB,iBAAY;cACP,KAAI,AAAK,IAAD,KAAI;AACjB,cAAI,AAAgB,mBAAH,QAAM;AACR,YAAb,gBAAS,aAAT,iBAAY;;AAEF,YAAR,gBAAF,aAAE,iBAAF;;AAEI,UAAJ,YAAF,aAAE,aAAF;AACoB,UAApB,iBAAY;;AAEZ;;;IAGN;;kCApXc;IAJV,gBAAW,CAAC;IACZ,YAAO;IACP,iBAAY;IAEF;;EAAI;;;;;;;;;;;;;;;;;;;;;;;;gCAuXL;AACX,UAAG,AACsB,cADxB,CAAC,KAAI,MAAU,aAAF,CAAC,KAAI,MAChB,aAAF,CAAC,KAAI,MAAU,aAAF,CAAC,KAAI,MAChB,aAAF,CAAC,KAAI,MAAU,aAAF,CAAC,KAAI;EAAK;4CAER,GAAO,GAAO,GAAO;AACrC,UAAwD,EAA5C,AAAM,AAAmB,aAArC,gBAAS,CAAC,MAAK,KAAiB,aAAZ,gBAAS,CAAC,MAAK,UAAgB,aAAZ,gBAAS,CAAC,MAAK,uBAAI,gBAAS,CAAC;EAAC;sCAExD;AACf,QAAM,aAAF,CAAC,KAAI,MAAQ,aAAF,CAAC,KAAI;AAClB,YAAS,cAAF,CAAC,IAAG;;AAGb,QAAM,aAAF,CAAC,KAAI,MAAQ,aAAF,CAAC,KAAI;AAClB,YAAS,cAAF,CAAC,IAAG;;AAGb,QAAM,aAAF,CAAC,KAAI,MAAQ,aAAF,CAAC,KAAI;AAClB,YAAS,cAAF,CAAC,IAAG;;AAGb,UAAO,EAAC;EACV;kEAEqC;AAC/B,gBAAQ,AAAM,KAAD,SAAO,gBAAO;AAE3B;AACJ,aAAS,IAAI,GAAG,AAAE,CAAD,gBAAG,AAAM,KAAD,YAAS,IAAA,AAAC,CAAA;AAC3B,iBAAO,AAAK,KAAA,QAAC,CAAC;AACd,mBAAS,yBAAkB,IAAI;AAErC,UAAW,aAAP,MAAM,IAAG,AAAK,IAAD,YACZ,AAAa,YAAD,IAAI,QAAe,aAAP,MAAM,iBAAG,YAAY;AAC3B,QAArB,eAAe,MAAM;AACrB,YAAI,AAAa,YAAD,KAAI;AAClB;;;;AAKN,QAAI,YAAY,IAAI,QAAQ,YAAY,KAAI;AAO/B,MANX,QAAQ,AAAM,AAMX,KANU,oBAAK,QAAC;AACjB,YAAI,AAAK,AAAO,IAAR,uBAAU,YAAY;AAC5B,gBAAO;;AAEP,gBAAO,AAAK,KAAD,aAAW,YAAY;;;;AAKhB,IAAxB,AAAM,KAAD;AAEL,qBAAO,AAAM,KAAD,4BAAe,eAAQ,AAAM,KAAD;AACrB,MAAjB,AAAM,KAAD,YAAU;;AAGjB,qBAAO,AAAM,KAAD,4BAAe,eAAQ,AAAM,KAAD;AACpB,MAAlB,AAAM,KAAD;;AAGP,UAAO,AAAM,MAAD,QAAM;EACpB;wDAE6B;AACvB,YAAI;AACR,WAAO,AAAE,CAAD,GAAG,AAAI,GAAD,YAAY,AAAG,AAAI,GAAJ,QAAC,CAAC,MAAK,OAAO,AAAG,AAAI,GAAJ,QAAC,CAAC,MAAK;AAChD,MAAH,IAAA,AAAC,CAAA;;AAEH,UAAO,EAAC;EACV;oCAEoB;AAAQ,UAAA,AAAuB,0BAAL,GAAG,MAAK,AAAI,GAAD;EAAO","file":"language.ddc.js"}');
  // Exports:
  return {
    language: language,
    src__language__printer: printer,
    src__language__parser: parser$,
    src__language__lexer: lexer$
  };
});

//# sourceMappingURL=language.ddc.js.map
