define(['dart_sdk', 'packages/collection/src/comparators', 'packages/source_span/source_span'], function(dart_sdk, packages__collection__src__comparators, packages__source_span__source_span) {
  'use strict';
  const core = dart_sdk.core;
  const _interceptors = dart_sdk._interceptors;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const equality = packages__collection__src__comparators.src__equality;
  const file = packages__source_span__source_span.src__file;
  const visitor = Object.create(dart.library);
  const ast = Object.create(dart.library);
  const transformer = Object.create(dart.library);
  const $add = dartx.add;
  const $expand = dartx.expand;
  const $forEach = dartx.forEach;
  const $runtimeType = dartx.runtimeType;
  const $_equals = dartx._equals;
  const $map = dartx.map;
  const $cast = dartx.cast;
  const $toList = dartx.toList;
  const $fold = dartx.fold;
  let ListOfNull = () => (ListOfNull = dart.constFn(core.List$(core.Null)))();
  let SimpleVisitorOfListOfNull = () => (SimpleVisitorOfListOfNull = dart.constFn(visitor.SimpleVisitor$(ListOfNull())))();
  let ListOfNode = () => (ListOfNode = dart.constFn(core.List$(ast.Node)))();
  let ObjectToNull = () => (ObjectToNull = dart.constFn(dart.fnType(core.Null, [core.Object])))();
  let JSArrayOfObject = () => (JSArrayOfObject = dart.constFn(_interceptors.JSArray$(core.Object)))();
  let NodeTovoid = () => (NodeTovoid = dart.constFn(dart.fnType(dart.void, [ast.Node])))();
  let DocumentNodeAndTransformingVisitorToDocumentNode = () => (DocumentNodeAndTransformingVisitorToDocumentNode = dart.constFn(dart.fnType(ast.DocumentNode, [ast.DocumentNode, transformer.TransformingVisitor])))();
  let ArgumentNodeAndTransformingVisitorToArgumentNode = () => (ArgumentNodeAndTransformingVisitorToArgumentNode = dart.constFn(dart.fnType(ast.ArgumentNode, [ast.ArgumentNode, transformer.TransformingVisitor])))();
  let BooleanValueNodeAndTransformingVisitorToBooleanValueNode = () => (BooleanValueNodeAndTransformingVisitorToBooleanValueNode = dart.constFn(dart.fnType(ast.BooleanValueNode, [ast.BooleanValueNode, transformer.TransformingVisitor])))();
  let DefaultValueNodeAndTransformingVisitorToDefaultValueNode = () => (DefaultValueNodeAndTransformingVisitorToDefaultValueNode = dart.constFn(dart.fnType(ast.DefaultValueNode, [ast.DefaultValueNode, transformer.TransformingVisitor])))();
  let DirectiveDefinitionNodeAndTransformingVisitorToDirectiveDefinitionNode = () => (DirectiveDefinitionNodeAndTransformingVisitorToDirectiveDefinitionNode = dart.constFn(dart.fnType(ast.DirectiveDefinitionNode, [ast.DirectiveDefinitionNode, transformer.TransformingVisitor])))();
  let DirectiveNodeAndTransformingVisitorToDirectiveNode = () => (DirectiveNodeAndTransformingVisitorToDirectiveNode = dart.constFn(dart.fnType(ast.DirectiveNode, [ast.DirectiveNode, transformer.TransformingVisitor])))();
  let EnumTypeDefinitionNodeAndTransformingVisitorToEnumTypeDefinitionNode = () => (EnumTypeDefinitionNodeAndTransformingVisitorToEnumTypeDefinitionNode = dart.constFn(dart.fnType(ast.EnumTypeDefinitionNode, [ast.EnumTypeDefinitionNode, transformer.TransformingVisitor])))();
  let EnumTypeExtensionNodeAndTransformingVisitorToEnumTypeExtensionNode = () => (EnumTypeExtensionNodeAndTransformingVisitorToEnumTypeExtensionNode = dart.constFn(dart.fnType(ast.EnumTypeExtensionNode, [ast.EnumTypeExtensionNode, transformer.TransformingVisitor])))();
  let EnumValueDefinitionNodeAndTransformingVisitorToEnumValueDefinitionNode = () => (EnumValueDefinitionNodeAndTransformingVisitorToEnumValueDefinitionNode = dart.constFn(dart.fnType(ast.EnumValueDefinitionNode, [ast.EnumValueDefinitionNode, transformer.TransformingVisitor])))();
  let EnumValueNodeAndTransformingVisitorToEnumValueNode = () => (EnumValueNodeAndTransformingVisitorToEnumValueNode = dart.constFn(dart.fnType(ast.EnumValueNode, [ast.EnumValueNode, transformer.TransformingVisitor])))();
  let FieldDefinitionNodeAndTransformingVisitorToFieldDefinitionNode = () => (FieldDefinitionNodeAndTransformingVisitorToFieldDefinitionNode = dart.constFn(dart.fnType(ast.FieldDefinitionNode, [ast.FieldDefinitionNode, transformer.TransformingVisitor])))();
  let FieldNodeAndTransformingVisitorToFieldNode = () => (FieldNodeAndTransformingVisitorToFieldNode = dart.constFn(dart.fnType(ast.FieldNode, [ast.FieldNode, transformer.TransformingVisitor])))();
  let FloatValueNodeAndTransformingVisitorToFloatValueNode = () => (FloatValueNodeAndTransformingVisitorToFloatValueNode = dart.constFn(dart.fnType(ast.FloatValueNode, [ast.FloatValueNode, transformer.TransformingVisitor])))();
  let FragmentDefinitionNodeAndTransformingVisitorToFragmentDefinitionNode = () => (FragmentDefinitionNodeAndTransformingVisitorToFragmentDefinitionNode = dart.constFn(dart.fnType(ast.FragmentDefinitionNode, [ast.FragmentDefinitionNode, transformer.TransformingVisitor])))();
  let FragmentSpreadNodeAndTransformingVisitorToFragmentSpreadNode = () => (FragmentSpreadNodeAndTransformingVisitorToFragmentSpreadNode = dart.constFn(dart.fnType(ast.FragmentSpreadNode, [ast.FragmentSpreadNode, transformer.TransformingVisitor])))();
  let InlineFragmentNodeAndTransformingVisitorToInlineFragmentNode = () => (InlineFragmentNodeAndTransformingVisitorToInlineFragmentNode = dart.constFn(dart.fnType(ast.InlineFragmentNode, [ast.InlineFragmentNode, transformer.TransformingVisitor])))();
  let InputObjectTypeDefinitionNodeAndTransformingVisitorToInputObjectTypeDefinitionNode = () => (InputObjectTypeDefinitionNodeAndTransformingVisitorToInputObjectTypeDefinitionNode = dart.constFn(dart.fnType(ast.InputObjectTypeDefinitionNode, [ast.InputObjectTypeDefinitionNode, transformer.TransformingVisitor])))();
  let InputObjectTypeExtensionNodeAndTransformingVisitorToInputObjectTypeExtensionNode = () => (InputObjectTypeExtensionNodeAndTransformingVisitorToInputObjectTypeExtensionNode = dart.constFn(dart.fnType(ast.InputObjectTypeExtensionNode, [ast.InputObjectTypeExtensionNode, transformer.TransformingVisitor])))();
  let InputValueDefinitionNodeAndTransformingVisitorToInputValueDefinitionNode = () => (InputValueDefinitionNodeAndTransformingVisitorToInputValueDefinitionNode = dart.constFn(dart.fnType(ast.InputValueDefinitionNode, [ast.InputValueDefinitionNode, transformer.TransformingVisitor])))();
  let IntValueNodeAndTransformingVisitorToIntValueNode = () => (IntValueNodeAndTransformingVisitorToIntValueNode = dart.constFn(dart.fnType(ast.IntValueNode, [ast.IntValueNode, transformer.TransformingVisitor])))();
  let InterfaceTypeDefinitionNodeAndTransformingVisitorToInterfaceTypeDefinitionNode = () => (InterfaceTypeDefinitionNodeAndTransformingVisitorToInterfaceTypeDefinitionNode = dart.constFn(dart.fnType(ast.InterfaceTypeDefinitionNode, [ast.InterfaceTypeDefinitionNode, transformer.TransformingVisitor])))();
  let InterfaceTypeExtensionNodeAndTransformingVisitorToInterfaceTypeExtensionNode = () => (InterfaceTypeExtensionNodeAndTransformingVisitorToInterfaceTypeExtensionNode = dart.constFn(dart.fnType(ast.InterfaceTypeExtensionNode, [ast.InterfaceTypeExtensionNode, transformer.TransformingVisitor])))();
  let ListTypeNodeAndTransformingVisitorToListTypeNode = () => (ListTypeNodeAndTransformingVisitorToListTypeNode = dart.constFn(dart.fnType(ast.ListTypeNode, [ast.ListTypeNode, transformer.TransformingVisitor])))();
  let ListValueNodeAndTransformingVisitorToListValueNode = () => (ListValueNodeAndTransformingVisitorToListValueNode = dart.constFn(dart.fnType(ast.ListValueNode, [ast.ListValueNode, transformer.TransformingVisitor])))();
  let NameNodeAndTransformingVisitorToNameNode = () => (NameNodeAndTransformingVisitorToNameNode = dart.constFn(dart.fnType(ast.NameNode, [ast.NameNode, transformer.TransformingVisitor])))();
  let NamedTypeNodeAndTransformingVisitorToNamedTypeNode = () => (NamedTypeNodeAndTransformingVisitorToNamedTypeNode = dart.constFn(dart.fnType(ast.NamedTypeNode, [ast.NamedTypeNode, transformer.TransformingVisitor])))();
  let NullValueNodeAndTransformingVisitorToNullValueNode = () => (NullValueNodeAndTransformingVisitorToNullValueNode = dart.constFn(dart.fnType(ast.NullValueNode, [ast.NullValueNode, transformer.TransformingVisitor])))();
  let ObjectFieldNodeAndTransformingVisitorToObjectFieldNode = () => (ObjectFieldNodeAndTransformingVisitorToObjectFieldNode = dart.constFn(dart.fnType(ast.ObjectFieldNode, [ast.ObjectFieldNode, transformer.TransformingVisitor])))();
  let ObjectTypeDefinitionNodeAndTransformingVisitorToObjectTypeDefinitionNode = () => (ObjectTypeDefinitionNodeAndTransformingVisitorToObjectTypeDefinitionNode = dart.constFn(dart.fnType(ast.ObjectTypeDefinitionNode, [ast.ObjectTypeDefinitionNode, transformer.TransformingVisitor])))();
  let ObjectTypeExtensionNodeAndTransformingVisitorToObjectTypeExtensionNode = () => (ObjectTypeExtensionNodeAndTransformingVisitorToObjectTypeExtensionNode = dart.constFn(dart.fnType(ast.ObjectTypeExtensionNode, [ast.ObjectTypeExtensionNode, transformer.TransformingVisitor])))();
  let ObjectValueNodeAndTransformingVisitorToObjectValueNode = () => (ObjectValueNodeAndTransformingVisitorToObjectValueNode = dart.constFn(dart.fnType(ast.ObjectValueNode, [ast.ObjectValueNode, transformer.TransformingVisitor])))();
  let OperationDefinitionNodeAndTransformingVisitorToOperationDefinitionNode = () => (OperationDefinitionNodeAndTransformingVisitorToOperationDefinitionNode = dart.constFn(dart.fnType(ast.OperationDefinitionNode, [ast.OperationDefinitionNode, transformer.TransformingVisitor])))();
  let OperationTypeDefinitionNodeAndTransformingVisitorToOperationTypeDefinitionNode = () => (OperationTypeDefinitionNodeAndTransformingVisitorToOperationTypeDefinitionNode = dart.constFn(dart.fnType(ast.OperationTypeDefinitionNode, [ast.OperationTypeDefinitionNode, transformer.TransformingVisitor])))();
  let ScalarTypeDefinitionNodeAndTransformingVisitorToScalarTypeDefinitionNode = () => (ScalarTypeDefinitionNodeAndTransformingVisitorToScalarTypeDefinitionNode = dart.constFn(dart.fnType(ast.ScalarTypeDefinitionNode, [ast.ScalarTypeDefinitionNode, transformer.TransformingVisitor])))();
  let ScalarTypeExtensionNodeAndTransformingVisitorToScalarTypeExtensionNode = () => (ScalarTypeExtensionNodeAndTransformingVisitorToScalarTypeExtensionNode = dart.constFn(dart.fnType(ast.ScalarTypeExtensionNode, [ast.ScalarTypeExtensionNode, transformer.TransformingVisitor])))();
  let SchemaDefinitionNodeAndTransformingVisitorToSchemaDefinitionNode = () => (SchemaDefinitionNodeAndTransformingVisitorToSchemaDefinitionNode = dart.constFn(dart.fnType(ast.SchemaDefinitionNode, [ast.SchemaDefinitionNode, transformer.TransformingVisitor])))();
  let SchemaExtensionNodeAndTransformingVisitorToSchemaExtensionNode = () => (SchemaExtensionNodeAndTransformingVisitorToSchemaExtensionNode = dart.constFn(dart.fnType(ast.SchemaExtensionNode, [ast.SchemaExtensionNode, transformer.TransformingVisitor])))();
  let SelectionSetNodeAndTransformingVisitorToSelectionSetNode = () => (SelectionSetNodeAndTransformingVisitorToSelectionSetNode = dart.constFn(dart.fnType(ast.SelectionSetNode, [ast.SelectionSetNode, transformer.TransformingVisitor])))();
  let StringValueNodeAndTransformingVisitorToStringValueNode = () => (StringValueNodeAndTransformingVisitorToStringValueNode = dart.constFn(dart.fnType(ast.StringValueNode, [ast.StringValueNode, transformer.TransformingVisitor])))();
  let TypeConditionNodeAndTransformingVisitorToTypeConditionNode = () => (TypeConditionNodeAndTransformingVisitorToTypeConditionNode = dart.constFn(dart.fnType(ast.TypeConditionNode, [ast.TypeConditionNode, transformer.TransformingVisitor])))();
  let UnionTypeDefinitionNodeAndTransformingVisitorToUnionTypeDefinitionNode = () => (UnionTypeDefinitionNodeAndTransformingVisitorToUnionTypeDefinitionNode = dart.constFn(dart.fnType(ast.UnionTypeDefinitionNode, [ast.UnionTypeDefinitionNode, transformer.TransformingVisitor])))();
  let UnionTypeExtensionNodeAndTransformingVisitorToUnionTypeExtensionNode = () => (UnionTypeExtensionNodeAndTransformingVisitorToUnionTypeExtensionNode = dart.constFn(dart.fnType(ast.UnionTypeExtensionNode, [ast.UnionTypeExtensionNode, transformer.TransformingVisitor])))();
  let VariableDefinitionNodeAndTransformingVisitorToVariableDefinitionNode = () => (VariableDefinitionNodeAndTransformingVisitorToVariableDefinitionNode = dart.constFn(dart.fnType(ast.VariableDefinitionNode, [ast.VariableDefinitionNode, transformer.TransformingVisitor])))();
  let VariableNodeAndTransformingVisitorToVariableNode = () => (VariableNodeAndTransformingVisitorToVariableNode = dart.constFn(dart.fnType(ast.VariableNode, [ast.VariableNode, transformer.TransformingVisitor])))();
  const CT = Object.create(null);
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.constList([], SimpleVisitorOfListOfNull());
    },
    get C2() {
      return C2 = dart.const({
        __proto__: equality.DefaultEquality.prototype
      });
    },
    get C1() {
      return C1 = dart.const({
        __proto__: equality.DeepCollectionEquality.prototype,
        [DeepCollectionEquality__unordered]: false,
        [DeepCollectionEquality__base]: C2 || CT.C2
      });
    },
    get C3() {
      return C3 = dart.constList([], ast.DefinitionNode);
    },
    get C4() {
      return C4 = dart.const({
        __proto__: ast.OperationType.prototype,
        [_name$]: "OperationType.query",
        index: 0
      });
    },
    get C5() {
      return C5 = dart.const({
        __proto__: ast.OperationType.prototype,
        [_name$]: "OperationType.mutation",
        index: 1
      });
    },
    get C6() {
      return C6 = dart.const({
        __proto__: ast.OperationType.prototype,
        [_name$]: "OperationType.subscription",
        index: 2
      });
    },
    get C7() {
      return C7 = dart.constList([C4 || CT.C4, C5 || CT.C5, C6 || CT.C6], ast.OperationType);
    },
    get C8() {
      return C8 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.query",
        index: 0
      });
    },
    get C9() {
      return C9 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.mutation",
        index: 1
      });
    },
    get C10() {
      return C10 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.subscription",
        index: 2
      });
    },
    get C11() {
      return C11 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.field",
        index: 3
      });
    },
    get C12() {
      return C12 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.fragmentDefinition",
        index: 4
      });
    },
    get C13() {
      return C13 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.fragmentSpread",
        index: 5
      });
    },
    get C14() {
      return C14 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.inlineFragment",
        index: 6
      });
    },
    get C15() {
      return C15 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.schema",
        index: 7
      });
    },
    get C16() {
      return C16 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.scalar",
        index: 8
      });
    },
    get C17() {
      return C17 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.object",
        index: 9
      });
    },
    get C18() {
      return C18 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.fieldDefinition",
        index: 10
      });
    },
    get C19() {
      return C19 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.argumentDefinition",
        index: 11
      });
    },
    get C20() {
      return C20 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.interface",
        index: 12
      });
    },
    get C21() {
      return C21 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.union",
        index: 13
      });
    },
    get C22() {
      return C22 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.enumDefinition",
        index: 14
      });
    },
    get C23() {
      return C23 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.enumValue",
        index: 15
      });
    },
    get C24() {
      return C24 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.inputObject",
        index: 16
      });
    },
    get C25() {
      return C25 = dart.const({
        __proto__: ast.DirectiveLocation.prototype,
        [_name$]: "DirectiveLocation.inputFieldDefinition",
        index: 17
      });
    },
    get C26() {
      return C26 = dart.constList([C8 || CT.C8, C9 || CT.C9, C10 || CT.C10, C11 || CT.C11, C12 || CT.C12, C13 || CT.C13, C14 || CT.C14, C15 || CT.C15, C16 || CT.C16, C17 || CT.C17, C18 || CT.C18, C19 || CT.C19, C20 || CT.C20, C21 || CT.C21, C22 || CT.C22, C23 || CT.C23, C24 || CT.C24, C25 || CT.C25], ast.DirectiveLocation);
    },
    get C27() {
      return C27 = dart.constList([], ast.VariableDefinitionNode);
    },
    get C28() {
      return C28 = dart.constList([], ast.DirectiveNode);
    },
    get C29() {
      return C29 = dart.constList([], ast.SelectionNode);
    },
    get C30() {
      return C30 = dart.constList([], ast.ArgumentNode);
    },
    get C31() {
      return C31 = dart.constList([], ast.ValueNode);
    },
    get C32() {
      return C32 = dart.constList([], ast.ObjectFieldNode);
    },
    get C33() {
      return C33 = dart.constList([], ast.OperationTypeDefinitionNode);
    },
    get C34() {
      return C34 = dart.constList([], ast.NamedTypeNode);
    },
    get C35() {
      return C35 = dart.constList([], ast.FieldDefinitionNode);
    },
    get C36() {
      return C36 = dart.constList([], ast.InputValueDefinitionNode);
    },
    get C37() {
      return C37 = dart.constList([], ast.EnumValueDefinitionNode);
    },
    get C38() {
      return C38 = dart.constList([], ast.DirectiveLocation);
    },
    get C39() {
      return C39 = dart.constList([], transformer.TransformingVisitor);
    }
  });
  const _is_Visitor_default = Symbol('_is_Visitor_default');
  visitor.Visitor$ = dart.generic(R => {
    class Visitor extends core.Object {}
    (Visitor.new = function() {
      ;
    }).prototype = Visitor.prototype;
    dart.addTypeTests(Visitor);
    Visitor.prototype[_is_Visitor_default] = true;
    dart.setLibraryUri(Visitor, "package:gql/src/ast/visitor.dart");
    return Visitor;
  });
  visitor.Visitor = visitor.Visitor$();
  dart.addTypeTests(visitor.Visitor, _is_Visitor_default);
  const _is_SimpleVisitor_default = Symbol('_is_SimpleVisitor_default');
  visitor.SimpleVisitor$ = dart.generic(R => {
    class SimpleVisitor extends core.Object {
      visitDocumentNode(node) {
        return null;
      }
      visitNameNode(node) {
        return null;
      }
      visitDirectiveNode(node) {
        return null;
      }
      visitListTypeNode(node) {
        return null;
      }
      visitNamedTypeNode(node) {
        return null;
      }
      visitDefaultValueNode(node) {
        return null;
      }
      visitVariableDefinitionNode(node) {
        return null;
      }
      visitObjectFieldNode(node) {
        return null;
      }
      visitObjectValueNode(node) {
        return null;
      }
      visitListValueNode(node) {
        return null;
      }
      visitEnumValueNode(node) {
        return null;
      }
      visitNullValueNode(node) {
        return null;
      }
      visitBooleanValueNode(node) {
        return null;
      }
      visitStringValueNode(node) {
        return null;
      }
      visitFloatValueNode(node) {
        return null;
      }
      visitIntValueNode(node) {
        return null;
      }
      visitVariableNode(node) {
        return null;
      }
      visitTypeConditionNode(node) {
        return null;
      }
      visitFragmentDefinitionNode(node) {
        return null;
      }
      visitInlineFragmentNode(node) {
        return null;
      }
      visitFragmentSpreadNode(node) {
        return null;
      }
      visitArgumentNode(node) {
        return null;
      }
      visitFieldNode(node) {
        return null;
      }
      visitSelectionSetNode(node) {
        return null;
      }
      visitOperationDefinitionNode(node) {
        return null;
      }
      visitSchemaDefinitionNode(node) {
        return null;
      }
      visitOperationTypeDefinitionNode(node) {
        return null;
      }
      visitScalarTypeDefinitionNode(node) {
        return null;
      }
      visitObjectTypeDefinitionNode(node) {
        return null;
      }
      visitFieldDefinitionNode(node) {
        return null;
      }
      visitInputValueDefinitionNode(node) {
        return null;
      }
      visitInterfaceTypeDefinitionNode(node) {
        return null;
      }
      visitUnionTypeDefinitionNode(node) {
        return null;
      }
      visitEnumTypeDefinitionNode(node) {
        return null;
      }
      visitEnumValueDefinitionNode(node) {
        return null;
      }
      visitInputObjectTypeDefinitionNode(node) {
        return null;
      }
      visitDirectiveDefinitionNode(node) {
        return null;
      }
      visitSchemaExtensionNode(node) {
        return null;
      }
      visitScalarTypeExtensionNode(node) {
        return null;
      }
      visitObjectTypeExtensionNode(node) {
        return null;
      }
      visitInterfaceTypeExtensionNode(node) {
        return null;
      }
      visitUnionTypeExtensionNode(node) {
        return null;
      }
      visitEnumTypeExtensionNode(node) {
        return null;
      }
      visitInputObjectTypeExtensionNode(node) {
        return null;
      }
    }
    (SimpleVisitor.new = function() {
      ;
    }).prototype = SimpleVisitor.prototype;
    dart.addTypeTests(SimpleVisitor);
    SimpleVisitor.prototype[_is_SimpleVisitor_default] = true;
    SimpleVisitor[dart.implements] = () => [visitor.Visitor$(R)];
    dart.setMethodSignature(SimpleVisitor, () => ({
      __proto__: dart.getMethods(SimpleVisitor.__proto__),
      visitDocumentNode: dart.fnType(R, [ast.DocumentNode]),
      visitNameNode: dart.fnType(R, [ast.NameNode]),
      visitDirectiveNode: dart.fnType(R, [ast.DirectiveNode]),
      visitListTypeNode: dart.fnType(R, [ast.ListTypeNode]),
      visitNamedTypeNode: dart.fnType(R, [ast.NamedTypeNode]),
      visitDefaultValueNode: dart.fnType(R, [ast.DefaultValueNode]),
      visitVariableDefinitionNode: dart.fnType(R, [ast.VariableDefinitionNode]),
      visitObjectFieldNode: dart.fnType(R, [ast.ObjectFieldNode]),
      visitObjectValueNode: dart.fnType(R, [ast.ObjectValueNode]),
      visitListValueNode: dart.fnType(R, [ast.ListValueNode]),
      visitEnumValueNode: dart.fnType(R, [ast.EnumValueNode]),
      visitNullValueNode: dart.fnType(R, [ast.NullValueNode]),
      visitBooleanValueNode: dart.fnType(R, [ast.BooleanValueNode]),
      visitStringValueNode: dart.fnType(R, [ast.StringValueNode]),
      visitFloatValueNode: dart.fnType(R, [ast.FloatValueNode]),
      visitIntValueNode: dart.fnType(R, [ast.IntValueNode]),
      visitVariableNode: dart.fnType(R, [ast.VariableNode]),
      visitTypeConditionNode: dart.fnType(R, [ast.TypeConditionNode]),
      visitFragmentDefinitionNode: dart.fnType(R, [ast.FragmentDefinitionNode]),
      visitInlineFragmentNode: dart.fnType(R, [ast.InlineFragmentNode]),
      visitFragmentSpreadNode: dart.fnType(R, [ast.FragmentSpreadNode]),
      visitArgumentNode: dart.fnType(R, [ast.ArgumentNode]),
      visitFieldNode: dart.fnType(R, [ast.FieldNode]),
      visitSelectionSetNode: dart.fnType(R, [ast.SelectionSetNode]),
      visitOperationDefinitionNode: dart.fnType(R, [ast.OperationDefinitionNode]),
      visitSchemaDefinitionNode: dart.fnType(R, [ast.SchemaDefinitionNode]),
      visitOperationTypeDefinitionNode: dart.fnType(R, [ast.OperationTypeDefinitionNode]),
      visitScalarTypeDefinitionNode: dart.fnType(R, [ast.ScalarTypeDefinitionNode]),
      visitObjectTypeDefinitionNode: dart.fnType(R, [ast.ObjectTypeDefinitionNode]),
      visitFieldDefinitionNode: dart.fnType(R, [ast.FieldDefinitionNode]),
      visitInputValueDefinitionNode: dart.fnType(R, [ast.InputValueDefinitionNode]),
      visitInterfaceTypeDefinitionNode: dart.fnType(R, [ast.InterfaceTypeDefinitionNode]),
      visitUnionTypeDefinitionNode: dart.fnType(R, [ast.UnionTypeDefinitionNode]),
      visitEnumTypeDefinitionNode: dart.fnType(R, [ast.EnumTypeDefinitionNode]),
      visitEnumValueDefinitionNode: dart.fnType(R, [ast.EnumValueDefinitionNode]),
      visitInputObjectTypeDefinitionNode: dart.fnType(R, [ast.InputObjectTypeDefinitionNode]),
      visitDirectiveDefinitionNode: dart.fnType(R, [ast.DirectiveDefinitionNode]),
      visitSchemaExtensionNode: dart.fnType(R, [ast.SchemaExtensionNode]),
      visitScalarTypeExtensionNode: dart.fnType(R, [ast.ScalarTypeExtensionNode]),
      visitObjectTypeExtensionNode: dart.fnType(R, [ast.ObjectTypeExtensionNode]),
      visitInterfaceTypeExtensionNode: dart.fnType(R, [ast.InterfaceTypeExtensionNode]),
      visitUnionTypeExtensionNode: dart.fnType(R, [ast.UnionTypeExtensionNode]),
      visitEnumTypeExtensionNode: dart.fnType(R, [ast.EnumTypeExtensionNode]),
      visitInputObjectTypeExtensionNode: dart.fnType(R, [ast.InputObjectTypeExtensionNode])
    }));
    dart.setLibraryUri(SimpleVisitor, "package:gql/src/ast/visitor.dart");
    return SimpleVisitor;
  });
  visitor.SimpleVisitor = visitor.SimpleVisitor$();
  dart.addTypeTests(visitor.SimpleVisitor, _is_SimpleVisitor_default);
  visitor.RecursiveVisitor = class RecursiveVisitor extends core.Object {
    visitArgumentNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitBooleanValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitDefaultValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitDirectiveDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitDirectiveNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitDocumentNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitEnumTypeDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitEnumTypeExtensionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitEnumValueDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitEnumValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitFieldDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitFieldNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitFloatValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitFragmentDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitFragmentSpreadNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitInlineFragmentNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitInputObjectTypeDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitInputObjectTypeExtensionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitInputValueDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitIntValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitInterfaceTypeDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitInterfaceTypeExtensionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitListTypeNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitListValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitNameNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitNamedTypeNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitNullValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitObjectFieldNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitObjectTypeDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitObjectTypeExtensionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitObjectValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitOperationDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitOperationTypeDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitScalarTypeDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitScalarTypeExtensionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitSchemaDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitSchemaExtensionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitSelectionSetNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitStringValueNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitTypeConditionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitUnionTypeDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitUnionTypeExtensionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitVariableDefinitionNode(node) {
      return node.visitChildren(dart.void, this);
    }
    visitVariableNode(node) {
      return node.visitChildren(dart.void, this);
    }
  };
  (visitor.RecursiveVisitor.new = function() {
    ;
  }).prototype = visitor.RecursiveVisitor.prototype;
  dart.addTypeTests(visitor.RecursiveVisitor);
  visitor.RecursiveVisitor[dart.implements] = () => [visitor.Visitor$(dart.void)];
  dart.setMethodSignature(visitor.RecursiveVisitor, () => ({
    __proto__: dart.getMethods(visitor.RecursiveVisitor.__proto__),
    visitArgumentNode: dart.fnType(dart.void, [ast.ArgumentNode]),
    visitBooleanValueNode: dart.fnType(dart.void, [ast.BooleanValueNode]),
    visitDefaultValueNode: dart.fnType(dart.void, [ast.DefaultValueNode]),
    visitDirectiveDefinitionNode: dart.fnType(dart.void, [ast.DirectiveDefinitionNode]),
    visitDirectiveNode: dart.fnType(dart.void, [ast.DirectiveNode]),
    visitDocumentNode: dart.fnType(dart.void, [ast.DocumentNode]),
    visitEnumTypeDefinitionNode: dart.fnType(dart.void, [ast.EnumTypeDefinitionNode]),
    visitEnumTypeExtensionNode: dart.fnType(dart.void, [ast.EnumTypeExtensionNode]),
    visitEnumValueDefinitionNode: dart.fnType(dart.void, [ast.EnumValueDefinitionNode]),
    visitEnumValueNode: dart.fnType(dart.void, [ast.EnumValueNode]),
    visitFieldDefinitionNode: dart.fnType(dart.void, [ast.FieldDefinitionNode]),
    visitFieldNode: dart.fnType(dart.void, [ast.FieldNode]),
    visitFloatValueNode: dart.fnType(dart.void, [ast.FloatValueNode]),
    visitFragmentDefinitionNode: dart.fnType(dart.void, [ast.FragmentDefinitionNode]),
    visitFragmentSpreadNode: dart.fnType(dart.void, [ast.FragmentSpreadNode]),
    visitInlineFragmentNode: dart.fnType(dart.void, [ast.InlineFragmentNode]),
    visitInputObjectTypeDefinitionNode: dart.fnType(dart.void, [ast.InputObjectTypeDefinitionNode]),
    visitInputObjectTypeExtensionNode: dart.fnType(dart.void, [ast.InputObjectTypeExtensionNode]),
    visitInputValueDefinitionNode: dart.fnType(dart.void, [ast.InputValueDefinitionNode]),
    visitIntValueNode: dart.fnType(dart.void, [ast.IntValueNode]),
    visitInterfaceTypeDefinitionNode: dart.fnType(dart.void, [ast.InterfaceTypeDefinitionNode]),
    visitInterfaceTypeExtensionNode: dart.fnType(dart.void, [ast.InterfaceTypeExtensionNode]),
    visitListTypeNode: dart.fnType(dart.void, [ast.ListTypeNode]),
    visitListValueNode: dart.fnType(dart.void, [ast.ListValueNode]),
    visitNameNode: dart.fnType(dart.void, [ast.NameNode]),
    visitNamedTypeNode: dart.fnType(dart.void, [ast.NamedTypeNode]),
    visitNullValueNode: dart.fnType(dart.void, [ast.NullValueNode]),
    visitObjectFieldNode: dart.fnType(dart.void, [ast.ObjectFieldNode]),
    visitObjectTypeDefinitionNode: dart.fnType(dart.void, [ast.ObjectTypeDefinitionNode]),
    visitObjectTypeExtensionNode: dart.fnType(dart.void, [ast.ObjectTypeExtensionNode]),
    visitObjectValueNode: dart.fnType(dart.void, [ast.ObjectValueNode]),
    visitOperationDefinitionNode: dart.fnType(dart.void, [ast.OperationDefinitionNode]),
    visitOperationTypeDefinitionNode: dart.fnType(dart.void, [ast.OperationTypeDefinitionNode]),
    visitScalarTypeDefinitionNode: dart.fnType(dart.void, [ast.ScalarTypeDefinitionNode]),
    visitScalarTypeExtensionNode: dart.fnType(dart.void, [ast.ScalarTypeExtensionNode]),
    visitSchemaDefinitionNode: dart.fnType(dart.void, [ast.SchemaDefinitionNode]),
    visitSchemaExtensionNode: dart.fnType(dart.void, [ast.SchemaExtensionNode]),
    visitSelectionSetNode: dart.fnType(dart.void, [ast.SelectionSetNode]),
    visitStringValueNode: dart.fnType(dart.void, [ast.StringValueNode]),
    visitTypeConditionNode: dart.fnType(dart.void, [ast.TypeConditionNode]),
    visitUnionTypeDefinitionNode: dart.fnType(dart.void, [ast.UnionTypeDefinitionNode]),
    visitUnionTypeExtensionNode: dart.fnType(dart.void, [ast.UnionTypeExtensionNode]),
    visitVariableDefinitionNode: dart.fnType(dart.void, [ast.VariableDefinitionNode]),
    visitVariableNode: dart.fnType(dart.void, [ast.VariableNode])
  }));
  dart.setLibraryUri(visitor.RecursiveVisitor, "package:gql/src/ast/visitor.dart");
  let C0;
  const _is_AccumulatingVisitor_default = Symbol('_is_AccumulatingVisitor_default');
  const visitors$ = dart.privateName(visitor, "AccumulatingVisitor.visitors");
  const accumulator = dart.privateName(visitor, "AccumulatingVisitor.accumulator");
  visitor.AccumulatingVisitor$ = dart.generic(A => {
    let JSArrayOfA = () => (JSArrayOfA = dart.constFn(_interceptors.JSArray$(A)))();
    let ListOfA = () => (ListOfA = dart.constFn(core.List$(A)))();
    let SimpleVisitorOfListOfA = () => (SimpleVisitorOfListOfA = dart.constFn(visitor.SimpleVisitor$(ListOfA())))();
    let ListOfSimpleVisitorOfListOfA = () => (ListOfSimpleVisitorOfListOfA = dart.constFn(core.List$(SimpleVisitorOfListOfA())))();
    let SimpleVisitorOfListOfAToListOfA = () => (SimpleVisitorOfListOfAToListOfA = dart.constFn(dart.fnType(ListOfA(), [SimpleVisitorOfListOfA()])))();
    class AccumulatingVisitor extends visitor.RecursiveVisitor {
      get visitors() {
        return this[visitors$];
      }
      set visitors(value) {
        this[visitors$] = ListOfSimpleVisitorOfListOfA()._check(value);
      }
      get accumulator() {
        return this[accumulator];
      }
      set accumulator(value) {
        this[accumulator] = ListOfA()._check(value);
      }
      visitArgumentNode(node) {
        this.accumulator = (() => {
          let t0 = JSArrayOfA().of([]);
          for (let t1 of this.accumulator)
            t0[$add](t1);
          for (let t2 of this.visitors[$expand](A, dart.fn(visitor => {
            let t2;
            t2 = visitor.visitArgumentNode(node);
            return t2 == null ? JSArrayOfA().of([]) : t2;
          }, SimpleVisitorOfListOfAToListOfA())))
            t0[$add](t2);
          return t0;
        })();
        super.visitArgumentNode(node);
      }
      visitBooleanValueNode(node) {
        this.accumulator = (() => {
          let t3 = JSArrayOfA().of([]);
          for (let t4 of this.accumulator)
            t3[$add](t4);
          for (let t5 of this.visitors[$expand](A, dart.fn(visitor => {
            let t5;
            t5 = visitor.visitBooleanValueNode(node);
            return t5 == null ? JSArrayOfA().of([]) : t5;
          }, SimpleVisitorOfListOfAToListOfA())))
            t3[$add](t5);
          return t3;
        })();
        super.visitBooleanValueNode(node);
      }
      visitDefaultValueNode(node) {
        this.accumulator = (() => {
          let t6 = JSArrayOfA().of([]);
          for (let t7 of this.accumulator)
            t6[$add](t7);
          for (let t8 of this.visitors[$expand](A, dart.fn(visitor => {
            let t8;
            t8 = visitor.visitDefaultValueNode(node);
            return t8 == null ? JSArrayOfA().of([]) : t8;
          }, SimpleVisitorOfListOfAToListOfA())))
            t6[$add](t8);
          return t6;
        })();
        super.visitDefaultValueNode(node);
      }
      visitDirectiveDefinitionNode(node) {
        this.accumulator = (() => {
          let t9 = JSArrayOfA().of([]);
          for (let t10 of this.accumulator)
            t9[$add](t10);
          for (let t11 of this.visitors[$expand](A, dart.fn(visitor => {
            let t11;
            t11 = visitor.visitDirectiveDefinitionNode(node);
            return t11 == null ? JSArrayOfA().of([]) : t11;
          }, SimpleVisitorOfListOfAToListOfA())))
            t9[$add](t11);
          return t9;
        })();
        super.visitDirectiveDefinitionNode(node);
      }
      visitDirectiveNode(node) {
        this.accumulator = (() => {
          let t12 = JSArrayOfA().of([]);
          for (let t13 of this.accumulator)
            t12[$add](t13);
          for (let t14 of this.visitors[$expand](A, dart.fn(visitor => {
            let t14;
            t14 = visitor.visitDirectiveNode(node);
            return t14 == null ? JSArrayOfA().of([]) : t14;
          }, SimpleVisitorOfListOfAToListOfA())))
            t12[$add](t14);
          return t12;
        })();
        super.visitDirectiveNode(node);
      }
      visitDocumentNode(node) {
        this.accumulator = (() => {
          let t15 = JSArrayOfA().of([]);
          for (let t16 of this.accumulator)
            t15[$add](t16);
          for (let t17 of this.visitors[$expand](A, dart.fn(visitor => {
            let t17;
            t17 = visitor.visitDocumentNode(node);
            return t17 == null ? JSArrayOfA().of([]) : t17;
          }, SimpleVisitorOfListOfAToListOfA())))
            t15[$add](t17);
          return t15;
        })();
        super.visitDocumentNode(node);
      }
      visitEnumTypeDefinitionNode(node) {
        this.accumulator = (() => {
          let t18 = JSArrayOfA().of([]);
          for (let t19 of this.accumulator)
            t18[$add](t19);
          for (let t20 of this.visitors[$expand](A, dart.fn(visitor => {
            let t20;
            t20 = visitor.visitEnumTypeDefinitionNode(node);
            return t20 == null ? JSArrayOfA().of([]) : t20;
          }, SimpleVisitorOfListOfAToListOfA())))
            t18[$add](t20);
          return t18;
        })();
        super.visitEnumTypeDefinitionNode(node);
      }
      visitEnumTypeExtensionNode(node) {
        this.accumulator = (() => {
          let t21 = JSArrayOfA().of([]);
          for (let t22 of this.accumulator)
            t21[$add](t22);
          for (let t23 of this.visitors[$expand](A, dart.fn(visitor => {
            let t23;
            t23 = visitor.visitEnumTypeExtensionNode(node);
            return t23 == null ? JSArrayOfA().of([]) : t23;
          }, SimpleVisitorOfListOfAToListOfA())))
            t21[$add](t23);
          return t21;
        })();
        super.visitEnumTypeExtensionNode(node);
      }
      visitEnumValueDefinitionNode(node) {
        this.accumulator = (() => {
          let t24 = JSArrayOfA().of([]);
          for (let t25 of this.accumulator)
            t24[$add](t25);
          for (let t26 of this.visitors[$expand](A, dart.fn(visitor => {
            let t26;
            t26 = visitor.visitEnumValueDefinitionNode(node);
            return t26 == null ? JSArrayOfA().of([]) : t26;
          }, SimpleVisitorOfListOfAToListOfA())))
            t24[$add](t26);
          return t24;
        })();
        super.visitEnumValueDefinitionNode(node);
      }
      visitEnumValueNode(node) {
        this.accumulator = (() => {
          let t27 = JSArrayOfA().of([]);
          for (let t28 of this.accumulator)
            t27[$add](t28);
          for (let t29 of this.visitors[$expand](A, dart.fn(visitor => {
            let t29;
            t29 = visitor.visitEnumValueNode(node);
            return t29 == null ? JSArrayOfA().of([]) : t29;
          }, SimpleVisitorOfListOfAToListOfA())))
            t27[$add](t29);
          return t27;
        })();
        super.visitEnumValueNode(node);
      }
      visitFieldDefinitionNode(node) {
        this.accumulator = (() => {
          let t30 = JSArrayOfA().of([]);
          for (let t31 of this.accumulator)
            t30[$add](t31);
          for (let t32 of this.visitors[$expand](A, dart.fn(visitor => {
            let t32;
            t32 = visitor.visitFieldDefinitionNode(node);
            return t32 == null ? JSArrayOfA().of([]) : t32;
          }, SimpleVisitorOfListOfAToListOfA())))
            t30[$add](t32);
          return t30;
        })();
        super.visitFieldDefinitionNode(node);
      }
      visitFieldNode(node) {
        this.accumulator = (() => {
          let t33 = JSArrayOfA().of([]);
          for (let t34 of this.accumulator)
            t33[$add](t34);
          for (let t35 of this.visitors[$expand](A, dart.fn(visitor => {
            let t35;
            t35 = visitor.visitFieldNode(node);
            return t35 == null ? JSArrayOfA().of([]) : t35;
          }, SimpleVisitorOfListOfAToListOfA())))
            t33[$add](t35);
          return t33;
        })();
        super.visitFieldNode(node);
      }
      visitFloatValueNode(node) {
        this.accumulator = (() => {
          let t36 = JSArrayOfA().of([]);
          for (let t37 of this.accumulator)
            t36[$add](t37);
          for (let t38 of this.visitors[$expand](A, dart.fn(visitor => {
            let t38;
            t38 = visitor.visitFloatValueNode(node);
            return t38 == null ? JSArrayOfA().of([]) : t38;
          }, SimpleVisitorOfListOfAToListOfA())))
            t36[$add](t38);
          return t36;
        })();
        super.visitFloatValueNode(node);
      }
      visitFragmentDefinitionNode(node) {
        this.accumulator = (() => {
          let t39 = JSArrayOfA().of([]);
          for (let t40 of this.accumulator)
            t39[$add](t40);
          for (let t41 of this.visitors[$expand](A, dart.fn(visitor => {
            let t41;
            t41 = visitor.visitFragmentDefinitionNode(node);
            return t41 == null ? JSArrayOfA().of([]) : t41;
          }, SimpleVisitorOfListOfAToListOfA())))
            t39[$add](t41);
          return t39;
        })();
        super.visitFragmentDefinitionNode(node);
      }
      visitFragmentSpreadNode(node) {
        this.accumulator = (() => {
          let t42 = JSArrayOfA().of([]);
          for (let t43 of this.accumulator)
            t42[$add](t43);
          for (let t44 of this.visitors[$expand](A, dart.fn(visitor => {
            let t44;
            t44 = visitor.visitFragmentSpreadNode(node);
            return t44 == null ? JSArrayOfA().of([]) : t44;
          }, SimpleVisitorOfListOfAToListOfA())))
            t42[$add](t44);
          return t42;
        })();
        super.visitFragmentSpreadNode(node);
      }
      visitInlineFragmentNode(node) {
        this.accumulator = (() => {
          let t45 = JSArrayOfA().of([]);
          for (let t46 of this.accumulator)
            t45[$add](t46);
          for (let t47 of this.visitors[$expand](A, dart.fn(visitor => {
            let t47;
            t47 = visitor.visitInlineFragmentNode(node);
            return t47 == null ? JSArrayOfA().of([]) : t47;
          }, SimpleVisitorOfListOfAToListOfA())))
            t45[$add](t47);
          return t45;
        })();
        super.visitInlineFragmentNode(node);
      }
      visitInputObjectTypeDefinitionNode(node) {
        this.accumulator = (() => {
          let t48 = JSArrayOfA().of([]);
          for (let t49 of this.accumulator)
            t48[$add](t49);
          for (let t50 of this.visitors[$expand](A, dart.fn(visitor => {
            let t50;
            t50 = visitor.visitInputObjectTypeDefinitionNode(node);
            return t50 == null ? JSArrayOfA().of([]) : t50;
          }, SimpleVisitorOfListOfAToListOfA())))
            t48[$add](t50);
          return t48;
        })();
        super.visitInputObjectTypeDefinitionNode(node);
      }
      visitInputObjectTypeExtensionNode(node) {
        this.accumulator = (() => {
          let t51 = JSArrayOfA().of([]);
          for (let t52 of this.accumulator)
            t51[$add](t52);
          for (let t53 of this.visitors[$expand](A, dart.fn(visitor => {
            let t53;
            t53 = visitor.visitInputObjectTypeExtensionNode(node);
            return t53 == null ? JSArrayOfA().of([]) : t53;
          }, SimpleVisitorOfListOfAToListOfA())))
            t51[$add](t53);
          return t51;
        })();
        super.visitInputObjectTypeExtensionNode(node);
      }
      visitInputValueDefinitionNode(node) {
        this.accumulator = (() => {
          let t54 = JSArrayOfA().of([]);
          for (let t55 of this.accumulator)
            t54[$add](t55);
          for (let t56 of this.visitors[$expand](A, dart.fn(visitor => {
            let t56;
            t56 = visitor.visitInputValueDefinitionNode(node);
            return t56 == null ? JSArrayOfA().of([]) : t56;
          }, SimpleVisitorOfListOfAToListOfA())))
            t54[$add](t56);
          return t54;
        })();
        super.visitInputValueDefinitionNode(node);
      }
      visitIntValueNode(node) {
        this.accumulator = (() => {
          let t57 = JSArrayOfA().of([]);
          for (let t58 of this.accumulator)
            t57[$add](t58);
          for (let t59 of this.visitors[$expand](A, dart.fn(visitor => {
            let t59;
            t59 = visitor.visitIntValueNode(node);
            return t59 == null ? JSArrayOfA().of([]) : t59;
          }, SimpleVisitorOfListOfAToListOfA())))
            t57[$add](t59);
          return t57;
        })();
        super.visitIntValueNode(node);
      }
      visitInterfaceTypeDefinitionNode(node) {
        this.accumulator = (() => {
          let t60 = JSArrayOfA().of([]);
          for (let t61 of this.accumulator)
            t60[$add](t61);
          for (let t62 of this.visitors[$expand](A, dart.fn(visitor => {
            let t62;
            t62 = visitor.visitInterfaceTypeDefinitionNode(node);
            return t62 == null ? JSArrayOfA().of([]) : t62;
          }, SimpleVisitorOfListOfAToListOfA())))
            t60[$add](t62);
          return t60;
        })();
        super.visitInterfaceTypeDefinitionNode(node);
      }
      visitInterfaceTypeExtensionNode(node) {
        this.accumulator = (() => {
          let t63 = JSArrayOfA().of([]);
          for (let t64 of this.accumulator)
            t63[$add](t64);
          for (let t65 of this.visitors[$expand](A, dart.fn(visitor => {
            let t65;
            t65 = visitor.visitInterfaceTypeExtensionNode(node);
            return t65 == null ? JSArrayOfA().of([]) : t65;
          }, SimpleVisitorOfListOfAToListOfA())))
            t63[$add](t65);
          return t63;
        })();
        super.visitInterfaceTypeExtensionNode(node);
      }
      visitListTypeNode(node) {
        this.accumulator = (() => {
          let t66 = JSArrayOfA().of([]);
          for (let t67 of this.accumulator)
            t66[$add](t67);
          for (let t68 of this.visitors[$expand](A, dart.fn(visitor => {
            let t68;
            t68 = visitor.visitListTypeNode(node);
            return t68 == null ? JSArrayOfA().of([]) : t68;
          }, SimpleVisitorOfListOfAToListOfA())))
            t66[$add](t68);
          return t66;
        })();
        super.visitListTypeNode(node);
      }
      visitListValueNode(node) {
        this.accumulator = (() => {
          let t69 = JSArrayOfA().of([]);
          for (let t70 of this.accumulator)
            t69[$add](t70);
          for (let t71 of this.visitors[$expand](A, dart.fn(visitor => {
            let t71;
            t71 = visitor.visitListValueNode(node);
            return t71 == null ? JSArrayOfA().of([]) : t71;
          }, SimpleVisitorOfListOfAToListOfA())))
            t69[$add](t71);
          return t69;
        })();
        super.visitListValueNode(node);
      }
      visitNameNode(node) {
        this.accumulator = (() => {
          let t72 = JSArrayOfA().of([]);
          for (let t73 of this.accumulator)
            t72[$add](t73);
          for (let t74 of this.visitors[$expand](A, dart.fn(visitor => {
            let t74;
            t74 = visitor.visitNameNode(node);
            return t74 == null ? JSArrayOfA().of([]) : t74;
          }, SimpleVisitorOfListOfAToListOfA())))
            t72[$add](t74);
          return t72;
        })();
        super.visitNameNode(node);
      }
      visitNamedTypeNode(node) {
        this.accumulator = (() => {
          let t75 = JSArrayOfA().of([]);
          for (let t76 of this.accumulator)
            t75[$add](t76);
          for (let t77 of this.visitors[$expand](A, dart.fn(visitor => {
            let t77;
            t77 = visitor.visitNamedTypeNode(node);
            return t77 == null ? JSArrayOfA().of([]) : t77;
          }, SimpleVisitorOfListOfAToListOfA())))
            t75[$add](t77);
          return t75;
        })();
        super.visitNamedTypeNode(node);
      }
      visitNullValueNode(node) {
        this.accumulator = (() => {
          let t78 = JSArrayOfA().of([]);
          for (let t79 of this.accumulator)
            t78[$add](t79);
          for (let t80 of this.visitors[$expand](A, dart.fn(visitor => {
            let t80;
            t80 = visitor.visitNullValueNode(node);
            return t80 == null ? JSArrayOfA().of([]) : t80;
          }, SimpleVisitorOfListOfAToListOfA())))
            t78[$add](t80);
          return t78;
        })();
        super.visitNullValueNode(node);
      }
      visitObjectFieldNode(node) {
        this.accumulator = (() => {
          let t81 = JSArrayOfA().of([]);
          for (let t82 of this.accumulator)
            t81[$add](t82);
          for (let t83 of this.visitors[$expand](A, dart.fn(visitor => {
            let t83;
            t83 = visitor.visitObjectFieldNode(node);
            return t83 == null ? JSArrayOfA().of([]) : t83;
          }, SimpleVisitorOfListOfAToListOfA())))
            t81[$add](t83);
          return t81;
        })();
        super.visitObjectFieldNode(node);
      }
      visitObjectTypeDefinitionNode(node) {
        this.accumulator = (() => {
          let t84 = JSArrayOfA().of([]);
          for (let t85 of this.accumulator)
            t84[$add](t85);
          for (let t86 of this.visitors[$expand](A, dart.fn(visitor => {
            let t86;
            t86 = visitor.visitObjectTypeDefinitionNode(node);
            return t86 == null ? JSArrayOfA().of([]) : t86;
          }, SimpleVisitorOfListOfAToListOfA())))
            t84[$add](t86);
          return t84;
        })();
        super.visitObjectTypeDefinitionNode(node);
      }
      visitObjectTypeExtensionNode(node) {
        this.accumulator = (() => {
          let t87 = JSArrayOfA().of([]);
          for (let t88 of this.accumulator)
            t87[$add](t88);
          for (let t89 of this.visitors[$expand](A, dart.fn(visitor => {
            let t89;
            t89 = visitor.visitObjectTypeExtensionNode(node);
            return t89 == null ? JSArrayOfA().of([]) : t89;
          }, SimpleVisitorOfListOfAToListOfA())))
            t87[$add](t89);
          return t87;
        })();
        super.visitObjectTypeExtensionNode(node);
      }
      visitObjectValueNode(node) {
        this.accumulator = (() => {
          let t90 = JSArrayOfA().of([]);
          for (let t91 of this.accumulator)
            t90[$add](t91);
          for (let t92 of this.visitors[$expand](A, dart.fn(visitor => {
            let t92;
            t92 = visitor.visitObjectValueNode(node);
            return t92 == null ? JSArrayOfA().of([]) : t92;
          }, SimpleVisitorOfListOfAToListOfA())))
            t90[$add](t92);
          return t90;
        })();
        super.visitObjectValueNode(node);
      }
      visitOperationDefinitionNode(node) {
        this.accumulator = (() => {
          let t93 = JSArrayOfA().of([]);
          for (let t94 of this.accumulator)
            t93[$add](t94);
          for (let t95 of this.visitors[$expand](A, dart.fn(visitor => {
            let t95;
            t95 = visitor.visitOperationDefinitionNode(node);
            return t95 == null ? JSArrayOfA().of([]) : t95;
          }, SimpleVisitorOfListOfAToListOfA())))
            t93[$add](t95);
          return t93;
        })();
        super.visitOperationDefinitionNode(node);
      }
      visitOperationTypeDefinitionNode(node) {
        this.accumulator = (() => {
          let t96 = JSArrayOfA().of([]);
          for (let t97 of this.accumulator)
            t96[$add](t97);
          for (let t98 of this.visitors[$expand](A, dart.fn(visitor => {
            let t98;
            t98 = visitor.visitOperationTypeDefinitionNode(node);
            return t98 == null ? JSArrayOfA().of([]) : t98;
          }, SimpleVisitorOfListOfAToListOfA())))
            t96[$add](t98);
          return t96;
        })();
        super.visitOperationTypeDefinitionNode(node);
      }
      visitScalarTypeDefinitionNode(node) {
        this.accumulator = (() => {
          let t99 = JSArrayOfA().of([]);
          for (let t100 of this.accumulator)
            t99[$add](t100);
          for (let t101 of this.visitors[$expand](A, dart.fn(visitor => {
            let t101;
            t101 = visitor.visitScalarTypeDefinitionNode(node);
            return t101 == null ? JSArrayOfA().of([]) : t101;
          }, SimpleVisitorOfListOfAToListOfA())))
            t99[$add](t101);
          return t99;
        })();
        super.visitScalarTypeDefinitionNode(node);
      }
      visitScalarTypeExtensionNode(node) {
        this.accumulator = (() => {
          let t102 = JSArrayOfA().of([]);
          for (let t103 of this.accumulator)
            t102[$add](t103);
          for (let t104 of this.visitors[$expand](A, dart.fn(visitor => {
            let t104;
            t104 = visitor.visitScalarTypeExtensionNode(node);
            return t104 == null ? JSArrayOfA().of([]) : t104;
          }, SimpleVisitorOfListOfAToListOfA())))
            t102[$add](t104);
          return t102;
        })();
        super.visitScalarTypeExtensionNode(node);
      }
      visitSchemaDefinitionNode(node) {
        this.accumulator = (() => {
          let t105 = JSArrayOfA().of([]);
          for (let t106 of this.accumulator)
            t105[$add](t106);
          for (let t107 of this.visitors[$expand](A, dart.fn(visitor => {
            let t107;
            t107 = visitor.visitSchemaDefinitionNode(node);
            return t107 == null ? JSArrayOfA().of([]) : t107;
          }, SimpleVisitorOfListOfAToListOfA())))
            t105[$add](t107);
          return t105;
        })();
        super.visitSchemaDefinitionNode(node);
      }
      visitSchemaExtensionNode(node) {
        this.accumulator = (() => {
          let t108 = JSArrayOfA().of([]);
          for (let t109 of this.accumulator)
            t108[$add](t109);
          for (let t110 of this.visitors[$expand](A, dart.fn(visitor => {
            let t110;
            t110 = visitor.visitSchemaExtensionNode(node);
            return t110 == null ? JSArrayOfA().of([]) : t110;
          }, SimpleVisitorOfListOfAToListOfA())))
            t108[$add](t110);
          return t108;
        })();
        super.visitSchemaExtensionNode(node);
      }
      visitSelectionSetNode(node) {
        this.accumulator = (() => {
          let t111 = JSArrayOfA().of([]);
          for (let t112 of this.accumulator)
            t111[$add](t112);
          for (let t113 of this.visitors[$expand](A, dart.fn(visitor => {
            let t113;
            t113 = visitor.visitSelectionSetNode(node);
            return t113 == null ? JSArrayOfA().of([]) : t113;
          }, SimpleVisitorOfListOfAToListOfA())))
            t111[$add](t113);
          return t111;
        })();
        super.visitSelectionSetNode(node);
      }
      visitStringValueNode(node) {
        this.accumulator = (() => {
          let t114 = JSArrayOfA().of([]);
          for (let t115 of this.accumulator)
            t114[$add](t115);
          for (let t116 of this.visitors[$expand](A, dart.fn(visitor => {
            let t116;
            t116 = visitor.visitStringValueNode(node);
            return t116 == null ? JSArrayOfA().of([]) : t116;
          }, SimpleVisitorOfListOfAToListOfA())))
            t114[$add](t116);
          return t114;
        })();
        super.visitStringValueNode(node);
      }
      visitTypeConditionNode(node) {
        this.accumulator = (() => {
          let t117 = JSArrayOfA().of([]);
          for (let t118 of this.accumulator)
            t117[$add](t118);
          for (let t119 of this.visitors[$expand](A, dart.fn(visitor => {
            let t119;
            t119 = visitor.visitTypeConditionNode(node);
            return t119 == null ? JSArrayOfA().of([]) : t119;
          }, SimpleVisitorOfListOfAToListOfA())))
            t117[$add](t119);
          return t117;
        })();
        super.visitTypeConditionNode(node);
      }
      visitUnionTypeDefinitionNode(node) {
        this.accumulator = (() => {
          let t120 = JSArrayOfA().of([]);
          for (let t121 of this.accumulator)
            t120[$add](t121);
          for (let t122 of this.visitors[$expand](A, dart.fn(visitor => {
            let t122;
            t122 = visitor.visitUnionTypeDefinitionNode(node);
            return t122 == null ? JSArrayOfA().of([]) : t122;
          }, SimpleVisitorOfListOfAToListOfA())))
            t120[$add](t122);
          return t120;
        })();
        super.visitUnionTypeDefinitionNode(node);
      }
      visitUnionTypeExtensionNode(node) {
        this.accumulator = (() => {
          let t123 = JSArrayOfA().of([]);
          for (let t124 of this.accumulator)
            t123[$add](t124);
          for (let t125 of this.visitors[$expand](A, dart.fn(visitor => {
            let t125;
            t125 = visitor.visitUnionTypeExtensionNode(node);
            return t125 == null ? JSArrayOfA().of([]) : t125;
          }, SimpleVisitorOfListOfAToListOfA())))
            t123[$add](t125);
          return t123;
        })();
        super.visitUnionTypeExtensionNode(node);
      }
      visitVariableDefinitionNode(node) {
        this.accumulator = (() => {
          let t126 = JSArrayOfA().of([]);
          for (let t127 of this.accumulator)
            t126[$add](t127);
          for (let t128 of this.visitors[$expand](A, dart.fn(visitor => {
            let t128;
            t128 = visitor.visitVariableDefinitionNode(node);
            return t128 == null ? JSArrayOfA().of([]) : t128;
          }, SimpleVisitorOfListOfAToListOfA())))
            t126[$add](t128);
          return t126;
        })();
        super.visitVariableDefinitionNode(node);
      }
      visitVariableNode(node) {
        this.accumulator = (() => {
          let t129 = JSArrayOfA().of([]);
          for (let t130 of this.accumulator)
            t129[$add](t130);
          for (let t131 of this.visitors[$expand](A, dart.fn(visitor => {
            let t131;
            t131 = visitor.visitVariableNode(node);
            return t131 == null ? JSArrayOfA().of([]) : t131;
          }, SimpleVisitorOfListOfAToListOfA())))
            t129[$add](t131);
          return t129;
        })();
        super.visitVariableNode(node);
      }
    }
    (AccumulatingVisitor.new = function(opts) {
      let visitors = opts && 'visitors' in opts ? opts.visitors : C0 || CT.C0;
      this[accumulator] = JSArrayOfA().of([]);
      this[visitors$] = visitors;
      ;
    }).prototype = AccumulatingVisitor.prototype;
    dart.addTypeTests(AccumulatingVisitor);
    AccumulatingVisitor.prototype[_is_AccumulatingVisitor_default] = true;
    dart.setLibraryUri(AccumulatingVisitor, "package:gql/src/ast/visitor.dart");
    dart.setFieldSignature(AccumulatingVisitor, () => ({
      __proto__: dart.getFields(AccumulatingVisitor.__proto__),
      visitors: dart.fieldType(core.List$(visitor.SimpleVisitor$(core.List$(A)))),
      accumulator: dart.fieldType(core.List$(A))
    }));
    return AccumulatingVisitor;
  });
  visitor.AccumulatingVisitor = visitor.AccumulatingVisitor$();
  dart.addTypeTests(visitor.AccumulatingVisitor, _is_AccumulatingVisitor_default);
  const _children = dart.privateName(ast, "_children");
  const DeepCollectionEquality__unordered = dart.privateName(equality, "DeepCollectionEquality._unordered");
  let C2;
  const DeepCollectionEquality__base = dart.privateName(equality, "DeepCollectionEquality._base");
  let C1;
  const span$ = dart.privateName(ast, "Node.span");
  ast.Node = class Node extends core.Object {
    get span() {
      return this[span$];
    }
    set span(value) {
      super.span = value;
    }
    visitChildren(R, v) {
      return this[_children][$forEach](dart.fn(child => {
        if (ListOfNode().is(child)) {
          ast._visitAll(R, child, v);
        } else if (ast.Node.is(child)) {
          ast._visitOne(R, child, v);
        }
      }, ObjectToNull()));
    }
    _equals(o) {
      if (o == null) return false;
      if (ast.Node.is(o)) {
        if (!dart.equals(dart.runtimeType(o), this[$runtimeType])) return false;
        return (C1 || CT.C1).equals(o[_children], this[_children]);
      }
      return false;
    }
    get hashCode() {
      return (C1 || CT.C1).hash(this[_children]);
    }
  };
  (ast.Node.new = function(span) {
    this[span$] = span;
    ;
  }).prototype = ast.Node.prototype;
  dart.addTypeTests(ast.Node);
  dart.setMethodSignature(ast.Node, () => ({
    __proto__: dart.getMethods(ast.Node.__proto__),
    visitChildren: dart.gFnType(R => [dart.void, [visitor.Visitor$(R)]]),
    _equals: dart.fnType(core.bool, [core.Object]),
    [$_equals]: dart.fnType(core.bool, [core.Object])
  }));
  dart.setLibraryUri(ast.Node, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.Node, () => ({
    __proto__: dart.getFields(ast.Node.__proto__),
    span: dart.finalFieldType(file.FileSpan)
  }));
  dart.defineExtensionMethods(ast.Node, ['_equals']);
  dart.defineExtensionAccessors(ast.Node, ['hashCode']);
  let C3;
  const definitions$ = dart.privateName(ast, "DocumentNode.definitions");
  ast.DocumentNode = class DocumentNode extends ast.Node {
    get definitions() {
      return this[definitions$];
    }
    set definitions(value) {
      super.definitions = value;
    }
    accept(R, v) {
      return v.visitDocumentNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.definitions]);
    }
  };
  (ast.DocumentNode.new = function(opts) {
    let definitions = opts && 'definitions' in opts ? opts.definitions : C3 || CT.C3;
    let span = opts && 'span' in opts ? opts.span : null;
    this[definitions$] = definitions;
    if (!(definitions != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 70, 16, "definitions != null");
    ast.DocumentNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.DocumentNode.prototype;
  dart.addTypeTests(ast.DocumentNode);
  dart.setMethodSignature(ast.DocumentNode, () => ({
    __proto__: dart.getMethods(ast.DocumentNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.DocumentNode, () => ({
    __proto__: dart.getGetters(ast.DocumentNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.DocumentNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.DocumentNode, () => ({
    __proto__: dart.getFields(ast.DocumentNode.__proto__),
    definitions: dart.finalFieldType(core.List$(ast.DefinitionNode))
  }));
  const name$ = dart.privateName(ast, "DefinitionNode.name");
  ast.DefinitionNode = class DefinitionNode extends ast.Node {
    get name() {
      return this[name$];
    }
    set name(value) {
      super.name = value;
    }
  };
  (ast.DefinitionNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[name$] = name;
    ast.DefinitionNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.DefinitionNode.prototype;
  dart.addTypeTests(ast.DefinitionNode);
  dart.setLibraryUri(ast.DefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.DefinitionNode, () => ({
    __proto__: dart.getFields(ast.DefinitionNode.__proto__),
    name: dart.finalFieldType(ast.NameNode)
  }));
  ast.ExecutableDefinitionNode = class ExecutableDefinitionNode extends ast.DefinitionNode {};
  (ast.ExecutableDefinitionNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let span = opts && 'span' in opts ? opts.span : null;
    ast.ExecutableDefinitionNode.__proto__.new.call(this, {name: name, span: span});
    ;
  }).prototype = ast.ExecutableDefinitionNode.prototype;
  dart.addTypeTests(ast.ExecutableDefinitionNode);
  dart.setLibraryUri(ast.ExecutableDefinitionNode, "package:gql/src/ast/ast.dart");
  const _name$ = dart.privateName(ast, "_name");
  let C4;
  let C5;
  let C6;
  let C7;
  ast.OperationType = class OperationType extends core.Object {
    toString() {
      return this[_name$];
    }
  };
  (ast.OperationType.new = function(index, _name) {
    this.index = index;
    this[_name$] = _name;
    ;
  }).prototype = ast.OperationType.prototype;
  dart.addTypeTests(ast.OperationType);
  dart.setLibraryUri(ast.OperationType, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.OperationType, () => ({
    __proto__: dart.getFields(ast.OperationType.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(ast.OperationType, ['toString']);
  ast.OperationType.query = C4 || CT.C4;
  ast.OperationType.mutation = C5 || CT.C5;
  ast.OperationType.subscription = C6 || CT.C6;
  ast.OperationType.values = C7 || CT.C7;
  let C8;
  let C9;
  let C10;
  let C11;
  let C12;
  let C13;
  let C14;
  let C15;
  let C16;
  let C17;
  let C18;
  let C19;
  let C20;
  let C21;
  let C22;
  let C23;
  let C24;
  let C25;
  let C26;
  ast.DirectiveLocation = class DirectiveLocation extends core.Object {
    toString() {
      return this[_name$];
    }
  };
  (ast.DirectiveLocation.new = function(index, _name) {
    this.index = index;
    this[_name$] = _name;
    ;
  }).prototype = ast.DirectiveLocation.prototype;
  dart.addTypeTests(ast.DirectiveLocation);
  dart.setLibraryUri(ast.DirectiveLocation, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.DirectiveLocation, () => ({
    __proto__: dart.getFields(ast.DirectiveLocation.__proto__),
    index: dart.finalFieldType(core.int),
    [_name$]: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(ast.DirectiveLocation, ['toString']);
  ast.DirectiveLocation.query = C8 || CT.C8;
  ast.DirectiveLocation.mutation = C9 || CT.C9;
  ast.DirectiveLocation.subscription = C10 || CT.C10;
  ast.DirectiveLocation.field = C11 || CT.C11;
  ast.DirectiveLocation.fragmentDefinition = C12 || CT.C12;
  ast.DirectiveLocation.fragmentSpread = C13 || CT.C13;
  ast.DirectiveLocation.inlineFragment = C14 || CT.C14;
  ast.DirectiveLocation.schema = C15 || CT.C15;
  ast.DirectiveLocation.scalar = C16 || CT.C16;
  ast.DirectiveLocation.object = C17 || CT.C17;
  ast.DirectiveLocation.fieldDefinition = C18 || CT.C18;
  ast.DirectiveLocation.argumentDefinition = C19 || CT.C19;
  ast.DirectiveLocation.interface = C20 || CT.C20;
  ast.DirectiveLocation.union = C21 || CT.C21;
  ast.DirectiveLocation.enumDefinition = C22 || CT.C22;
  ast.DirectiveLocation.enumValue = C23 || CT.C23;
  ast.DirectiveLocation.inputObject = C24 || CT.C24;
  ast.DirectiveLocation.inputFieldDefinition = C25 || CT.C25;
  ast.DirectiveLocation.values = C26 || CT.C26;
  let C27;
  let C28;
  const type$ = dart.privateName(ast, "OperationDefinitionNode.type");
  const variableDefinitions$ = dart.privateName(ast, "OperationDefinitionNode.variableDefinitions");
  const directives$ = dart.privateName(ast, "OperationDefinitionNode.directives");
  const selectionSet$ = dart.privateName(ast, "OperationDefinitionNode.selectionSet");
  ast.OperationDefinitionNode = class OperationDefinitionNode extends ast.ExecutableDefinitionNode {
    get type() {
      return this[type$];
    }
    set type(value) {
      super.type = value;
    }
    get variableDefinitions() {
      return this[variableDefinitions$];
    }
    set variableDefinitions(value) {
      super.variableDefinitions = value;
    }
    get directives() {
      return this[directives$];
    }
    set directives(value) {
      super.directives = value;
    }
    get selectionSet() {
      return this[selectionSet$];
    }
    set selectionSet(value) {
      super.selectionSet = value;
    }
    accept(R, v) {
      return v.visitOperationDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.type, this.selectionSet, this.variableDefinitions, this.directives]);
    }
  };
  (ast.OperationDefinitionNode.new = function(opts) {
    let type = opts && 'type' in opts ? opts.type : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let variableDefinitions = opts && 'variableDefinitions' in opts ? opts.variableDefinitions : C27 || CT.C27;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let selectionSet = opts && 'selectionSet' in opts ? opts.selectionSet : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[type$] = type;
    this[variableDefinitions$] = variableDefinitions;
    this[directives$] = directives;
    this[selectionSet$] = selectionSet;
    if (!(variableDefinitions != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 146, 16, "variableDefinitions != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 147, 16, "directives != null");
    if (!(selectionSet != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 148, 16, "selectionSet != null");
    ast.OperationDefinitionNode.__proto__.new.call(this, {name: name, span: span});
    ;
  }).prototype = ast.OperationDefinitionNode.prototype;
  dart.addTypeTests(ast.OperationDefinitionNode);
  dart.setMethodSignature(ast.OperationDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.OperationDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.OperationDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.OperationDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.OperationDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.OperationDefinitionNode, () => ({
    __proto__: dart.getFields(ast.OperationDefinitionNode.__proto__),
    type: dart.finalFieldType(ast.OperationType),
    variableDefinitions: dart.finalFieldType(core.List$(ast.VariableDefinitionNode)),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode)),
    selectionSet: dart.finalFieldType(ast.SelectionSetNode)
  }));
  let C29;
  const selections$ = dart.privateName(ast, "SelectionSetNode.selections");
  ast.SelectionSetNode = class SelectionSetNode extends ast.Node {
    get selections() {
      return this[selections$];
    }
    set selections(value) {
      super.selections = value;
    }
    accept(R, v) {
      return v.visitSelectionSetNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.selections]);
    }
  };
  (ast.SelectionSetNode.new = function(opts) {
    let selections = opts && 'selections' in opts ? opts.selections : C29 || CT.C29;
    let span = opts && 'span' in opts ? opts.span : null;
    this[selections$] = selections;
    ast.SelectionSetNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.SelectionSetNode.prototype;
  dart.addTypeTests(ast.SelectionSetNode);
  dart.setMethodSignature(ast.SelectionSetNode, () => ({
    __proto__: dart.getMethods(ast.SelectionSetNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.SelectionSetNode, () => ({
    __proto__: dart.getGetters(ast.SelectionSetNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.SelectionSetNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.SelectionSetNode, () => ({
    __proto__: dart.getFields(ast.SelectionSetNode.__proto__),
    selections: dart.finalFieldType(core.List$(ast.SelectionNode))
  }));
  ast.SelectionNode = class SelectionNode extends ast.Node {};
  (ast.SelectionNode.new = function(span) {
    ast.SelectionNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.SelectionNode.prototype;
  dart.addTypeTests(ast.SelectionNode);
  dart.setLibraryUri(ast.SelectionNode, "package:gql/src/ast/ast.dart");
  let C30;
  const alias$ = dart.privateName(ast, "FieldNode.alias");
  const name$0 = dart.privateName(ast, "FieldNode.name");
  const arguments$ = dart.privateName(ast, "FieldNode.arguments");
  const directives$0 = dart.privateName(ast, "FieldNode.directives");
  const selectionSet$0 = dart.privateName(ast, "FieldNode.selectionSet");
  ast.FieldNode = class FieldNode extends ast.SelectionNode {
    get alias() {
      return this[alias$];
    }
    set alias(value) {
      super.alias = value;
    }
    get name() {
      return this[name$0];
    }
    set name(value) {
      super.name = value;
    }
    get arguments() {
      return this[arguments$];
    }
    set arguments(value) {
      super.arguments = value;
    }
    get directives() {
      return this[directives$0];
    }
    set directives(value) {
      super.directives = value;
    }
    get selectionSet() {
      return this[selectionSet$0];
    }
    set selectionSet(value) {
      super.selectionSet = value;
    }
    accept(R, v) {
      return v.visitFieldNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.alias, this.name, this.selectionSet, this.arguments, this.directives]);
    }
  };
  (ast.FieldNode.new = function(opts) {
    let alias = opts && 'alias' in opts ? opts.alias : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let $arguments = opts && 'arguments' in opts ? opts.arguments : C30 || CT.C30;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let selectionSet = opts && 'selectionSet' in opts ? opts.selectionSet : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[alias$] = alias;
    this[name$0] = name;
    this[arguments$] = $arguments;
    this[directives$0] = directives;
    this[selectionSet$0] = selectionSet;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 206, 16, "name != null");
    if (!($arguments != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 207, 16, "arguments != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 208, 16, "directives != null");
    ast.FieldNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.FieldNode.prototype;
  dart.addTypeTests(ast.FieldNode);
  dart.setMethodSignature(ast.FieldNode, () => ({
    __proto__: dart.getMethods(ast.FieldNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.FieldNode, () => ({
    __proto__: dart.getGetters(ast.FieldNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.FieldNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.FieldNode, () => ({
    __proto__: dart.getFields(ast.FieldNode.__proto__),
    alias: dart.finalFieldType(ast.NameNode),
    name: dart.finalFieldType(ast.NameNode),
    arguments: dart.finalFieldType(core.List$(ast.ArgumentNode)),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode)),
    selectionSet: dart.finalFieldType(ast.SelectionSetNode)
  }));
  const name$1 = dart.privateName(ast, "ArgumentNode.name");
  const value$ = dart.privateName(ast, "ArgumentNode.value");
  ast.ArgumentNode = class ArgumentNode extends ast.Node {
    get name() {
      return this[name$1];
    }
    set name(value) {
      super.name = value;
    }
    get value() {
      return this[value$];
    }
    set value(value) {
      super.value = value;
    }
    accept(R, v) {
      return v.visitArgumentNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.value]);
    }
  };
  (ast.ArgumentNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let value = opts && 'value' in opts ? opts.value : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[name$1] = name;
    this[value$] = value;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 233, 16, "name != null");
    if (!(value != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 234, 16, "value != null");
    ast.ArgumentNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.ArgumentNode.prototype;
  dart.addTypeTests(ast.ArgumentNode);
  dart.setMethodSignature(ast.ArgumentNode, () => ({
    __proto__: dart.getMethods(ast.ArgumentNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ArgumentNode, () => ({
    __proto__: dart.getGetters(ast.ArgumentNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ArgumentNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.ArgumentNode, () => ({
    __proto__: dart.getFields(ast.ArgumentNode.__proto__),
    name: dart.finalFieldType(ast.NameNode),
    value: dart.finalFieldType(ast.ValueNode)
  }));
  const name$2 = dart.privateName(ast, "FragmentSpreadNode.name");
  const directives$1 = dart.privateName(ast, "FragmentSpreadNode.directives");
  ast.FragmentSpreadNode = class FragmentSpreadNode extends ast.SelectionNode {
    get name() {
      return this[name$2];
    }
    set name(value) {
      super.name = value;
    }
    get directives() {
      return this[directives$1];
    }
    set directives(value) {
      super.directives = value;
    }
    accept(R, v) {
      return v.visitFragmentSpreadNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.directives]);
    }
  };
  (ast.FragmentSpreadNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[name$2] = name;
    this[directives$1] = directives;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 256, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 257, 16, "directives != null");
    ast.FragmentSpreadNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.FragmentSpreadNode.prototype;
  dart.addTypeTests(ast.FragmentSpreadNode);
  dart.setMethodSignature(ast.FragmentSpreadNode, () => ({
    __proto__: dart.getMethods(ast.FragmentSpreadNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.FragmentSpreadNode, () => ({
    __proto__: dart.getGetters(ast.FragmentSpreadNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.FragmentSpreadNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.FragmentSpreadNode, () => ({
    __proto__: dart.getFields(ast.FragmentSpreadNode.__proto__),
    name: dart.finalFieldType(ast.NameNode),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode))
  }));
  const typeCondition$ = dart.privateName(ast, "InlineFragmentNode.typeCondition");
  const directives$2 = dart.privateName(ast, "InlineFragmentNode.directives");
  const selectionSet$1 = dart.privateName(ast, "InlineFragmentNode.selectionSet");
  ast.InlineFragmentNode = class InlineFragmentNode extends ast.SelectionNode {
    get typeCondition() {
      return this[typeCondition$];
    }
    set typeCondition(value) {
      super.typeCondition = value;
    }
    get directives() {
      return this[directives$2];
    }
    set directives(value) {
      super.directives = value;
    }
    get selectionSet() {
      return this[selectionSet$1];
    }
    set selectionSet(value) {
      super.selectionSet = value;
    }
    accept(R, v) {
      return v.visitInlineFragmentNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.typeCondition, this.selectionSet, this.directives]);
    }
  };
  (ast.InlineFragmentNode.new = function(opts) {
    let typeCondition = opts && 'typeCondition' in opts ? opts.typeCondition : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let selectionSet = opts && 'selectionSet' in opts ? opts.selectionSet : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[typeCondition$] = typeCondition;
    this[directives$2] = directives;
    this[selectionSet$1] = selectionSet;
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 282, 16, "directives != null");
    if (!(selectionSet != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 283, 16, "selectionSet != null");
    ast.InlineFragmentNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.InlineFragmentNode.prototype;
  dart.addTypeTests(ast.InlineFragmentNode);
  dart.setMethodSignature(ast.InlineFragmentNode, () => ({
    __proto__: dart.getMethods(ast.InlineFragmentNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.InlineFragmentNode, () => ({
    __proto__: dart.getGetters(ast.InlineFragmentNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.InlineFragmentNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.InlineFragmentNode, () => ({
    __proto__: dart.getFields(ast.InlineFragmentNode.__proto__),
    typeCondition: dart.finalFieldType(ast.TypeConditionNode),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode)),
    selectionSet: dart.finalFieldType(ast.SelectionSetNode)
  }));
  const typeCondition$0 = dart.privateName(ast, "FragmentDefinitionNode.typeCondition");
  const directives$3 = dart.privateName(ast, "FragmentDefinitionNode.directives");
  const selectionSet$2 = dart.privateName(ast, "FragmentDefinitionNode.selectionSet");
  ast.FragmentDefinitionNode = class FragmentDefinitionNode extends ast.ExecutableDefinitionNode {
    get typeCondition() {
      return this[typeCondition$0];
    }
    set typeCondition(value) {
      super.typeCondition = value;
    }
    get directives() {
      return this[directives$3];
    }
    set directives(value) {
      super.directives = value;
    }
    get selectionSet() {
      return this[selectionSet$2];
    }
    set selectionSet(value) {
      super.selectionSet = value;
    }
    accept(R, v) {
      return v.visitFragmentDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.typeCondition, this.selectionSet, this.directives]);
    }
  };
  (ast.FragmentDefinitionNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let typeCondition = opts && 'typeCondition' in opts ? opts.typeCondition : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let selectionSet = opts && 'selectionSet' in opts ? opts.selectionSet : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[typeCondition$0] = typeCondition;
    this[directives$3] = directives;
    this[selectionSet$2] = selectionSet;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 310, 16, "name != null");
    if (!(typeCondition != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 311, 16, "typeCondition != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 312, 16, "directives != null");
    if (!(selectionSet != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 313, 16, "selectionSet != null");
    ast.FragmentDefinitionNode.__proto__.new.call(this, {name: name, span: span});
    ;
  }).prototype = ast.FragmentDefinitionNode.prototype;
  dart.addTypeTests(ast.FragmentDefinitionNode);
  dart.setMethodSignature(ast.FragmentDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.FragmentDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.FragmentDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.FragmentDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.FragmentDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.FragmentDefinitionNode, () => ({
    __proto__: dart.getFields(ast.FragmentDefinitionNode.__proto__),
    typeCondition: dart.finalFieldType(ast.TypeConditionNode),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode)),
    selectionSet: dart.finalFieldType(ast.SelectionSetNode)
  }));
  const on$ = dart.privateName(ast, "TypeConditionNode.on");
  ast.TypeConditionNode = class TypeConditionNode extends ast.Node {
    get on() {
      return this[on$];
    }
    set on(value) {
      super.on = value;
    }
    accept(R, v) {
      return v.visitTypeConditionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.on]);
    }
  };
  (ast.TypeConditionNode.new = function(opts) {
    let on = opts && 'on' in opts ? opts.on : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[on$] = on;
    if (!(on != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 337, 16, "on != null");
    ast.TypeConditionNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.TypeConditionNode.prototype;
  dart.addTypeTests(ast.TypeConditionNode);
  dart.setMethodSignature(ast.TypeConditionNode, () => ({
    __proto__: dart.getMethods(ast.TypeConditionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.TypeConditionNode, () => ({
    __proto__: dart.getGetters(ast.TypeConditionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.TypeConditionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.TypeConditionNode, () => ({
    __proto__: dart.getFields(ast.TypeConditionNode.__proto__),
    on: dart.finalFieldType(ast.NamedTypeNode)
  }));
  ast.ValueNode = class ValueNode extends ast.Node {};
  (ast.ValueNode.new = function(span) {
    ast.ValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.ValueNode.prototype;
  dart.addTypeTests(ast.ValueNode);
  dart.setLibraryUri(ast.ValueNode, "package:gql/src/ast/ast.dart");
  const name$3 = dart.privateName(ast, "VariableNode.name");
  ast.VariableNode = class VariableNode extends ast.ValueNode {
    get name() {
      return this[name$3];
    }
    set name(value) {
      super.name = value;
    }
    accept(R, v) {
      return v.visitVariableNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name]);
    }
  };
  (ast.VariableNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[name$3] = name;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 359, 16, "name != null");
    ast.VariableNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.VariableNode.prototype;
  dart.addTypeTests(ast.VariableNode);
  dart.setMethodSignature(ast.VariableNode, () => ({
    __proto__: dart.getMethods(ast.VariableNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.VariableNode, () => ({
    __proto__: dart.getGetters(ast.VariableNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.VariableNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.VariableNode, () => ({
    __proto__: dart.getFields(ast.VariableNode.__proto__),
    name: dart.finalFieldType(ast.NameNode)
  }));
  const value$0 = dart.privateName(ast, "IntValueNode.value");
  ast.IntValueNode = class IntValueNode extends ast.ValueNode {
    get value() {
      return this[value$0];
    }
    set value(value) {
      super.value = value;
    }
    accept(R, v) {
      return v.visitIntValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.value]);
    }
  };
  (ast.IntValueNode.new = function(opts) {
    let value = opts && 'value' in opts ? opts.value : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[value$0] = value;
    if (!(value != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 377, 16, "value != null");
    ast.IntValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.IntValueNode.prototype;
  dart.addTypeTests(ast.IntValueNode);
  dart.setMethodSignature(ast.IntValueNode, () => ({
    __proto__: dart.getMethods(ast.IntValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.IntValueNode, () => ({
    __proto__: dart.getGetters(ast.IntValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.IntValueNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.IntValueNode, () => ({
    __proto__: dart.getFields(ast.IntValueNode.__proto__),
    value: dart.finalFieldType(core.String)
  }));
  const value$1 = dart.privateName(ast, "FloatValueNode.value");
  ast.FloatValueNode = class FloatValueNode extends ast.ValueNode {
    get value() {
      return this[value$1];
    }
    set value(value) {
      super.value = value;
    }
    accept(R, v) {
      return v.visitFloatValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.value]);
    }
  };
  (ast.FloatValueNode.new = function(opts) {
    let value = opts && 'value' in opts ? opts.value : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[value$1] = value;
    if (!(value != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 395, 16, "value != null");
    ast.FloatValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.FloatValueNode.prototype;
  dart.addTypeTests(ast.FloatValueNode);
  dart.setMethodSignature(ast.FloatValueNode, () => ({
    __proto__: dart.getMethods(ast.FloatValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.FloatValueNode, () => ({
    __proto__: dart.getGetters(ast.FloatValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.FloatValueNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.FloatValueNode, () => ({
    __proto__: dart.getFields(ast.FloatValueNode.__proto__),
    value: dart.finalFieldType(core.String)
  }));
  const value$2 = dart.privateName(ast, "StringValueNode.value");
  const isBlock$ = dart.privateName(ast, "StringValueNode.isBlock");
  ast.StringValueNode = class StringValueNode extends ast.ValueNode {
    get value() {
      return this[value$2];
    }
    set value(value) {
      super.value = value;
    }
    get isBlock() {
      return this[isBlock$];
    }
    set isBlock(value) {
      super.isBlock = value;
    }
    accept(R, v) {
      return v.visitStringValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.value, this.isBlock]);
    }
  };
  (ast.StringValueNode.new = function(opts) {
    let value = opts && 'value' in opts ? opts.value : null;
    let isBlock = opts && 'isBlock' in opts ? opts.isBlock : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[value$2] = value;
    this[isBlock$] = isBlock;
    if (!(value != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 416, 16, "value != null");
    if (!(isBlock != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 417, 16, "isBlock != null");
    ast.StringValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.StringValueNode.prototype;
  dart.addTypeTests(ast.StringValueNode);
  dart.setMethodSignature(ast.StringValueNode, () => ({
    __proto__: dart.getMethods(ast.StringValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.StringValueNode, () => ({
    __proto__: dart.getGetters(ast.StringValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.StringValueNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.StringValueNode, () => ({
    __proto__: dart.getFields(ast.StringValueNode.__proto__),
    value: dart.finalFieldType(core.String),
    isBlock: dart.finalFieldType(core.bool)
  }));
  const value$3 = dart.privateName(ast, "BooleanValueNode.value");
  ast.BooleanValueNode = class BooleanValueNode extends ast.ValueNode {
    get value() {
      return this[value$3];
    }
    set value(value) {
      super.value = value;
    }
    accept(R, v) {
      return v.visitBooleanValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.value]);
    }
  };
  (ast.BooleanValueNode.new = function(opts) {
    let value = opts && 'value' in opts ? opts.value : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[value$3] = value;
    if (!(value != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 436, 16, "value != null");
    ast.BooleanValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.BooleanValueNode.prototype;
  dart.addTypeTests(ast.BooleanValueNode);
  dart.setMethodSignature(ast.BooleanValueNode, () => ({
    __proto__: dart.getMethods(ast.BooleanValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.BooleanValueNode, () => ({
    __proto__: dart.getGetters(ast.BooleanValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.BooleanValueNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.BooleanValueNode, () => ({
    __proto__: dart.getFields(ast.BooleanValueNode.__proto__),
    value: dart.finalFieldType(core.bool)
  }));
  ast.NullValueNode = class NullValueNode extends ast.ValueNode {
    accept(R, v) {
      return v.visitNullValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([]);
    }
  };
  (ast.NullValueNode.new = function(opts) {
    let span = opts && 'span' in opts ? opts.span : null;
    ast.NullValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.NullValueNode.prototype;
  dart.addTypeTests(ast.NullValueNode);
  dart.setMethodSignature(ast.NullValueNode, () => ({
    __proto__: dart.getMethods(ast.NullValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.NullValueNode, () => ({
    __proto__: dart.getGetters(ast.NullValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.NullValueNode, "package:gql/src/ast/ast.dart");
  const name$4 = dart.privateName(ast, "EnumValueNode.name");
  ast.EnumValueNode = class EnumValueNode extends ast.ValueNode {
    get name() {
      return this[name$4];
    }
    set name(value) {
      super.name = value;
    }
    accept(R, v) {
      return v.visitEnumValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name]);
    }
  };
  (ast.EnumValueNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[name$4] = name;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 466, 16, "name != null");
    ast.EnumValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.EnumValueNode.prototype;
  dart.addTypeTests(ast.EnumValueNode);
  dart.setMethodSignature(ast.EnumValueNode, () => ({
    __proto__: dart.getMethods(ast.EnumValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.EnumValueNode, () => ({
    __proto__: dart.getGetters(ast.EnumValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.EnumValueNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.EnumValueNode, () => ({
    __proto__: dart.getFields(ast.EnumValueNode.__proto__),
    name: dart.finalFieldType(ast.NameNode)
  }));
  let C31;
  const values$ = dart.privateName(ast, "ListValueNode.values");
  ast.ListValueNode = class ListValueNode extends ast.ValueNode {
    get values() {
      return this[values$];
    }
    set values(value) {
      super.values = value;
    }
    accept(R, v) {
      return v.visitListValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.values]);
    }
  };
  (ast.ListValueNode.new = function(opts) {
    let values = opts && 'values' in opts ? opts.values : C31 || CT.C31;
    let span = opts && 'span' in opts ? opts.span : null;
    this[values$] = values;
    ast.ListValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.ListValueNode.prototype;
  dart.addTypeTests(ast.ListValueNode);
  dart.setMethodSignature(ast.ListValueNode, () => ({
    __proto__: dart.getMethods(ast.ListValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ListValueNode, () => ({
    __proto__: dart.getGetters(ast.ListValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ListValueNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.ListValueNode, () => ({
    __proto__: dart.getFields(ast.ListValueNode.__proto__),
    values: dart.finalFieldType(core.List$(ast.ValueNode))
  }));
  let C32;
  const fields$ = dart.privateName(ast, "ObjectValueNode.fields");
  ast.ObjectValueNode = class ObjectValueNode extends ast.ValueNode {
    get fields() {
      return this[fields$];
    }
    set fields(value) {
      super.fields = value;
    }
    accept(R, v) {
      return v.visitObjectValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.fields]);
    }
  };
  (ast.ObjectValueNode.new = function(opts) {
    let fields = opts && 'fields' in opts ? opts.fields : C32 || CT.C32;
    let span = opts && 'span' in opts ? opts.span : null;
    this[fields$] = fields;
    ast.ObjectValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.ObjectValueNode.prototype;
  dart.addTypeTests(ast.ObjectValueNode);
  dart.setMethodSignature(ast.ObjectValueNode, () => ({
    __proto__: dart.getMethods(ast.ObjectValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ObjectValueNode, () => ({
    __proto__: dart.getGetters(ast.ObjectValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ObjectValueNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.ObjectValueNode, () => ({
    __proto__: dart.getFields(ast.ObjectValueNode.__proto__),
    fields: dart.finalFieldType(core.List$(ast.ObjectFieldNode))
  }));
  const name$5 = dart.privateName(ast, "ObjectFieldNode.name");
  const value$4 = dart.privateName(ast, "ObjectFieldNode.value");
  ast.ObjectFieldNode = class ObjectFieldNode extends ast.Node {
    get name() {
      return this[name$5];
    }
    set name(value) {
      super.name = value;
    }
    get value() {
      return this[value$4];
    }
    set value(value) {
      super.value = value;
    }
    accept(R, v) {
      return v.visitObjectFieldNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.value]);
    }
  };
  (ast.ObjectFieldNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let value = opts && 'value' in opts ? opts.value : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[name$5] = name;
    this[value$4] = value;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 521, 16, "name != null");
    if (!(value != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 522, 16, "value != null");
    ast.ObjectFieldNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.ObjectFieldNode.prototype;
  dart.addTypeTests(ast.ObjectFieldNode);
  dart.setMethodSignature(ast.ObjectFieldNode, () => ({
    __proto__: dart.getMethods(ast.ObjectFieldNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ObjectFieldNode, () => ({
    __proto__: dart.getGetters(ast.ObjectFieldNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ObjectFieldNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.ObjectFieldNode, () => ({
    __proto__: dart.getFields(ast.ObjectFieldNode.__proto__),
    name: dart.finalFieldType(ast.NameNode),
    value: dart.finalFieldType(ast.ValueNode)
  }));
  const variable$ = dart.privateName(ast, "VariableDefinitionNode.variable");
  const type$0 = dart.privateName(ast, "VariableDefinitionNode.type");
  const defaultValue$ = dart.privateName(ast, "VariableDefinitionNode.defaultValue");
  const directives$4 = dart.privateName(ast, "VariableDefinitionNode.directives");
  ast.VariableDefinitionNode = class VariableDefinitionNode extends ast.Node {
    get variable() {
      return this[variable$];
    }
    set variable(value) {
      super.variable = value;
    }
    get type() {
      return this[type$0];
    }
    set type(value) {
      super.type = value;
    }
    get defaultValue() {
      return this[defaultValue$];
    }
    set defaultValue(value) {
      super.defaultValue = value;
    }
    get directives() {
      return this[directives$4];
    }
    set directives(value) {
      super.directives = value;
    }
    accept(R, v) {
      return v.visitVariableDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.variable, this.type, this.defaultValue, this.directives]);
    }
  };
  (ast.VariableDefinitionNode.new = function(opts) {
    let variable = opts && 'variable' in opts ? opts.variable : null;
    let type = opts && 'type' in opts ? opts.type : null;
    let defaultValue = opts && 'defaultValue' in opts ? opts.defaultValue : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[variable$] = variable;
    this[type$0] = type;
    this[defaultValue$] = defaultValue;
    this[directives$4] = directives;
    if (!(variable != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 550, 16, "variable != null");
    if (!(type != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 551, 16, "type != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 552, 16, "directives != null");
    ast.VariableDefinitionNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.VariableDefinitionNode.prototype;
  dart.addTypeTests(ast.VariableDefinitionNode);
  dart.setMethodSignature(ast.VariableDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.VariableDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.VariableDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.VariableDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.VariableDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.VariableDefinitionNode, () => ({
    __proto__: dart.getFields(ast.VariableDefinitionNode.__proto__),
    variable: dart.finalFieldType(ast.VariableNode),
    type: dart.finalFieldType(ast.TypeNode),
    defaultValue: dart.finalFieldType(ast.DefaultValueNode),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode))
  }));
  const value$5 = dart.privateName(ast, "DefaultValueNode.value");
  ast.DefaultValueNode = class DefaultValueNode extends ast.Node {
    get value() {
      return this[value$5];
    }
    set value(value) {
      super.value = value;
    }
    accept(R, v) {
      return v.visitDefaultValueNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.value]);
    }
  };
  (ast.DefaultValueNode.new = function(opts) {
    let value = opts && 'value' in opts ? opts.value : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[value$5] = value;
    ast.DefaultValueNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.DefaultValueNode.prototype;
  dart.addTypeTests(ast.DefaultValueNode);
  dart.setMethodSignature(ast.DefaultValueNode, () => ({
    __proto__: dart.getMethods(ast.DefaultValueNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.DefaultValueNode, () => ({
    __proto__: dart.getGetters(ast.DefaultValueNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.DefaultValueNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.DefaultValueNode, () => ({
    __proto__: dart.getFields(ast.DefaultValueNode.__proto__),
    value: dart.finalFieldType(ast.ValueNode)
  }));
  const isNonNull$ = dart.privateName(ast, "TypeNode.isNonNull");
  ast.TypeNode = class TypeNode extends ast.Node {
    get isNonNull() {
      return this[isNonNull$];
    }
    set isNonNull(value) {
      super.isNonNull = value;
    }
  };
  (ast.TypeNode.new = function(isNonNull, span) {
    this[isNonNull$] = isNonNull;
    if (!(isNonNull != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 588, 16, "isNonNull != null");
    ast.TypeNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.TypeNode.prototype;
  dart.addTypeTests(ast.TypeNode);
  dart.setLibraryUri(ast.TypeNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.TypeNode, () => ({
    __proto__: dart.getFields(ast.TypeNode.__proto__),
    isNonNull: dart.finalFieldType(core.bool)
  }));
  const name$6 = dart.privateName(ast, "NamedTypeNode.name");
  ast.NamedTypeNode = class NamedTypeNode extends ast.TypeNode {
    get name() {
      return this[name$6];
    }
    set name(value) {
      super.name = value;
    }
    accept(R, v) {
      return v.visitNamedTypeNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.isNonNull, this.name]);
    }
  };
  (ast.NamedTypeNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let isNonNull = opts && 'isNonNull' in opts ? opts.isNonNull : false;
    let span = opts && 'span' in opts ? opts.span : null;
    this[name$6] = name;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 599, 16, "name != null");
    if (!(isNonNull != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 600, 16, "isNonNull != null");
    ast.NamedTypeNode.__proto__.new.call(this, isNonNull, span);
    ;
  }).prototype = ast.NamedTypeNode.prototype;
  dart.addTypeTests(ast.NamedTypeNode);
  dart.setMethodSignature(ast.NamedTypeNode, () => ({
    __proto__: dart.getMethods(ast.NamedTypeNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.NamedTypeNode, () => ({
    __proto__: dart.getGetters(ast.NamedTypeNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.NamedTypeNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.NamedTypeNode, () => ({
    __proto__: dart.getFields(ast.NamedTypeNode.__proto__),
    name: dart.finalFieldType(ast.NameNode)
  }));
  const type$1 = dart.privateName(ast, "ListTypeNode.type");
  ast.ListTypeNode = class ListTypeNode extends ast.TypeNode {
    get type() {
      return this[type$1];
    }
    set type(value) {
      super.type = value;
    }
    accept(R, v) {
      return v.visitListTypeNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.isNonNull, this.type]);
    }
  };
  (ast.ListTypeNode.new = function(opts) {
    let type = opts && 'type' in opts ? opts.type : null;
    let isNonNull = opts && 'isNonNull' in opts ? opts.isNonNull : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[type$1] = type;
    if (!(type != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 620, 16, "type != null");
    if (!(isNonNull != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 621, 16, "isNonNull != null");
    ast.ListTypeNode.__proto__.new.call(this, isNonNull, span);
    ;
  }).prototype = ast.ListTypeNode.prototype;
  dart.addTypeTests(ast.ListTypeNode);
  dart.setMethodSignature(ast.ListTypeNode, () => ({
    __proto__: dart.getMethods(ast.ListTypeNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ListTypeNode, () => ({
    __proto__: dart.getGetters(ast.ListTypeNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ListTypeNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.ListTypeNode, () => ({
    __proto__: dart.getFields(ast.ListTypeNode.__proto__),
    type: dart.finalFieldType(ast.TypeNode)
  }));
  const name$7 = dart.privateName(ast, "DirectiveNode.name");
  const arguments$0 = dart.privateName(ast, "DirectiveNode.arguments");
  ast.DirectiveNode = class DirectiveNode extends ast.Node {
    get name() {
      return this[name$7];
    }
    set name(value) {
      super.name = value;
    }
    get arguments() {
      return this[arguments$0];
    }
    set arguments(value) {
      super.arguments = value;
    }
    accept(R, v) {
      return v.visitDirectiveNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.arguments]);
    }
  };
  (ast.DirectiveNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let $arguments = opts && 'arguments' in opts ? opts.arguments : C30 || CT.C30;
    let span = opts && 'span' in opts ? opts.span : null;
    this[name$7] = name;
    this[arguments$0] = $arguments;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 643, 16, "name != null");
    if (!($arguments != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 644, 16, "arguments != null");
    ast.DirectiveNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.DirectiveNode.prototype;
  dart.addTypeTests(ast.DirectiveNode);
  dart.setMethodSignature(ast.DirectiveNode, () => ({
    __proto__: dart.getMethods(ast.DirectiveNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.DirectiveNode, () => ({
    __proto__: dart.getGetters(ast.DirectiveNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.DirectiveNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.DirectiveNode, () => ({
    __proto__: dart.getFields(ast.DirectiveNode.__proto__),
    name: dart.finalFieldType(ast.NameNode),
    arguments: dart.finalFieldType(core.List$(ast.ArgumentNode))
  }));
  const value$6 = dart.privateName(ast, "NameNode.value");
  ast.NameNode = class NameNode extends ast.Node {
    get value() {
      return this[value$6];
    }
    set value(value) {
      super.value = value;
    }
    accept(R, v) {
      return v.visitNameNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.value]);
    }
  };
  (ast.NameNode.new = function(opts) {
    let value = opts && 'value' in opts ? opts.value : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[value$6] = value;
    if (!(value != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 663, 16, "value != null");
    ast.NameNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.NameNode.prototype;
  dart.addTypeTests(ast.NameNode);
  dart.setMethodSignature(ast.NameNode, () => ({
    __proto__: dart.getMethods(ast.NameNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.NameNode, () => ({
    __proto__: dart.getGetters(ast.NameNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.NameNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.NameNode, () => ({
    __proto__: dart.getFields(ast.NameNode.__proto__),
    value: dart.finalFieldType(core.String)
  }));
  ast.TypeSystemDefinitionNode = class TypeSystemDefinitionNode extends ast.DefinitionNode {};
  (ast.TypeSystemDefinitionNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let span = opts && 'span' in opts ? opts.span : null;
    ast.TypeSystemDefinitionNode.__proto__.new.call(this, {name: name, span: span});
    ;
  }).prototype = ast.TypeSystemDefinitionNode.prototype;
  dart.addTypeTests(ast.TypeSystemDefinitionNode);
  dart.setLibraryUri(ast.TypeSystemDefinitionNode, "package:gql/src/ast/ast.dart");
  const description$ = dart.privateName(ast, "TypeDefinitionNode.description");
  const directives$5 = dart.privateName(ast, "TypeDefinitionNode.directives");
  ast.TypeDefinitionNode = class TypeDefinitionNode extends ast.TypeSystemDefinitionNode {
    get description() {
      return this[description$];
    }
    set description(value) {
      super.description = value;
    }
    get directives() {
      return this[directives$5];
    }
    set directives(value) {
      super.directives = value;
    }
  };
  (ast.TypeDefinitionNode.new = function(opts) {
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[description$] = description;
    this[directives$5] = directives;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 694, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 695, 16, "directives != null");
    ast.TypeDefinitionNode.__proto__.new.call(this, {name: name, span: span});
    ;
  }).prototype = ast.TypeDefinitionNode.prototype;
  dart.addTypeTests(ast.TypeDefinitionNode);
  dart.setLibraryUri(ast.TypeDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.TypeDefinitionNode, () => ({
    __proto__: dart.getFields(ast.TypeDefinitionNode.__proto__),
    description: dart.finalFieldType(ast.StringValueNode),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode))
  }));
  ast.TypeSystemExtensionNode = class TypeSystemExtensionNode extends ast.TypeSystemDefinitionNode {};
  (ast.TypeSystemExtensionNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let span = opts && 'span' in opts ? opts.span : null;
    ast.TypeSystemExtensionNode.__proto__.new.call(this, {name: name, span: span});
    ;
  }).prototype = ast.TypeSystemExtensionNode.prototype;
  dart.addTypeTests(ast.TypeSystemExtensionNode);
  dart.setLibraryUri(ast.TypeSystemExtensionNode, "package:gql/src/ast/ast.dart");
  const directives$6 = dart.privateName(ast, "TypeExtensionNode.directives");
  ast.TypeExtensionNode = class TypeExtensionNode extends ast.TypeSystemExtensionNode {
    get directives() {
      return this[directives$6];
    }
    set directives(value) {
      super.directives = value;
    }
  };
  (ast.TypeExtensionNode.new = function(opts) {
    let span = opts && 'span' in opts ? opts.span : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    this[directives$6] = directives;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 719, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 720, 16, "directives != null");
    ast.TypeExtensionNode.__proto__.new.call(this, {name: name, span: span});
    ;
  }).prototype = ast.TypeExtensionNode.prototype;
  dart.addTypeTests(ast.TypeExtensionNode);
  dart.setLibraryUri(ast.TypeExtensionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.TypeExtensionNode, () => ({
    __proto__: dart.getFields(ast.TypeExtensionNode.__proto__),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode))
  }));
  let C33;
  const directives$7 = dart.privateName(ast, "SchemaDefinitionNode.directives");
  const operationTypes$ = dart.privateName(ast, "SchemaDefinitionNode.operationTypes");
  ast.SchemaDefinitionNode = class SchemaDefinitionNode extends ast.TypeSystemDefinitionNode {
    get directives() {
      return this[directives$7];
    }
    set directives(value) {
      super.directives = value;
    }
    get operationTypes() {
      return this[operationTypes$];
    }
    set operationTypes(value) {
      super.operationTypes = value;
    }
    accept(R, v) {
      return v.visitSchemaDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.directives, this.operationTypes]);
    }
  };
  (ast.SchemaDefinitionNode.new = function(opts) {
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let operationTypes = opts && 'operationTypes' in opts ? opts.operationTypes : C33 || CT.C33;
    let span = opts && 'span' in opts ? opts.span : null;
    this[directives$7] = directives;
    this[operationTypes$] = operationTypes;
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 735, 16, "directives != null");
    if (!(operationTypes != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 736, 16, "operationTypes != null");
    ast.SchemaDefinitionNode.__proto__.new.call(this, {name: null, span: span});
    ;
  }).prototype = ast.SchemaDefinitionNode.prototype;
  dart.addTypeTests(ast.SchemaDefinitionNode);
  dart.setMethodSignature(ast.SchemaDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.SchemaDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.SchemaDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.SchemaDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.SchemaDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.SchemaDefinitionNode, () => ({
    __proto__: dart.getFields(ast.SchemaDefinitionNode.__proto__),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode)),
    operationTypes: dart.finalFieldType(core.List$(ast.OperationTypeDefinitionNode))
  }));
  const operation$ = dart.privateName(ast, "OperationTypeDefinitionNode.operation");
  const type$2 = dart.privateName(ast, "OperationTypeDefinitionNode.type");
  ast.OperationTypeDefinitionNode = class OperationTypeDefinitionNode extends ast.Node {
    get operation() {
      return this[operation$];
    }
    set operation(value) {
      super.operation = value;
    }
    get type() {
      return this[type$2];
    }
    set type(value) {
      super.type = value;
    }
    accept(R, v) {
      return v.visitOperationTypeDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.operation, this.type]);
    }
  };
  (ast.OperationTypeDefinitionNode.new = function(opts) {
    let operation = opts && 'operation' in opts ? opts.operation : null;
    let type = opts && 'type' in opts ? opts.type : null;
    let span = opts && 'span' in opts ? opts.span : null;
    this[operation$] = operation;
    this[type$2] = type;
    if (!(operation != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 760, 16, "operation != null");
    if (!(type != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 761, 16, "type != null");
    ast.OperationTypeDefinitionNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.OperationTypeDefinitionNode.prototype;
  dart.addTypeTests(ast.OperationTypeDefinitionNode);
  dart.setMethodSignature(ast.OperationTypeDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.OperationTypeDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.OperationTypeDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.OperationTypeDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.OperationTypeDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.OperationTypeDefinitionNode, () => ({
    __proto__: dart.getFields(ast.OperationTypeDefinitionNode.__proto__),
    operation: dart.finalFieldType(ast.OperationType),
    type: dart.finalFieldType(ast.NamedTypeNode)
  }));
  ast.ScalarTypeDefinitionNode = class ScalarTypeDefinitionNode extends ast.TypeDefinitionNode {
    accept(R, v) {
      return v.visitScalarTypeDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives]);
    }
  };
  (ast.ScalarTypeDefinitionNode.new = function(opts) {
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 780, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 781, 16, "directives != null");
    ast.ScalarTypeDefinitionNode.__proto__.new.call(this, {span: span, name: name, description: description, directives: directives});
    ;
  }).prototype = ast.ScalarTypeDefinitionNode.prototype;
  dart.addTypeTests(ast.ScalarTypeDefinitionNode);
  dart.setMethodSignature(ast.ScalarTypeDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.ScalarTypeDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ScalarTypeDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.ScalarTypeDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ScalarTypeDefinitionNode, "package:gql/src/ast/ast.dart");
  let C34;
  let C35;
  const interfaces$ = dart.privateName(ast, "ObjectTypeDefinitionNode.interfaces");
  const fields$0 = dart.privateName(ast, "ObjectTypeDefinitionNode.fields");
  ast.ObjectTypeDefinitionNode = class ObjectTypeDefinitionNode extends ast.TypeDefinitionNode {
    get interfaces() {
      return this[interfaces$];
    }
    set interfaces(value) {
      super.interfaces = value;
    }
    get fields() {
      return this[fields$0];
    }
    set fields(value) {
      super.fields = value;
    }
    accept(R, v) {
      return v.visitObjectTypeDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives, this.interfaces, this.fields]);
    }
  };
  (ast.ObjectTypeDefinitionNode.new = function(opts) {
    let interfaces = opts && 'interfaces' in opts ? opts.interfaces : C34 || CT.C34;
    let fields = opts && 'fields' in opts ? opts.fields : C35 || CT.C35;
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[interfaces$] = interfaces;
    this[fields$0] = fields;
    if (!(interfaces != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 811, 16, "interfaces != null");
    if (!(fields != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 812, 16, "fields != null");
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 813, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 814, 16, "directives != null");
    ast.ObjectTypeDefinitionNode.__proto__.new.call(this, {span: span, name: name, description: description, directives: directives});
    ;
  }).prototype = ast.ObjectTypeDefinitionNode.prototype;
  dart.addTypeTests(ast.ObjectTypeDefinitionNode);
  dart.setMethodSignature(ast.ObjectTypeDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.ObjectTypeDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ObjectTypeDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.ObjectTypeDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ObjectTypeDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.ObjectTypeDefinitionNode, () => ({
    __proto__: dart.getFields(ast.ObjectTypeDefinitionNode.__proto__),
    interfaces: dart.finalFieldType(core.List$(ast.NamedTypeNode)),
    fields: dart.finalFieldType(core.List$(ast.FieldDefinitionNode))
  }));
  let C36;
  const description$0 = dart.privateName(ast, "FieldDefinitionNode.description");
  const name$8 = dart.privateName(ast, "FieldDefinitionNode.name");
  const type$3 = dart.privateName(ast, "FieldDefinitionNode.type");
  const directives$8 = dart.privateName(ast, "FieldDefinitionNode.directives");
  const args$ = dart.privateName(ast, "FieldDefinitionNode.args");
  ast.FieldDefinitionNode = class FieldDefinitionNode extends ast.Node {
    get description() {
      return this[description$0];
    }
    set description(value) {
      super.description = value;
    }
    get name() {
      return this[name$8];
    }
    set name(value) {
      super.name = value;
    }
    get type() {
      return this[type$3];
    }
    set type(value) {
      super.type = value;
    }
    get directives() {
      return this[directives$8];
    }
    set directives(value) {
      super.directives = value;
    }
    get args() {
      return this[args$];
    }
    set args(value) {
      super.args = value;
    }
    accept(R, v) {
      return v.visitFieldDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives, this.type, this.args]);
    }
  };
  (ast.FieldDefinitionNode.new = function(opts) {
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let type = opts && 'type' in opts ? opts.type : null;
    let args = opts && 'args' in opts ? opts.args : C36 || CT.C36;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[description$0] = description;
    this[name$8] = name;
    this[type$3] = type;
    this[args$] = args;
    this[directives$8] = directives;
    if (!(type != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 849, 16, "type != null");
    if (!(args != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 850, 16, "args != null");
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 851, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 852, 16, "directives != null");
    ast.FieldDefinitionNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.FieldDefinitionNode.prototype;
  dart.addTypeTests(ast.FieldDefinitionNode);
  dart.setMethodSignature(ast.FieldDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.FieldDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.FieldDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.FieldDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.FieldDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.FieldDefinitionNode, () => ({
    __proto__: dart.getFields(ast.FieldDefinitionNode.__proto__),
    description: dart.finalFieldType(ast.StringValueNode),
    name: dart.finalFieldType(ast.NameNode),
    type: dart.finalFieldType(ast.TypeNode),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode)),
    args: dart.finalFieldType(core.List$(ast.InputValueDefinitionNode))
  }));
  const description$1 = dart.privateName(ast, "InputValueDefinitionNode.description");
  const name$9 = dart.privateName(ast, "InputValueDefinitionNode.name");
  const type$4 = dart.privateName(ast, "InputValueDefinitionNode.type");
  const defaultValue$0 = dart.privateName(ast, "InputValueDefinitionNode.defaultValue");
  const directives$9 = dart.privateName(ast, "InputValueDefinitionNode.directives");
  ast.InputValueDefinitionNode = class InputValueDefinitionNode extends ast.Node {
    get description() {
      return this[description$1];
    }
    set description(value) {
      super.description = value;
    }
    get name() {
      return this[name$9];
    }
    set name(value) {
      super.name = value;
    }
    get type() {
      return this[type$4];
    }
    set type(value) {
      super.type = value;
    }
    get defaultValue() {
      return this[defaultValue$0];
    }
    set defaultValue(value) {
      super.defaultValue = value;
    }
    get directives() {
      return this[directives$9];
    }
    set directives(value) {
      super.directives = value;
    }
    accept(R, v) {
      return v.visitInputValueDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives, this.type, this.defaultValue]);
    }
  };
  (ast.InputValueDefinitionNode.new = function(opts) {
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let type = opts && 'type' in opts ? opts.type : null;
    let defaultValue = opts && 'defaultValue' in opts ? opts.defaultValue : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[description$1] = description;
    this[name$9] = name;
    this[type$4] = type;
    this[defaultValue$0] = defaultValue;
    this[directives$9] = directives;
    if (!(type != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 882, 16, "type != null");
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 883, 16, "name != null");
    if (!(type != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 884, 16, "type != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 885, 16, "directives != null");
    ast.InputValueDefinitionNode.__proto__.new.call(this, span);
    ;
  }).prototype = ast.InputValueDefinitionNode.prototype;
  dart.addTypeTests(ast.InputValueDefinitionNode);
  dart.setMethodSignature(ast.InputValueDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.InputValueDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.InputValueDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.InputValueDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.InputValueDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.InputValueDefinitionNode, () => ({
    __proto__: dart.getFields(ast.InputValueDefinitionNode.__proto__),
    description: dart.finalFieldType(ast.StringValueNode),
    name: dart.finalFieldType(ast.NameNode),
    type: dart.finalFieldType(ast.TypeNode),
    defaultValue: dart.finalFieldType(ast.ValueNode),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode))
  }));
  const fields$1 = dart.privateName(ast, "InterfaceTypeDefinitionNode.fields");
  ast.InterfaceTypeDefinitionNode = class InterfaceTypeDefinitionNode extends ast.TypeDefinitionNode {
    get fields() {
      return this[fields$1];
    }
    set fields(value) {
      super.fields = value;
    }
    accept(R, v) {
      return v.visitInterfaceTypeDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives, this.fields]);
    }
  };
  (ast.InterfaceTypeDefinitionNode.new = function(opts) {
    let fields = opts && 'fields' in opts ? opts.fields : C35 || CT.C35;
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[fields$1] = fields;
    if (!(fields != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 910, 16, "fields != null");
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 911, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 912, 16, "directives != null");
    ast.InterfaceTypeDefinitionNode.__proto__.new.call(this, {span: span, name: name, description: description, directives: directives});
    ;
  }).prototype = ast.InterfaceTypeDefinitionNode.prototype;
  dart.addTypeTests(ast.InterfaceTypeDefinitionNode);
  dart.setMethodSignature(ast.InterfaceTypeDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.InterfaceTypeDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.InterfaceTypeDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.InterfaceTypeDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.InterfaceTypeDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.InterfaceTypeDefinitionNode, () => ({
    __proto__: dart.getFields(ast.InterfaceTypeDefinitionNode.__proto__),
    fields: dart.finalFieldType(core.List$(ast.FieldDefinitionNode))
  }));
  const types$ = dart.privateName(ast, "UnionTypeDefinitionNode.types");
  ast.UnionTypeDefinitionNode = class UnionTypeDefinitionNode extends ast.TypeDefinitionNode {
    get types() {
      return this[types$];
    }
    set types(value) {
      super.types = value;
    }
    accept(R, v) {
      return v.visitUnionTypeDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives, this.types]);
    }
  };
  (ast.UnionTypeDefinitionNode.new = function(opts) {
    let types = opts && 'types' in opts ? opts.types : C34 || CT.C34;
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[types$] = types;
    if (!(types != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 941, 16, "types != null");
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 942, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 943, 16, "directives != null");
    ast.UnionTypeDefinitionNode.__proto__.new.call(this, {span: span, name: name, description: description, directives: directives});
    ;
  }).prototype = ast.UnionTypeDefinitionNode.prototype;
  dart.addTypeTests(ast.UnionTypeDefinitionNode);
  dart.setMethodSignature(ast.UnionTypeDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.UnionTypeDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.UnionTypeDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.UnionTypeDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.UnionTypeDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.UnionTypeDefinitionNode, () => ({
    __proto__: dart.getFields(ast.UnionTypeDefinitionNode.__proto__),
    types: dart.finalFieldType(core.List$(ast.NamedTypeNode))
  }));
  let C37;
  const values$0 = dart.privateName(ast, "EnumTypeDefinitionNode.values");
  ast.EnumTypeDefinitionNode = class EnumTypeDefinitionNode extends ast.TypeDefinitionNode {
    get values() {
      return this[values$0];
    }
    set values(value) {
      super.values = value;
    }
    accept(R, v) {
      return v.visitEnumTypeDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives, this.values]);
    }
  };
  (ast.EnumTypeDefinitionNode.new = function(opts) {
    let values = opts && 'values' in opts ? opts.values : C37 || CT.C37;
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[values$0] = values;
    if (!(values != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 972, 16, "values != null");
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 973, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 974, 16, "directives != null");
    ast.EnumTypeDefinitionNode.__proto__.new.call(this, {span: span, name: name, description: description, directives: directives});
    ;
  }).prototype = ast.EnumTypeDefinitionNode.prototype;
  dart.addTypeTests(ast.EnumTypeDefinitionNode);
  dart.setMethodSignature(ast.EnumTypeDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.EnumTypeDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.EnumTypeDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.EnumTypeDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.EnumTypeDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.EnumTypeDefinitionNode, () => ({
    __proto__: dart.getFields(ast.EnumTypeDefinitionNode.__proto__),
    values: dart.finalFieldType(core.List$(ast.EnumValueDefinitionNode))
  }));
  ast.EnumValueDefinitionNode = class EnumValueDefinitionNode extends ast.TypeDefinitionNode {
    accept(R, v) {
      return v.visitEnumValueDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives]);
    }
  };
  (ast.EnumValueDefinitionNode.new = function(opts) {
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1000, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1001, 16, "directives != null");
    ast.EnumValueDefinitionNode.__proto__.new.call(this, {span: span, name: name, description: description, directives: directives});
    ;
  }).prototype = ast.EnumValueDefinitionNode.prototype;
  dart.addTypeTests(ast.EnumValueDefinitionNode);
  dart.setMethodSignature(ast.EnumValueDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.EnumValueDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.EnumValueDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.EnumValueDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.EnumValueDefinitionNode, "package:gql/src/ast/ast.dart");
  const fields$2 = dart.privateName(ast, "InputObjectTypeDefinitionNode.fields");
  ast.InputObjectTypeDefinitionNode = class InputObjectTypeDefinitionNode extends ast.TypeDefinitionNode {
    get fields() {
      return this[fields$2];
    }
    set fields(value) {
      super.fields = value;
    }
    accept(R, v) {
      return v.visitInputObjectTypeDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.directives, this.fields]);
    }
  };
  (ast.InputObjectTypeDefinitionNode.new = function(opts) {
    let fields = opts && 'fields' in opts ? opts.fields : C36 || CT.C36;
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[fields$2] = fields;
    if (!(fields != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1029, 16, "fields != null");
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1030, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1031, 16, "directives != null");
    ast.InputObjectTypeDefinitionNode.__proto__.new.call(this, {span: span, name: name, description: description, directives: directives});
    ;
  }).prototype = ast.InputObjectTypeDefinitionNode.prototype;
  dart.addTypeTests(ast.InputObjectTypeDefinitionNode);
  dart.setMethodSignature(ast.InputObjectTypeDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.InputObjectTypeDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.InputObjectTypeDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.InputObjectTypeDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.InputObjectTypeDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.InputObjectTypeDefinitionNode, () => ({
    __proto__: dart.getFields(ast.InputObjectTypeDefinitionNode.__proto__),
    fields: dart.finalFieldType(core.List$(ast.InputValueDefinitionNode))
  }));
  let C38;
  const description$2 = dart.privateName(ast, "DirectiveDefinitionNode.description");
  const args$0 = dart.privateName(ast, "DirectiveDefinitionNode.args");
  const locations$ = dart.privateName(ast, "DirectiveDefinitionNode.locations");
  const repeatable$ = dart.privateName(ast, "DirectiveDefinitionNode.repeatable");
  ast.DirectiveDefinitionNode = class DirectiveDefinitionNode extends ast.TypeSystemDefinitionNode {
    get description() {
      return this[description$2];
    }
    set description(value) {
      super.description = value;
    }
    get args() {
      return this[args$0];
    }
    set args(value) {
      super.args = value;
    }
    get locations() {
      return this[locations$];
    }
    set locations(value) {
      super.locations = value;
    }
    get repeatable() {
      return this[repeatable$];
    }
    set repeatable(value) {
      super.repeatable = value;
    }
    accept(R, v) {
      return v.visitDirectiveDefinitionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.description, this.locations, this.args, this.repeatable]);
    }
  };
  (ast.DirectiveDefinitionNode.new = function(opts) {
    let description = opts && 'description' in opts ? opts.description : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let args = opts && 'args' in opts ? opts.args : C36 || CT.C36;
    let locations = opts && 'locations' in opts ? opts.locations : C38 || CT.C38;
    let repeatable = opts && 'repeatable' in opts ? opts.repeatable : false;
    let span = opts && 'span' in opts ? opts.span : null;
    this[description$2] = description;
    this[args$0] = args;
    this[locations$] = locations;
    this[repeatable$] = repeatable;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1064, 16, "name != null");
    if (!(args != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1065, 16, "args != null");
    if (!(locations != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1066, 16, "locations != null");
    if (!(repeatable != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1067, 16, "repeatable != null");
    ast.DirectiveDefinitionNode.__proto__.new.call(this, {name: name, span: span});
    ;
  }).prototype = ast.DirectiveDefinitionNode.prototype;
  dart.addTypeTests(ast.DirectiveDefinitionNode);
  dart.setMethodSignature(ast.DirectiveDefinitionNode, () => ({
    __proto__: dart.getMethods(ast.DirectiveDefinitionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.DirectiveDefinitionNode, () => ({
    __proto__: dart.getGetters(ast.DirectiveDefinitionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.DirectiveDefinitionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.DirectiveDefinitionNode, () => ({
    __proto__: dart.getFields(ast.DirectiveDefinitionNode.__proto__),
    description: dart.finalFieldType(ast.StringValueNode),
    args: dart.finalFieldType(core.List$(ast.InputValueDefinitionNode)),
    locations: dart.finalFieldType(core.List$(ast.DirectiveLocation)),
    repeatable: dart.finalFieldType(core.bool)
  }));
  const directives$10 = dart.privateName(ast, "SchemaExtensionNode.directives");
  const operationTypes$0 = dart.privateName(ast, "SchemaExtensionNode.operationTypes");
  ast.SchemaExtensionNode = class SchemaExtensionNode extends ast.TypeSystemExtensionNode {
    get directives() {
      return this[directives$10];
    }
    set directives(value) {
      super.directives = value;
    }
    get operationTypes() {
      return this[operationTypes$0];
    }
    set operationTypes(value) {
      super.operationTypes = value;
    }
    accept(R, v) {
      return v.visitSchemaExtensionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.directives, this.operationTypes]);
    }
  };
  (ast.SchemaExtensionNode.new = function(opts) {
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let operationTypes = opts && 'operationTypes' in opts ? opts.operationTypes : C33 || CT.C33;
    let span = opts && 'span' in opts ? opts.span : null;
    this[directives$10] = directives;
    this[operationTypes$0] = operationTypes;
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1094, 16, "directives != null");
    if (!(operationTypes != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1095, 16, "operationTypes != null");
    ast.SchemaExtensionNode.__proto__.new.call(this, {name: null, span: span});
    ;
  }).prototype = ast.SchemaExtensionNode.prototype;
  dart.addTypeTests(ast.SchemaExtensionNode);
  dart.setMethodSignature(ast.SchemaExtensionNode, () => ({
    __proto__: dart.getMethods(ast.SchemaExtensionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.SchemaExtensionNode, () => ({
    __proto__: dart.getGetters(ast.SchemaExtensionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.SchemaExtensionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.SchemaExtensionNode, () => ({
    __proto__: dart.getFields(ast.SchemaExtensionNode.__proto__),
    directives: dart.finalFieldType(core.List$(ast.DirectiveNode)),
    operationTypes: dart.finalFieldType(core.List$(ast.OperationTypeDefinitionNode))
  }));
  ast.ScalarTypeExtensionNode = class ScalarTypeExtensionNode extends ast.TypeExtensionNode {
    accept(R, v) {
      return v.visitScalarTypeExtensionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.directives]);
    }
  };
  (ast.ScalarTypeExtensionNode.new = function(opts) {
    let span = opts && 'span' in opts ? opts.span : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1117, 16, "name != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1118, 16, "directives != null");
    ast.ScalarTypeExtensionNode.__proto__.new.call(this, {span: span, name: name, directives: directives});
    ;
  }).prototype = ast.ScalarTypeExtensionNode.prototype;
  dart.addTypeTests(ast.ScalarTypeExtensionNode);
  dart.setMethodSignature(ast.ScalarTypeExtensionNode, () => ({
    __proto__: dart.getMethods(ast.ScalarTypeExtensionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ScalarTypeExtensionNode, () => ({
    __proto__: dart.getGetters(ast.ScalarTypeExtensionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ScalarTypeExtensionNode, "package:gql/src/ast/ast.dart");
  const interfaces$0 = dart.privateName(ast, "ObjectTypeExtensionNode.interfaces");
  const fields$3 = dart.privateName(ast, "ObjectTypeExtensionNode.fields");
  ast.ObjectTypeExtensionNode = class ObjectTypeExtensionNode extends ast.TypeExtensionNode {
    get interfaces() {
      return this[interfaces$0];
    }
    set interfaces(value) {
      super.interfaces = value;
    }
    get fields() {
      return this[fields$3];
    }
    set fields(value) {
      super.fields = value;
    }
    accept(R, v) {
      return v.visitObjectTypeExtensionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.directives, this.interfaces, this.fields]);
    }
  };
  (ast.ObjectTypeExtensionNode.new = function(opts) {
    let name = opts && 'name' in opts ? opts.name : null;
    let interfaces = opts && 'interfaces' in opts ? opts.interfaces : C34 || CT.C34;
    let fields = opts && 'fields' in opts ? opts.fields : C35 || CT.C35;
    let span = opts && 'span' in opts ? opts.span : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    this[interfaces$0] = interfaces;
    this[fields$3] = fields;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1145, 16, "name != null");
    if (!(interfaces != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1146, 16, "interfaces != null");
    if (!(fields != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1147, 16, "fields != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1148, 16, "directives != null");
    ast.ObjectTypeExtensionNode.__proto__.new.call(this, {span: span, name: name, directives: directives});
    ;
  }).prototype = ast.ObjectTypeExtensionNode.prototype;
  dart.addTypeTests(ast.ObjectTypeExtensionNode);
  dart.setMethodSignature(ast.ObjectTypeExtensionNode, () => ({
    __proto__: dart.getMethods(ast.ObjectTypeExtensionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.ObjectTypeExtensionNode, () => ({
    __proto__: dart.getGetters(ast.ObjectTypeExtensionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.ObjectTypeExtensionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.ObjectTypeExtensionNode, () => ({
    __proto__: dart.getFields(ast.ObjectTypeExtensionNode.__proto__),
    interfaces: dart.finalFieldType(core.List$(ast.NamedTypeNode)),
    fields: dart.finalFieldType(core.List$(ast.FieldDefinitionNode))
  }));
  const fields$4 = dart.privateName(ast, "InterfaceTypeExtensionNode.fields");
  ast.InterfaceTypeExtensionNode = class InterfaceTypeExtensionNode extends ast.TypeExtensionNode {
    get fields() {
      return this[fields$4];
    }
    set fields(value) {
      super.fields = value;
    }
    accept(R, v) {
      return v.visitInterfaceTypeExtensionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.directives, this.fields]);
    }
  };
  (ast.InterfaceTypeExtensionNode.new = function(opts) {
    let fields = opts && 'fields' in opts ? opts.fields : C35 || CT.C35;
    let name = opts && 'name' in opts ? opts.name : null;
    let span = opts && 'span' in opts ? opts.span : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    this[fields$4] = fields;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1175, 16, "name != null");
    if (!(fields != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1176, 16, "fields != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1177, 16, "directives != null");
    ast.InterfaceTypeExtensionNode.__proto__.new.call(this, {span: span, name: name, directives: directives});
    ;
  }).prototype = ast.InterfaceTypeExtensionNode.prototype;
  dart.addTypeTests(ast.InterfaceTypeExtensionNode);
  dart.setMethodSignature(ast.InterfaceTypeExtensionNode, () => ({
    __proto__: dart.getMethods(ast.InterfaceTypeExtensionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.InterfaceTypeExtensionNode, () => ({
    __proto__: dart.getGetters(ast.InterfaceTypeExtensionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.InterfaceTypeExtensionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.InterfaceTypeExtensionNode, () => ({
    __proto__: dart.getFields(ast.InterfaceTypeExtensionNode.__proto__),
    fields: dart.finalFieldType(core.List$(ast.FieldDefinitionNode))
  }));
  const types$0 = dart.privateName(ast, "UnionTypeExtensionNode.types");
  ast.UnionTypeExtensionNode = class UnionTypeExtensionNode extends ast.TypeExtensionNode {
    get types() {
      return this[types$0];
    }
    set types(value) {
      super.types = value;
    }
    accept(R, v) {
      return v.visitUnionTypeExtensionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.directives, this.types]);
    }
  };
  (ast.UnionTypeExtensionNode.new = function(opts) {
    let types = opts && 'types' in opts ? opts.types : C34 || CT.C34;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    let span = opts && 'span' in opts ? opts.span : null;
    this[types$0] = types;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1203, 16, "name != null");
    if (!(types != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1204, 16, "types != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1205, 16, "directives != null");
    ast.UnionTypeExtensionNode.__proto__.new.call(this, {span: span, name: name, directives: directives});
    ;
  }).prototype = ast.UnionTypeExtensionNode.prototype;
  dart.addTypeTests(ast.UnionTypeExtensionNode);
  dart.setMethodSignature(ast.UnionTypeExtensionNode, () => ({
    __proto__: dart.getMethods(ast.UnionTypeExtensionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.UnionTypeExtensionNode, () => ({
    __proto__: dart.getGetters(ast.UnionTypeExtensionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.UnionTypeExtensionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.UnionTypeExtensionNode, () => ({
    __proto__: dart.getFields(ast.UnionTypeExtensionNode.__proto__),
    types: dart.finalFieldType(core.List$(ast.NamedTypeNode))
  }));
  const values$1 = dart.privateName(ast, "EnumTypeExtensionNode.values");
  ast.EnumTypeExtensionNode = class EnumTypeExtensionNode extends ast.TypeExtensionNode {
    get values() {
      return this[values$1];
    }
    set values(value) {
      super.values = value;
    }
    accept(R, v) {
      return v.visitEnumTypeExtensionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.directives, this.values]);
    }
  };
  (ast.EnumTypeExtensionNode.new = function(opts) {
    let values = opts && 'values' in opts ? opts.values : C37 || CT.C37;
    let span = opts && 'span' in opts ? opts.span : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    this[values$1] = values;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1231, 16, "name != null");
    if (!(values != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1232, 16, "values != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1233, 16, "directives != null");
    ast.EnumTypeExtensionNode.__proto__.new.call(this, {span: span, name: name, directives: directives});
    ;
  }).prototype = ast.EnumTypeExtensionNode.prototype;
  dart.addTypeTests(ast.EnumTypeExtensionNode);
  dart.setMethodSignature(ast.EnumTypeExtensionNode, () => ({
    __proto__: dart.getMethods(ast.EnumTypeExtensionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.EnumTypeExtensionNode, () => ({
    __proto__: dart.getGetters(ast.EnumTypeExtensionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.EnumTypeExtensionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.EnumTypeExtensionNode, () => ({
    __proto__: dart.getFields(ast.EnumTypeExtensionNode.__proto__),
    values: dart.finalFieldType(core.List$(ast.EnumValueDefinitionNode))
  }));
  const fields$5 = dart.privateName(ast, "InputObjectTypeExtensionNode.fields");
  ast.InputObjectTypeExtensionNode = class InputObjectTypeExtensionNode extends ast.TypeExtensionNode {
    get fields() {
      return this[fields$5];
    }
    set fields(value) {
      super.fields = value;
    }
    accept(R, v) {
      return v.visitInputObjectTypeExtensionNode(this);
    }
    get [_children]() {
      return JSArrayOfObject().of([this.name, this.directives, this.fields]);
    }
  };
  (ast.InputObjectTypeExtensionNode.new = function(opts) {
    let fields = opts && 'fields' in opts ? opts.fields : C36 || CT.C36;
    let span = opts && 'span' in opts ? opts.span : null;
    let name = opts && 'name' in opts ? opts.name : null;
    let directives = opts && 'directives' in opts ? opts.directives : C28 || CT.C28;
    this[fields$5] = fields;
    if (!(name != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1259, 16, "name != null");
    if (!(fields != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1260, 16, "fields != null");
    if (!(directives != null)) dart.assertFailed(null, "org-dartlang-app:///packages/gql/src/ast/ast.dart", 1261, 16, "directives != null");
    ast.InputObjectTypeExtensionNode.__proto__.new.call(this, {span: span, name: name, directives: directives});
    ;
  }).prototype = ast.InputObjectTypeExtensionNode.prototype;
  dart.addTypeTests(ast.InputObjectTypeExtensionNode);
  dart.setMethodSignature(ast.InputObjectTypeExtensionNode, () => ({
    __proto__: dart.getMethods(ast.InputObjectTypeExtensionNode.__proto__),
    accept: dart.gFnType(R => [R, [visitor.Visitor$(R)]])
  }));
  dart.setGetterSignature(ast.InputObjectTypeExtensionNode, () => ({
    __proto__: dart.getGetters(ast.InputObjectTypeExtensionNode.__proto__),
    [_children]: core.List$(core.Object)
  }));
  dart.setLibraryUri(ast.InputObjectTypeExtensionNode, "package:gql/src/ast/ast.dart");
  dart.setFieldSignature(ast.InputObjectTypeExtensionNode, () => ({
    __proto__: dart.getFields(ast.InputObjectTypeExtensionNode.__proto__),
    fields: dart.finalFieldType(core.List$(ast.InputValueDefinitionNode))
  }));
  ast._visitOne = function _visitOne(R, node, v) {
    let t132;
    t132 = node;
    return t132 == null ? null : t132.accept(dart.void, v);
  };
  ast._visitAll = function _visitAll(R, nodes, v) {
    let t132;
    t132 = nodes;
    return t132 == null ? null : t132[$forEach](dart.fn(node => ast._visitOne(R, node, v), NodeTovoid()));
  };
  transformer.TransformingVisitor = class TransformingVisitor extends visitor.Visitor$(ast.Node) {
    visitDocumentNode(node) {
      return node;
    }
    visitNameNode(node) {
      return node;
    }
    visitDirectiveNode(node) {
      return node;
    }
    visitListTypeNode(node) {
      return node;
    }
    visitNamedTypeNode(node) {
      return node;
    }
    visitDefaultValueNode(node) {
      return node;
    }
    visitVariableDefinitionNode(node) {
      return node;
    }
    visitObjectFieldNode(node) {
      return node;
    }
    visitObjectValueNode(node) {
      return node;
    }
    visitListValueNode(node) {
      return node;
    }
    visitEnumValueNode(node) {
      return node;
    }
    visitNullValueNode(node) {
      return node;
    }
    visitBooleanValueNode(node) {
      return node;
    }
    visitStringValueNode(node) {
      return node;
    }
    visitFloatValueNode(node) {
      return node;
    }
    visitIntValueNode(node) {
      return node;
    }
    visitVariableNode(node) {
      return node;
    }
    visitTypeConditionNode(node) {
      return node;
    }
    visitFragmentDefinitionNode(node) {
      return node;
    }
    visitInlineFragmentNode(node) {
      return node;
    }
    visitFragmentSpreadNode(node) {
      return node;
    }
    visitArgumentNode(node) {
      return node;
    }
    visitFieldNode(node) {
      return node;
    }
    visitSelectionSetNode(node) {
      return node;
    }
    visitOperationDefinitionNode(node) {
      return node;
    }
    visitSchemaDefinitionNode(node) {
      return node;
    }
    visitOperationTypeDefinitionNode(node) {
      return node;
    }
    visitScalarTypeDefinitionNode(node) {
      return node;
    }
    visitObjectTypeDefinitionNode(node) {
      return node;
    }
    visitFieldDefinitionNode(node) {
      return node;
    }
    visitInputValueDefinitionNode(node) {
      return node;
    }
    visitInterfaceTypeDefinitionNode(node) {
      return node;
    }
    visitUnionTypeDefinitionNode(node) {
      return node;
    }
    visitEnumTypeDefinitionNode(node) {
      return node;
    }
    visitEnumValueDefinitionNode(node) {
      return node;
    }
    visitInputObjectTypeDefinitionNode(node) {
      return node;
    }
    visitDirectiveDefinitionNode(node) {
      return node;
    }
    visitSchemaExtensionNode(node) {
      return node;
    }
    visitScalarTypeExtensionNode(node) {
      return node;
    }
    visitObjectTypeExtensionNode(node) {
      return node;
    }
    visitInterfaceTypeExtensionNode(node) {
      return node;
    }
    visitUnionTypeExtensionNode(node) {
      return node;
    }
    visitEnumTypeExtensionNode(node) {
      return node;
    }
    visitInputObjectTypeExtensionNode(node) {
      return node;
    }
  };
  (transformer.TransformingVisitor.new = function() {
    ;
  }).prototype = transformer.TransformingVisitor.prototype;
  dart.addTypeTests(transformer.TransformingVisitor);
  dart.setMethodSignature(transformer.TransformingVisitor, () => ({
    __proto__: dart.getMethods(transformer.TransformingVisitor.__proto__),
    visitDocumentNode: dart.fnType(ast.DocumentNode, [ast.DocumentNode]),
    visitNameNode: dart.fnType(ast.NameNode, [ast.NameNode]),
    visitDirectiveNode: dart.fnType(ast.DirectiveNode, [ast.DirectiveNode]),
    visitListTypeNode: dart.fnType(ast.ListTypeNode, [ast.ListTypeNode]),
    visitNamedTypeNode: dart.fnType(ast.NamedTypeNode, [ast.NamedTypeNode]),
    visitDefaultValueNode: dart.fnType(ast.DefaultValueNode, [ast.DefaultValueNode]),
    visitVariableDefinitionNode: dart.fnType(ast.VariableDefinitionNode, [ast.VariableDefinitionNode]),
    visitObjectFieldNode: dart.fnType(ast.ObjectFieldNode, [ast.ObjectFieldNode]),
    visitObjectValueNode: dart.fnType(ast.ObjectValueNode, [ast.ObjectValueNode]),
    visitListValueNode: dart.fnType(ast.ListValueNode, [ast.ListValueNode]),
    visitEnumValueNode: dart.fnType(ast.EnumValueNode, [ast.EnumValueNode]),
    visitNullValueNode: dart.fnType(ast.NullValueNode, [ast.NullValueNode]),
    visitBooleanValueNode: dart.fnType(ast.BooleanValueNode, [ast.BooleanValueNode]),
    visitStringValueNode: dart.fnType(ast.StringValueNode, [ast.StringValueNode]),
    visitFloatValueNode: dart.fnType(ast.FloatValueNode, [ast.FloatValueNode]),
    visitIntValueNode: dart.fnType(ast.IntValueNode, [ast.IntValueNode]),
    visitVariableNode: dart.fnType(ast.VariableNode, [ast.VariableNode]),
    visitTypeConditionNode: dart.fnType(ast.TypeConditionNode, [ast.TypeConditionNode]),
    visitFragmentDefinitionNode: dart.fnType(ast.FragmentDefinitionNode, [ast.FragmentDefinitionNode]),
    visitInlineFragmentNode: dart.fnType(ast.InlineFragmentNode, [ast.InlineFragmentNode]),
    visitFragmentSpreadNode: dart.fnType(ast.FragmentSpreadNode, [ast.FragmentSpreadNode]),
    visitArgumentNode: dart.fnType(ast.ArgumentNode, [ast.ArgumentNode]),
    visitFieldNode: dart.fnType(ast.FieldNode, [ast.FieldNode]),
    visitSelectionSetNode: dart.fnType(ast.SelectionSetNode, [ast.SelectionSetNode]),
    visitOperationDefinitionNode: dart.fnType(ast.OperationDefinitionNode, [ast.OperationDefinitionNode]),
    visitSchemaDefinitionNode: dart.fnType(ast.SchemaDefinitionNode, [ast.SchemaDefinitionNode]),
    visitOperationTypeDefinitionNode: dart.fnType(ast.OperationTypeDefinitionNode, [ast.OperationTypeDefinitionNode]),
    visitScalarTypeDefinitionNode: dart.fnType(ast.ScalarTypeDefinitionNode, [ast.ScalarTypeDefinitionNode]),
    visitObjectTypeDefinitionNode: dart.fnType(ast.ObjectTypeDefinitionNode, [ast.ObjectTypeDefinitionNode]),
    visitFieldDefinitionNode: dart.fnType(ast.FieldDefinitionNode, [ast.FieldDefinitionNode]),
    visitInputValueDefinitionNode: dart.fnType(ast.InputValueDefinitionNode, [ast.InputValueDefinitionNode]),
    visitInterfaceTypeDefinitionNode: dart.fnType(ast.InterfaceTypeDefinitionNode, [ast.InterfaceTypeDefinitionNode]),
    visitUnionTypeDefinitionNode: dart.fnType(ast.UnionTypeDefinitionNode, [ast.UnionTypeDefinitionNode]),
    visitEnumTypeDefinitionNode: dart.fnType(ast.EnumTypeDefinitionNode, [ast.EnumTypeDefinitionNode]),
    visitEnumValueDefinitionNode: dart.fnType(ast.EnumValueDefinitionNode, [ast.EnumValueDefinitionNode]),
    visitInputObjectTypeDefinitionNode: dart.fnType(ast.InputObjectTypeDefinitionNode, [ast.InputObjectTypeDefinitionNode]),
    visitDirectiveDefinitionNode: dart.fnType(ast.DirectiveDefinitionNode, [ast.DirectiveDefinitionNode]),
    visitSchemaExtensionNode: dart.fnType(ast.SchemaExtensionNode, [ast.SchemaExtensionNode]),
    visitScalarTypeExtensionNode: dart.fnType(ast.ScalarTypeExtensionNode, [ast.ScalarTypeExtensionNode]),
    visitObjectTypeExtensionNode: dart.fnType(ast.ObjectTypeExtensionNode, [ast.ObjectTypeExtensionNode]),
    visitInterfaceTypeExtensionNode: dart.fnType(ast.InterfaceTypeExtensionNode, [ast.InterfaceTypeExtensionNode]),
    visitUnionTypeExtensionNode: dart.fnType(ast.UnionTypeExtensionNode, [ast.UnionTypeExtensionNode]),
    visitEnumTypeExtensionNode: dart.fnType(ast.EnumTypeExtensionNode, [ast.EnumTypeExtensionNode]),
    visitInputObjectTypeExtensionNode: dart.fnType(ast.InputObjectTypeExtensionNode, [ast.InputObjectTypeExtensionNode])
  }));
  dart.setLibraryUri(transformer.TransformingVisitor, "package:gql/src/ast/transformer.dart");
  let C39;
  const _visitOne$ = dart.privateName(transformer, "_visitOne");
  const _visitAll$ = dart.privateName(transformer, "_visitAll");
  transformer._Transformer = class _Transformer extends visitor.Visitor$(ast.Node) {
    [_visitOne$](N, node) {
      if (node == null) return node;
      return N.as(node.accept(ast.Node, this));
    }
    [_visitAll$](N, nodes) {
      if (nodes == null) return nodes;
      return nodes[$map](ast.Node, dart.fn(node => node.accept(ast.Node, this), dart.fnType(ast.Node, [N])))[$cast](N)[$toList]({growable: false});
    }
    visitDocumentNode(node) {
      let updatedNode = new ast.DocumentNode.new({definitions: this[_visitAll$](ast.DefinitionNode, node.definitions)});
      return this.visitors[$fold](ast.DocumentNode, updatedNode, dart.fn((prev, visitor) => visitor.visitDocumentNode(prev), DocumentNodeAndTransformingVisitorToDocumentNode()));
    }
    visitArgumentNode(node) {
      let updatedNode = new ast.ArgumentNode.new({name: this[_visitOne$](ast.NameNode, node.name), value: this[_visitOne$](ast.ValueNode, node.value)});
      return this.visitors[$fold](ast.ArgumentNode, updatedNode, dart.fn((prev, visitor) => visitor.visitArgumentNode(prev), ArgumentNodeAndTransformingVisitorToArgumentNode()));
    }
    visitBooleanValueNode(node) {
      let updatedNode = new ast.BooleanValueNode.new({value: node.value});
      return this.visitors[$fold](ast.BooleanValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitBooleanValueNode(prev), BooleanValueNodeAndTransformingVisitorToBooleanValueNode()));
    }
    visitDefaultValueNode(node) {
      let updatedNode = new ast.DefaultValueNode.new({value: this[_visitOne$](ast.ValueNode, node.value)});
      return this.visitors[$fold](ast.DefaultValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitDefaultValueNode(prev), DefaultValueNodeAndTransformingVisitorToDefaultValueNode()));
    }
    visitDirectiveDefinitionNode(node) {
      let updatedNode = new ast.DirectiveDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), locations: node.locations, repeatable: node.repeatable, args: this[_visitAll$](ast.InputValueDefinitionNode, node.args)});
      return this.visitors[$fold](ast.DirectiveDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitDirectiveDefinitionNode(prev), DirectiveDefinitionNodeAndTransformingVisitorToDirectiveDefinitionNode()));
    }
    visitDirectiveNode(node) {
      let updatedNode = new ast.DirectiveNode.new({arguments: this[_visitAll$](ast.ArgumentNode, node.arguments), name: this[_visitOne$](ast.NameNode, node.name)});
      return this.visitors[$fold](ast.DirectiveNode, updatedNode, dart.fn((prev, visitor) => visitor.visitDirectiveNode(prev), DirectiveNodeAndTransformingVisitorToDirectiveNode()));
    }
    visitEnumTypeDefinitionNode(node) {
      let updatedNode = new ast.EnumTypeDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives), values: this[_visitAll$](ast.EnumValueDefinitionNode, node.values)});
      return this.visitors[$fold](ast.EnumTypeDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitEnumTypeDefinitionNode(prev), EnumTypeDefinitionNodeAndTransformingVisitorToEnumTypeDefinitionNode()));
    }
    visitEnumTypeExtensionNode(node) {
      let updatedNode = new ast.EnumTypeExtensionNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives), values: this[_visitAll$](ast.EnumValueDefinitionNode, node.values)});
      return this.visitors[$fold](ast.EnumTypeExtensionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitEnumTypeExtensionNode(prev), EnumTypeExtensionNodeAndTransformingVisitorToEnumTypeExtensionNode()));
    }
    visitEnumValueDefinitionNode(node) {
      let updatedNode = new ast.EnumValueDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives)});
      return this.visitors[$fold](ast.EnumValueDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitEnumValueDefinitionNode(prev), EnumValueDefinitionNodeAndTransformingVisitorToEnumValueDefinitionNode()));
    }
    visitEnumValueNode(node) {
      let updatedNode = new ast.EnumValueNode.new({name: this[_visitOne$](ast.NameNode, node.name)});
      return this.visitors[$fold](ast.EnumValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitEnumValueNode(prev), EnumValueNodeAndTransformingVisitorToEnumValueNode()));
    }
    visitFieldDefinitionNode(node) {
      let updatedNode = new ast.FieldDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives), args: this[_visitAll$](ast.InputValueDefinitionNode, node.args), type: this[_visitOne$](ast.TypeNode, node.type)});
      return this.visitors[$fold](ast.FieldDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitFieldDefinitionNode(prev), FieldDefinitionNodeAndTransformingVisitorToFieldDefinitionNode()));
    }
    visitFieldNode(node) {
      let updatedNode = new ast.FieldNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives), alias: this[_visitOne$](ast.NameNode, node.alias), arguments: this[_visitAll$](ast.ArgumentNode, node.arguments), selectionSet: this[_visitOne$](ast.SelectionSetNode, node.selectionSet)});
      return this.visitors[$fold](ast.FieldNode, updatedNode, dart.fn((prev, visitor) => visitor.visitFieldNode(prev), FieldNodeAndTransformingVisitorToFieldNode()));
    }
    visitFloatValueNode(node) {
      let updatedNode = new ast.FloatValueNode.new({value: node.value});
      return this.visitors[$fold](ast.FloatValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitFloatValueNode(prev), FloatValueNodeAndTransformingVisitorToFloatValueNode()));
    }
    visitFragmentDefinitionNode(node) {
      let updatedNode = new ast.FragmentDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives), selectionSet: this[_visitOne$](ast.SelectionSetNode, node.selectionSet), typeCondition: this[_visitOne$](ast.TypeConditionNode, node.typeCondition)});
      return this.visitors[$fold](ast.FragmentDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitFragmentDefinitionNode(prev), FragmentDefinitionNodeAndTransformingVisitorToFragmentDefinitionNode()));
    }
    visitFragmentSpreadNode(node) {
      let updatedNode = new ast.FragmentSpreadNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives)});
      return this.visitors[$fold](ast.FragmentSpreadNode, updatedNode, dart.fn((prev, visitor) => visitor.visitFragmentSpreadNode(prev), FragmentSpreadNodeAndTransformingVisitorToFragmentSpreadNode()));
    }
    visitInlineFragmentNode(node) {
      let updatedNode = new ast.InlineFragmentNode.new({directives: this[_visitAll$](ast.DirectiveNode, node.directives), selectionSet: this[_visitOne$](ast.SelectionSetNode, node.selectionSet), typeCondition: this[_visitOne$](ast.TypeConditionNode, node.typeCondition)});
      return this.visitors[$fold](ast.InlineFragmentNode, updatedNode, dart.fn((prev, visitor) => visitor.visitInlineFragmentNode(prev), InlineFragmentNodeAndTransformingVisitorToInlineFragmentNode()));
    }
    visitInputObjectTypeDefinitionNode(node) {
      let updatedNode = new ast.InputObjectTypeDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives), fields: this[_visitAll$](ast.InputValueDefinitionNode, node.fields)});
      return this.visitors[$fold](ast.InputObjectTypeDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitInputObjectTypeDefinitionNode(prev), InputObjectTypeDefinitionNodeAndTransformingVisitorToInputObjectTypeDefinitionNode()));
    }
    visitInputObjectTypeExtensionNode(node) {
      let updatedNode = new ast.InputObjectTypeExtensionNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives), fields: this[_visitAll$](ast.InputValueDefinitionNode, node.fields)});
      return this.visitors[$fold](ast.InputObjectTypeExtensionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitInputObjectTypeExtensionNode(prev), InputObjectTypeExtensionNodeAndTransformingVisitorToInputObjectTypeExtensionNode()));
    }
    visitInputValueDefinitionNode(node) {
      let updatedNode = new ast.InputValueDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives), defaultValue: this[_visitOne$](ast.ValueNode, node.defaultValue), type: this[_visitOne$](ast.TypeNode, node.type)});
      return this.visitors[$fold](ast.InputValueDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitInputValueDefinitionNode(prev), InputValueDefinitionNodeAndTransformingVisitorToInputValueDefinitionNode()));
    }
    visitIntValueNode(node) {
      let updatedNode = new ast.IntValueNode.new({value: node.value});
      return this.visitors[$fold](ast.IntValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitIntValueNode(prev), IntValueNodeAndTransformingVisitorToIntValueNode()));
    }
    visitInterfaceTypeDefinitionNode(node) {
      let updatedNode = new ast.InterfaceTypeDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives), fields: this[_visitAll$](ast.FieldDefinitionNode, node.fields)});
      return this.visitors[$fold](ast.InterfaceTypeDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitInterfaceTypeDefinitionNode(prev), InterfaceTypeDefinitionNodeAndTransformingVisitorToInterfaceTypeDefinitionNode()));
    }
    visitInterfaceTypeExtensionNode(node) {
      let updatedNode = new ast.InterfaceTypeExtensionNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives), fields: this[_visitAll$](ast.FieldDefinitionNode, node.fields)});
      return this.visitors[$fold](ast.InterfaceTypeExtensionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitInterfaceTypeExtensionNode(prev), InterfaceTypeExtensionNodeAndTransformingVisitorToInterfaceTypeExtensionNode()));
    }
    visitListTypeNode(node) {
      let updatedNode = new ast.ListTypeNode.new({type: this[_visitOne$](ast.TypeNode, node.type), isNonNull: node.isNonNull});
      return this.visitors[$fold](ast.ListTypeNode, updatedNode, dart.fn((prev, visitor) => visitor.visitListTypeNode(prev), ListTypeNodeAndTransformingVisitorToListTypeNode()));
    }
    visitListValueNode(node) {
      let updatedNode = new ast.ListValueNode.new({values: this[_visitAll$](ast.ValueNode, node.values)});
      return this.visitors[$fold](ast.ListValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitListValueNode(prev), ListValueNodeAndTransformingVisitorToListValueNode()));
    }
    visitNameNode(node) {
      let updatedNode = new ast.NameNode.new({span: node.span, value: node.value});
      return this.visitors[$fold](ast.NameNode, updatedNode, dart.fn((prev, visitor) => visitor.visitNameNode(prev), NameNodeAndTransformingVisitorToNameNode()));
    }
    visitNamedTypeNode(node) {
      let updatedNode = new ast.NamedTypeNode.new({isNonNull: node.isNonNull, name: this[_visitOne$](ast.NameNode, node.name)});
      return this.visitors[$fold](ast.NamedTypeNode, updatedNode, dart.fn((prev, visitor) => visitor.visitNamedTypeNode(prev), NamedTypeNodeAndTransformingVisitorToNamedTypeNode()));
    }
    visitNullValueNode(node) {
      let updatedNode = new ast.NullValueNode.new();
      return this.visitors[$fold](ast.NullValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitNullValueNode(prev), NullValueNodeAndTransformingVisitorToNullValueNode()));
    }
    visitObjectFieldNode(node) {
      let updatedNode = new ast.ObjectFieldNode.new({name: this[_visitOne$](ast.NameNode, node.name), value: this[_visitOne$](ast.ValueNode, node.value)});
      return this.visitors[$fold](ast.ObjectFieldNode, updatedNode, dart.fn((prev, visitor) => visitor.visitObjectFieldNode(prev), ObjectFieldNodeAndTransformingVisitorToObjectFieldNode()));
    }
    visitObjectTypeDefinitionNode(node) {
      let updatedNode = new ast.ObjectTypeDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives), fields: this[_visitAll$](ast.FieldDefinitionNode, node.fields), interfaces: this[_visitAll$](ast.NamedTypeNode, node.interfaces)});
      return this.visitors[$fold](ast.ObjectTypeDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitObjectTypeDefinitionNode(prev), ObjectTypeDefinitionNodeAndTransformingVisitorToObjectTypeDefinitionNode()));
    }
    visitObjectTypeExtensionNode(node) {
      let updatedNode = new ast.ObjectTypeExtensionNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives), fields: this[_visitAll$](ast.FieldDefinitionNode, node.fields), interfaces: this[_visitAll$](ast.NamedTypeNode, node.interfaces)});
      return this.visitors[$fold](ast.ObjectTypeExtensionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitObjectTypeExtensionNode(prev), ObjectTypeExtensionNodeAndTransformingVisitorToObjectTypeExtensionNode()));
    }
    visitObjectValueNode(node) {
      let updatedNode = new ast.ObjectValueNode.new({fields: this[_visitAll$](ast.ObjectFieldNode, node.fields)});
      return this.visitors[$fold](ast.ObjectValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitObjectValueNode(prev), ObjectValueNodeAndTransformingVisitorToObjectValueNode()));
    }
    visitOperationDefinitionNode(node) {
      let updatedNode = new ast.OperationDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives), type: node.type, selectionSet: this[_visitOne$](ast.SelectionSetNode, node.selectionSet), variableDefinitions: this[_visitAll$](ast.VariableDefinitionNode, node.variableDefinitions)});
      return this.visitors[$fold](ast.OperationDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitOperationDefinitionNode(prev), OperationDefinitionNodeAndTransformingVisitorToOperationDefinitionNode()));
    }
    visitOperationTypeDefinitionNode(node) {
      let updatedNode = new ast.OperationTypeDefinitionNode.new({operation: node.operation, type: this[_visitOne$](ast.NamedTypeNode, node.type)});
      return this.visitors[$fold](ast.OperationTypeDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitOperationTypeDefinitionNode(prev), OperationTypeDefinitionNodeAndTransformingVisitorToOperationTypeDefinitionNode()));
    }
    visitScalarTypeDefinitionNode(node) {
      let updatedNode = new ast.ScalarTypeDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives)});
      return this.visitors[$fold](ast.ScalarTypeDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitScalarTypeDefinitionNode(prev), ScalarTypeDefinitionNodeAndTransformingVisitorToScalarTypeDefinitionNode()));
    }
    visitScalarTypeExtensionNode(node) {
      let updatedNode = new ast.ScalarTypeExtensionNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives)});
      return this.visitors[$fold](ast.ScalarTypeExtensionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitScalarTypeExtensionNode(prev), ScalarTypeExtensionNodeAndTransformingVisitorToScalarTypeExtensionNode()));
    }
    visitSchemaDefinitionNode(node) {
      let updatedNode = new ast.SchemaDefinitionNode.new({directives: this[_visitAll$](ast.DirectiveNode, node.directives), operationTypes: this[_visitAll$](ast.OperationTypeDefinitionNode, node.operationTypes)});
      return this.visitors[$fold](ast.SchemaDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitSchemaDefinitionNode(prev), SchemaDefinitionNodeAndTransformingVisitorToSchemaDefinitionNode()));
    }
    visitSchemaExtensionNode(node) {
      let updatedNode = new ast.SchemaExtensionNode.new({directives: this[_visitAll$](ast.DirectiveNode, node.directives), operationTypes: this[_visitAll$](ast.OperationTypeDefinitionNode, node.operationTypes)});
      return this.visitors[$fold](ast.SchemaExtensionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitSchemaExtensionNode(prev), SchemaExtensionNodeAndTransformingVisitorToSchemaExtensionNode()));
    }
    visitSelectionSetNode(node) {
      let updatedNode = new ast.SelectionSetNode.new({selections: this[_visitAll$](ast.SelectionNode, node.selections)});
      return this.visitors[$fold](ast.SelectionSetNode, updatedNode, dart.fn((prev, visitor) => visitor.visitSelectionSetNode(prev), SelectionSetNodeAndTransformingVisitorToSelectionSetNode()));
    }
    visitStringValueNode(node) {
      let updatedNode = new ast.StringValueNode.new({isBlock: node.isBlock, value: node.value});
      return this.visitors[$fold](ast.StringValueNode, updatedNode, dart.fn((prev, visitor) => visitor.visitStringValueNode(prev), StringValueNodeAndTransformingVisitorToStringValueNode()));
    }
    visitTypeConditionNode(node) {
      let updatedNode = new ast.TypeConditionNode.new({on: this[_visitOne$](ast.NamedTypeNode, node.on)});
      return this.visitors[$fold](ast.TypeConditionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitTypeConditionNode(prev), TypeConditionNodeAndTransformingVisitorToTypeConditionNode()));
    }
    visitUnionTypeDefinitionNode(node) {
      let updatedNode = new ast.UnionTypeDefinitionNode.new({name: this[_visitOne$](ast.NameNode, node.name), description: this[_visitOne$](ast.StringValueNode, node.description), directives: this[_visitAll$](ast.DirectiveNode, node.directives), types: this[_visitAll$](ast.NamedTypeNode, node.types)});
      return this.visitors[$fold](ast.UnionTypeDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitUnionTypeDefinitionNode(prev), UnionTypeDefinitionNodeAndTransformingVisitorToUnionTypeDefinitionNode()));
    }
    visitUnionTypeExtensionNode(node) {
      let updatedNode = new ast.UnionTypeExtensionNode.new({name: this[_visitOne$](ast.NameNode, node.name), directives: this[_visitAll$](ast.DirectiveNode, node.directives), types: this[_visitAll$](ast.NamedTypeNode, node.types)});
      return this.visitors[$fold](ast.UnionTypeExtensionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitUnionTypeExtensionNode(prev), UnionTypeExtensionNodeAndTransformingVisitorToUnionTypeExtensionNode()));
    }
    visitVariableDefinitionNode(node) {
      let updatedNode = new ast.VariableDefinitionNode.new({directives: this[_visitAll$](ast.DirectiveNode, node.directives), defaultValue: this[_visitOne$](ast.DefaultValueNode, node.defaultValue), type: this[_visitOne$](ast.TypeNode, node.type), variable: this[_visitOne$](ast.VariableNode, node.variable)});
      return this.visitors[$fold](ast.VariableDefinitionNode, updatedNode, dart.fn((prev, visitor) => visitor.visitVariableDefinitionNode(prev), VariableDefinitionNodeAndTransformingVisitorToVariableDefinitionNode()));
    }
    visitVariableNode(node) {
      let updatedNode = new ast.VariableNode.new({name: this[_visitOne$](ast.NameNode, node.name)});
      return this.visitors[$fold](ast.VariableNode, updatedNode, dart.fn((prev, visitor) => visitor.visitVariableNode(prev), VariableNodeAndTransformingVisitorToVariableNode()));
    }
  };
  (transformer._Transformer.new = function(opts) {
    let visitors = opts && 'visitors' in opts ? opts.visitors : C39 || CT.C39;
    this.visitors = visitors;
    ;
  }).prototype = transformer._Transformer.prototype;
  dart.addTypeTests(transformer._Transformer);
  dart.setMethodSignature(transformer._Transformer, () => ({
    __proto__: dart.getMethods(transformer._Transformer.__proto__),
    [_visitOne$]: dart.gFnType(N => [N, [N]], N => [ast.Node]),
    [_visitAll$]: dart.gFnType(N => [core.List$(N), [core.List$(N)]], N => [ast.Node]),
    visitDocumentNode: dart.fnType(ast.DocumentNode, [ast.DocumentNode]),
    visitArgumentNode: dart.fnType(ast.ArgumentNode, [ast.ArgumentNode]),
    visitBooleanValueNode: dart.fnType(ast.BooleanValueNode, [ast.BooleanValueNode]),
    visitDefaultValueNode: dart.fnType(ast.DefaultValueNode, [ast.DefaultValueNode]),
    visitDirectiveDefinitionNode: dart.fnType(ast.DirectiveDefinitionNode, [ast.DirectiveDefinitionNode]),
    visitDirectiveNode: dart.fnType(ast.DirectiveNode, [ast.DirectiveNode]),
    visitEnumTypeDefinitionNode: dart.fnType(ast.EnumTypeDefinitionNode, [ast.EnumTypeDefinitionNode]),
    visitEnumTypeExtensionNode: dart.fnType(ast.EnumTypeExtensionNode, [ast.EnumTypeExtensionNode]),
    visitEnumValueDefinitionNode: dart.fnType(ast.EnumValueDefinitionNode, [ast.EnumValueDefinitionNode]),
    visitEnumValueNode: dart.fnType(ast.EnumValueNode, [ast.EnumValueNode]),
    visitFieldDefinitionNode: dart.fnType(ast.FieldDefinitionNode, [ast.FieldDefinitionNode]),
    visitFieldNode: dart.fnType(ast.FieldNode, [ast.FieldNode]),
    visitFloatValueNode: dart.fnType(ast.FloatValueNode, [ast.FloatValueNode]),
    visitFragmentDefinitionNode: dart.fnType(ast.FragmentDefinitionNode, [ast.FragmentDefinitionNode]),
    visitFragmentSpreadNode: dart.fnType(ast.FragmentSpreadNode, [ast.FragmentSpreadNode]),
    visitInlineFragmentNode: dart.fnType(ast.InlineFragmentNode, [ast.InlineFragmentNode]),
    visitInputObjectTypeDefinitionNode: dart.fnType(ast.InputObjectTypeDefinitionNode, [ast.InputObjectTypeDefinitionNode]),
    visitInputObjectTypeExtensionNode: dart.fnType(ast.InputObjectTypeExtensionNode, [ast.InputObjectTypeExtensionNode]),
    visitInputValueDefinitionNode: dart.fnType(ast.InputValueDefinitionNode, [ast.InputValueDefinitionNode]),
    visitIntValueNode: dart.fnType(ast.IntValueNode, [ast.IntValueNode]),
    visitInterfaceTypeDefinitionNode: dart.fnType(ast.InterfaceTypeDefinitionNode, [ast.InterfaceTypeDefinitionNode]),
    visitInterfaceTypeExtensionNode: dart.fnType(ast.InterfaceTypeExtensionNode, [ast.InterfaceTypeExtensionNode]),
    visitListTypeNode: dart.fnType(ast.ListTypeNode, [ast.ListTypeNode]),
    visitListValueNode: dart.fnType(ast.ListValueNode, [ast.ListValueNode]),
    visitNameNode: dart.fnType(ast.NameNode, [ast.NameNode]),
    visitNamedTypeNode: dart.fnType(ast.NamedTypeNode, [ast.NamedTypeNode]),
    visitNullValueNode: dart.fnType(ast.NullValueNode, [ast.NullValueNode]),
    visitObjectFieldNode: dart.fnType(ast.ObjectFieldNode, [ast.ObjectFieldNode]),
    visitObjectTypeDefinitionNode: dart.fnType(ast.ObjectTypeDefinitionNode, [ast.ObjectTypeDefinitionNode]),
    visitObjectTypeExtensionNode: dart.fnType(ast.ObjectTypeExtensionNode, [ast.ObjectTypeExtensionNode]),
    visitObjectValueNode: dart.fnType(ast.ObjectValueNode, [ast.ObjectValueNode]),
    visitOperationDefinitionNode: dart.fnType(ast.OperationDefinitionNode, [ast.OperationDefinitionNode]),
    visitOperationTypeDefinitionNode: dart.fnType(ast.OperationTypeDefinitionNode, [ast.OperationTypeDefinitionNode]),
    visitScalarTypeDefinitionNode: dart.fnType(ast.ScalarTypeDefinitionNode, [ast.ScalarTypeDefinitionNode]),
    visitScalarTypeExtensionNode: dart.fnType(ast.ScalarTypeExtensionNode, [ast.ScalarTypeExtensionNode]),
    visitSchemaDefinitionNode: dart.fnType(ast.SchemaDefinitionNode, [ast.SchemaDefinitionNode]),
    visitSchemaExtensionNode: dart.fnType(ast.SchemaExtensionNode, [ast.SchemaExtensionNode]),
    visitSelectionSetNode: dart.fnType(ast.SelectionSetNode, [ast.SelectionSetNode]),
    visitStringValueNode: dart.fnType(ast.StringValueNode, [ast.StringValueNode]),
    visitTypeConditionNode: dart.fnType(ast.TypeConditionNode, [ast.TypeConditionNode]),
    visitUnionTypeDefinitionNode: dart.fnType(ast.UnionTypeDefinitionNode, [ast.UnionTypeDefinitionNode]),
    visitUnionTypeExtensionNode: dart.fnType(ast.UnionTypeExtensionNode, [ast.UnionTypeExtensionNode]),
    visitVariableDefinitionNode: dart.fnType(ast.VariableDefinitionNode, [ast.VariableDefinitionNode]),
    visitVariableNode: dart.fnType(ast.VariableNode, [ast.VariableNode])
  }));
  dart.setLibraryUri(transformer._Transformer, "package:gql/src/ast/transformer.dart");
  dart.setFieldSignature(transformer._Transformer, () => ({
    __proto__: dart.getFields(transformer._Transformer.__proto__),
    visitors: dart.fieldType(core.List$(transformer.TransformingVisitor))
  }));
  transformer.transform = function transform(N, node, visitors) {
    return N.as(node.accept(ast.Node, new transformer._Transformer.new({visitors: visitors})));
  };
  dart.trackLibraries("packages/gql/src/ast/ast", {
    "package:gql/src/ast/visitor.dart": visitor,
    "package:gql/src/ast/ast.dart": ast,
    "package:gql/src/ast/transformer.dart": transformer
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["visitor.dart","ast.dart","transformer.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IA+NA;;;;;;;;;;;wBAMiB;AAEX;MAAI;oBAIG;AAEP;MAAI;yBAIQ;AAEZ;MAAI;wBAIO;AAEX;MAAI;yBAIQ;AAEZ;MAAI;4BAIW;AAEf;MAAI;kCAIiB;AAErB;MAAI;2BAIU;AAEd;MAAI;2BAIU;AAEd;MAAI;yBAIQ;AAEZ;MAAI;yBAIQ;AAEZ;MAAI;yBAIQ;AAEZ;MAAI;4BAIW;AAEf;MAAI;2BAIU;AAEd;MAAI;0BAIS;AAEb;MAAI;wBAIO;AAEX;MAAI;wBAIO;AAEX;MAAI;6BAIY;AAEhB;MAAI;kCAIiB;AAErB;MAAI;8BAIa;AAEjB;MAAI;8BAIa;AAEjB;MAAI;wBAIO;AAEX;MAAI;qBAII;AAER;MAAI;4BAIW;AAEf;MAAI;mCAIkB;AAEtB;MAAI;gCAIe;AAEnB;MAAI;uCAIsB;AAE1B;MAAI;oCAImB;AAEvB;MAAI;oCAImB;AAEvB;MAAI;+BAIc;AAElB;MAAI;oCAImB;AAEvB;MAAI;uCAIsB;AAE1B;MAAI;mCAIkB;AAEtB;MAAI;kCAIiB;AAErB;MAAI;mCAIkB;AAEtB;MAAI;yCAIwB;AAE5B;MAAI;mCAIkB;AAEtB;MAAI;+BAIc;AAElB;MAAI;mCAIkB;AAEtB;MAAI;mCAIkB;AAEtB;MAAI;sCAIqB;AAEzB;MAAI;kCAIiB;AAErB;MAAI;iCAIgB;AAEpB;MAAI;wCAIuB;AAE3B;MAAI;;;;IACV;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;sBASiB;AAEX,YAAA,AAAK,KAAD,0BAAe;IAAK;0BAIT;AAEf,YAAA,AAAK,KAAD,0BAAe;IAAK;0BAIT;AAEf,YAAA,AAAK,KAAD,0BAAe;IAAK;iCAIF;AAEtB,YAAA,AAAK,KAAD,0BAAe;IAAK;uBAIZ;AAEZ,YAAA,AAAK,KAAD,0BAAe;IAAK;sBAIb;AAEX,YAAA,AAAK,KAAD,0BAAe;IAAK;gCAIH;AAErB,YAAA,AAAK,KAAD,0BAAe;IAAK;+BAIJ;AAEpB,YAAA,AAAK,KAAD,0BAAe;IAAK;iCAIF;AAEtB,YAAA,AAAK,KAAD,0BAAe;IAAK;uBAIZ;AAEZ,YAAA,AAAK,KAAD,0BAAe;IAAK;6BAIN;AAElB,YAAA,AAAK,KAAD,0BAAe;IAAK;mBAIhB;AAER,YAAA,AAAK,KAAD,0BAAe;IAAK;wBAIX;AAEb,YAAA,AAAK,KAAD,0BAAe;IAAK;gCAIH;AAErB,YAAA,AAAK,KAAD,0BAAe;IAAK;4BAIP;AAEjB,YAAA,AAAK,KAAD,0BAAe;IAAK;4BAIP;AAEjB,YAAA,AAAK,KAAD,0BAAe;IAAK;uCAII;AAE5B,YAAA,AAAK,KAAD,0BAAe;IAAK;sCAIG;AAE3B,YAAA,AAAK,KAAD,0BAAe;IAAK;kCAID;AAEvB,YAAA,AAAK,KAAD,0BAAe;IAAK;sBAIb;AAEX,YAAA,AAAK,KAAD,0BAAe;IAAK;qCAIE;AAE1B,YAAA,AAAK,KAAD,0BAAe;IAAK;oCAIC;AAEzB,YAAA,AAAK,KAAD,0BAAe;IAAK;sBAIb;AAEX,YAAA,AAAK,KAAD,0BAAe;IAAK;uBAIZ;AAEZ,YAAA,AAAK,KAAD,0BAAe;IAAK;kBAIjB;AAEP,YAAA,AAAK,KAAD,0BAAe;IAAK;uBAIZ;AAEZ,YAAA,AAAK,KAAD,0BAAe;IAAK;uBAIZ;AAEZ,YAAA,AAAK,KAAD,0BAAe;IAAK;yBAIV;AAEd,YAAA,AAAK,KAAD,0BAAe;IAAK;kCAID;AAEvB,YAAA,AAAK,KAAD,0BAAe;IAAK;iCAIF;AAEtB,YAAA,AAAK,KAAD,0BAAe;IAAK;yBAIV;AAEd,YAAA,AAAK,KAAD,0BAAe;IAAK;iCAIF;AAEtB,YAAA,AAAK,KAAD,0BAAe;IAAK;qCAIE;AAE1B,YAAA,AAAK,KAAD,0BAAe;IAAK;kCAID;AAEvB,YAAA,AAAK,KAAD,0BAAe;IAAK;iCAIF;AAEtB,YAAA,AAAK,KAAD,0BAAe;IAAK;8BAIL;AAEnB,YAAA,AAAK,KAAD,0BAAe;IAAK;6BAIN;AAElB,YAAA,AAAK,KAAD,0BAAe;IAAK;0BAIT;AAEf,YAAA,AAAK,KAAD,0BAAe;IAAK;yBAIV;AAEd,YAAA,AAAK,KAAD,0BAAe;IAAK;2BAIR;AAEhB,YAAA,AAAK,KAAD,0BAAe;IAAK;iCAIF;AAEtB,YAAA,AAAK,KAAD,0BAAe;IAAK;gCAIH;AAErB,YAAA,AAAK,KAAD,0BAAe;IAAK;gCAIH;AAErB,YAAA,AAAK,KAAD,0BAAe;IAAK;sBAIb;AAEX,YAAA,AAAK,KAAD,0BAAe;IAAK;;;;EAC9B;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAQ+B;;;;;;MACrB;;;;;;wBAQO;AAOZ,QALD,mBAAc;;AACZ,wBAAG;AAAH;AACA,wBAAG,AAAS,2BACV,QAAC;;AAAY,iBAAA,AAAQ,OAAD,mBAAmB,IAAI;yBAAtB,OAA8B;;AADrD;;;AAK2B,QAAvB,wBAAkB,IAAI;MAC9B;4BAImB;AAOhB,QALD,mBAAc;;AACZ,wBAAG;AAAH;AACA,wBAAG,AAAS,2BACV,QAAC;;AAAY,iBAAA,AAAQ,OAAD,uBAAuB,IAAI;yBAA1B,OAAkC;;AADzD;;;AAK+B,QAA3B,4BAAsB,IAAI;MAClC;4BAImB;AAOhB,QALD,mBAAc;;AACZ,wBAAG;AAAH;AACA,wBAAG,AAAS,2BACV,QAAC;;AAAY,iBAAA,AAAQ,OAAD,uBAAuB,IAAI;yBAA1B,OAAkC;;AADzD;;;AAK+B,QAA3B,4BAAsB,IAAI;MAClC;mCAI0B;AAOvB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,8BAA8B,IAAI;0BAAjC,OAAyC;;AADhE;;;AAKsC,QAAlC,mCAA6B,IAAI;MACzC;yBAIgB;AAOb,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,oBAAoB,IAAI;0BAAvB,OAA+B;;AADtD;;;AAK4B,QAAxB,yBAAmB,IAAI;MAC/B;wBAIe;AAOZ,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,mBAAmB,IAAI;0BAAtB,OAA8B;;AADrD;;;AAK2B,QAAvB,wBAAkB,IAAI;MAC9B;kCAIyB;AAOtB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,6BAA6B,IAAI;0BAAhC,OAAwC;;AAD/D;;;AAKqC,QAAjC,kCAA4B,IAAI;MACxC;iCAIwB;AAOrB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,4BAA4B,IAAI;0BAA/B,OAAuC;;AAD9D;;;AAKoC,QAAhC,iCAA2B,IAAI;MACvC;mCAI0B;AAOvB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,8BAA8B,IAAI;0BAAjC,OAAyC;;AADhE;;;AAKsC,QAAlC,mCAA6B,IAAI;MACzC;yBAIgB;AAOb,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,oBAAoB,IAAI;0BAAvB,OAA+B;;AADtD;;;AAK4B,QAAxB,yBAAmB,IAAI;MAC/B;+BAIsB;AAOnB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,0BAA0B,IAAI;0BAA7B,OAAqC;;AAD5D;;;AAKkC,QAA9B,+BAAyB,IAAI;MACrC;qBAIY;AAOT,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,gBAAgB,IAAI;0BAAnB,OAA2B;;AADlD;;;AAKwB,QAApB,qBAAe,IAAI;MAC3B;0BAIiB;AAOd,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,qBAAqB,IAAI;0BAAxB,OAAgC;;AADvD;;;AAK6B,QAAzB,0BAAoB,IAAI;MAChC;kCAIyB;AAOtB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,6BAA6B,IAAI;0BAAhC,OAAwC;;AAD/D;;;AAKqC,QAAjC,kCAA4B,IAAI;MACxC;8BAIqB;AAOlB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,yBAAyB,IAAI;0BAA5B,OAAoC;;AAD3D;;;AAKiC,QAA7B,8BAAwB,IAAI;MACpC;8BAIqB;AAOlB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,yBAAyB,IAAI;0BAA5B,OAAoC;;AAD3D;;;AAKiC,QAA7B,8BAAwB,IAAI;MACpC;yCAIgC;AAO7B,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,oCAAoC,IAAI;0BAAvC,OAA+C;;AADtE;;;AAK4C,QAAxC,yCAAmC,IAAI;MAC/C;wCAI+B;AAO5B,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,mCAAmC,IAAI;0BAAtC,OAA8C;;AADrE;;;AAK2C,QAAvC,wCAAkC,IAAI;MAC9C;oCAI2B;AAOxB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,+BAA+B,IAAI;0BAAlC,OAA0C;;AADjE;;;AAKuC,QAAnC,oCAA8B,IAAI;MAC1C;wBAIe;AAOZ,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,mBAAmB,IAAI;0BAAtB,OAA8B;;AADrD;;;AAK2B,QAAvB,wBAAkB,IAAI;MAC9B;uCAI8B;AAO3B,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,kCAAkC,IAAI;0BAArC,OAA6C;;AADpE;;;AAK0C,QAAtC,uCAAiC,IAAI;MAC7C;sCAI6B;AAO1B,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,iCAAiC,IAAI;0BAApC,OAA4C;;AADnE;;;AAKyC,QAArC,sCAAgC,IAAI;MAC5C;wBAIe;AAOZ,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,mBAAmB,IAAI;0BAAtB,OAA8B;;AADrD;;;AAK2B,QAAvB,wBAAkB,IAAI;MAC9B;yBAIgB;AAOb,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,oBAAoB,IAAI;0BAAvB,OAA+B;;AADtD;;;AAK4B,QAAxB,yBAAmB,IAAI;MAC/B;oBAIW;AAOR,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,eAAe,IAAI;0BAAlB,OAA0B;;AADjD;;;AAKuB,QAAnB,oBAAc,IAAI;MAC1B;yBAIgB;AAOb,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,oBAAoB,IAAI;0BAAvB,OAA+B;;AADtD;;;AAK4B,QAAxB,yBAAmB,IAAI;MAC/B;yBAIgB;AAOb,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,oBAAoB,IAAI;0BAAvB,OAA+B;;AADtD;;;AAK4B,QAAxB,yBAAmB,IAAI;MAC/B;2BAIkB;AAOf,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,sBAAsB,IAAI;0BAAzB,OAAiC;;AADxD;;;AAK8B,QAA1B,2BAAqB,IAAI;MACjC;oCAI2B;AAOxB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,+BAA+B,IAAI;0BAAlC,OAA0C;;AADjE;;;AAKuC,QAAnC,oCAA8B,IAAI;MAC1C;mCAI0B;AAOvB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,8BAA8B,IAAI;0BAAjC,OAAyC;;AADhE;;;AAKsC,QAAlC,mCAA6B,IAAI;MACzC;2BAIkB;AAOf,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,sBAAsB,IAAI;0BAAzB,OAAiC;;AADxD;;;AAK8B,QAA1B,2BAAqB,IAAI;MACjC;mCAI0B;AAOvB,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,8BAA8B,IAAI;0BAAjC,OAAyC;;AADhE;;;AAKsC,QAAlC,mCAA6B,IAAI;MACzC;uCAI8B;AAO3B,QALD,mBAAc;;AACZ,yBAAG;AAAH;AACA,yBAAG,AAAS,2BACV,QAAC;;AAAY,kBAAA,AAAQ,OAAD,kCAAkC,IAAI;0BAArC,OAA6C;;AADpE;;;AAK0C,QAAtC,uCAAiC,IAAI;MAC7C;oCAI2B;AAOxB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,+BAA+B,IAAI;2BAAlC,OAA0C;;AADjE;;;AAKuC,QAAnC,oCAA8B,IAAI;MAC1C;mCAI0B;AAOvB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,8BAA8B,IAAI;2BAAjC,OAAyC;;AADhE;;;AAKsC,QAAlC,mCAA6B,IAAI;MACzC;gCAIuB;AAOpB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,2BAA2B,IAAI;2BAA9B,OAAsC;;AAD7D;;;AAKmC,QAA/B,gCAA0B,IAAI;MACtC;+BAIsB;AAOnB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,0BAA0B,IAAI;2BAA7B,OAAqC;;AAD5D;;;AAKkC,QAA9B,+BAAyB,IAAI;MACrC;4BAImB;AAOhB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,uBAAuB,IAAI;2BAA1B,OAAkC;;AADzD;;;AAK+B,QAA3B,4BAAsB,IAAI;MAClC;2BAIkB;AAOf,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,sBAAsB,IAAI;2BAAzB,OAAiC;;AADxD;;;AAK8B,QAA1B,2BAAqB,IAAI;MACjC;6BAIoB;AAOjB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,wBAAwB,IAAI;2BAA3B,OAAmC;;AAD1D;;;AAKgC,QAA5B,6BAAuB,IAAI;MACnC;mCAI0B;AAOvB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,8BAA8B,IAAI;2BAAjC,OAAyC;;AADhE;;;AAKsC,QAAlC,mCAA6B,IAAI;MACzC;kCAIyB;AAOtB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,6BAA6B,IAAI;2BAAhC,OAAwC;;AAD/D;;;AAKqC,QAAjC,kCAA4B,IAAI;MACxC;kCAIyB;AAOtB,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,6BAA6B,IAAI;2BAAhC,OAAwC;;AAD/D;;;AAKqC,QAAjC,kCAA4B,IAAI;MACxC;wBAIe;AAOZ,QALD,mBAAc;;AACZ,0BAAG;AAAH;AACA,0BAAG,AAAS,2BACV,QAAC;;AAAY,mBAAA,AAAQ,OAAD,mBAAmB,IAAI;2BAAtB,OAA8B;;AADrD;;;AAK2B,QAAvB,wBAAkB,IAAI;MAC9B;;;UAzmBO;MAHC,oBAAc;MAGf;;IACL;;;;;;;;;;;;;;;;;;;;IC9uBa;;;;;;qBAOkB;AAAM,YAAA,AAAU,2BAC3C,QAAC;AACC,YAAU,gBAAN,KAAK;AACY,UAAnB,iBAAU,KAAK,EAAE,CAAC;cACb,KAAU,YAAN,KAAK;AACK,UAAnB,iBAAU,KAAK,EAAE,CAAC;;;IAGvB;;UAGmB;AACtB,UAAM,YAAF,CAAC;AACH,yBAAM,iBAAF,CAAC,GAAgB,qBAAa,MAAO;AAEzC,cAAsC,sBACpC,AAAE,CAAD,aACD;;AAIJ,YAAO;IACT;;AAGoB,YAA+B,oBAC7C;IACD;;;IAhCW;;EAAK;;;;;;;;;;;;;;;;;;IAuCM;;;;;;cASJ;AAAM,YAAA,AAAE,EAAD,mBAAmB;IAAK;;AAGrC,YAAQ,uBACnB;IACD;;;QAXE;QACI;IADJ;UAEM,AAAY,WAAD,IAAI;AACtB,8CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;IAYF;;;;;;;;QAGE;QACN;IADM;AAEZ,gDAAM,IAAI;;EAAC;;;;;;;;;QAKK;QACV;AACN,iEACS,IAAI,QACJ,IAAI;;EACX;;;;;;;;;;;IAQT;;2CAJK;;;;EAIL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAsBA;;+CAnBK;;;;EAmBL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAGsB;;;;;;IAEe;;;;;;IAET;;;;;;IAEH;;;;;;cAkBA;AAAM,YAAA,AAAE,EAAD,8BAA8B;IAAK;;AAGhD,YAAQ,uBACnB,WACA,WACA,mBACA,0BACA;IACD;;;QAxBY;QACN;QACJ;QACA;QACU;QACN;IALM;IAEV;IACA;IACU;UAEJ,AAAoB,mBAAD,IAAI;UACvB,AAAW,UAAD,IAAI;UACd,AAAa,YAAD,IAAI;AACvB,gEACQ,IAAI,QACJ,IAAI;;EACX;;;;;;;;;;;;;;;;;;;;;IAgBmB;;;;;;cAQH;AAAM,YAAA,AAAE,EAAD,uBAAuB;IAAK;;AAGzC,YAAQ,uBACnB;IACD;;;QAVE;QACI;IADJ;AAEF,kDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;oCAYa;AAAQ,+CAAM,IAAI;;EAAC;;;;;;;;;;IAIjC;;;;;;IAEA;;;;;;IAEU;;;;;;IAEC;;;;;;IAEH;;;;;;cAeA;AAAM,YAAA,AAAE,EAAD,gBAAgB;IAAK;;AAGlC,YAAQ,uBACnB,YACA,WACA,mBACA,gBACA;IACD;;;QArBE;QACU;QACV;QACA;QACA;QACI;IALJ;IACU;IACV;IACA;IACA;UAEM,AAAK,IAAD,IAAI;UACR,AAAU,cAAG;UACb,AAAW,UAAD,IAAI;AACrB,2CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;;;;IAgBF;;;;;;IAEC;;;;;;cAWO;AAAM,YAAA,AAAE,EAAD,mBAAmB;IAAK;;AAGrC,YAAQ,uBACnB,WACA;IACD;;;QAdY;QACA;QACN;IAFM;IACA;UAEJ,AAAK,IAAD,IAAI;UACR,AAAM,KAAD,IAAI;AAChB,8CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;IAaF;;;;;;IAEW;;;;;;cAWH;AAAM,YAAA,AAAE,EAAD,yBAAyB;IAAK;;AAG3C,YAAQ,uBACnB,WACA;IACD;;;QAdY;QACV;QACI;IAFM;IACV;UAEM,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,oDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;;IAaO;;;;;;IAEE;;;;;;IAEH;;;;;;cAYA;AAAM,YAAA,AAAE,EAAD,yBAAyB;IAAK;;AAG3C,YAAQ,uBACnB,oBACA,mBACA;IACD;;;QAhBE;QACA;QACU;QACN;IAHJ;IACA;IACU;UAEJ,AAAW,UAAD,IAAI;UACd,AAAa,YAAD,IAAI;AACvB,oDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;;;IAcO;;;;;;IAEE;;;;;;IAEH;;;;;;cAkBA;AAAM,YAAA,AAAE,EAAD,6BAA6B;IAAK;;AAG/C,YAAQ,uBACnB,WACA,oBACA,mBACA;IACD;;;QAvBgB;QACJ;QACV;QACU;QACN;IAHM;IACV;IACU;UAEJ,AAAK,IAAD,IAAI;UACR,AAAc,aAAD,IAAI;UACjB,AAAW,UAAD,IAAI;UACd,AAAa,YAAD,IAAI;AACvB,+DACQ,IAAI,QACJ,IAAI;;EACX;;;;;;;;;;;;;;;;;;;IAea;;;;;;cASG;AAAM,YAAA,AAAE,EAAD,wBAAwB;IAAK;;AAG1C,YAAQ,uBACnB;IACD;;;QAXY;QACN;IADM;UAEJ,AAAG,EAAD,IAAI;AACb,mDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;gCAYQ;AAAQ,2CAAM,IAAI;;EAAC;;;;;IAI7B;;;;;;cASQ;AAAM,YAAA,AAAE,EAAD,mBAAmB;IAAK;;AAGrC,YAAQ,uBACnB;IACD;;;QAXY;QACN;IADM;UAEJ,AAAK,IAAD,IAAI;AACf,8CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;IAYJ;;;;;;cASU;AAAM,YAAA,AAAE,EAAD,mBAAmB;IAAK;;AAGrC,YAAQ,uBACnB;IACD;;;QAXY;QACN;IADM;UAEJ,AAAM,KAAD,IAAI;AAChB,8CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;IAYJ;;;;;;cASU;AAAM,YAAA,AAAE,EAAD,qBAAqB;IAAK;;AAGvC,YAAQ,uBACnB;IACD;;;QAXY;QACN;IADM;UAEJ,AAAM,KAAD,IAAI;AAChB,gDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;IAYJ;;;;;;IAEF;;;;;;cAWY;AAAM,YAAA,AAAE,EAAD,sBAAsB;IAAK;;AAGxC,YAAQ,uBACnB,YACA;IACD;;;QAdY;QACA;QACN;IAFM;IACA;UAEJ,AAAM,KAAD,IAAI;UACT,AAAQ,OAAD,IAAI;AAClB,iDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;IAaN;;;;;;cASY;AAAM,YAAA,AAAE,EAAD,uBAAuB;IAAK;;AAGzC,YAAQ,uBACnB;IACD;;;QAXY;QACN;IADM;UAEJ,AAAM,KAAD,IAAI;AAChB,kDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;cAiBM;AAAM,YAAA,AAAE,EAAD,oBAAoB;IAAK;;AAGtC,YAAQ;IAAE;;;QAPhB;AACN,+CAAM,IAAI;;EAAC;;;;;;;;;;;;;IAUD;;;;;;cASQ;AAAM,YAAA,AAAE,EAAD,oBAAoB;IAAK;;AAGtC,YAAQ,uBACnB;IACD;;;QAXY;QACN;IADM;UAEJ,AAAK,IAAD,IAAI;AACf,+CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;IAYK;;;;;;cAQC;AAAM,YAAA,AAAE,EAAD,oBAAoB;IAAK;;AAGtC,YAAQ,uBACnB;IACD;;;QAVE;QACI;IADJ;AAEF,+CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;IAYY;;;;;;cAQL;AAAM,YAAA,AAAE,EAAD,sBAAsB;IAAK;;AAGxC,YAAQ,uBACnB;IACD;;;QAVE;QACI;IADJ;AAEF,iDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;IAYD;;;;;;IAEC;;;;;;cAWO;AAAM,YAAA,AAAE,EAAD,sBAAsB;IAAK;;AAGxC,YAAQ,uBACnB,WACA;IACD;;;QAdY;QACA;QACN;IAFM;IACA;UAEJ,AAAK,IAAD,IAAI;UACR,AAAM,KAAD,IAAI;AAChB,iDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;;;IAaE;;;;;;IAEJ;;;;;;IAEQ;;;;;;IAEG;;;;;;cAcH;AAAM,YAAA,AAAE,EAAD,6BAA6B;IAAK;;AAG/C,YAAQ,uBACnB,eACA,WACA,mBACA;IACD;;;QAnBY;QACA;QACV;QACA;QACI;IAJM;IACA;IACV;IACA;UAEM,AAAS,QAAD,IAAI;UACZ,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,wDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;;IAeD;;;;;;cAQO;AAAM,YAAA,AAAE,EAAD,uBAAuB;IAAK;;AAGzC,YAAQ,uBACnB;IACD;;;QAVY;QACN;IADM;AAEZ,kDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;IAYL;;;;;;;+BAES,WAAoB;IAApB;UACP,AAAU,SAAD,IAAI;AACpB,0CAAM,IAAI;;EAAC;;;;;;;;;IAIF;;;;;;cAWQ;AAAM,YAAA,AAAE,EAAD,oBAAoB;IAAK;;AAGtC,YAAQ,uBACnB,gBACA;IACD;;;QAdY;QACV;QACI;IAFM;UAGJ,AAAK,IAAD,IAAI;UACR,AAAU,SAAD,IAAI;AACpB,+CAAM,SAAS,EAAE,IAAI;;EAAC;;;;;;;;;;;;;;;;;IAab;;;;;;cAWQ;AAAM,YAAA,AAAE,EAAD,mBAAmB;IAAK;;AAGrC,YAAQ,uBACnB,gBACA;IACD;;;QAdY;QACA;QACN;IAFM;UAGJ,AAAK,IAAD,IAAI;UACR,AAAU,SAAD,IAAI;AACpB,8CAAM,SAAS,EAAE,IAAI;;EAAC;;;;;;;;;;;;;;;;;;IAab;;;;;;IAEU;;;;;;cAWF;AAAM,YAAA,AAAE,EAAD,oBAAoB;IAAK;;AAGtC,YAAQ,uBACnB,WACA;IACD;;;QAdY;QACV;QACI;IAFM;IACV;UAEM,AAAK,IAAD,IAAI;UACR,AAAU,cAAG;AACpB,+CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;IAaJ;;;;;;cASU;AAAM,YAAA,AAAE,EAAD,eAAe;IAAK;;AAGjC,YAAQ,uBACnB;IACD;;;QAXY;QACN;IADM;UAEJ,AAAM,KAAD,IAAI;AAChB,0CAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;QAaI;QACV;AACN,iEACS,IAAI,QACJ,IAAI;;EACX;;;;;;IAIe;;;;;;IACI;;;;;;;;QAGnB;QACc;QACd;QACI;IAHJ;IAEA;UAEM,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,2DACQ,IAAI,QACJ,IAAI;;EACX;;;;;;;;;;QAKc;QACV;AACN,gEACS,IAAI,QACJ,IAAI;;EACX;;;;;IAImB;;;;;;;;QAGf;QACU;QACd;;UACM,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,0DACQ,IAAI,QACJ,IAAI;;EACX;;;;;;;;;;;IAImB;;;;;;IACc;;;;;;cAcjB;AAAM,YAAA,AAAE,EAAD,2BAA2B;IAAK;;AAG7C,YAAQ,uBACnB,iBACA;IACD;;;QAjBE;QACA;QACI;IAFJ;IACA;UAEM,AAAW,UAAD,IAAI;UACd,AAAe,cAAD,IAAI;AACzB,6DACQ,YACA,IAAI;;EACX;;;;;;;;;;;;;;;;;;;IAaa;;;;;;IACA;;;;;;cAWG;AAAM,YAAA,AAAE,EAAD,kCAAkC;IAAK;;AAGpD,YAAQ,uBACnB,gBACA;IACD;;;QAdY;QACA;QACN;IAFM;IACA;UAEJ,AAAU,SAAD,IAAI;UACb,AAAK,IAAD,IAAI;AACf,6DAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;cA4BM;AAAM,YAAA,AAAE,EAAD,+BAA+B;IAAK;;AAGjD,YAAQ,uBACnB,WACA,kBACA;IACD;;;QArBa;QACG;QACC;QACX;UACE,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,iEACQ,IAAI,QACJ,IAAI,eACG,WAAW,cACZ,UAAU;;EACvB;;;;;;;;;;;;;;;;IAcmB;;;;;;IACM;;;;;;cAqBT;AAAM,YAAA,AAAE,EAAD,+BAA+B;IAAK;;AAGjD,YAAQ,uBACnB,WACA,kBACA,iBACA,iBACA;IACD;;;QA3BE;QACA;QACW;QACG;QACC;QACX;IALJ;IACA;UAKM,AAAW,UAAD,IAAI;UACd,AAAO,MAAD,IAAI;UACV,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,iEACQ,IAAI,QACJ,IAAI,eACG,WAAW,cACZ,UAAU;;EACvB;;;;;;;;;;;;;;;;;;;;;;;IAgBe;;;;;;IACP;;;;;;IACA;;;;;;IACW;;;;;;IACW;;;;;;cAgBd;AAAM,YAAA,AAAE,EAAD,0BAA0B;IAAK;;AAG5C,YAAQ,uBACnB,WACA,kBACA,iBACA,WACA;IACD;;;QAtBE;QACU;QACA;QACV;QACA;QACI;IALJ;IACU;IACA;IACV;IACA;UAEM,AAAK,IAAD,IAAI;UACR,AAAK,IAAD,IAAI;UACR,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,qDAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;;;;;;;IAgBK;;;;;;IACP;;;;;;IACA;;;;;;IACC;;;;;;IACU;;;;;;cAgBH;AAAM,YAAA,AAAE,EAAD,+BAA+B;IAAK;;AAGjD,YAAQ,uBACnB,WACA,kBACA,iBACA,WACA;IACD;;;QAtBE;QACU;QACA;QACV;QACA;QACI;IALJ;IACU;IACA;IACV;IACA;UAEM,AAAK,IAAD,IAAI;UACR,AAAK,IAAD,IAAI;UACR,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,0DAAM,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;;;IAgBe;;;;;;cAmBT;AAAM,YAAA,AAAE,EAAD,kCAAkC;IAAK;;AAGpD,YAAQ,uBACnB,WACA,kBACA,iBACA;IACD;;;QAxBE;QACW;QACG;QACC;QACX;IAJJ;UAKM,AAAO,MAAD,IAAI;UACV,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,oEACQ,IAAI,QACJ,IAAI,eACG,WAAW,cACZ,UAAU;;EACvB;;;;;;;;;;;;;;;;;IAemB;;;;;;cAmBH;AAAM,YAAA,AAAE,EAAD,8BAA8B;IAAK;;AAGhD,YAAQ,uBACnB,WACA,kBACA,iBACA;IACD;;;QAxBE;QACW;QACG;QACC;QACX;IAJJ;UAKM,AAAM,KAAD,IAAI;UACT,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,gEACQ,IAAI,QACJ,IAAI,eACG,WAAW,cACZ,UAAU;;EACvB;;;;;;;;;;;;;;;;;;IAe6B;;;;;;cAmBb;AAAM,YAAA,AAAE,EAAD,6BAA6B;IAAK;;AAG/C,YAAQ,uBACnB,WACA,kBACA,iBACA;IACD;;;QAxBE;QACW;QACG;QACC;QACX;IAJJ;UAKM,AAAO,MAAD,IAAI;UACV,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,+DACQ,IAAI,QACJ,IAAI,eACG,WAAW,cACZ,UAAU;;EACvB;;;;;;;;;;;;;;;;cA8BgB;AAAM,YAAA,AAAE,EAAD,8BAA8B;IAAK;;AAGhD,YAAQ,uBACnB,WACA,kBACA;IACD;;;QArBa;QACG;QACC;QACX;UACE,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,gEACQ,IAAI,QACJ,IAAI,eACG,WAAW,cACZ,UAAU;;EACvB;;;;;;;;;;;;;IAc8B;;;;;;cAmBd;AAAM,YAAA,AAAE,EAAD,oCAAoC;IAAK;;AAGtD,YAAQ,uBACnB,WACA,kBACA,iBACA;IACD;;;QAxBE;QACW;QACG;QACC;QACX;IAJJ;UAKM,AAAO,MAAD,IAAI;UACV,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,sEACQ,IAAI,QACJ,IAAI,eACG,WAAW,cACZ,UAAU;;EACvB;;;;;;;;;;;;;;;;;;;;;IAee;;;;;;IACe;;;;;;IACP;;;;;;IACnB;;;;;;cAmBY;AAAM,YAAA,AAAE,EAAD,8BAA8B;IAAK;;AAGhD,YAAQ,uBACnB,WACA,kBACA,gBACA,WACA;IACD;;;QAzBE;QACc;QACd;QACA;QACA;QACI;IALJ;IAEA;IACA;IACA;UAEM,AAAK,IAAD,IAAI;UACR,AAAK,IAAD,IAAI;UACR,AAAU,SAAD,IAAI;UACb,AAAW,UAAD,IAAI;AACrB,gEACQ,IAAI,QACJ,IAAI;;EACX;;;;;;;;;;;;;;;;;;;;;IAgBmB;;;;;;IACc;;;;;;cAcjB;AAAM,YAAA,AAAE,EAAD,0BAA0B;IAAK;;AAG5C,YAAQ,uBACnB,WACA,iBACA;IACD;;;QAlBE;QACA;QACI;IAFJ;IACA;UAEM,AAAW,UAAD,IAAI;UACd,AAAe,cAAD,IAAI;AACzB,4DACQ,YACA,IAAI;;EACX;;;;;;;;;;;;;;;;;cA2BgB;AAAM,YAAA,AAAE,EAAD,8BAA8B;IAAK;;AAGhD,YAAQ,uBACnB,WACA;IACD;;;QAlBM;QACU;QACC;UACT,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;AACrB,gEACQ,IAAI,QACJ,IAAI,cACE,UAAU;;EACvB;;;;;;;;;;;;;;IAamB;;;;;;IACM;;;;;;cAmBT;AAAM,YAAA,AAAE,EAAD,8BAA8B;IAAK;;AAGhD,YAAQ,uBACnB,WACA,iBACA,iBACA;IACD;;;QAxBgB;QACd;QACA;QACI;QACW;IAHf;IACA;UAGM,AAAK,IAAD,IAAI;UACR,AAAW,UAAD,IAAI;UACd,AAAO,MAAD,IAAI;UACV,AAAW,UAAD,IAAI;AACrB,gEACQ,IAAI,QACJ,IAAI,cACE,UAAU;;EACvB;;;;;;;;;;;;;;;;;;IAeyB;;;;;;cAiBT;AAAM,YAAA,AAAE,EAAD,iCAAiC;IAAK;;AAGnD,YAAQ,uBACnB,WACA,iBACA;IACD;;;QArBE;QACc;QACV;QACW;IAHf;UAIM,AAAK,IAAD,IAAI;UACR,AAAO,MAAD,IAAI;UACV,AAAW,UAAD,IAAI;AACrB,mEACQ,IAAI,QACJ,IAAI,cACE,UAAU;;EACvB;;;;;;;;;;;;;;;;;IAcmB;;;;;;cAiBH;AAAM,YAAA,AAAE,EAAD,6BAA6B;IAAK;;AAG/C,YAAQ,uBACnB,WACA,iBACA;IACD;;;QArBE;QACc;QACC;QACX;IAHJ;UAIM,AAAK,IAAD,IAAI;UACR,AAAM,KAAD,IAAI;UACT,AAAW,UAAD,IAAI;AACrB,+DACQ,IAAI,QACJ,IAAI,cACE,UAAU;;EACvB;;;;;;;;;;;;;;;;;IAc6B;;;;;;cAiBb;AAAM,YAAA,AAAE,EAAD,4BAA4B;IAAK;;AAG9C,YAAQ,uBACnB,WACA,iBACA;IACD;;;QArBE;QACI;QACU;QACC;IAHf;UAIM,AAAK,IAAD,IAAI;UACR,AAAO,MAAD,IAAI;UACV,AAAW,UAAD,IAAI;AACrB,8DACQ,IAAI,QACJ,IAAI,cACE,UAAU;;EACvB;;;;;;;;;;;;;;;;;IAc8B;;;;;;cAiBd;AAAM,YAAA,AAAE,EAAD,mCAAmC;IAAK;;AAGrD,YAAQ,uBACnB,WACA,iBACA;IACD;;;QArBE;QACI;QACU;QACC;IAHf;UAIM,AAAK,IAAD,IAAI;UACR,AAAO,MAAD,IAAI;UACV,AAAW,UAAD,IAAI;AACrB,qEACQ,IAAI,QACJ,IAAI,cACE,UAAU;;EACvB;;;;;;;;;;;;;;;wCA3uCF,MACM;;AAET,eAAI;0BAAJ,OAAM,uBAAO,CAAC;EAAC;wCAGN,OACA;;AAET,gBAAK;0BAAL,OAAO,eACL,QAAC,QAAS,iBAAU,IAAI,EAAE,CAAC;EAC5B;;sBCQY;AAEX,iBAAI;;kBAIG;AAEP,iBAAI;;uBAIQ;AAEZ,iBAAI;;sBAIO;AAEX,iBAAI;;uBAIQ;AAEZ,iBAAI;;0BAIW;AAEf,iBAAI;;gCAIiB;AAErB,iBAAI;;yBAIU;AAEd,iBAAI;;yBAIU;AAEd,iBAAI;;uBAIQ;AAEZ,iBAAI;;uBAIQ;AAEZ,iBAAI;;uBAIQ;AAEZ,iBAAI;;0BAIW;AAEf,iBAAI;;yBAIU;AAEd,iBAAI;;wBAIS;AAEb,iBAAI;;sBAIO;AAEX,iBAAI;;sBAIO;AAEX,iBAAI;;2BAIY;AAEhB,iBAAI;;gCAIiB;AAErB,iBAAI;;4BAIa;AAEjB,iBAAI;;4BAIa;AAEjB,iBAAI;;sBAIO;AAEX,iBAAI;;mBAII;AAER,iBAAI;;0BAIW;AAEf,iBAAI;;iCAIkB;AAEtB,iBAAI;;8BAIe;AAEnB,iBAAI;;qCAIsB;AAE1B,iBAAI;;kCAImB;AAEvB,iBAAI;;kCAImB;AAEvB,iBAAI;;6BAIc;AAElB,iBAAI;;kCAImB;AAEvB,iBAAI;;qCAIsB;AAE1B,iBAAI;;iCAIkB;AAEtB,iBAAI;;gCAIiB;AAErB,iBAAI;;iCAIkB;AAEtB,iBAAI;;uCAIwB;AAE5B,iBAAI;;iCAIkB;AAEtB,iBAAI;;6BAIc;AAElB,iBAAI;;iCAIkB;AAEtB,iBAAI;;iCAIkB;AAEtB,iBAAI;;oCAIqB;AAEzB,iBAAI;;gCAIiB;AAErB,iBAAI;;+BAIgB;AAEpB,iBAAI;;sCAIuB;AAE3B,iBAAI;;;;;EACV;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;oBAUM;AAEF,UAAI,AAAK,IAAD,IAAI,MAAM,MAAO,KAAI;AAE7B,YAAyB,MAAlB,AAAK,IAAD,kBAAQ;IACrB;oBAGU;AAER,UAAI,AAAM,KAAD,IAAI,MAAM,MAAO,MAAK;AAE/B,YAAO,AACF,AAMA,AACA,MARO,iBAEN,QACE,QAEE,AAAK,IAAD,kBAAQ,kEAGA;IACxB;sBAIe;AAEP,wBAAc,uCACL,qCAAU,AAAK,IAAD;AAG7B,YAAO,AAAS,wCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,mBAAmB,IAAI;IAErD;sBAIe;AAEP,wBAAc,gCACZ,+BAAU,AAAK,IAAD,eACb,gCAAU,AAAK,IAAD;AAGvB,YAAO,AAAS,wCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,mBAAmB,IAAI;IAErD;0BAImB;AAEX,wBAAc,qCACX,AAAK,IAAD;AAGb,YAAO,AAAS,4CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,uBAAuB,IAAI;IAEzD;0BAImB;AAEX,wBAAc,qCACX,gCAAU,AAAK,IAAD;AAGvB,YAAO,AAAS,4CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,uBAAuB,IAAI;IAEzD;iCAI0B;AAElB,wBAAc,2CACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,0BAChB,AAAK,IAAD,wBACH,AAAK,IAAD,mBACV,+CAAU,AAAK,IAAD;AAGtB,YAAO,AAAS,mDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,8BAA8B,IAAI;IAEhE;uBAIgB;AAER,wBAAc,sCACP,mCAAU,AAAK,IAAD,mBACnB,+BAAU,AAAK,IAAD;AAGtB,YAAO,AAAS,yCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,oBAAoB,IAAI;IAEtD;gCAIyB;AAEjB,wBAAc,0CACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD,sBAClB,8CAAU,AAAK,IAAD;AAGxB,YAAO,AAAS,kDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,6BAA6B,IAAI;IAE/D;+BAIwB;AAEhB,wBAAc,yCACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD,sBAClB,8CAAU,AAAK,IAAD;AAGxB,YAAO,AAAS,iDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,4BAA4B,IAAI;IAE9D;iCAI0B;AAElB,wBAAc,2CACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD;AAG5B,YAAO,AAAS,mDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,8BAA8B,IAAI;IAEhE;uBAIgB;AAER,wBAAc,iCACZ,+BAAU,AAAK,IAAD;AAGtB,YAAO,AAAS,yCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,oBAAoB,IAAI;IAEtD;6BAIsB;AAEd,wBAAc,uCACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD,oBACpB,+CAAU,AAAK,IAAD,cACd,+BAAU,AAAK,IAAD;AAGtB,YAAO,AAAS,+CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,0BAA0B,IAAI;IAE5D;mBAIY;AAEJ,wBAAc,6BACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD,qBACnB,+BAAU,AAAK,IAAD,oBACV,mCAAU,AAAK,IAAD,2BACX,uCAAU,AAAK,IAAD;AAG9B,YAAO,AAAS,qCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,gBAAgB,IAAI;IAElD;wBAIiB;AAET,wBAAc,mCACX,AAAK,IAAD;AAGb,YAAO,AAAS,0CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,qBAAqB,IAAI;IAEvD;gCAIyB;AAEjB,wBAAc,0CACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD,4BACZ,uCAAU,AAAK,IAAD,+BACb,wCAAU,AAAK,IAAD;AAG/B,YAAO,AAAS,kDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,6BAA6B,IAAI;IAE/D;4BAIqB;AAEb,wBAAc,sCACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD;AAG5B,YAAO,AAAS,8CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,yBAAyB,IAAI;IAE3D;4BAIqB;AAEb,wBAAc,4CACN,oCAAU,AAAK,IAAD,4BACZ,uCAAU,AAAK,IAAD,+BACb,wCAAU,AAAK,IAAD;AAG/B,YAAO,AAAS,8CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,yBAAyB,IAAI;IAE3D;uCAIgC;AAExB,wBAAc,iDACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD,sBAClB,+CAAU,AAAK,IAAD;AAGxB,YAAO,AAAS,yDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,oCAAoC,IAAI;IAEtE;sCAI+B;AAEvB,wBAAc,gDACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD,sBAClB,+CAAU,AAAK,IAAD;AAGxB,YAAO,AAAS,wDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,mCAAmC,IAAI;IAErE;kCAI2B;AAEnB,wBAAc,4CACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD,4BACZ,gCAAU,AAAK,IAAD,sBACtB,+BAAU,AAAK,IAAD;AAGtB,YAAO,AAAS,oDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,+BAA+B,IAAI;IAEjE;sBAIe;AAEP,wBAAc,iCACX,AAAK,IAAD;AAGb,YAAO,AAAS,wCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,mBAAmB,IAAI;IAErD;qCAI8B;AAEtB,wBAAc,+CACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD,sBAClB,0CAAU,AAAK,IAAD;AAGxB,YAAO,AAAS,uDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,kCAAkC,IAAI;IAEpE;oCAI6B;AAErB,wBAAc,8CACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD,sBAClB,0CAAU,AAAK,IAAD;AAGxB,YAAO,AAAS,sDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,iCAAiC,IAAI;IAEnE;sBAIe;AAEP,wBAAc,gCACZ,+BAAU,AAAK,IAAD,mBACT,AAAK,IAAD;AAGjB,YAAO,AAAS,wCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,mBAAmB,IAAI;IAErD;uBAIgB;AAER,wBAAc,mCACV,gCAAU,AAAK,IAAD;AAGxB,YAAO,AAAS,yCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,oBAAoB,IAAI;IAEtD;kBAIW;AAEH,wBAAc,4BAAe,AAAK,IAAD,cAAc,AAAK,IAAD;AAEzD,YAAO,AAAS,oCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,eAAe,IAAI;IAEjD;uBAIgB;AAER,wBAAc,sCACP,AAAK,IAAD,kBACT,+BAAU,AAAK,IAAD;AAGtB,YAAO,AAAS,yCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,oBAAoB,IAAI;IAEtD;uBAIgB;AAER,wBAAc;AAEpB,YAAO,AAAS,yCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,oBAAoB,IAAI;IAEtD;yBAIkB;AAEV,wBAAc,mCACZ,+BAAU,AAAK,IAAD,eACb,gCAAU,AAAK,IAAD;AAGvB,YAAO,AAAS,2CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,sBAAsB,IAAI;IAExD;kCAI2B;AAEnB,wBAAc,4CACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD,sBAClB,0CAAU,AAAK,IAAD,sBACV,oCAAU,AAAK,IAAD;AAG5B,YAAO,AAAS,oDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,+BAA+B,IAAI;IAEjE;iCAI0B;AAElB,wBAAc,2CACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD,sBAClB,0CAAU,AAAK,IAAD,sBACV,oCAAU,AAAK,IAAD;AAG5B,YAAO,AAAS,mDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,8BAA8B,IAAI;IAEhE;yBAIkB;AAEV,wBAAc,qCACV,sCAAU,AAAK,IAAD;AAGxB,YAAO,AAAS,2CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,sBAAsB,IAAI;IAExD;iCAI0B;AAElB,wBAAc,2CACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD,oBACpB,AAAK,IAAD,qBACI,uCAAU,AAAK,IAAD,qCACP,6CAAU,AAAK,IAAD;AAGrC,YAAO,AAAS,mDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,8BAA8B,IAAI;IAEhE;qCAI8B;AAEtB,wBAAc,oDACP,AAAK,IAAD,kBACT,oCAAU,AAAK,IAAD;AAGtB,YAAO,AAAS,uDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,kCAAkC,IAAI;IAEpE;kCAI2B;AAEnB,wBAAc,4CACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD;AAG5B,YAAO,AAAS,oDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,+BAA+B,IAAI;IAEjE;iCAI0B;AAElB,wBAAc,2CACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD;AAG5B,YAAO,AAAS,mDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,8BAA8B,IAAI;IAEhE;8BAIuB;AAEf,wBAAc,8CACN,oCAAU,AAAK,IAAD,8BACV,kDAAU,AAAK,IAAD;AAGhC,YAAO,AAAS,gDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,2BAA2B,IAAI;IAE7D;6BAIsB;AAEd,wBAAc,6CACN,oCAAU,AAAK,IAAD,8BACV,kDAAU,AAAK,IAAD;AAGhC,YAAO,AAAS,+CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,0BAA0B,IAAI;IAE5D;0BAImB;AAEX,wBAAc,0CACN,oCAAU,AAAK,IAAD;AAG5B,YAAO,AAAS,4CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,uBAAuB,IAAI;IAEzD;yBAIkB;AAEV,wBAAc,sCACT,AAAK,IAAD,iBACN,AAAK,IAAD;AAGb,YAAO,AAAS,2CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,sBAAsB,IAAI;IAExD;2BAIoB;AAEZ,wBAAc,mCACd,oCAAU,AAAK,IAAD;AAGpB,YAAO,AAAS,6CACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,wBAAwB,IAAI;IAE1D;iCAI0B;AAElB,wBAAc,2CACZ,+BAAU,AAAK,IAAD,qBACP,sCAAU,AAAK,IAAD,2BACf,oCAAU,AAAK,IAAD,qBACnB,oCAAU,AAAK,IAAD;AAGvB,YAAO,AAAS,mDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,8BAA8B,IAAI;IAEhE;gCAIyB;AAEjB,wBAAc,0CACZ,+BAAU,AAAK,IAAD,oBACR,oCAAU,AAAK,IAAD,qBACnB,oCAAU,AAAK,IAAD;AAGvB,YAAO,AAAS,kDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,6BAA6B,IAAI;IAE/D;gCAIyB;AAEjB,wBAAc,gDACN,oCAAU,AAAK,IAAD,4BACZ,uCAAU,AAAK,IAAD,sBACtB,+BAAU,AAAK,IAAD,kBACV,mCAAU,AAAK,IAAD;AAG1B,YAAO,AAAS,kDACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,6BAA6B,IAAI;IAE/D;sBAIe;AAEP,wBAAc,gCACZ,+BAAU,AAAK,IAAD;AAGtB,YAAO,AAAS,wCACd,WAAW,EACX,SAAC,MAAM,YAAY,AAAQ,OAAD,mBAAmB,IAAI;IAErD;;;QAnsBO;;;EACL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;gDA7RA,MACwB;AAExB,UAEE,MAFF,AAAK,IAAD,kBACF,4CAAuB,QAAQ;EAC3B","file":"ast.ddc.js"}');
  // Exports:
  return {
    src__ast__visitor: visitor,
    src__ast__ast: ast,
    src__ast__transformer: transformer
  };
});

//# sourceMappingURL=ast.ddc.js.map
