class DetailsList {
  final String name;
  final String count;
  final String imageUrl;

  DetailsList({this.imageUrl, this.name, this.count});
}

List<DetailsList> categoryData = [
  new DetailsList(
    imageUrl:   "assets/page1/tops.png",
    name: "Kandora",
    count: "6"
  ),
  new DetailsList(
    imageUrl:   "assets/page1/bottoms.png",
    name: "Ghutra",
    count: "6"
  ),
  new DetailsList(
    imageUrl:   "assets/page1/dress.png",
    name: "Underwear",
    count: "6"
  ),
  new DetailsList(
    imageUrl:   "assets/page1/coat.png",
    name: "Classic",
    count: "6"
  ),
  new DetailsList(
    imageUrl:   "assets/page1/suits.png",
    name: "Abaya",
    count: "6"
  ),
  new DetailsList(
    imageUrl:   "assets/page1/bottoms.png",
    name: "Scarf",
    count: "6"
  ),

];
