import 'package:laundry/home.dart';
import 'package:flutter/material.dart';
import 'package:laundry/pages/Introduction.dart';
import 'package:laundry/pages/first.dart';
import 'package:laundry/pages/four.dart';
import 'package:laundry/pages/second.dart';
import 'package:laundry/pages/third.dart';

import 'Splash/Splash1.dart';
import 'Splash/Splash2.dart';
import 'Splash/Splash3.dart';
import 'services_screen.dart';
import 'services_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Splash1(),
     routes: {
        Home.routeName : (ctx) => Home() ,
        FirstScreen.routeName : (ctx) => FirstScreen() ,
        SecondScreen.routeName : (ctx) => SecondScreen() ,
        ThirdScreen.routeName : (ctx) => ThirdScreen() ,
        FourScreen.routeName : (ctx) => FourScreen() ,
       ServicesScreen.routeName : (ctx) => ServicesScreen() ,
       Splash1.routeName: (ctx) => Splash1(),
       Splash2.routeName: (ctx) => Splash2(),
       Splash3.routeName: (ctx) => Splash3(),
       Introduction.routeName: (ctx) => Introduction(),
     },
     // debugShowCheckedModeBanner: false,
    );
  }
}