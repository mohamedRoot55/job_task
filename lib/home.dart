import 'package:flutter/material.dart';
import 'package:laundry/pages/first.dart';
import 'package:laundry/pages/four.dart';
import 'package:laundry/pages/second.dart';
import 'package:laundry/pages/third.dart';
import 'services_screen.dart';
import 'services_screen.dart';


class Home extends StatelessWidget {
  static const String routeName = "/Home" ;
  @override
  Widget build(BuildContext context) {
    final ScreenHeigh = MediaQuery.of(context).size.height ;
    final ScreenWidth = MediaQuery.of(context).size.width ;
    return Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child:


              Stack(
               overflow: Overflow.visible,
                 fit: StackFit.loose,
                  children: <Widget>[
                ClipPath(
                  clipper: ClippingClass(),
                  child: Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 4 / 7,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Color(0xff40dedf), Color(0xff0fb2ea)],
                      ),
                    ),
                  ),
                ),

                Positioned(
                  left: 40,
                  top: 50,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Hi",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 32,
                          )),

                    ],
                  ),
                ),
                Positioned(
                  left: 20,
                  top: 130,
                  right: 20,
                  child:
                  SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(

                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pushNamed(FirstScreen.routeName) ;
                              },
                              child: _customCard(
                                  imageUrl: "washing-machine.png",
                                  item: "Wash",
                                  duration: "1 Day"),
                            ),
                            GestureDetector(
                              onTap: () {
                               Navigator.of(context).pushNamed(SecondScreen.routeName) ;
                              },
                              child: _customCard(
                                  imageUrl: "iron.png", item: "Press", duration: "1 Day"),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushNamed(ThirdScreen.routeName) ;
                                },
                                child: _customCard(
                                    imageUrl: "wash-dry.png",
                                    item: "Wash & Press",
                                    duration: "2 Days")),
                            GestureDetector(
                              onTap: () {
                                print("ok");
                                Navigator.of(context).pushNamed(FourScreen.routeName) ;
                              },
                              child: _customCard(
                                  imageUrl: "all.png",
                                  item: "All Services",
                                  duration: "3 Days"),
                            ),
                          ],
                        ) ,
                      SizedBox(
                        height: 40,
                      ),
                      Text(
                          "Get your laundry washed, pressed, folded and delivered straight to your door in just 3 simple steps.",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                          )),
                        Container(
                          height: 120,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Container(
                                      height: ScreenHeigh * .1,
                                      width: ScreenWidth * .2,
                                      padding: const EdgeInsets.all(15),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(30),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.pink.withOpacity(.5),
                                                blurRadius: 0.5,
                                                offset: Offset(0.0, 0.0)),
                                          ]),
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(30),
                                          child: Image.asset("assets/images/home1.png"))),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    "it's your app",
                                    style: TextStyle(
                                        color: Colors.pink,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 9,
                                        fontStyle: FontStyle.italic),
                                  )
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                      padding: const EdgeInsets.all(15),
                                      height: ScreenHeigh * .1,
                                      width: ScreenWidth * .2,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(30),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.orange.withOpacity(.5),
                                                blurRadius: 0.5,
                                                offset: Offset(0.0, 0.0)),
                                          ]),
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(30),
                                          child: Image.asset("assets/images/location.png"))),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    "it's easy to get your client's location ",
                                    style: TextStyle(
                                        color: Colors.pink,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 9,
                                        fontStyle: FontStyle.italic),
                                  )
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                      height: ScreenHeigh * .1,
                                      width: ScreenWidth * .2,
                                      padding: const EdgeInsets.all(15),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(30),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.redAccent.withOpacity(0.5),
                                                blurRadius: 0.5,
                                                offset: Offset(0.0, 0.0)),
                                          ]),
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(30),
                                          child: Image.asset("assets/images/rocket1.png"))),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    "fast and heigh performace",
                                    style: TextStyle(
                                        color: Colors.pink,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 9,
                                        fontStyle: FontStyle.italic),
                                  )
                                ],
                              )
                            ],
                          ),
                        )

                      ],
                    ),
                  ),
                ),



              ]) ,

            ),
          ),
        );
  }

  _customCard({String imageUrl, String item, String duration}) {
    return SizedBox(
      height: 188,
      width: 160,
      child: Card(
        color: Colors.grey[100],
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        elevation: 20,
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset("assets/home_images/" + imageUrl),
              Align(
                alignment: Alignment.bottomLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      item,
                      style: TextStyle(fontSize: 24),
                    ),
                    Text(duration)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ClippingClass extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height);
    var controlPoint = Offset(size.width - (size.width / 2), size.height - 120);
    var endPoint = Offset(size.width, size.height);
    path.quadraticBezierTo(
        controlPoint.dx, controlPoint.dy, endPoint.dx, endPoint.dy);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
