import 'dart:async';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../home.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';

class ThirdScreen extends StatefulWidget {
  static final String routeName= '/ThirdScreen' ;
  @override
  _ThirdScreenState createState() => _ThirdScreenState();
}

class _ThirdScreenState extends State<ThirdScreen> {
  final Completer<WebViewController> _controller =
  Completer<WebViewController>();


  @override
  Widget build(BuildContext context) {
    final ScreenHeigh = MediaQuery.of(context).size.height;
    final ScreenWidth = MediaQuery.of(context).size.width;

    void OpenDialog(BuildContext context) {
      YYDialog().build(context)
        ..gravity = Gravity.center
        ..width = ScreenWidth * .8
        ..height = ScreenHeigh * .8
        ..barrierColor = Colors.black.withOpacity(.3)
        ..widget(
          Container(
            height:  ScreenHeigh * .78,
            width:  ScreenWidth * .78 ,
            child: WebView(
              key: Key("2") ,
              initialUrl: 'https://laundry.integratedart.co/shop',
              javascriptMode: JavascriptMode.unrestricted,
//              onWebViewCreated: (WebViewController webViewController) {
//                _controller.complete(webViewController);
//              },
            ),
          ),
        )
        ..animatedFunc = (child, animation) {
          return SlideTransition(
            child: child,
            position:
            Tween<Offset>(begin: Offset(0.2, 0.0), end: Offset(0.0, 0.0))
                .animate(animation),
          );
        }
        ..borderRadius = 4.0
        ..show();
    }

    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text("Third screen"),
          leading: NavigationControls(_controller.future),
//          actions: <Widget>[NavigationControls(_controller.future)],

          backgroundColor: Color(0xff0fb2ea),
        ),
        body: Builder(builder: (BuildContext context) {
          return Stack(
            children: <Widget>[
              WebView(
                key: Key("1") ,
                initialUrl: 'https://laundry.integratedart.co/shop',
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
              ),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton(backgroundColor: Colors.purple,
                    onPressed: () {
                      OpenDialog(context);
                    },
                    child: Icon(Icons.open_with, color: Colors.white,),
                  ),
                ),
              )
            ],
          );
        }));
  }
}

class NavigationControls extends StatelessWidget {
  const NavigationControls(this._webViewControllerFuture);

  final Future<WebViewController> _webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
        future: _webViewControllerFuture,
        builder:
            (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
          final bool webViewReady =
              snapshot.connectionState == ConnectionState.done;
          final WebViewController controller = snapshot.data;
          return Container(
            child: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: !webViewReady
                  ? null
                  : () async {
                if (await controller.canGoBack()) {
                  controller.goBack();
                } else {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Home()));
                  return;
                }
              },
            ),
          );
        });
  }
}
